import 'package:flutter/material.dart';

BorderRadius comicsComicsBoxComicsBoxClassBox(state) {
  /**
   * 是否为圆角
   */
  if (state) {
    return BorderRadius.vertical(
      top: Radius.elliptical(20, 20),
      bottom: Radius.elliptical(20, 20),
    ); // 也可控件一边圆角大小
  } else {
    return BorderRadius.vertical(
      top: Radius.elliptical(7, 7),
      bottom: Radius.elliptical(7, 7),
    ); // 也可控件一边圆角大小
  }
}

final $GlobalFilterClass = [
  "短篇",
  "韩漫",
  "CG",
  "禁书",
  "生肉",
  "耽美",
  "重口",
  "纯爱",
  "伪娘",
  "扶她",
];

Map<dynamic, dynamic> $GlobalFilterClassInfo = {
  /*  "短篇": {
    'color': Color(0x00feb02f), //背景颜色
    'TextColor': Color(0xff808080), //字体颜色
    'Selection': false, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(true), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xfffeb02f), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  }, */
  "短篇": {
    'color': Color(0xffff7fb6), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(true), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xfffeb02f), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "韩漫": {
    'color': Color(0xffff7fb6), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(true), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xffff7fb6), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "CG": {
    'color': Color(0xff829cfc), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(true), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xff829cfc), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "禁书": {
    'color': Color(0xffed97b7), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(true), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xffed97b7), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "生肉": {
    'color': Color(0xffabf597), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xffabf597), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "耽美": {
    'color': Color(0xff5ac6ff), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xff5ac6ff), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "重口": {
    'color': Color(0xffa77eff), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xffa77eff), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "纯爱": {
    'color': Color(0xffff5f69), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    "Original": {
      //原始数据
      'color': Color(0xffff5f69), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "伪娘": {
    'color': Color(0xffffd100), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    'Original': {
      //原始数据
      'color': Color(0xffffd100), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  },
  "扶她": {
    'color': Color(0xfffa9800), //背景颜色
    'TextColor': Color(0xffffffff), //字体颜色
    'Selection': true, //是否选中 不选中在渲染列表时候会屏蔽掉这个关键字的漫画
    'borderRadius': comicsComicsBoxComicsBoxClassBox(false), //图形 方 圆
    'Original': {
      //原始数据
      'color': Color(0xfffa9800), //原始背景色
      'TextColor': Color(0xffffffff), //原始字体色
    }
  }
}; //下载列表
