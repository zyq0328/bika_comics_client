/*
 * @Date: 2020-08-22 22:49:57
 * @名称: 动画 - 缩放
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 01:03:56
 * @FilePath: /bica_acg/lib/anime/SuoFang.anime.dart
 */
import 'package:flutter/material.dart';

/* 
 * 缩放
 */
class AnimeSuoFang extends PageRouteBuilder {
  final Widget widget;
  final bool opaque;
  AnimeSuoFang({this.widget, this.opaque})
      : super(
          // 设置过度时间
          transitionDuration: Duration(seconds: 1),
          // 构造器
          pageBuilder: (
            // 上下文和动画
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
          ) {
            return widget;
          },
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
            Widget child,
          ) {
            return ScaleTransition(
              scale: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                  parent: animaton1, curve: Curves.fastOutSlowIn)),
              child: child,
            );
          },
          opaque: opaque,
        );
}
