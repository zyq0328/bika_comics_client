/*
 * @Date: 2020-08-23 00:50:27
 * @名称: 动画 从下到上
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-24 20:41:05
 * @FilePath: /bica_acg/lib/anime/LiuDong.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Animation<double> upInsertAnime; //服务器列表
Animation<double> maskAnime; //遮罩层
double $loadingGifState = 0.0; //logo加载层透明度 最大1.0
AnimationController upInsertAnimeController; //logo图标
AnimationController maskAnimeController; //遮罩层
var splashBGTransparency = 0.0;
var announcementsHidden = 1.0;
var announcementsExit = false; //! 是否退出

class AnimeLiuDong extends StatefulWidget {
  AnimeLiuDong({
    Key key,
    this.widget,
  }) : super(key: key);
  final Widget widget;
  @override
  _AnimeLiuDongState createState() => _AnimeLiuDongState();
}

class _AnimeLiuDongState extends State<AnimeLiuDong>
    with SingleTickerProviderStateMixin {
  @override
  initState() {
    super.initState();
    announcementsExit = false; //! 是否退出
    upInsertAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    upInsertAnime =
        new Tween(begin: 0.0, end: 1334.0).animate(upInsertAnimeController)
          ..addListener(
            () {
              setState(() {
                // the state that has changed here is the serviceListAnime object’s value
              });
            },
          );
    upInsertAnimeController.forward(); //直执行一次
  }

  Widget build(BuildContext context) {
    var speed = 1330.0 - upInsertAnime.value;
    if (upInsertAnime.value == 0 && announcementsExit == true) {
      Navigator.pop(context);
    }
    if (speed <= 0) {
      speed = 0;
    }
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      child: MaterialApp(
        home: Container(
          child: Opacity(
            opacity: announcementsHidden,
            child: Stack(
              children: <Widget>[
                Mask(),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(speed)),
                  child: widget.widget,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  dispose() {
    // loadingAnimeController.dispose();
    super.dispose();
  }
}

class Mask extends StatefulWidget {
  _MaskState createState() => new _MaskState();
}

BuildContext context;

class _MaskState extends State<Mask> with SingleTickerProviderStateMixin {
  initState() {
    super.initState();
    maskAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    maskAnime = new Tween(begin: 0.0, end: 1.0).animate(maskAnimeController)
      ..addListener(() {
        setState(() {
          // the state that has changed here is the MaskAnime object’s value
        });
      });
    maskAnimeController.forward();
  }

  Widget build(BuildContext context) {
    return AnimatedOpacity(
      child: Container(
        child: Container(
          color: Color(0xFF000000),
        ),
      ),
      duration: Duration(milliseconds: 1),
      opacity: 1 - maskAnime.value,
    );
  }

  dispose() {
    super.dispose();
  }
}
