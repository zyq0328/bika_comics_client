/*
 * @Date: 2020-08-22 22:49:12
 * @名称: 动画 - 淡入淡出
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 01:03:03
 * @FilePath: /bica_acg/lib/anime/DanRuDanChu.anime.dart
 */
import 'package:flutter/material.dart';

/* 
 * 淡入淡出动画
 */
class AnimeDanRuDanChu extends PageRouteBuilder {
  final Widget widget;
  final bool opaque;

  AnimeDanRuDanChu({this.widget, this.opaque})
      : super(
          // 设置过度时间
          transitionDuration: Duration(seconds: 1),
          // 构造器
          pageBuilder: (
            // 上下文和动画
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
          ) {
            return widget;
          },
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
            Widget child,
          ) {
            // 渐变效果
            return FadeTransition(
              // 从0开始到1
              opacity: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                // 传入设置的动画
                parent: animaton1,
                // 设置效果，快进漫出   这里有很多内置的效果
                curve: Curves.fastOutSlowIn,
              )),
              child: child,
            );
          },
          opaque: opaque,
        );
}
