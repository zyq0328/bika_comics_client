/*
 * @Date: 2020-08-22 22:52:26
 * @名称: 动画 - 模糊
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 01:03:32
 * @FilePath: /bica_acg/lib/anime/MoHu.anime.dart
 */
import 'package:flutter/material.dart';

class AnimeMohu extends PageRouteBuilder {
  final Widget widget;
  final bool opaque;

  AnimeMohu(this.widget, this.opaque)
      : super(
          // 设置过度时间
          transitionDuration: Duration(seconds: 1),
          // 构造器
          pageBuilder: (
            // 上下文和动画
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
          ) {
            return widget;
          },
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animaton1,
            Animation<double> animaton2,
            Widget child,
          ) {
            return SlideTransition(
              position: Tween<Offset>(
                      // 设置滑动的 X , Y 轴
                      begin: Offset(-1.0, 0.0),
                      end: Offset(0.0, 0.0))
                  .animate(CurvedAnimation(
                      parent: animaton1, curve: Curves.fastOutSlowIn)),
              child: child,
            );
          },
          opaque: opaque,
        );
}
