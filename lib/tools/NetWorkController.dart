/*
 * @Date: 2019-09-24 23:15:27
 * @名称: 网络请求
 * @描述: 网络请求控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 14:47:08
 * @FilePath: /bika_comics_client/lib/tools/NetWorkController.dart
 * @最后编辑: 初雪桜
 */
import 'package:dio/dio.dart';
import 'package:pica_acg/tools/Controller/NetWork/Get.dart';

class NetWorkController {
  //网络控制器
  static void get(String url, Function callBack,
      {Map<String, dynamic> parameter,
      ResponseType responseType,
      Map<String, dynamic> headers,
      CancelToken requestName}) async {
    NetWork.get(
      url,
      (callBack1) {
        if (headers == null) {
          headers = {};
        }
        callBack(callBack1);
      },
      parameter: parameter,
      responseType: responseType,
      headers: headers,
      requestName: requestName,
    );
  }

  static void post(
    String url,
    Function callBack, {
    Map<String, dynamic> parameter,
    Map<String, dynamic> data,
    ResponseType responseType,
    CancelToken requestName,
    Map<String, dynamic> headers,
  }) async {
    if (headers == null) {
      headers = {};
    }
    NetWork.post(url, (callBack1) {
      callBack(callBack1);
    },
        parameter: parameter,
        responseType: responseType,
        requestName: requestName,
        headers: headers,
        data: data);
  }
}
