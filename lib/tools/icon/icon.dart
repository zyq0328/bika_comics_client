import 'package:flutter/widgets.dart';

class IconFont {
  static const String _family = 'iconfont';
  IconFont._();
  static const IconData icontanhao = IconData(0xe62a, fontFamily: _family);
  static const IconData iconCommentReply =
      IconData(0xe628, fontFamily: _family);
  static const IconData iconCommentLike = IconData(0xe629, fontFamily: _family);
  static const IconData iconShare = IconData(0xe627, fontFamily: _family);
  static const IconData iconHelp = IconData(0xe626, fontFamily: _family);
  static const IconData iconDirectionTransverse =
      IconData(0xe625, fontFamily: _family);
  static const IconData iconDirectionPortrait =
      IconData(0xe624, fontFamily: _family);
  static const IconData iconPanToNextPage =
      IconData(0xe623, fontFamily: _family);
  static const IconData iconPageUpAndDown =
      IconData(0xe622, fontFamily: _family);
  static const IconData iconAutoPage_ = IconData(0xe621, fontFamily: _family);
  static const IconData iconComment = IconData(0xe61a, fontFamily: _family);
  static const IconData iconList = IconData(0xe618, fontFamily: _family);
  static const IconData iconAtNight = IconData(0xe616, fontFamily: _family);
  static const IconData iconiconComment = IconData(0xe615, fontFamily: _family);
  static const IconData iconiconLike = IconData(0xe613, fontFamily: _family);
  static const IconData iconiconLikeNull =
      IconData(0xe60f, fontFamily: _family);
  static const IconData iconPunchTheClock =
      IconData(0xe60d, fontFamily: _family);
  static const IconData iconUp = IconData(0xe609, fontFamily: _family);
  static const IconData iconLeft = IconData(0xe60a, fontFamily: _family);
  static const IconData iconRight = IconData(0xe60b, fontFamily: _family);
  static const IconData iconDown = IconData(0xe60c, fontFamily: _family);
  static const IconData iconSearch = IconData(0xe608, fontFamily: _family);
  static const IconData iconProfile = IconData(0xe607, fontFamily: _family);
  static const IconData iconcategory = IconData(0xe606, fontFamily: _family);
  static const IconData iconsetting = IconData(0xe605, fontFamily: _family);
  static const IconData iconHome = IconData(0xe604, fontFamily: _family);
}
