/*
 * @Date: 2020-02-14 22:32:02
 * @名称: 下载页面的 从数据库读取需要下载的列表 所匹配数据库用的
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-14 22:32:34
 * @FilePath: /pica_acg/lib/tools/Db/Json/ComicsInfoDownload/DownloadDownloadList.dart
 */
class DownloadDownloadList {
  int id;
  String bookId;
  String title;
  String chapterId;
  int download;
  int success;
  int progress;

  DownloadDownloadList(
      {this.id,
      this.bookId,
      this.title,
      this.chapterId,
      this.download,
      this.success,
      this.progress});

  DownloadDownloadList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bookId = json['BookId'];
    title = json['Title'];
    chapterId = json['ChapterId'];
    download = json['Download'];
    success = json['Success'];
    progress = json['Progress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['BookId'] = this.bookId;
    data['Title'] = this.title;
    data['ChapterId'] = this.chapterId;
    data['Download'] = this.download;
    data['Success'] = this.success;
    data['Progress'] = this.progress;
    return data;
  }
}
