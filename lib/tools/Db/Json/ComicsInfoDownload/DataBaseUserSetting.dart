/*
 * @Date: 2020-04-21 23:05:04
 * @名称: 用户设置数据表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-04-21 23:05:37
 * @FilePath: /bika_comics_client/lib/tools/Db/Json/ComicsInfoDownload/DataBaseUserSetting.dart
 */
class DataBaseUserSetting {
  int screenOrientation;
  int pageTurningMode;
  double pageTurningInterval;
  int nightMode;
  int addDownloadTips;
  int openAppToContinueDownloading;

  DataBaseUserSetting(
      {this.screenOrientation,
      this.pageTurningMode,
      this.pageTurningInterval,
      this.nightMode,
      this.addDownloadTips,
      this.openAppToContinueDownloading});

  DataBaseUserSetting.fromJson(Map<String, dynamic> json) {
    screenOrientation = json['ScreenOrientation'];
    pageTurningMode = json['PageTurningMode'];
    pageTurningInterval = json['PageTurningInterval'];
    nightMode = json['NightMode'];
    addDownloadTips = json['AddDownloadTips'];
    openAppToContinueDownloading = json['OpenAppToContinueDownloading'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ScreenOrientation'] = this.screenOrientation;
    data['PageTurningMode'] = this.pageTurningMode;
    data['PageTurningInterval'] = this.pageTurningInterval;
    data['NightMode'] = this.nightMode;
    data['AddDownloadTips'] = this.addDownloadTips;
    data['OpenAppToContinueDownloading'] = this.openAppToContinueDownloading;
    return data;
  }
}
