/*
 * @Date: 2020-08-22 23:24:39
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 00:19:20
 * @FilePath: /bica_acg/lib/tools/Widget/RollWidget/RollWidget.dart
 */
import 'package:flutter/material.dart';

/*滚动组件 超出会滚动 */
class RollWidget extends StatefulWidget {
  final Widget child;
  final Axis direction;
  final Duration animationDuration, backDuration, pauseDuration;

  RollWidget({
    @required this.child,
    this.direction: Axis.horizontal,
    this.animationDuration: const Duration(milliseconds: 3000),
    this.backDuration: const Duration(milliseconds: 800),
    this.pauseDuration: const Duration(milliseconds: 800),
  });

  @override
  _RollWidgetState createState() => _RollWidgetState();
}

class _RollWidgetState extends State<RollWidget> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    scroll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: widget.child,
      scrollDirection: widget.direction,
      controller: scrollController,
    );
  }

  void scroll() async {
    while (true) {
      try {
        await Future.delayed(widget.pauseDuration);
        await scrollController.animateTo(
            scrollController.position.maxScrollExtent,
            duration: widget.animationDuration,
            curve: Curves.easeIn);
        await Future.delayed(widget.pauseDuration);
        await scrollController.animateTo(0.0,
            duration: widget.backDuration, curve: Curves.easeOut);
      } catch (e) {}
    }
  }
}
