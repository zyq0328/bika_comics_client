/*
 * @Date: 2020-01-31 14:44:24
 * @名称: 分类 - 漫画分类 - 列表 - 推荐分类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-22 23:41:04
 * @FilePath: /bica_acg/lib/tools/Widget/FilterClass/FilterClass.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/GlobalVarlableJson.dart';
import 'package:pica_acg/GlobalWidget.dart';

class FilterClass extends StatefulWidget {
  FilterClass({Key key}) : super(key: key);

  @override
  _FilterClassState createState() => _FilterClassState();
}

class _FilterClassState extends State<FilterClass> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(70),
      child: ListView.builder(
        itemCount: $GlobalFilterClass.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: TextModule(
              title: $GlobalFilterClass[index],
              color: $GlobalFilterClassInfo[$GlobalFilterClass[index]]['color'],
              textColor: $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                  ['TextColor'],
              borderRadius: $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                  ['borderRadius'],
            ),
            onTap: () {
              if ($GlobalFilterClassInfo[$GlobalFilterClass[index]]
                  ['Selection']) {
                $GlobalFilterClassInfo[$GlobalFilterClass[index]] = {
                  'color': Color(0x005ac6ff),
                  'TextColor': Color(0xff808080),
                  'Selection': false,
                  'borderRadius':
                      $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                          ['borderRadius'],
                  'Original': $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                      ['Original'],
                };

                Navigator.of(context).push(
                  PageRouteBuilder(
                    pageBuilder: (context, _, __) => GlobalTips(
                      body: '你已启用了一个本子的封印！如果要解除封印，请按列表顶部的类别按钮',
                      imageAsset: "images/Tisp/Success.png",
                      height: ScreenUtil().setHeight(900),
                    ),
                    opaque: false,
                  ),
                );
              } else {
                $GlobalFilterClassInfo[$GlobalFilterClass[index]] = {
                  'color': $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                      ["Original"]['color'],
                  'TextColor': $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                      ["Original"]['TextColor'],
                  'Selection': true,
                  'borderRadius':
                      $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                          ['borderRadius'],
                  'Original': $GlobalFilterClassInfo[$GlobalFilterClass[index]]
                      ['Original'],
                };
              }
              comicsComicsBodyComicsBodyContorllerState.currentState
                  .classificationUpdate();
              print($GlobalFilterClassInfo[$GlobalFilterClass[index]]
                  ['Selection']);
              setState(() {});
            },
          );
        },
      ),
    );
  }
}

class TextModule extends StatefulWidget {
  TextModule({
    Key key,
    this.title,
    this.color,
    this.borderRadius,
    this.textColor,
  }) : super(key: key);
  final String title;
  final Color color;
  final Color textColor;
  final BorderRadius borderRadius;
  @override
  _TextModuleState createState() => _TextModuleState();
}

class _TextModuleState extends State<TextModule> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(10),
          top: ScreenUtil().setHeight(10),
          bottom: ScreenUtil().setHeight(10),
        ),
        child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(30),
            right: ScreenUtil().setWidth(30),
          ),
          alignment: Alignment.center,
          child: Text(
            widget.title,
            style: TextStyle(
              height: ScreenUtil().setHeight(2.75), //!设置高度字体
              // backgroundColor: Color(0xffff0000),
              color: widget.textColor, // 字体颜色
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        decoration: new BoxDecoration(
          color: widget.color, // 底色
          border:
              new Border.all(color: Color(0xFFaaa9aa), width: 0.5), // 边色与边宽度
          //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
          borderRadius: widget.borderRadius,
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
