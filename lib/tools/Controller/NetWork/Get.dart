import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pica_acg/GlobalSettings.dart';

class NetWork {
  /*
   *
   * get请求
   * 
   * url 请求路径
   * 
   * callBack() function 回调
   * 
   * 可选参数
   * 
   * parameter json类型的 扩展参数
   * 
   * ResponseType 请求方式
   * 
   * requestName 请求名 可以使用请求名终止请求
   * 
   */

  static void get(
    String url,
    Function callBack, {
    Map<String, dynamic> parameter,
    ResponseType responseType,
    String userToKen,
    CancelToken requestName,
    Map<String, dynamic> headers,
  }) async {
    //代码
    Response response;
    if (headers == null || headers.length <= 0) {
      headers = {
        HttpHeaders.acceptHeader: "accept: application/json",
        "Authorization": (userToKen == null) ? $tempUserToKen : userToKen,
      };
    }
    if (parameter != null && parameter.isNotEmpty) {
      StringBuffer sb = new StringBuffer("?");
      parameter.forEach((key, value) {
        sb.write("$key" + "=" + "$value" + "&");
      });
      String paramStr = sb.toString();
      paramStr = paramStr.substring(0, paramStr.length - 1);
      url += paramStr;
    }
    Options options = Options(
      responseType: responseType,
      headers: headers,
    );
    try {
      //404
      response = await Dio().get(
        url,
        cancelToken: requestName,
        options: options,
      );
      //请求成功
      if (callBack != null) {
        callBack(response.data);
      }
    } on DioError catch (error) {
      print(error);
      print(error.type);
      /* 请求失败  */
      // Map data = Map();
      var data = {
        'code': "-1",
        'response': null,
      };
      callBack(data);
    }
  }
  /*
   *
   * Post请求
   * 
   * url 请求路径
   * 
   * callBack() function 回调
   * 
   * 可选参数
   * 
   * parameter json类型的 扩展参数
   * 
   * ResponseType 请求方式
   * 
   * requestName 请求名 可以使用请求名终止请求
   * 
   */

  static void post(
    String url,
    Function callBack, {
    Map<String, dynamic> parameter,
    Map<String, dynamic> headers,
    dynamic data,
    ResponseType responseType,
    CancelToken requestName,
    String userToKen,
  }) async {
    //代码
    if (headers.length <= 0) {
      headers = {
        HttpHeaders.acceptHeader: "accept: application/json",
        "Authorization": (userToKen == null) ? $tempUserToKen : userToKen,
      };
    }
    Response response;
    Options options = Options(
      responseType: responseType,
      headers: headers,
    );
    try {
      //404
      response = await Dio().post(
        url,
        cancelToken: requestName,
        queryParameters: parameter,
        data: data,
        options: options,
      );
      //请求成功
      if (callBack != null) {
        callBack(response.data);
      }
    } on DioError catch (error) {
      print(error);
      print(error.type);
      /* 请求失败  */
      // Map data = Map();
      var data = {
        'code': "-1",
        'response': null,
      };
      callBack(data);
    }
  }

  static void stop(
    String requestName,
    // Function callBack,
    /* {Map<String, dynamic> parameter,
      ResponseType responseType,
      String requestName} */
  ) async {
    //代码
    // Response response;
    final CancelToken requestName = CancelToken();
    // print(requestName);
    requestName.cancel('cancelled');
  }
}
