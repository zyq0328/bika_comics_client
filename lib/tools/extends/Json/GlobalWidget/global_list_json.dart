/*
 * @Date: 2020-02-01 21:24:22
 * @名称: 全局列表 解析用的
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-01 22:20:22
 * @FilePath: /pica_acg/lib/tools/extends/Json/GlobalWidget/global_list.dart
 */
import 'package:json_annotation/json_annotation.dart'; 
  
part 'global_list_json.g.dart';


@JsonSerializable()
  class GlobalListJson extends Object {

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'a_list')
  List<A_list> aList;

  @JsonKey(name: 'exitTitle')
  String exitTitle;

  @JsonKey(name: 'exitFuncton')
  String exitFuncton;

  GlobalListJson(this.title,this.aList,this.exitTitle,this.exitFuncton,);

  factory GlobalListJson.fromJson(Map<String, dynamic> srcJson) => _$GlobalListJsonFromJson(srcJson);

  Map<String, dynamic> toJson() => _$GlobalListJsonToJson(this);

}

  
@JsonSerializable()
  class A_list extends Object {

  @JsonKey(name: 'text')
  String text;

  @JsonKey(name: 'color')
  String color;

  A_list(this.text,this.color,);

  factory A_list.fromJson(Map<String, dynamic> srcJson) => _$A_listFromJson(srcJson);

  Map<String, dynamic> toJson() => _$A_listToJson(this);

}

  
