// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global_list_json.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GlobalListJson _$GlobalListJsonFromJson(Map<String, dynamic> json) {
  return GlobalListJson(
    json['title'] as String,
    (json['a_list'] as List)
        ?.map((e) =>
            e == null ? null : A_list.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['exitTitle'] as String,
    json['exitFuncton'] as String,
  );
}

Map<String, dynamic> _$GlobalListJsonToJson(GlobalListJson instance) =>
    <String, dynamic>{
      'title': instance.title,
      'a_list': instance.aList,
      'exitTitle': instance.exitTitle,
      'exitFuncton': instance.exitFuncton,
    };

A_list _$A_listFromJson(Map<String, dynamic> json) {
  return A_list(
    json['text'] as String,
    json['color'] as String,
  );
}

Map<String, dynamic> _$A_listToJson(A_list instance) => <String, dynamic>{
      'text': instance.text,
      'color': instance.color,
    };
