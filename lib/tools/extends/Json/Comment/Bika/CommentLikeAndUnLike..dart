/*
 * @Date: 2020-03-20 21:44:19
 * @名称: 评论列表 喜欢 和不喜欢
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-20 21:44:47
 * @FilePath: /pica_acg/lib/tools/extends/Json/Comment/Bika/CommentLikeAndUnLike..dart
 */
class CommentLikeAndUnLike {
  String code;
  String message;
  Data data;

  CommentLikeAndUnLike({this.code, this.message, this.data});

  CommentLikeAndUnLike.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String action;

  Data({this.action});

  Data.fromJson(Map<String, dynamic> json) {
    action = json['action'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['action'] = this.action;
    return data;
  }
}
