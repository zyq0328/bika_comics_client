/*
 * @Date: 2020-02-24 18:49:36
 * @名称: 子评论解析
 * @版本: 0.02
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-27 01:08:04
 * @FilePath: /bica_acg/lib/tools/extends/Json/Comment/Bika/SonComment.dart
 * //todo       warning ⚠️ warning
 * //todo       这里引入了主评论的解析文件 因为需要用到 主评论的 用户头像哪一块 否则会冲突

 */

class SonComment {
  String message;
  Data data;
  String code;

  SonComment({this.message, this.data, this.code});

  SonComment.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  Comments comments;

  Data({this.comments});

  Data.fromJson(Map<String, dynamic> json) {
    comments = json['comments'] != null
        ? new Comments.fromJson(json['comments'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comments != null) {
      data['comments'] = this.comments.toJson();
    }
    return data;
  }
}

class Comments {
  int page;
  int pages;
  List<Docs> docs;
  int total;
  int limit;

  Comments({this.page, this.pages, this.docs, this.total, this.limit});

  Comments.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    pages = json['pages'];
    if (json['docs'] != null) {
      docs = new List<Docs>();
      json['docs'].forEach((v) {
        docs.add(new Docs.fromJson(v));
      });
    }
    total = json['total'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['pages'] = this.pages;
    if (this.docs != null) {
      data['docs'] = this.docs.map((v) => v.toJson()).toList();
    }
    data['total'] = this.total;
    data['limit'] = this.limit;
    return data;
  }
}

class Docs {
  String sId;
  String content;
  String sComic;
  bool isTop;
  bool hide;
  String createdAt;
  User uUser;
  String sParent;
  String commentId;
  String sonTableName;
  int likesCount;
  int commentCount;
  bool isLiked;

  Docs(
      {this.sId,
      this.content,
      this.sComic,
      this.isTop,
      this.hide,
      this.createdAt,
      this.uUser,
      this.sParent,
      this.commentId,
      this.sonTableName,
      this.likesCount,
      this.commentCount,
      this.isLiked});

  Docs.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    content = json['content'];
    sComic = json['_comic'];
    isTop = json['isTop'];
    hide = json['hide'];
    createdAt = json['created_at'];
    uUser = json['_user'] != null ? new User.fromJson(json['_user']) : null;
    sParent = json['_parent'];
    commentId = json['comment_id'];
    sonTableName = json['son_table_name'];
    likesCount = json['likesCount'];
    commentCount = json['commentCount'];
    isLiked = json['isLiked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['content'] = this.content;
    data['_comic'] = this.sComic;
    data['isTop'] = this.isTop;
    data['hide'] = this.hide;
    data['created_at'] = this.createdAt;
    if (this.uUser != null) {
      data['_user'] = this.uUser.toJson();
    }
    data['_parent'] = this.sParent;
    data['comment_id'] = this.commentId;
    data['son_table_name'] = this.sonTableName;
    data['likesCount'] = this.likesCount;
    data['commentCount'] = this.commentCount;
    data['isLiked'] = this.isLiked;
    return data;
  }
}

class User {
  List<String> characters;
  int exp;
  String slogan;
  bool verified;
  String sId;
  String title;
  int level;
  Avatar avatar;
  String role;
  String character;
  String gender;
  String name;

  User(
      {this.characters,
      this.exp,
      this.slogan,
      this.verified,
      this.sId,
      this.title,
      this.level,
      this.avatar,
      this.role,
      this.character,
      this.gender,
      this.name});

  User.fromJson(Map<String, dynamic> json) {
    characters = json['characters'].cast<String>();
    exp = json['exp'];
    slogan = json['slogan'];
    verified = json['verified'];
    sId = json['_id'];
    title = json['title'];
    level = json['level'];
    avatar =
        json['avatar'] != null ? new Avatar.fromJson(json['avatar']) : null;
    role = json['role'];
    character = json['character'];
    gender = json['gender'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['characters'] = this.characters;
    data['exp'] = this.exp;
    data['slogan'] = this.slogan;
    data['verified'] = this.verified;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['level'] = this.level;
    if (this.avatar != null) {
      data['avatar'] = this.avatar.toJson();
    }
    data['role'] = this.role;
    data['character'] = this.character;
    data['gender'] = this.gender;
    data['name'] = this.name;
    return data;
  }
}

class Avatar {
  String path;
  String originalName;
  String fileServer;

  Avatar({this.path, this.originalName, this.fileServer});

  Avatar.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    originalName = json['originalName'];
    fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this.path;
    data['originalName'] = this.originalName;
    data['fileServer'] = this.fileServer;
    return data;
  }
}
