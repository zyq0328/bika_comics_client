/*
 * @Date: 2020-01-11 22:35:23
 * @名称: 分类里面的分类列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 22:35:50
 */
import 'package:json_annotation/json_annotation.dart';

part 'popular_classification.g.dart';

@JsonSerializable()
class PopularClassification extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  PopularClassification(
    this.code,
    this.message,
    this.data,
  );

  factory PopularClassification.fromJson(Map<String, dynamic> srcJson) =>
      _$PopularClassificationFromJson(srcJson);

  Map<String, dynamic> toJson() => _$PopularClassificationToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'categories')
  List<Categories> categories;

  Data(
    this.categories,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Categories extends Object {
  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'thumb')
  Thumb thumb;

  @JsonKey(name: 'isWeb')
  bool isWeb;

  @JsonKey(name: 'active')
  bool active;

  @JsonKey(name: 'link')
  String link;

  Categories(
    this.title,
    this.thumb,
    this.isWeb,
    this.active,
    this.link,
  );

  factory Categories.fromJson(Map<String, dynamic> srcJson) =>
      _$CategoriesFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CategoriesToJson(this);
}

@JsonSerializable()
class Thumb extends Object {
  @JsonKey(name: 'originalName')
  String originalName;

  @JsonKey(name: 'path')
  String path;

  @JsonKey(name: 'fileServer')
  String fileServer;

  Thumb(
    this.originalName,
    this.path,
    this.fileServer,
  );

  factory Thumb.fromJson(Map<String, dynamic> srcJson) =>
      _$ThumbFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ThumbToJson(this);
}
