// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_classification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PopularClassification _$PopularClassificationFromJson(
    Map<String, dynamic> json) {
  return PopularClassification(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PopularClassificationToJson(
        PopularClassification instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Categories.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'categories': instance.categories,
    };

Categories _$CategoriesFromJson(Map<String, dynamic> json) {
  return Categories(
    json['title'] as String,
    json['thumb'] == null
        ? null
        : Thumb.fromJson(json['thumb'] as Map<String, dynamic>),
    json['isWeb'] as bool,
    json['active'] as bool,
    json['link'] as String,
  );
}

Map<String, dynamic> _$CategoriesToJson(Categories instance) =>
    <String, dynamic>{
      'title': instance.title,
      'thumb': instance.thumb,
      'isWeb': instance.isWeb,
      'active': instance.active,
      'link': instance.link,
    };

Thumb _$ThumbFromJson(Map<String, dynamic> json) {
  return Thumb(
    json['originalName'] as String,
    json['path'] as String,
    json['fileServer'] as String,
  );
}

Map<String, dynamic> _$ThumbToJson(Thumb instance) => <String, dynamic>{
      'originalName': instance.originalName,
      'path': instance.path,
      'fileServer': instance.fileServer,
    };
