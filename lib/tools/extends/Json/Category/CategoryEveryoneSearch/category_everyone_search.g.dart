// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_everyone_search.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryEveryoneSearch _$CategoryEveryoneSearchFromJson(
    Map<String, dynamic> json) {
  return CategoryEveryoneSearch(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CategoryEveryoneSearchToJson(
        CategoryEveryoneSearch instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    (json['keywords'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'keywords': instance.keywords,
    };
