/*
 * @Date: 2020-01-11 10:32:32
 * @名称: 分类页面 大家都在搜
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 22:40:06
 */
import 'package:json_annotation/json_annotation.dart';

part 'category_everyone_search.g.dart';

@JsonSerializable()
class CategoryEveryoneSearch extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  CategoryEveryoneSearch(
    this.code,
    this.message,
    this.data,
  );

  factory CategoryEveryoneSearch.fromJson(Map<String, dynamic> srcJson) =>
      _$CategoryEveryoneSearchFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CategoryEveryoneSearchToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'keywords')
  List<String> keywords;

  Data(
    this.keywords,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}
