/*
 * @Date: 2020-02-24 18:59:08
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-07 19:36:03
 * @FilePath: /bika_comics_client/lib/tools/extends/Json/ServiceList/service_list_json.g.dart
 * @最后编辑: 初雪桜
 */
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_list_json.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceListJson _$ServiceListJsonFromJson(Map<String, dynamic> json) {
  return ServiceListJson(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ServiceListJsonToJson(ServiceListJson instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    (json['sources'] as List)
        ?.map((e) =>
            e == null ? null : Sources.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'sources': instance.sources,
    };

Sources _$SourcesFromJson(Map<String, dynamic> json) {
  return Sources(
    json['title'] as String,
    json['code'] as String,
    json['url'] as String,
    json['chat'] as String,
  );
}

Map<String, dynamic> _$SourcesToJson(Sources instance) => <String, dynamic>{
      'title': instance.title,
      'code': instance.code,
      'url': instance.url,
      'chat': instance.chat,
    };
