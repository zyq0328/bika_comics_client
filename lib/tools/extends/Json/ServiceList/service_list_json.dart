import 'package:json_annotation/json_annotation.dart';

part 'service_list_json.g.dart';

@JsonSerializable()
class ServiceListJson extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  ServiceListJson(
    this.code,
    this.message,
    this.data,
  );

  factory ServiceListJson.fromJson(Map<String, dynamic> srcJson) =>
      _$ServiceListJsonFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ServiceListJsonToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'sources')
  List<Sources> sources;

  Data(
    this.sources,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Sources extends Object {
  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'url')
  String url;

  @JsonKey(name: 'chat')
  String chat;

  Sources(
    this.title,
    this.code,
    this.url,
    this.chat,
  );

  factory Sources.fromJson(Map<String, dynamic> srcJson) =>
      _$SourcesFromJson(srcJson);

  Map<String, dynamic> toJson() => _$SourcesToJson(this);
}
