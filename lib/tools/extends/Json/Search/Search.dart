class SearchJson {
  String _message;
  Data _data;
  String _code;

  SearchJson({String message, Data data, String code}) {
    this._message = message;
    this._data = data;
    this._code = code;
  }

  String get message => _message;
  set message(String message) => _message = message;
  Data get data => _data;
  set data(Data data) => _data = data;
  String get code => _code;
  set code(String code) => _code = code;

  SearchJson.fromJson(Map<String, dynamic> json) {
    _message = json['message'];
    _data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    _code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    data['code'] = this._code;
    return data;
  }
}

class Data {
  Comics _comics;

  Data({Comics comics}) {
    this._comics = comics;
  }

  Comics get comics => _comics;
  set comics(Comics comics) => _comics = comics;

  Data.fromJson(Map<String, dynamic> json) {
    _comics =
        json['comics'] != null ? new Comics.fromJson(json['comics']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._comics != null) {
      data['comics'] = this._comics.toJson();
    }
    return data;
  }
}

class Comics {
  int _page;
  int _pages;
  List<Docs> _docs;
  int _total;
  int _limit;

  Comics({int page, int pages, List<Docs> docs, int total, int limit}) {
    this._page = page;
    this._pages = pages;
    this._docs = docs;
    this._total = total;
    this._limit = limit;
  }

  int get page => _page;
  set page(int page) => _page = page;
  int get pages => _pages;
  set pages(int pages) => _pages = pages;
  List<Docs> get docs => _docs;
  set docs(List<Docs> docs) => _docs = docs;
  int get total => _total;
  set total(int total) => _total = total;
  int get limit => _limit;
  set limit(int limit) => _limit = limit;

  Comics.fromJson(Map<String, dynamic> json) {
    _page = json['page'];
    _pages = json['pages'];
    if (json['docs'] != null) {
      _docs = new List<Docs>();
      json['docs'].forEach((v) {
        _docs.add(new Docs.fromJson(v));
      });
    }
    _total = json['total'];
    _limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this._page;
    data['pages'] = this._pages;
    if (this._docs != null) {
      data['docs'] = this._docs.map((v) => v.toJson()).toList();
    }
    data['total'] = this._total;
    data['limit'] = this._limit;
    return data;
  }
}

class Docs {
  int _totalLikes;
  List<String> _categories;
  Thumb _thumb;
  String _id;
  int _likesCount;
  List<Author> _author;
  int _pagesCount;
  String _sId;
  int _totalViews;
  String _title;
  int _epsCount;
  bool _finished;
  List<ChineseTeam> _chineseTeam;

  Docs(
      {int totalLikes,
      List<String> categories,
      Thumb thumb,
      String id,
      int likesCount,
      List<Author> author,
      int pagesCount,
      String sId,
      int totalViews,
      String title,
      int epsCount,
      bool finished,
      List<ChineseTeam> chineseTeam}) {
    this._totalLikes = totalLikes;
    this._categories = categories;
    this._thumb = thumb;
    this._id = id;
    this._likesCount = likesCount;
    this._author = author;
    this._pagesCount = pagesCount;
    this._sId = sId;
    this._totalViews = totalViews;
    this._title = title;
    this._epsCount = epsCount;
    this._finished = finished;
    this._chineseTeam = chineseTeam;
  }

  int get totalLikes => _totalLikes;
  set totalLikes(int totalLikes) => _totalLikes = totalLikes;
  List<String> get categories => _categories;
  set categories(List<String> categories) => _categories = categories;
  Thumb get thumb => _thumb;
  set thumb(Thumb thumb) => _thumb = thumb;
  String get id => _id;
  set id(String id) => _id = id;
  int get likesCount => _likesCount;
  set likesCount(int likesCount) => _likesCount = likesCount;
  List<Author> get author => _author;
  set author(List<Author> author) => _author = author;
  int get pagesCount => _pagesCount;
  set pagesCount(int pagesCount) => _pagesCount = pagesCount;
  String get sId => _sId;
  set sId(String sId) => _sId = sId;
  int get totalViews => _totalViews;
  set totalViews(int totalViews) => _totalViews = totalViews;
  String get title => _title;
  set title(String title) => _title = title;
  int get epsCount => _epsCount;
  set epsCount(int epsCount) => _epsCount = epsCount;
  bool get finished => _finished;
  set finished(bool finished) => _finished = finished;
  List<ChineseTeam> get chineseTeam => _chineseTeam;
  set chineseTeam(List<ChineseTeam> chineseTeam) => _chineseTeam = chineseTeam;

  Docs.fromJson(Map<String, dynamic> json) {
    _totalLikes = json['totalLikes'];
    _categories = json['categories'].cast<String>();
    _thumb = json['thumb'] != null ? new Thumb.fromJson(json['thumb']) : null;
    _id = json['id'];
    _likesCount = json['likesCount'];
    if (json['author'] != null) {
      _author = new List<Author>();
      json['author'].forEach((v) {
        _author.add(new Author.fromJson(v));
      });
    }
    _pagesCount = json['pagesCount'];
    _sId = json['_id'];
    _totalViews = json['totalViews'];
    _title = json['title'];
    _epsCount = json['epsCount'];
    _finished = json['finished'];
    if (json['chineseTeam'] != null) {
      _chineseTeam = new List<ChineseTeam>();
      json['chineseTeam'].forEach((v) {
        _chineseTeam.add(new ChineseTeam.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalLikes'] = this._totalLikes;
    data['categories'] = this._categories;
    if (this._thumb != null) {
      data['thumb'] = this._thumb.toJson();
    }
    data['id'] = this._id;
    data['likesCount'] = this._likesCount;
    if (this._author != null) {
      data['author'] = this._author.map((v) => v.toJson()).toList();
    }
    data['pagesCount'] = this._pagesCount;
    data['_id'] = this._sId;
    data['totalViews'] = this._totalViews;
    data['title'] = this._title;
    data['epsCount'] = this._epsCount;
    data['finished'] = this._finished;
    if (this._chineseTeam != null) {
      data['chineseTeam'] = this._chineseTeam.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Thumb {
  String _path;
  String _originalName;
  String _fileServer;

  Thumb({String path, String originalName, String fileServer}) {
    this._path = path;
    this._originalName = originalName;
    this._fileServer = fileServer;
  }

  String get path => _path;
  set path(String path) => _path = path;
  String get originalName => _originalName;
  set originalName(String originalName) => _originalName = originalName;
  String get fileServer => _fileServer;
  set fileServer(String fileServer) => _fileServer = fileServer;

  Thumb.fromJson(Map<String, dynamic> json) {
    _path = json['path'];
    _originalName = json['originalName'];
    _fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this._path;
    data['originalName'] = this._originalName;
    data['fileServer'] = this._fileServer;
    return data;
  }
}

class Author {
  String _name;
  String _userId;

  Author({String name, String userId}) {
    this._name = name;
    this._userId = userId;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get userId => _userId;
  set userId(String userId) => _userId = userId;

  Author.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['user_id'] = this._userId;
    return data;
  }
}

class ChineseTeam {
  Null _name;
  String _userId;

  ChineseTeam({Null name, String userId}) {
    this._name = name;
    this._userId = userId;
  }

  Null get name => _name;
  set name(Null name) => _name = name;
  String get userId => _userId;
  set userId(String userId) => _userId = userId;

  ChineseTeam.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['user_id'] = this._userId;
    return data;
  }
}
