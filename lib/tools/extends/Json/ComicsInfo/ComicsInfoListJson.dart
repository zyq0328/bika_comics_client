class ComicsInfoListJson {
  String code;
  String message;
  Data data;
  String error;

  ComicsInfoListJson({this.code, this.message, this.data, this.error});

  ComicsInfoListJson.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    error = json['error'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Pages pages;
  Ep ep;

  Data({this.pages, this.ep});

  Data.fromJson(Map<String, dynamic> json) {
    pages = json['pages'] != null ? new Pages.fromJson(json['pages']) : null;
    ep = json['ep'] != null ? new Ep.fromJson(json['ep']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pages != null) {
      data['pages'] = this.pages.toJson();
    }
    if (this.ep != null) {
      data['ep'] = this.ep.toJson();
    }
    return data;
  }
}

class Pages {
  List<Docs> docs;
  int total;
  int limit;
  int page;
  int pages;

  Pages({this.docs, this.total, this.limit, this.page, this.pages});

  Pages.fromJson(Map<String, dynamic> json) {
    if (json['docs'] != null) {
      docs = new List<Docs>();
      json['docs'].forEach((v) {
        docs.add(new Docs.fromJson(v));
      });
    }
    total = json['total'];
    limit = json['limit'];
    page = json['page'];
    pages = json['pages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.docs != null) {
      data['docs'] = this.docs.map((v) => v.toJson()).toList();
    }
    data['total'] = this.total;
    data['limit'] = this.limit;
    data['page'] = this.page;
    data['pages'] = this.pages;
    return data;
  }
}

class Docs {
  String sId;
  Media media;
  String id;
  int isPath;

  Docs({this.sId, this.media, this.isPath, this.id});

  Docs.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    media = json['media'] != null ? new Media.fromJson(json['media']) : null;
    id = json['id'];
    isPath = json['is_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['is_path'] = this.isPath;
    if (this.media != null) {
      data['media'] = this.media.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Media {
  String originalName;
  String path;
  String fileServer;

  Media({this.originalName, this.path, this.fileServer});

  Media.fromJson(Map<String, dynamic> json) {
    originalName = json['originalName'];
    path = json['path'];
    fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['originalName'] = this.originalName;
    data['path'] = this.path;
    data['fileServer'] = this.fileServer;
    return data;
  }
}

class Ep {
  String sId;
  String title;

  Ep({this.sId, this.title});

  Ep.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    return data;
  }
}
