import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class Recommendation {
  String code;
  String message;
  Data data;

  Recommendation({this.code, this.message, this.data});

  Recommendation.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Comics> comics;

  Data({this.comics});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['comics'] != null) {
      comics = new List<Comics>();
      json['comics'].forEach((v) {
        comics.add(new Comics.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comics != null) {
      data['comics'] = this.comics.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comics {
  String sId;
  String id;
  String title;
  bool finished;
  int pagesCount;
  int epsCount;
  List<String> categories;
  Thumb thumb;
  int likesCount;
  List<Author> author;
  List<ChineseTeam> chineseTeam;

  Comics(
      {this.sId,
      this.id,
      this.title,
      this.finished,
      this.pagesCount,
      this.epsCount,
      this.categories,
      this.thumb,
      this.likesCount,
      this.author,
      this.chineseTeam});

  Comics.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    id = json['id'];
    title = json['title'];
    finished = json['finished'];
    pagesCount = json['pagesCount'];
    epsCount = json['epsCount'];
    categories = json['categories'].cast<String>();
    thumb = json['thumb'] != null ? new Thumb.fromJson(json['thumb']) : null;
    likesCount = json['likesCount'];
    if (json['author'] != null) {
      author = new List<Author>();
      json['author'].forEach((v) {
        author.add(new Author.fromJson(v));
      });
    }
    if (json['chineseTeam'] != null) {
      chineseTeam = new List<ChineseTeam>();
      json['chineseTeam'].forEach((v) {
        chineseTeam.add(new ChineseTeam.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['id'] = this.id;
    data['title'] = this.title;
    data['finished'] = this.finished;
    data['pagesCount'] = this.pagesCount;
    data['epsCount'] = this.epsCount;
    data['categories'] = this.categories;
    if (this.thumb != null) {
      data['thumb'] = this.thumb.toJson();
    }
    data['likesCount'] = this.likesCount;
    if (this.author != null) {
      data['author'] = this.author.map((v) => v.toJson()).toList();
    }
    if (this.chineseTeam != null) {
      data['chineseTeam'] = this.chineseTeam.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Thumb {
  String originalName;
  String path;
  String fileServer;

  Thumb({this.originalName, this.path, this.fileServer});

  Thumb.fromJson(Map<String, dynamic> json) {
    originalName = json['originalName'];
    path = json['path'];
    fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['originalName'] = this.originalName;
    data['path'] = this.path;
    data['fileServer'] = this.fileServer;
    return data;
  }
}
