// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'all_chapter_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AllChapterList _$AllChapterListFromJson(Map<String, dynamic> json) {
  return AllChapterList(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AllChapterListToJson(AllChapterList instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['eps'] == null
        ? null
        : Eps.fromJson(json['eps'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'eps': instance.eps,
    };

Eps _$EpsFromJson(Map<String, dynamic> json) {
  return Eps(
    (json['docs'] as List)
        ?.map(
            (e) => e == null ? null : Docs.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['total'] as int,
    json['limit'] as int,
    json['page'] as int,
    json['pages'] as int,
  );
}

Map<String, dynamic> _$EpsToJson(Eps instance) => <String, dynamic>{
      'docs': instance.docs,
      'total': instance.total,
      'limit': instance.limit,
      'page': instance.page,
      'pages': instance.pages,
    };

Docs _$DocsFromJson(Map<String, dynamic> json) {
  return Docs(
    json['_id'] as String,
    json['title'] as String,
    json['order'] as int,
    json['updated_at'] as String,
    json['id'] as String,
  );
}

Map<String, dynamic> _$DocsToJson(Docs instance) => <String, dynamic>{
      '_id': instance.sid,
      'title': instance.title,
      'order': instance.order,
      'updated_at': instance.updatedAt,
      'id': instance.id,
    };
