class ComicsInfo {
  String code;
  String message;
  Data data;

  ComicsInfo({this.code, this.message, this.data});

  ComicsInfo.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Comic comic;

  Data({this.comic});

  Data.fromJson(Map<String, dynamic> json) {
    comic = json['comic'] != null ? new Comic.fromJson(json['comic']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comic != null) {
      data['comic'] = this.comic.toJson();
    }
    return data;
  }
}

class Comic {
  String sId;
  Creator cCreator;
  String title;
  String description;
  Avatar thumb;
  List<Author> author;
  List<ChineseTeam> chineseTeam;
  List<String> categories;
  List<String> tags;
  int pagesCount;
  int epsCount;
  bool finished;
  String updatedAt;
  String createdAt;
  bool allowDownload;
  bool allowComment;
  int totalLikes;
  int totalViews;
  int viewsCount;
  int likesCount;
  bool isFavourite;
  bool isLiked;
  int commentsCount;

  Comic(
      {this.sId,
      this.cCreator,
      this.title,
      this.description,
      this.thumb,
      this.author,
      this.chineseTeam,
      this.categories,
      this.tags,
      this.pagesCount,
      this.epsCount,
      this.finished,
      this.updatedAt,
      this.createdAt,
      this.allowDownload,
      this.allowComment,
      this.totalLikes,
      this.totalViews,
      this.viewsCount,
      this.likesCount,
      this.isFavourite,
      this.isLiked,
      this.commentsCount});

  Comic.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    cCreator = json['_creator'] != null
        ? new Creator.fromJson(json['_creator'])
        : null;
    title = json['title'];
    description = json['description'];
    thumb = json['thumb'] != null ? new Avatar.fromJson(json['thumb']) : null;
    if (json['author'] != null) {
      author = new List<Author>();
      json['author'].forEach((v) {
        author.add(new Author.fromJson(v));
      });
    }
    if (json['chineseTeam'] != null) {
      chineseTeam = new List<ChineseTeam>();
      json['chineseTeam'].forEach((v) {
        chineseTeam.add(new ChineseTeam.fromJson(v));
      });
    }
    categories = json['categories'].cast<String>();
    tags = json['tags'].cast<String>();
    pagesCount = json['pagesCount'];
    epsCount = json['epsCount'];
    finished = json['finished'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    allowDownload = json['allowDownload'];
    allowComment = json['allowComment'];
    totalLikes = json['totalLikes'];
    totalViews = json['totalViews'];
    viewsCount = json['viewsCount'];
    likesCount = json['likesCount'];
    isFavourite = json['isFavourite'];
    isLiked = json['isLiked'];
    commentsCount = json['commentsCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.cCreator != null) {
      data['_creator'] = this.cCreator.toJson();
    }
    data['title'] = this.title;
    data['description'] = this.description;
    if (this.thumb != null) {
      data['thumb'] = this.thumb.toJson();
    }
    if (this.author != null) {
      data['author'] = this.author.map((v) => v.toJson()).toList();
    }
    if (this.chineseTeam != null) {
      data['chineseTeam'] = this.chineseTeam.map((v) => v.toJson()).toList();
    }
    data['categories'] = this.categories;
    data['tags'] = this.tags;
    data['pagesCount'] = this.pagesCount;
    data['epsCount'] = this.epsCount;
    data['finished'] = this.finished;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['allowDownload'] = this.allowDownload;
    data['allowComment'] = this.allowComment;
    data['totalLikes'] = this.totalLikes;
    data['totalViews'] = this.totalViews;
    data['viewsCount'] = this.viewsCount;
    data['likesCount'] = this.likesCount;
    data['isFavourite'] = this.isFavourite;
    data['isLiked'] = this.isLiked;
    data['commentsCount'] = this.commentsCount;
    return data;
  }
}

class Creator {
  String sId;
  String gender;
  String name;
  String slogan;
  String title;
  bool verified;
  int exp;
  int level;
  List<String> characters;
  String role;
  Avatar avatar;
  String character;

  Creator(
      {this.sId,
      this.gender,
      this.name,
      this.slogan,
      this.title,
      this.verified,
      this.exp,
      this.level,
      this.characters,
      this.role,
      this.avatar,
      this.character});

  Creator.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    gender = json['gender'];
    name = json['name'];
    slogan = json['slogan'];
    title = json['title'];
    verified = json['verified'];
    exp = json['exp'];
    level = json['level'];
    characters = json['characters'].cast<String>();
    role = json['role'];
    avatar =
        json['avatar'] != null ? new Avatar.fromJson(json['avatar']) : null;
    character = json['character'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['gender'] = this.gender;
    data['name'] = this.name;
    data['slogan'] = this.slogan;
    data['title'] = this.title;
    data['verified'] = this.verified;
    data['exp'] = this.exp;
    data['level'] = this.level;
    data['characters'] = this.characters;
    data['role'] = this.role;
    if (this.avatar != null) {
      data['avatar'] = this.avatar.toJson();
    }
    data['character'] = this.character;
    return data;
  }
}

class Avatar {
  String fileServer;
  String path;
  String originalName;

  Avatar({this.fileServer, this.path, this.originalName});

  Avatar.fromJson(Map<String, dynamic> json) {
    fileServer = json['fileServer'];
    path = json['path'];
    originalName = json['originalName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fileServer'] = this.fileServer;
    data['path'] = this.path;
    data['originalName'] = this.originalName;
    return data;
  }
}

class Author {
  String name;
  String userId;

  Author({this.name, this.userId});

  Author.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['user_id'] = this.userId;
    return data;
  }
}

class ChineseTeam {
  String name;
  String userId;

  ChineseTeam({this.name, this.userId});

  ChineseTeam.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['user_id'] = this.userId;
    return data;
  }
}
