import 'package:json_annotation/json_annotation.dart';

part 'all_chapter_list.g.dart';

@JsonSerializable()
class AllChapterList extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  AllChapterList(
    this.code,
    this.message,
    this.data,
  );

  factory AllChapterList.fromJson(Map<String, dynamic> srcJson) =>
      _$AllChapterListFromJson(srcJson);

  Map<String, dynamic> toJson() => _$AllChapterListToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'eps')
  Eps eps;

  Data(
    this.eps,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Eps extends Object {
  @JsonKey(name: 'docs')
  List<Docs> docs;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'limit')
  int limit;

  @JsonKey(name: 'page')
  int page;

  @JsonKey(name: 'pages')
  int pages;

  Eps(
    this.docs,
    this.total,
    this.limit,
    this.page,
    this.pages,
  );

  factory Eps.fromJson(Map<String, dynamic> srcJson) => _$EpsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$EpsToJson(this);
}

@JsonSerializable()
class Docs extends Object {
  @JsonKey(name: '_id')
  String sid;

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'order')
  int order;

  @JsonKey(name: 'updated_at')
  String updatedAt;

  @JsonKey(name: 'id')
  String id;

  Docs(
    this.sid,
    this.title,
    this.order,
    this.updatedAt,
    this.id,
  );

  factory Docs.fromJson(Map<String, dynamic> srcJson) =>
      _$DocsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DocsToJson(this);
}
