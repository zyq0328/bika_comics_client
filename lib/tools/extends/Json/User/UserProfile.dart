class UserProfile {
  String message;
  Data data;
  String code;

  UserProfile({this.message, this.data, this.code});

  UserProfile.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  User user;

  Data({this.user});

  Data.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  List<String> characters;
  String createdAt;
  bool verified;
  String sId;
  double exp;
  String title;
  int level;
  String birthday;
  String character;
  String email;
  String slogan;
  String gender;
  String name;

  User(
      {this.characters,
      this.createdAt,
      this.verified,
      this.sId,
      this.exp,
      this.title,
      this.level,
      this.birthday,
      this.character,
      this.email,
      this.slogan,
      this.gender,
      this.name});

  User.fromJson(Map<String, dynamic> json) {
    characters = json['characters'].cast<String>();
    createdAt = json['created_at'];
    verified = json['verified'];
    sId = json['_id'];
    exp = json['exp'];
    title = json['title'];
    level = json['level'];
    birthday = json['birthday'];
    character = json['character'];
    email = json['email'];
    slogan = json['slogan'];
    gender = json['gender'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['characters'] = this.characters;
    data['created_at'] = this.createdAt;
    data['verified'] = this.verified;
    data['_id'] = this.sId;
    data['exp'] = this.exp;
    data['title'] = this.title;
    data['level'] = this.level;
    data['birthday'] = this.birthday;
    data['character'] = this.character;
    data['email'] = this.email;
    data['slogan'] = this.slogan;
    data['gender'] = this.gender;
    data['name'] = this.name;
    return data;
  }
}
