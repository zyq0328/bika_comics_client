/*
 * @Date: 2020-02-29 18:57:17
 * @名称: 用户登录 json处理
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-07 19:35:26
 * @FilePath: /bika_comics_client/lib/tools/extends/Json/User/UserLoginJsonAnalysis.dart
 */
class UserLoginJsonAnalysis {
  String _message;
  Data _data;
  String _code;
  String _detail;
  String _error;

  UserLoginJsonAnalysis(
      {String message, Data data, String code, String detail, String error}) {
    this._message = message;
    this._data = data;
    this._code = code;
    this._detail = detail;
    this._error = error;
  }

  String get message => _message;
  set message(String message) => _message = message;
  Data get data => _data;
  set data(Data data) => _data = data;
  String get code => _code;
  set code(String code) => _code = code;
  String get detail => _detail;
  set detail(String detail) => _detail = detail;
  String get error => _error;
  set error(String error) => _error = error;

  UserLoginJsonAnalysis.fromJson(Map<String, dynamic> json) {
    _message = json['message'];
    _data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    _code = json['code'];
    _detail = json['detail'];
    _error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    data['code'] = this._code;
    data['detail'] = this._detail;
    data['error'] = this._error;
    return data;
  }
}

class Data {
  String _token;

  Data({String token}) {
    this._token = token;
  }

  String get token => _token;
  set token(String token) => _token = token;

  Data.fromJson(Map<String, dynamic> json) {
    _token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this._token;
    return data;
  }
}
