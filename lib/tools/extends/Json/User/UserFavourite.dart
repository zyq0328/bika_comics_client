class UserFavourite {
  String code;
  String message;
  Data data;

  UserFavourite({this.code, this.message, this.data});

  UserFavourite.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Comics comics;

  Data({this.comics});

  Data.fromJson(Map<String, dynamic> json) {
    comics =
        json['comics'] != null ? new Comics.fromJson(json['comics']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comics != null) {
      data['comics'] = this.comics.toJson();
    }
    return data;
  }
}

class Comics {
  List<Docs> docs;
  int total;
  int limit;
  int page;
  int pages;

  Comics({this.docs, this.total, this.limit, this.page, this.pages});

  Comics.fromJson(Map<String, dynamic> json) {
    if (json['docs'] != null) {
      docs = new List<Docs>();
      json['docs'].forEach((v) {
        docs.add(new Docs.fromJson(v));
      });
    }
    total = json['total'];
    limit = json['limit'];
    page = json['page'];
    pages = json['pages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.docs != null) {
      data['docs'] = this.docs.map((v) => v.toJson()).toList();
    }
    data['total'] = this.total;
    data['limit'] = this.limit;
    data['page'] = this.page;
    data['pages'] = this.pages;
    return data;
  }
}

class Docs {
  String sId;
  String title;
  String author;
  String pagesCount;
  String epsCount;
  bool finished;
  List<String> categories;
  Thumb thumb;
  String id;
  int likesCount;

  Docs(
      {this.sId,
      this.title,
      this.author,
      this.pagesCount,
      this.epsCount,
      this.finished,
      this.categories,
      this.thumb,
      this.id,
      this.likesCount});

  Docs.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    author = json['author'];
    pagesCount = json['pagesCount'];
    epsCount = json['epsCount'];
    finished = json['finished'];
    categories = json['categories'].cast<String>();
    thumb = json['thumb'] != null ? new Thumb.fromJson(json['thumb']) : null;
    id = json['id'];
    likesCount = json['likesCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['author'] = this.author;
    data['pagesCount'] = this.pagesCount;
    data['epsCount'] = this.epsCount;
    data['finished'] = this.finished;
    data['categories'] = this.categories;
    if (this.thumb != null) {
      data['thumb'] = this.thumb.toJson();
    }
    data['id'] = this.id;
    data['likesCount'] = this.likesCount;
    return data;
  }
}

class Thumb {
  String originalName;
  String path;
  String fileServer;

  Thumb({this.originalName, this.path, this.fileServer});

  Thumb.fromJson(Map<String, dynamic> json) {
    originalName = json['originalName'];
    path = json['path'];
    fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['originalName'] = this.originalName;
    data['path'] = this.path;
    data['fileServer'] = this.fileServer;
    return data;
  }
}
