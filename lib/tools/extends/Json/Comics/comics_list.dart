import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsList {
  String message;
  Data data;
  String code;

  ComicsList({this.message, this.data, this.code});

  ComicsList.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['code'] = this.code;
    return data;
  }
}

class Data {
  Comics comics;

  Data({this.comics});

  Data.fromJson(Map<String, dynamic> json) {
    comics =
        json['comics'] != null ? new Comics.fromJson(json['comics']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.comics != null) {
      data['comics'] = this.comics.toJson();
    }
    return data;
  }
}

class Comics {
  int page;
  int pages;
  List<Docs> docs;
  int total;
  int limit;

  Comics({this.page, this.pages, this.docs, this.total, this.limit});

  Comics.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    pages = json['pages'];
    if (json['docs'] != null) {
      docs = new List<Docs>();
      json['docs'].forEach((v) {
        docs.add(new Docs.fromJson(v));
      });
    }
    total = json['total'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['pages'] = this.pages;
    if (this.docs != null) {
      data['docs'] = this.docs.map((v) => v.toJson()).toList();
    }
    data['total'] = this.total;
    data['limit'] = this.limit;
    return data;
  }
}

class Docs {
  int totalLikes;
  List<String> categories;
  Thumb thumb;
  String id;
  int likesCount;
  List<Author> author;
  int pagesCount;
  String sId;
  int totalViews;
  String title;
  int epsCount;
  bool finished;
  List<ChineseTeam> chineseTeam;

  Docs(
      {this.totalLikes,
      this.categories,
      this.thumb,
      this.id,
      this.likesCount,
      this.author,
      this.pagesCount,
      this.sId,
      this.totalViews,
      this.title,
      this.epsCount,
      this.finished,
      this.chineseTeam});

  Docs.fromJson(Map<String, dynamic> json) {
    totalLikes = json['totalLikes'];
    categories = json['categories'].cast<String>();
    thumb = json['thumb'] != null ? new Thumb.fromJson(json['thumb']) : null;
    id = json['id'];
    likesCount = json['likesCount'];
    if (json['author'] != null) {
      author = new List<Author>();
      json['author'].forEach((v) {
        author.add(new Author.fromJson(v));
      });
    }
    pagesCount = json['pagesCount'];
    sId = json['_id'];
    totalViews = json['totalViews'];
    title = json['title'];
    epsCount = json['epsCount'];
    finished = json['finished'];
    if (json['chineseTeam'] != null) {
      chineseTeam = new List<ChineseTeam>();
      json['chineseTeam'].forEach((v) {
        chineseTeam.add(new ChineseTeam.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalLikes'] = this.totalLikes;
    data['categories'] = this.categories;
    if (this.thumb != null) {
      data['thumb'] = this.thumb.toJson();
    }
    data['id'] = this.id;
    data['likesCount'] = this.likesCount;
    if (this.author != null) {
      data['author'] = this.author.map((v) => v.toJson()).toList();
    }
    data['pagesCount'] = this.pagesCount;
    data['_id'] = this.sId;
    data['totalViews'] = this.totalViews;
    data['title'] = this.title;
    data['epsCount'] = this.epsCount;
    data['finished'] = this.finished;
    if (this.chineseTeam != null) {
      data['chineseTeam'] = this.chineseTeam.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Thumb {
  String path;
  String originalName;
  String fileServer;

  Thumb({this.path, this.originalName, this.fileServer});

  Thumb.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    originalName = json['originalName'];
    fileServer = json['fileServer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this.path;
    data['originalName'] = this.originalName;
    data['fileServer'] = this.fileServer;
    return data;
  }
}
