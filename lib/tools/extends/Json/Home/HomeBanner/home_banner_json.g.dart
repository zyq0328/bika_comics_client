// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_banner_json.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeBannerJson _$HomeBannerJsonFromJson(Map<String, dynamic> json) {
  return HomeBannerJson(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$HomeBannerJsonToJson(HomeBannerJson instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    (json['banners'] as List)
        ?.map((e) =>
            e == null ? null : Banners.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'banners': instance.banners,
    };

Banners _$BannersFromJson(Map<String, dynamic> json) {
  return Banners(
    json['_id'] as String,
    json['title'] as String,
    json['shortDescription'] as String,
    json['type'] as String,
    json['link'] as String,
    json['thumb'] == null
        ? null
        : Thumb.fromJson(json['thumb'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$BannersToJson(Banners instance) => <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'shortDescription': instance.shortDescription,
      'type': instance.type,
      'link': instance.link,
      'thumb': instance.thumb,
    };

Thumb _$ThumbFromJson(Map<String, dynamic> json) {
  return Thumb(
    json['fileServer'] as String,
    json['path'] as String,
    json['originalName'] as String,
  );
}

Map<String, dynamic> _$ThumbToJson(Thumb instance) => <String, dynamic>{
      'fileServer': instance.fileServer,
      'path': instance.path,
      'originalName': instance.originalName,
    };
