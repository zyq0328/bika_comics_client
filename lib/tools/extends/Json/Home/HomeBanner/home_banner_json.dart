/*
 * @Date: 2019-11-30 16:42:42
 * @名称: 主页的banner
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 22:36:08
 */
import 'package:json_annotation/json_annotation.dart';

part 'home_banner_json.g.dart';

@JsonSerializable()
class HomeBannerJson extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  HomeBannerJson(
    this.code,
    this.message,
    this.data,
  );

  factory HomeBannerJson.fromJson(Map<String, dynamic> srcJson) =>
      _$HomeBannerJsonFromJson(srcJson);

  Map<String, dynamic> toJson() => _$HomeBannerJsonToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'banners')
  List<Banners> banners;

  Data(
    this.banners,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Banners extends Object {
  @JsonKey(name: '_id')
  String id;

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'shortDescription')
  String shortDescription;

  @JsonKey(name: 'type')
  String type;

  @JsonKey(name: 'link')
  String link;

  @JsonKey(name: 'thumb')
  Thumb thumb;

  Banners(
    this.id,
    this.title,
    this.shortDescription,
    this.type,
    this.link,
    this.thumb,
  );

  factory Banners.fromJson(Map<String, dynamic> srcJson) =>
      _$BannersFromJson(srcJson);

  Map<String, dynamic> toJson() => _$BannersToJson(this);
}

@JsonSerializable()
class Thumb extends Object {
  @JsonKey(name: 'fileServer')
  String fileServer;

  @JsonKey(name: 'path')
  String path;

  @JsonKey(name: 'originalName')
  String originalName;

  Thumb(
    this.fileServer,
    this.path,
    this.originalName,
  );

  factory Thumb.fromJson(Map<String, dynamic> srcJson) =>
      _$ThumbFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ThumbToJson(this);
}
