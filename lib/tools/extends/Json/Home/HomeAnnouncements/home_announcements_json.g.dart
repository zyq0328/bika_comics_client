// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_announcements_json.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeAnnouncementsJson _$HomeAnnouncementsJsonFromJson(
    Map<String, dynamic> json) {
  return HomeAnnouncementsJson(
    json['code'] as String,
    json['message'] as String,
    json['data'] == null
        ? null
        : Data.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$HomeAnnouncementsJsonToJson(
        HomeAnnouncementsJson instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    json['announcements'] == null
        ? null
        : Announcements.fromJson(json['announcements'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'announcements': instance.announcements,
    };

Announcements _$AnnouncementsFromJson(Map<String, dynamic> json) {
  return Announcements(
    (json['docs'] as List)
        ?.map(
            (e) => e == null ? null : Docs.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['total'] as int,
    json['limit'] as int,
    json['page'] as String,
    json['pages'] as int,
  );
}

Map<String, dynamic> _$AnnouncementsToJson(Announcements instance) =>
    <String, dynamic>{
      'docs': instance.docs,
      'total': instance.total,
      'limit': instance.limit,
      'page': instance.page,
      'pages': instance.pages,
    };

Docs _$DocsFromJson(Map<String, dynamic> json) {
  return Docs(
    json['_id'] as String,
    json['title'] as String,
    json['content'] as String,
    json['created_at'] as String,
    json['thumb'] == null
        ? null
        : Thumb.fromJson(json['thumb'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DocsToJson(Docs instance) => <String, dynamic>{
      '_id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'created_at': instance.createdAt,
      'thumb': instance.thumb,
    };

Thumb _$ThumbFromJson(Map<String, dynamic> json) {
  return Thumb(
    json['originalName'] as String,
    json['path'] as String,
    json['fileServer'] as String,
  );
}

Map<String, dynamic> _$ThumbToJson(Thumb instance) => <String, dynamic>{
      'originalName': instance.originalName,
      'path': instance.path,
      'fileServer': instance.fileServer,
    };
