import 'package:json_annotation/json_annotation.dart';

part 'home_announcements_json.g.dart';

@JsonSerializable()
class HomeAnnouncementsJson extends Object {
  @JsonKey(name: 'code')
  String code;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'data')
  Data data;

  HomeAnnouncementsJson(
    this.code,
    this.message,
    this.data,
  );

  factory HomeAnnouncementsJson.fromJson(Map<String, dynamic> srcJson) =>
      _$HomeAnnouncementsJsonFromJson(srcJson);

  Map<String, dynamic> toJson() => _$HomeAnnouncementsJsonToJson(this);
}

@JsonSerializable()
class Data extends Object {
  @JsonKey(name: 'announcements')
  Announcements announcements;

  Data(
    this.announcements,
  );

  factory Data.fromJson(Map<String, dynamic> srcJson) =>
      _$DataFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class Announcements extends Object {
  @JsonKey(name: 'docs')
  List<Docs> docs;

  @JsonKey(name: 'total')
  int total;

  @JsonKey(name: 'limit')
  int limit;

  @JsonKey(name: 'page')
  String page;

  @JsonKey(name: 'pages')
  int pages;

  Announcements(
    this.docs,
    this.total,
    this.limit,
    this.page,
    this.pages,
  );

  factory Announcements.fromJson(Map<String, dynamic> srcJson) =>
      _$AnnouncementsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$AnnouncementsToJson(this);
}

@JsonSerializable()
class Docs extends Object {
  @JsonKey(name: '_id')
  String id;

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'content')
  String content;

  @JsonKey(name: 'created_at')
  String createdAt;

  @JsonKey(name: 'thumb')
  Thumb thumb;

  Docs(
    this.id,
    this.title,
    this.content,
    this.createdAt,
    this.thumb,
  );

  factory Docs.fromJson(Map<String, dynamic> srcJson) =>
      _$DocsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DocsToJson(this);
}

@JsonSerializable()
class Thumb extends Object {
  @JsonKey(name: 'originalName')
  String originalName;

  @JsonKey(name: 'path')
  String path;

  @JsonKey(name: 'fileServer')
  String fileServer;

  Thumb(
    this.originalName,
    this.path,
    this.fileServer,
  );

  factory Thumb.fromJson(Map<String, dynamic> srcJson) =>
      _$ThumbFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ThumbToJson(this);
}
