/*
 * @Date: 2020-02-10 20:36:56
 * @名称: 数据库 控制类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 16:42:25
 * @FilePath: /bica_acg/lib/DataBaseFunction.dart
 */
import 'package:pica_acg/GlobalSettings.dart';
import 'package:sqflite/sqflite.dart';

Future<Database> dataBaseLink() async {
  //链接表
  var databasesPath = await getDatabasesPath();
  String path = databasesPath + '/' + $global_DataBaseName + '.db';
  return openDatabase(path, version: 1,
      onCreate: (Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
      'CREATE TABLE DownloadList (id INTEGER PRIMARY KEY, BookId TEXT, Title TEXT, NetWorkCoverUrl TEXT, LocalCoverUrl TEXT)',
      //!创建下载表 里面记录 下载信息 分别有 id  自增 书籍id 文本 标题 文本 分卷id
      /** DownloadList
       * BookId [TEXT] 书籍id
       * Title [TEXT] 标题
       * NetWorkCoverUrl [TEXT] 服务器图片路径
       * LocalCoverUrl [TEXT] 服务器封面下载到本地后保存的名字
       */
    );
    await db.execute(
      'CREATE TABLE BookDownloadChapterList (id INTEGER PRIMARY KEY, BookId TEXT, Title TEXT, ChapterId TEXT, Download bool, Success bool, Progress float)',
      //!创建下载表的分卷表 里面记录 下载信息 分别有 id  自增 书籍id 文本 标题 文本 分卷id 下载 是否下载成功 是否 Progress进度 文本 0.0
      /** DownloadList
       * BookId [TEXT] 书籍id
       * Title [TEXT] 标题
       * ChapterId [TEXT] 分卷id
       * Download [bool] 下载
       * Success [bool] 是否下载成功
       * Progress [float] 进度
       */
    );
    await db.execute(
      'CREATE TABLE BookDownloadChapterInfo (id INTEGER PRIMARY KEY, BookId TEXT, ChapterId TEXT, FileName TEXT, FileSrc TEXT)',
      //!创建书籍分卷信息表 里面记录 下载的书籍的id  分卷的id 文件名 文件路径
      /** BookDownloadChapterInfo
       * BookId [TEXT] 下载的书籍的id
       * ChapterId [TEXT] 分卷的id
       * FileName [TEXT] 文件名
       * FileSrc [TEXT] 文件路径
       */
    );
    await db.execute(
      'CREATE TABLE BookDownloadChapterImagesList (id INTEGER PRIMARY KEY, BookId TEXT, ChapterId TEXT, ImagesName TEXT, SeverAddress TEXT, PictureAliases TEXT, AllImages INT, ThisIsImages INT, DownloadSuccess Bool, IsPath bool)',
      //!创建书籍分卷图片列表 - 表 里面记录 下载的书籍的id  分卷的id 图片名称 图片服务器地址 图片别名 这一篇总共多少张图片 这是第几张 下载是否成功 所有的
      /** BookDownloadChapterImagesList
       *BookId [TEXT] 下载的书籍的id
       *ChapterId [TEXT] 分卷的id
       *ImagesName [TEXT] 图片名称
       *SeverAddress [TEXT] 图片服务器地址
       *PictureAliases [TEXT] 图片别名
       *AllImages [INT] 这一篇总共多少张图片
       *ThisIsImages [INT] 这是第几张
       *DownloadSuccess [Bool] 下载是否成功
       */
    );
    await db.execute(
      'CREATE TABLE UserInfo (id INTEGER PRIMARY KEY, UserName TEXT, PassWord TEXT, ToKen TEXT)',
      //!创建用户信息 - 表 里面记录 用户名 密码 ToKen
      /** UserInfo
       * UserName [TEXT] 用户名
       * PassWord [TEXT] 密码
       * ToKen [TEXT] ToKen
       */
    );
    await db.execute(
      'CREATE TABLE UserSetting (id INTEGER PRIMARY KEY, ScreenOrientation bool, PageTurningMode bool, PageTurningInterval float, NightMode bool, AddDownloadTips bool, OpenAppToContinueDownloading bool)',
      //!创建用户设置表 - 表 里面记录 屏幕方向 (真[横屏]假[竖屏]) 翻页模式 (真[上下]假[左右]) 自动翻页时间 夜间模式 (真假) 添加下载提示 (真假) 打开app继续下载 (真假)
      /** UserSetting
       * ScreenOrientation [bool] 屏幕方向
       * PageTurningMode [bool] 翻页模式
       * PageTurningInterval [float] 自动翻页时间
       * NightMode [bool] 夜间模式
       * AddDownloadTips [bool] 添加下载提示
       * OpenAppToContinueDownloading [bool] 打开app继续下载
       */
    );
  });
}

dataBaseTableDoesItExist($tabName) async {
  //表是否存在
  var dataBaseTableDoesItExist = dataBaseLink().then((result) async {
    var res = await result.rawQuery(
        "select * from Sqlite_master where type = 'table' and name = '${$tabName}'");
    // return res != null && res.length > 0;
    return (res.length > 0) ? true : false;
  });
  return dataBaseTableDoesItExist;
}

dataBaseInsert($tableName, Map<String, dynamic> data) {
  var dataBaseInsert = dataBaseLink().then(
    (callBack) => {
      callBack.insert($tableName, data),
    },
  );
  return dataBaseInsert;
}

dataBaseDelete($tableName, String where, List<dynamic> whereArgs) {
  //! where字段 可以用 比如id = ？( ？等于匹配符号)
  //! 多个条件 就是 条件一 = ？ and 条件二 = ？
  var dataBaseInsert = dataBaseLink().then(
    (callBack) => {
      callBack.delete(
        $tableName,
        where: where,
        whereArgs: whereArgs,
      ),
    },
  );
  return dataBaseInsert;
}

Future<int> dataBaseUpdate(
    $tableName, dynamic data, String where, List<dynamic> whereArgs) {
  //! where字段 可以用 比如id = ？( ？等于匹配符号)
  //! 多个条件 就是 条件一 = ？ and 条件二 = ？
  return dataBaseLink().then(
    (callBack) {
      return callBack
          .update(
        $tableName,
        data,
        where: where,
        whereArgs: whereArgs,
      )
          .then((value) {
        return value;
      });
    },
  );
}

Future<List> dataBaseSelect(
  $tableName,
  String where,
  List<dynamic> whereArgs, {
  bool distinct,
  List<String> columns,
  String groupBy,
  String having,
  String orderBy,
  int limit,
  int offset,
}) async {
  //! where字段 可以用 比如id = ？( ？等于匹配符号)
  //! 多个条件 就是 条件一 = ？ and 条件二 = ？
  return dataBaseLink().then(
    (callBack) {
      return callBack
          .query(
        $tableName,
        where: where,
        whereArgs: whereArgs,
        distinct: distinct,
        columns: columns,
        groupBy: groupBy,
        having: having,
        orderBy: orderBy,
        limit: limit,
        offset: offset,
      )
          .then(
        (value) {
          return value;
        },
      );
    },
  );
}
