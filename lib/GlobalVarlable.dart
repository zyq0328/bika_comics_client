/*
 * @Date: 2020-02-17 18:53:52
 * @名称: 全局变量
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:24:52
 * @FilePath: /bica_acg/lib/GlobalVarlable.dart
 */
import 'package:pica_acg/tools/Db/Json/ComicsInfoDownload/DataBaseUserSetting.dart';

Map $globalVarlableBookCover = {
  "Src": "", //@a 图片超链接
  "SeverName": "", //@d 保存文件名
}; //漫画封面 //@a 此选项禁止初始化数据
bool $globalVarlableDownloadState = false; //下载状态
Map $globalVarlableDownloadList = Map(); //下载进度里面的内容为 其中取值为 0.0-1.0
Map<dynamic, dynamic> downList = Map(); //下载列表
double brightness = 0; //屏幕亮度
DataBaseUserSetting setting = DataBaseUserSetting.fromJson({
  "ScreenOrientation": 0,
  "PageTurningMode": 0,
  "PageTurningInterval": 0.5,
  "NightMode": 0,
  "AddDownloadTips": 1,
  "OpenAppToContinueDownloading": 0
});
var dataSpace = ""; //存储数据空间
