/*
 * @Date: 2020-04-01 23:02:24
 * @名称: 中文语言
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-22 22:17:19
 * @FilePath: /bica_acg/lib/LanguAge/zh_cn.dart
 */
class ZhCn {
  final $languAgeOrdinary = {
    "basic": {
      "return": "<<返回",
      "tips_expButtonErrorName": "查看评论",
      "tips_Title": "您已看完本章节", //阅读完成章节提示
      "tips_Subheading": "是否查看评论或进行讨论", //阅读完成章节提示
    }
  };

  final $languAgeReadingComicsReadingComicsBodyController = {
    "readOver": {
      "tips_expButtonSuccessName": "返回",
      "tips_expButtonErrorName": "查看评论",
      "tips_Title": "您已看完本章节", //阅读完成章节提示
      "tips_Subheading": "是否查看评论或进行讨论", //阅读完成章节提示
    }
  };

  final $languAgeLoginController = {
    "userTips": {
      "A-00001": "请填写用户名或密码",
    }
  };
  final $languAgeSearchController = {
    "box": {
      "title": "搜索:",
      "skip_page": "跳转",
    }
  };
}
