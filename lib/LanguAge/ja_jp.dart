/*
 * @Date: 2020-04-02 12:33:00
 * @名称: 日本語の言語
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-22 23:28:06
 * @FilePath: /pica_acg/lib/LanguAge/ja_jp.dart
 */
class JaJp {
  final $languAgeOrdinary = {
    "basic": {
      "return": "<<戻る",
      "tips_expButtonErrorName": "コメントを見る",
      "tips_Title": "この章はもう読み終わりました。", //阅读完成章节提示
      "tips_Subheading": "コメントを表示するかどうか、または検討します。", //阅读完成章节提示
    }
  };

  final $languAgeReadingComicsReadingComicsBodyController = {
    "readOver": {
      "tips_expButtonSuccessName": "戻る",
      "tips_expButtonErrorName": "コメントを見る",
      "tips_Title": "この章はもう読み終わりました。", //阅读完成章节提示
      "tips_Subheading": "コメントを表示するかどうか、または検討します。", //阅读完成章节提示
    }
  };
  final $languAgeLoginController = {
    "readOver": {
      "A-00001": "ユーザー名またはパスワードを入力してください", //请填写用户名或密码
    }
  };
  final $languAgeSearchController = {
    "box": {
      "title": "検索:",
      "skip_page": "ジャンプ",
    }
  };
}
