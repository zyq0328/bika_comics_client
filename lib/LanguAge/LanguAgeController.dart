import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/LanguAge/ja_jp.dart';
import 'package:pica_acg/LanguAge/zh_cn.dart';

languAre() {
  var $languAre;
  switch (languAge) {
    case "zh_cn":
      $languAre = ZhCn();
      break;
    case "ja_jp":
      $languAre = JaJp();
      break;
    default:
  }
  return $languAre;
  // $languAre()
}
