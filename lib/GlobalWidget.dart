/*
 * @Date: 2019-11-27 18:16:36
 * @名称: 全局组件 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:18:47
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:extended_image/extended_image.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/GlobalVarlable.dart';

class GlobalTips extends StatefulWidget {
  /* GlobalTips
 * 
 * @Date: 2019-12-23 18:39:15
 * 
 * @函数名: 提示框
 * 
 * @信息: 弹出一个提示
 * 
 * @body {String}　文本内容不能为空 如果  expand　= true 请随便填写
 * 
 * @height {Double} 提示框高度
 * 
 * @islogo {Bool} 是否显示图标 
 * 
 * @expand {Bool} 是否为高级提示 
 * 
 * @expandBody {Widget} 组件 
 * 
 * @return: Widget
 * 
 * 示例代码 - 普通提示
 * 
   ```dart
   /// Navigator.of(context).push(
   ///  PageRouteBuilder(
   ///    pageBuilder: (context, _, __) => GlobalTips(
   ///        body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
   ///        height: ScreenUtil().setHeight(900),
   ///        imageAsset:'图标资源'
   ///      ),
   ///    opaque: false,
   ///  ),
   /// );
  ```
  自定义提示

  ```dart

   /// Navigator.of(context).push(
   ///  PageRouteBuilder(
   ///    pageBuilder: (context, _, __) => GlobalTips(
   ///      body: 'a',
   ///      height: 1000,
   ///      islogo: false,
   ///      expand: true,
   ///      expandBody: Container(
   ///        margin: EdgeInsets.only(
   ///          top: ScreenUtil().setHeight(119),
   ///        ),
   ///        height: ScreenUtil().setHeight(900),
   ///        alignment: Alignment.topCenter,
   ///        child: ClipRRect(
   ///          borderRadius: new BorderRadius.vertical(
   ///            top: Radius.elliptical(20, 20),
   ///          ),
   ///          child: SingleChildScrollView(
   ///            child: Column(
   ///              children: [
   ///                Image(
   ///                  image: AssetImage(
   ///                    'images/Other/Other1.jpeg',
   ///                  ),
   ///                ),
   ///                Container(
   ///                  child: Text(
   ///                    'Waifu新面孔！',
   ///                    style: TextStyle(
   ///                      fontSize: 30.0,
   ///                      fontWeight: FontWeight.w700,
   ///                      color: Color(0xFFed97b7),
   ///                      decoration: TextDecoration.none,
   ///                    ),
   ///                  ),
   ///                  margin: EdgeInsets.only(
   ///                    top: ScreenUtil().setHeight(5),
   ///                  ),
   ///                  alignment: Alignment.center,
   ///                ),
   ///                SizedBox(
   ///                  width: ScreenUtil().setWidth(470),
   ///                  child: Text(
   ///                    '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
   ///                    softWrap: true,
   ///                    style: TextStyle(
   ///                      // fontWeight: FontWeight.w700,
   ///                      // fontSize: 14.0,
   ///                      color: Color.fromRGBO(79, 79, 79, 1),
   ///                      decoration: TextDecoration.none,
   ///                    ),
   ///                  ),
   ///                ),
   ///              ],
   ///            ),
   ///          ),
   ///        ),
   ///      ),
   ///    ),
   ///    opaque: false,
   ///  ),
   /// );
  ```

 **/
  GlobalTips({
    Key key,
    this.body = ' ',
    this.height,
    this.islogo = true,
    this.expand = false,
    this.expandBody,
    this.title = '',
    this.imageAsset = 'images/Tisp/UnknownError.png',
    this.context,
  }) : super(key: key);
  final String body;
  final String title;
  final String imageAsset;
  final double height;
  final bool islogo;
  final bool expand;
  final Widget expandBody;
  final BuildContext context;
  @override
  _GlobalTipsState createState() => _GlobalTipsState();
}

class _GlobalTipsState extends State<GlobalTips> {
  @override
  Widget build(BuildContext context) {
    var logoTop = 0;
    if (widget.islogo) {
      logoTop = 2;
    } else {
      logoTop = 0;
    }

    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Stack(
      children: <Widget>[
        Opacity(
          //遮罩层
          opacity: 0.5,
          child: Container(
            color: Color(0xff000000), // 底色
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Container(
            height: ScreenUtil().setHeight(widget.height),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    //文本面板
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Visibility(
                            visible: widget.title.length == 0 ? false : true,
                            child: Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(20),
                              ),
                              child: Text(
                                widget.title,
                                style: TextStyle(
                                  fontSize: 19 /* ScreenUtil().setWidth(30) */,
                                  color: Color(0xFFFF00F2),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                            ),
                          ),
                        ),

                        Opacity(
                          opacity: widget.islogo ? 1.0 : 0.0,
                          child: Container(
                            //表情
                            alignment: Alignment.center,
                            height: widget.height / 3,
                            child: Container(
                              child: Image.asset(
                                widget.imageAsset,
                                height: ScreenUtil().setHeight(450),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: ScreenUtil().setWidth(550),
                          child: Stack(
                            children: <Widget>[
                              Opacity(
                                opacity: widget.expand ? 1.0 : 0.0,
                                child: Container(
                                  //文本
                                  alignment: Alignment.center,
                                  child: widget.expandBody,
                                ),
                              ),
                              // 默认文本
                              Opacity(
                                opacity: widget.expand ? 0.0 : 1.0,
                                child: Container(
                                  //文���
                                  alignment: Alignment.center,
                                  child: Text(
                                    widget.body,
                                    // '哔咔哔咔被玩坏了!\n这肯定不是哔���的问题!\n绝对不是!',
                                    style: TextStyle(
                                      fontSize: /* 14 */ ScreenUtil()
                                          .setWidth(30),
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF000000),
                                      decoration: TextDecoration.none,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(top: ScreenUtil().setWidth(400)),
                        //   width: ScreenUtil().setWidth(550),
                        //   height: ScreenUtil().setHeight(2),
                        //   decoration: new BoxDecoration(
                        //     border: new Border.all(color: Color(0xFFFF0000), width: 0.5), // 边色与边宽度
                        //   ),
                        // ),

                        Container(
                          child: Container(
                            child: new FlatButton(
                              child: new Text(
                                '确定',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19.0,
                                  color: Color.fromRGBO(237, 153, 184, 1),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context); //关闭对话框
                                if (widget.context != null) {
                                  Navigator.pop(widget.context); //关闭对话框
                                }
                              },
                            ),
                            height: ScreenUtil().setHeight(80),
                            width: ScreenUtil().setWidth(550),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: new Border(
                                top: Divider.createBorderSide(
                                  context,
                                  color: Color.fromRGBO(229, 229, 231, 1),
                                  width: 1,
                                ),
                              ), // ��色与边宽度
                            ),
                          ),

                          // margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        ),
                      ],
                    ),
                    width: ScreenUtil().setWidth(550),
                    height: ScreenUtil().setHeight(widget.height),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                          color: Color(0xFFf092b5), width: 1.0), // 边色与边宽度
                      color: Color.fromRGBO(255, 255, 255, 50), // 底色
                      //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                      borderRadius: new BorderRadius.vertical(
                          top: Radius.elliptical(20, 20),
                          bottom: Radius.elliptical(20, 20)), // 也可控件一边圆角大小
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class GlobalTipsExtend extends StatefulWidget {
  /* GlobalTipsExtend
 * 
 * @Date: 2019-12-23 18:39:15
 * 
 * @函数名: 提示框
 * 
 * @信息: 弹出一个提示
 * 
 * @body {String}　文本内容不能为空 如果  expand　= true 请随便填写
 * 
 * @height {Double} 提示框高度
 * 
 * @logo {Map} 是否显示图标 
 * 
 *     {
 *      'islogo':true
 *      'logoSrc':''  
 *     }
 * 
 * @expand {Bool} 是否为高级提示 
 * 
 * @expandBody {Widget} 组件 
 * 
 * @expButton {Map} 一个json 示例代码
 * 
 *  ```json
 *    {
 * 
 *      'isExpButton': true, //!是否开启扩展
 * 
 *      'expButtonSuccessName': '不要再说了',//!按钮 成功的  标题
 * 
 *      'expButtonSuccessOntab': () => {  //!点击事件
 * 
 *            print('aa'), //!代码
 * 
 *          },
 *      'expButtonErrorName': '确定',//！按钮失败的标题
 *
 *       'expButtonErrorOntab': () => {//!点击事件
 *
 *             Navigator.pop(context), //!代码
 *
 *           },
 * 
 *    },
 * ```
 * 
 * @return: Widget
 * 
 * 示例代码 - 普通提示
 * 
   ```dart
   /// Navigator.of(context).push(
   ///  PageRouteBuilder(
   ///    pageBuilder: (context, _, __) => GlobalTipsExtend(
   ///        body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
   ///        height: ScreenUtil().setHeight(900),
   ///      ),
   ///    opaque: false,
   ///  ),
   /// );
  ```
  自定义提示

  ```dart

   /// Navigator.of(context).push(
   ///  PageRouteBuilder(
   ///    pageBuilder: (context, _, __) => GlobalTipsExtend(
   ///      body: 'a',
   ///      height: 1000,
   ///      islogo: false,
   ///      expand: true,
   ///      
   ///      expButton: {
   ///      "isExpButton": false,
   ///      "expButtonSuccessName": 'aaaa',
   ///      "expButtonSuccessOntab": () {
   ///        print('object');
   ///      },
   ///      "expButtonErrorName": 'aaaa',
   ///      "expButtonErrorOntab": () {
   ///        print('object1');
   ///      },
   ///    },
   ///      expandBody: Container(
   ///        margin: EdgeInsets.only(
   ///          top: ScreenUtil().setHeight(119),
   ///        ),
   ///        height: ScreenUtil().setHeight(900),
   ///        alignment: Alignment.topCenter,
   ///        child: ClipRRect(
   ///          borderRadius: new BorderRadius.vertical(
   ///            top: Radius.elliptical(20, 20),
   ///          ),
   ///          child: SingleChildScrollView(
   ///            child: Column(
   ///              children: [
   ///                Image(
   ///                  image: AssetImage(
   ///                    'images/Other/Other1.jpeg',
   ///                  ),
   ///                ),
   ///                Container(
   ///                  child: Text(
   ///                    'Waifu新面孔！',
   ///                    style: TextStyle(
   ///                      fontSize: 30.0,
   ///                      fontWeight: FontWeight.w700,
   ///                      color: Color(0xFFed97b7),
   ///                      decoration: TextDecoration.none,
   ///                    ),
   ///                  ),
   ///                  margin: EdgeInsets.only(
   ///                    top: ScreenUtil().setHeight(5),
   ///                  ),
   ///                  alignment: Alignment.center,
   ///                ),
   ///                SizedBox(
   ///                  width: ScreenUtil().setWidth(470),
   ///                  child: Text(
   ///                    '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
   ///                    softWrap: true,
   ///                    style: TextStyle(
   ///                      // fontWeight: FontWeight.w700,
   ///                      // fontSize: 14.0,
   ///                      color: Color.fromRGBO(79, 79, 79, 1),
   ///                      decoration: TextDecoration.none,
   ///                    ),
   ///                  ),
   ///                ),
   ///              ],
   ///            ),
   ///          ),
   ///        ),
   ///      ),
   ///    ),
   ///    opaque: false,
   ///  ),
   /// );
  ```

 **/
  GlobalTipsExtend({
    Key key,
    this.body = ' ',
    this.height,
    this.logo,
    this.expand = false,
    this.expandBody,
    this.title = '',
    this.expButton,
  }) : super(key: key);
  final String body;
  final String title;
  final double height;
  final Map logo;
  final bool expand;
  final Widget expandBody;
  final Map expButton;
  @override
  _GlobalTipsExtendState createState() => _GlobalTipsExtendState();
}

class _GlobalTipsExtendState extends State<GlobalTipsExtend> {
  @override
  Widget build(BuildContext context) {
    var logoTop = 0;
    var $isExpButton = 0.0;
    var $isButton = 1.0;
    if (widget.logo['isLogo'] == true) {
      logoTop = 2;
    } else {
      widget.logo["logoSrc"] = "images/Tisp/UnknownError.png";
    }
    if (widget.expButton['isExpButton'] != null &&
        widget.expButton['isExpButton'] != '' &&
        widget.expButton['isExpButton']) {
      $isExpButton = 1.0;
      $isButton = 0.0;
    }
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Stack(
      children: <Widget>[
        Opacity(
          //遮罩层
          opacity: 0.5,
          child: Container(
            color: Color(0xff000000), // 底色
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Container(
            height: ScreenUtil().setHeight(widget.height),
            decoration: new BoxDecoration(
              border: new Border.all(
                  color: Color(0xFFf092b5), width: 1.0), // 边色与边宽度
              color: Color(0xFFFFFFFF), // 底色
              //        borderRadius: new BorderRadius.circular((20.0)), // 圆���度
              borderRadius: new BorderRadius.vertical(
                  top: Radius.elliptical(20, 20),
                  bottom: Radius.elliptical(20, 20)), // 也可控件���边圆角大小
            ),
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(550),
            child: Column(
              children: <Widget>[
                Container(
                  child: Visibility(
                    visible: widget.title.length == 0 ? false : true,
                    child: Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(20),
                      ),
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          fontSize: 19 /* ScreenUtil().setWidth(30) */,
                          color: Color(0xFFFF00F2),
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Visibility(
                    visible: (widget.logo["isLogo"]) ? true : false,
                    child: Container(
                      //表情
                      // alignment: Alignment.center,
                      child: Container(
                        child: Image.asset(
                          widget.logo["logoSrc"],
                        ),
                      ),
                    ),
                  ),
                  height: widget.height / 3,
                ),
                Container(
                  width: ScreenUtil().setWidth(550),
                  child: Column(
                    children: <Widget>[
                      Visibility(
                        visible: widget.expand ? true : false,
                        child: Container(
                          width: ScreenUtil().setWidth(550),
                          //文本
                          margin: EdgeInsets.only(),
                          child: widget.expandBody,
                        ),
                      ),
                      // 默认文本
                      Visibility(
                        visible: widget.expand ? false : true,
                        child: Container(
                          //文本
                          margin: EdgeInsets.only(
                            right: ScreenUtil().setWidth(10),
                            left: ScreenUtil().setWidth(10),
                            // top: ScreenUtil().setHeight(50),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            (widget.body != null)
                                ? widget.body
                                : "咔哔咔被玩坏了!\n这肯定不是哔咔的问题!\n绝对不是!",
                            // '�����������������������������咔哔咔被玩坏了!\n这肯定不是哔咔的问题!\n绝对不是!',
                            style: TextStyle(
                              fontSize: 14 /* ScreenUtil().setWidth(30) */,
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF000000),
                              decoration: TextDecoration.none,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                //!按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮
                Container(
                  // alignment: EdgeInsets.,
                  child: Container(
                    child: Stack(
                      children: [
                        //!吧两个按钮放到一起重叠 如果条件为真 则 第一个 透明度为0
                        Opacity(
                          //!透明度
                          opacity:
                              $isButton, //?利用Stack 层叠原理 显示一个正常的button然后判断是不是 非高级的button 是的话 显示这个

                          child: new FlatButton(
                            child: new Container(
                              alignment: Alignment.center,
                              child: new Text(
                                '确定',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19.0,
                                  color: Color.fromRGBO(237, 153, 184, 1),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context); //关闭对话框
                            },
                          ),
                        ),
                        Opacity(
                          //!透明度
                          opacity:
                              $isExpButton, //?利用Stack 层叠原理 显示一个正常的button然后判断是不是 非高级的button 是的话 显示这个
                          child: new Row(
                            // ? 按钮横向排列
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(
                                  //! 设置按钮 左右间距
                                  left: ScreenUtil().setWidth(5),
                                  top: ScreenUtil().setHeight(10),
                                ),
                                /* margin:
                              EdgeInsets.only(left: ScreenUtil().setWidth(70)), */
                                child: new FlatButton(
                                  child: new Container(
                                    alignment: Alignment.center,
                                    width: ScreenUtil().setWidth(200),
                                    child: new Text(
                                      widget.expButton['expButtonSuccessName'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 19.0,
                                        color: Color.fromRGBO(237, 153, 184, 1),
                                        decoration: TextDecoration.none,
                                      ),
                                    ),
                                  ),
                                  onPressed: () {
                                    widget.expButton['expButtonSuccessOntab']();
                                  },
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  //! 设置按钮 左右间距
                                  right: ScreenUtil().setWidth(5),
                                  top: ScreenUtil().setHeight(10),
                                ),
                                /*  margin:
                              EdgeInsets.only(left: ScreenUtil().setWidth(70)), */
                                child: new FlatButton(
                                  child: new Container(
                                    alignment: Alignment.center,
                                    width: ScreenUtil().setWidth(200),
                                    child: new Text(
                                      widget.expButton['expButtonErrorName'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 19.0,
                                        color: Color.fromRGBO(237, 153, 184, 1),
                                        decoration: TextDecoration.none,
                                      ),
                                    ),
                                  ),
                                  onPressed: () {
                                    widget.expButton['expButtonErrorOntab']();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    height: ScreenUtil().setHeight(80),
                    width: ScreenUtil().setWidth(550),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: new Border(
                        top: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ), // 边色与边宽度
                    ),
                  ),
                ),
                //!按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按���-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮-按钮
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
        ),
      ],
    );
  }
}

class GlobalList extends StatefulWidget {
  /*
   * 列表
   */
  GlobalList(this.list, {Key key}) : super(key: key);
  final Map list;
  @override
  _GlobalListState createState() => _GlobalListState();
}

class _GlobalListState extends State<GlobalList> {
  @override
  Widget build(BuildContext context) {
    var height =
        widget.list['a_list'].length * 84 + 180; //动态计算组建高度 180是 两段默认文字高度
    var slidingState; //滚动状态
    if (height > 928) {
      //判断最大高度是不是 iphone 6s 的高度 928 是的话 等比例最大928
      slidingState = AlwaysScrollableScrollPhysics(); //可以滚动
      height = 928;
    } else {
      //不是设置不可滚动
      slidingState = NeverScrollableScrollPhysics(); //不可滚动
    }
    return Container(
      // 背景色 半透明
      color: Color.fromRGBO(0, 0, 0, 0.2),
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(120),
          right: ScreenUtil().setWidth(120),
          top: ScreenUtil().setHeight((1334 - height) / 2),
          bottom: ScreenUtil().setHeight(
            1334 - (1334 - height) - 100,
          ),
        ),
        child: ClipRRect(
          borderRadius: new BorderRadius.vertical(
            top: Radius.elliptical(20, 20),
            bottom: Radius.elliptical(20, 20),
          ), // 也可控件���边圆角大小

          child: ConstrainedBox(
            //设置一个限制宽高的盒子
            constraints: BoxConstraints(
              minHeight: ScreenUtil().setHeight(180), //最小高度
              maxHeight: ScreenUtil().setHeight(928), //最大高度
              // minWidth: double.infinity, // //宽度尽可能大
            ),
            child: Container(
              //一个盒子
              color: Color(0xffffffff), //盒子背景颜色
              // height: ScreenUtil().setHeight(930),
              height: ScreenUtil().setHeight(height), //盒子高度 动态获取
              child: Column(
                // 竖排列表
                children: <Widget>[
                  Container(
                    //标题
                    height: ScreenUtil().setHeight(100),
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(20),
                    ),
                    child: Text(
                      widget.list['title'],
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Color(0xff000000),
                        fontSize: (widget.list['titleSize'] != null)
                            ? widget.list['titleSize']
                            : ScreenUtil().setSp(45),
                      ),
                    ),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xFFb1b0b2),
                          width: 0.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    //内容
                    height: ScreenUtil().setHeight(height - 210),
                    child: MediaQuery.removePadding(
                      removeTop: true,
                      context: context,
                      child: ListView.separated(
                        // physics: const AlwaysScrollableScrollPhysics(),//!允许滑动
                        // physics: const NeverScrollableScrollPhysics(),//!不允许滑动
                        physics: slidingState, //!不允许滑动
                        separatorBuilder: (BuildContext context, int index) {
                          return Divider(
                            color: Colors.grey,
                          );
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: Container(
                              height: ScreenUtil().setHeight(52),
                              alignment: Alignment.center,
                              child: Text(
                                widget.list['a_list'][index]['text'],
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: HexColor(
                                      widget.list['a_list'][index]['color']),
                                  fontSize: (widget.list['a_list'][index]
                                              ['fontSize'] !=
                                          null)
                                      ? widget.list['a_list'][index]['fontSize']
                                      : ScreenUtil().setSp(40),
                                ),
                              ),
                            ),
                            onTap: () {
                              widget.list['a_list'][index]['function']();
                            },
                          );
                        },
                        itemCount: widget.list['a_list'].length,
                      ),
                    ),
                  ),
                  Container(
                    //按钮
                    height: ScreenUtil().setHeight(80),
                    child: GestureDetector(
                      child: Text(
                        widget.list['exitTitle'],
                        style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Color(0xfffe4439),
                          fontSize: ScreenUtil().setSp(45),
                        ),
                      ),
                      onTap: () {
                        widget.list['exitFuncton']();
                      },
                    ),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: Color(0xFFb1b0b2),
                          width: 0.5,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    hexColor = hexColor.replaceAll('0X', '');
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class NetworkSuperImage extends StatefulWidget {
  NetworkSuperImage({Key key, this.url, this.fit}) : super(key: key);
  final url;
  BoxFit fit = BoxFit.fill;
  @override
  _NetworkImageSuperState createState() => _NetworkImageSuperState();
}

class _NetworkImageSuperState extends State<NetworkSuperImage>
    with SingleTickerProviderStateMixin {
  AnimationController danRuDanChu;
  @override
  void initState() {
    danRuDanChu = AnimationController(
        vsync: this,
        duration: Duration(seconds: 3),
        lowerBound: 0.0,
        upperBound: 1.0);
    super.initState();
  }

  @override
  void dispose() {
    danRuDanChu.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var url = widget.url;
    return ExtendedImage.network(
      url,
      fit: widget.fit,
      cache: true,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            danRuDanChu.reset();
            return Image.asset(
                'images/ReadingComics/LoadingDiagram.png'); //加载中动画
            break;
          case LoadState.completed: //加载完成
            danRuDanChu.forward();
            return FadeTransition(
              opacity: danRuDanChu,
              child: ExtendedRawImage(
                image: state.extendedImageInfo?.image,
                width: ScreenUtil()
                    .setWidth($global_HomeBannerBannerImagesAndBoxWidth),
                height: ScreenUtil()
                    .setHeight($global_HomeBannerBannerImagesAndBoxHeight),
              ),
            );
            break;
          case LoadState.failed:
            danRuDanChu.reset();
            state.imageProvider.evict();
            return GestureDetector(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image.asset(
                    "images/Other/Other1.jpeg",
                    fit: BoxFit.fill,
                  ),
                  Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: Text(
                      "图片丢了！请联系作者找回",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19.0,
                        color: Color.fromRGBO(237, 153, 184, 1),
                        decoration: TextDecoration.none,
                      ),
                    ),
                  )
                ],
              ),
              onTap: () {
                state.reLoadImage();
              },
            );
            break;
        }
        return Container();
      },
    );
  }
}

/*
 * 弹出列表 - 改
 */
class TispList extends StatefulWidget {
  /*
   * 列表
   */
  TispList(this.list, {this.widghtWidth, Key key}) : super(key: key);
  final Map list;
  final double widghtWidth; //宽度
  _TispListState createState() => _TispListState();
}

class _TispListState extends State<TispList> {
  @override
  Widget build(BuildContext context) {
    List<Widget> listBuildSuccess = List.generate(
      widget.list['a_list'].length,
      (index) {
        return Container(
          width: (widget.widghtWidth == null)
              ? ScreenUtil().setWidth(550)
              : widget.widghtWidth,
          height: ScreenUtil().setWidth(80),
          child: GestureDetector(
            child: Container(
              height: ScreenUtil().setHeight(52),
              alignment: Alignment.center,
              child: Text(
                widget.list['a_list'][index]['text'],
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: HexColor(widget.list['a_list'][index]['color']),
                  fontSize: (widget.list['a_list'][index]['fontSize'] != null)
                      ? widget.list['a_list'][index]['fontSize']
                      : ScreenUtil().setSp(40),
                ),
              ),
            ),
            onTap: () {
              widget.list['a_list'][index]['function']();
            },
          ),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 0.5,
                color: Color(0xffb2b1b1),
              ),
            ),
          ),
        );
      },
    );
    return ClipRRect(
      borderRadius: new BorderRadius.vertical(
        top: Radius.elliptical(20, 20),
        bottom: Radius.elliptical(20, 20),
      ),
      child: Container(
        color: Colors.red,
        child: Container(
          //一个盒子
          color: Color(0xffffffff), //盒子背景颜色
          // height: ScreenUtil().setHeight(930),
          child: Column(
            // 竖排列表
            children: <Widget>[
              Container(
                //标题
                width: (widget.widghtWidth == null)
                    ? ScreenUtil().setWidth(550)
                    : widget.widghtWidth,
                height: ScreenUtil().setHeight(100),
                alignment: Alignment.center,
                child: Text(
                  widget.list['title'],
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Color(0xff000000),
                      fontSize: (widget.list['titleSize'] != null)
                          ? widget.list['titleSize']
                          : ScreenUtil().setSp(65),
                      fontWeight: FontWeight.w700),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xFFb1b0b2),
                      width: 0.5,
                    ),
                  ),
                ),
              ),
              Column(
                children: listBuildSuccess,
              ),
              Container(
                //按钮
                height: ScreenUtil().setHeight(100),
                child: GestureDetector(
                  child: Text(
                    widget.list['exitTitle'],
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Color(0xfffe4439),
                        fontSize: ScreenUtil().setSp(45),
                        fontWeight: FontWeight.w700),
                  ),
                  onTap: () {
                    widget.list['exitFuncton']();
                  },
                ),
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
 * @name: 高级图片展示 - 网络
 * @test: test font
 * @msg: 具有加载失败显示
 * @param {String} url 
 * @param {BoxFit.fill} fit 
 */
class NetworkCustomImage extends StatefulWidget {
  NetworkCustomImage({Key key, this.url, this.fit}) : super(key: key);
  final url;
  BoxFit fit = BoxFit.fill;
  @override
  _CustomImageState createState() => _CustomImageState();
}

class _CustomImageState extends State<NetworkCustomImage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
        vsync: this,
        duration: Duration(seconds: 3),
        lowerBound: 0.0,
        upperBound: 1.0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var url = widget.url;
    return ExtendedImage.network(
      url,
      fit: widget.fit,
      cache: true,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            _controller.reset();
            return SizedBox(
              child: Container(
                child: LoadingIndicator(
                  indicatorType:
                      $global_HomeBannerBannerImagesAndBoxLogingAnime,
                  color: $global_HomeBannerBannerImagesAndBoxLogingAnimeColor,
                ),
              ),
              width: ScreenUtil()
                  .setWidth($global_HomeBannerBannerImagesAndBoxLogingWidth),
              height: ScreenUtil()
                  .setHeight($global_HomeBannerBannerImagesAndBoxLogingHeight),
            );
            break;
          case LoadState.completed:
            _controller.forward();
            return FadeTransition(
              opacity: _controller,
              child: ExtendedRawImage(
                image: state.extendedImageInfo?.image,
                width: ScreenUtil()
                    .setWidth($global_HomeBannerBannerImagesAndBoxWidth),
                height: ScreenUtil()
                    .setHeight($global_HomeBannerBannerImagesAndBoxHeight),
              ),
            );
            break;
          case LoadState.failed:
            _controller.reset();
            state.imageProvider.evict();
            return GestureDetector(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image.asset(
                    "images/Other/Other1.jpeg",
                    fit: BoxFit.fill,
                  ),
                  Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: Text(
                      "图片丢了！请联系作者找回",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19.0,
                        color: Color.fromRGBO(237, 153, 184, 1),
                        decoration: TextDecoration.none,
                      ),
                    ),
                  )
                ],
              ),
              onTap: () {
                state.reLoadImage();
              },
            );
            break;
        }
        return Container();
      },
    );
  }
}

/*
 * @name: 高级图片展示 - 本地
 * @test: test font
 * @msg: 具有加载失败显示
 * @param {String} url 
 * @param {BuildContext} context  //弹出提示用
 * @param {BoxFit.fill} fit 
 * @return widget
 */
class LocalCustomImage extends StatefulWidget {
  LocalCustomImage({Key key, this.src, this.fit = BoxFit.contain})
      : super(key: key);
  final src;
  final BoxFit fit;
  @override
  _LocalCustomImageState createState() => _LocalCustomImageState();
}

class _LocalCustomImageState extends State<LocalCustomImage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
        vsync: this,
        duration: Duration(seconds: 3),
        lowerBound: 0.0,
        upperBound: 1.0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var src = dataSpace + widget.src;
    return ExtendedImage.asset(
      src,
      fit: widget.fit,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            _controller.reset();
            return SizedBox(
              child: Container(
                child: LoadingIndicator(
                  indicatorType:
                      $global_HomeBannerBannerImagesAndBoxLogingAnime,
                  color: $global_HomeBannerBannerImagesAndBoxLogingAnimeColor,
                ),
              ),
              width: ScreenUtil()
                  .setWidth($global_HomeBannerBannerImagesAndBoxLogingWidth),
              height: ScreenUtil()
                  .setHeight($global_HomeBannerBannerImagesAndBoxLogingHeight),
            );
            break;
          case LoadState.completed:
            _controller.forward();
            return FadeTransition(
              opacity: _controller,
              child: ExtendedRawImage(
                image: state.extendedImageInfo?.image,
                width: ScreenUtil()
                    .setWidth($global_HomeBannerBannerImagesAndBoxWidth),
                height: ScreenUtil()
                    .setHeight($global_HomeBannerBannerImagesAndBoxHeight),
              ),
            );
            break;
          case LoadState.failed:
            _controller.reset();
            state.imageProvider.evict();
            return Stack(
              fit: StackFit.expand,
              children: <Widget>[
                GestureDetector(
                  child: Image.asset(
                    "images/Other/Other1.jpeg",
                    fit: widget.fit,
                  ),
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (context, _, __) => GlobalTips(
                          body: "图片疑似损坏",
                          height: ScreenUtil().setHeight(900),
                        ),
                        opaque: false,
                      ),
                    );
                  },
                ),
                Positioned(
                  bottom: 50,
                  left: 0.0,
                  right: 0.0,
                  child: Text(
                    "图片丢了！请联系作者找回",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 19.0,
                      color: Color.fromRGBO(237, 153, 184, 1),
                      decoration: TextDecoration.none,
                    ),
                  ),
                )
              ],
            );

            break;
        }
        return Container();
      },
    );
  }
}
