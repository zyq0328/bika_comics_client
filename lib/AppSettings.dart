/*
 * @Date: 2020-07-03 23:01:54
 * @名称: App设置
 * @描述: app基本信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 11:42:59
 * @FilePath: /bika_comics_client/lib/AppSettings.dart
 * @最后编辑: 初雪桜
 */
final $appBuildVersion = "33"; //app编译版本
final $apiKey = "2587EFC6F859B4E3A1D8B6D33B272"; //应用密钥
final $appPlatform = "ios"; //应用类型
final $appVersion = "2.1.2.6"; //应用版本
final $appUUID = "D8D0C193-E4E4-440F-A1D1-2DC9E8EBB5F5"; //应用UUID
final $acceptLanguage = "zh-cn" /* 'ja-jp' */; //应用语言
final $requestToVerifyEncryptionKey =
    "kKMTTGDj5ab9wBQA2lnx8qCSI4JInc9sVq825CDlzXj8NQEvkrhJr4thEJLhQ67JCBkOBT39TyJfICunTh3zy0GRBAbt7uRTgjPIltbEiyY8XmYhEi1EpEQbrMKGIL8L"; //sha256加密密钥
final $requestToVerifyEncryptionKeySha512 =
    "DGNMO8NlJw3aqh6opP7gTbpUta23Zk0I4xJthrE0FpB3PKzhinyBsZjYHtXuxEi99lPhfUAGhVUrr5M5OxYtZF3V18G8ZM5AEZmkcaDn3x1rBZPXS5TAynGkhFdZu0pj26uWiSDXiocGoGheZ8DtTE3K2dbKdBKf6OtjfxbVD5hxDRhqkAA34sIBzYRsDPRRiCj2Xn5CNQXxvMSHiEyQMjxpskXuKOFimXr3oG4wjGZIqjcvWg3gM4xwYQ0PLAw"; //sha512加密密钥
