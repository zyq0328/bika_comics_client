/*
 * @Date: 2019-12-27 22:16:38
 * @名称: 公告界面总界面
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-09 10:28:52
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Announcements/Widget/AnnouncementsBoxController.dart';
import 'package:pica_acg/Pages/Announcements/Widget/Box/AnnouncementsList.dart';

Animation<double> upInsertAnime; //服务器列表
Animation<double> maskAnime; //遮罩层
double $loadingGifState = 0.0; //logo加载层透明度 最大1.0
AnimationController upInsertAnimeController; //logo图标
AnimationController maskAnimeController; //遮罩层
var splashBGTransparency = 0.0;
var announcementsHidden = 1.0;
var announcementsExit = false; //! 是否退出

class AnnouncementsAnime extends StatefulWidget {
  AnnouncementsAnime({
    Key key,
  }) : super(key: key);
  @override
  _AnnouncementsAnimeState createState() => _AnnouncementsAnimeState();
}

class _AnnouncementsAnimeState extends State<AnnouncementsAnime>
    with SingleTickerProviderStateMixin {
  @override
  initState() {
    super.initState();
    announcementsExit = false; //! 是否退出
    upInsertAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    upInsertAnime =
        new Tween(begin: 0.0, end: 1334.0).animate(upInsertAnimeController)
          ..addListener(
            () {
              setState(() {
                // the state that has changed here is the serviceListAnime object’s value
              });
            },
          );
    upInsertAnimeController.forward(); //直执行一次
  }

  Widget build(BuildContext context) {
    var speed = 1330.0 - upInsertAnime.value;
    if (upInsertAnime.value == 0 && announcementsExit == true) {
      Navigator.pop(context);
    }
    if (speed <= 0) {
      speed = 0;
    }
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      child: MaterialApp(
        home: Container(
          child: Opacity(
            opacity: announcementsHidden,
            child: Stack(
              children: <Widget>[
                Mask(),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(speed)),
                  child: HomePage1View(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  dispose() {
    // loadingAnimeController.dispose();
    super.dispose();
  }
}

class Mask extends StatefulWidget {
  _MaskState createState() => new _MaskState();
}

BuildContext context;

class _MaskState extends State<Mask> with SingleTickerProviderStateMixin {
  initState() {
    super.initState();
    maskAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    maskAnime = new Tween(begin: 0.0, end: 1.0).animate(maskAnimeController)
      ..addListener(() {
        setState(() {
          // the state that has changed here is the MaskAnime object’s value
        });
      });
    maskAnimeController.forward();
  }

  Widget build(BuildContext context) {
    return AnimatedOpacity(
      child: Container(
        child: Container(
          color: Color(0xFF000000),
        ),
      ),
      duration: Duration(milliseconds: 1),
      opacity: 1 - maskAnime.value,
    );
  }

  dispose() {
    super.dispose();
  }
}

class HomePage1View extends StatefulWidget {
  HomePage1View({
    Key key,
  }) : super(key: key);
  @override
  _HomePage1ViewState createState() => _HomePage1ViewState();
}

class _HomePage1ViewState extends State<HomePage1View> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          AnnouncementsBoxController(),
          Expanded(
            child: AnnouncementsControllerAnnouncementsList(),
          ),
        ],
      ),
    );
  }
}
