/*
 * @Date: 2019-12-27 22:32:13
 * @名称: 公告面板控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-09 00:57:07
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Announcements/Widget/Box/AnnouncementsBackGround.dart';
import 'package:pica_acg/Pages/Announcements/Widget/Box/AnnouncementsConBack.dart';
import 'package:pica_acg/Pages/Announcements/Widget/Box/AnnouncementsTitle.dart';

class AnnouncementsBoxController extends StatefulWidget {
  AnnouncementsBoxController({
    Key key,
  }) : super(key: key);
  @override
  _AnnouncementsBoxControllerState createState() =>
      _AnnouncementsBoxControllerState();
}

class _AnnouncementsBoxControllerState
    extends State<AnnouncementsBoxController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          AnnouncementsControllerAnnouncementsBackGround(),
          AnnouncementsControllerAnnouncementsTitle(),
          AnnouncementsAnnouncementsConBack(),
        ],
      ),
    );
  }
}
