/*
 * @Date: 2019-12-27 23:07:28
 * @名称: 这是一个返回按钮
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-04-01 23:42:32
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';
import 'package:pica_acg/Pages/Announcements/Announcements.dart';
import 'package:pica_acg/Pages/Announcements/Widget/Box/AnnouncementsList.dart';

class AnnouncementsAnnouncementsConBack extends StatefulWidget {
  AnnouncementsAnnouncementsConBack({
    Key key,
  }) : super(key: key);
  @override
  _AnnouncementsAnnouncementsConBackState createState() =>
      _AnnouncementsAnnouncementsConBackState();
}

class _AnnouncementsAnnouncementsConBackState
    extends State<AnnouncementsAnnouncementsConBack> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return GestureDetector(
      onTap: () {
        // Navigator.pop(widget.announcementsAnime);
        if (announcementsListRequestSuccess) {
        } else {
          announcementsListRequest.cancel('cancelled');
        }
        announcementsListRequestStop = true;
        announcementsExit = true;
        upInsertAnimeController.reverse(); //直执行一次
      },
      child: Container(
        width: ScreenUtil().setWidth(750),
        height: ScreenUtil().setHeight(70),
        margin: EdgeInsets.only(
            top: ScreenUtil().setHeight(50), left: ScreenUtil().setWidth(30)),
        child: Text(
          languAre().$languAgeOrdinary["basic"]["return"],
          textAlign: TextAlign.left,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 19.0,
            color: Color(0xFF515151),
            decoration: TextDecoration.none,
          ),
        ),
        decoration: BoxDecoration(
          border: new Border(
            bottom: Divider.createBorderSide(
              context,
              color: Color.fromRGBO(229, 229, 231, 1),
              width: 2,
            ),
          ), // 边色与边宽度
        ),
      ),
    );
  }
}
