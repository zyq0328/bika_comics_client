/*
 * @Date: 2019-12-27 22:41:25
 * @名称: 公告页面标题
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2019-12-28 11:24:36
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AnnouncementsControllerAnnouncementsTitle extends StatefulWidget {
  AnnouncementsControllerAnnouncementsTitle({Key key}) : super(key: key);

  @override
  _AnnouncementsControllerAnnouncementsTitleState createState() =>
      _AnnouncementsControllerAnnouncementsTitleState();
}

class _AnnouncementsControllerAnnouncementsTitleState
    extends State<AnnouncementsControllerAnnouncementsTitle> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      color: Color(0xFFfefefe),
      child: Container(
        width: ScreenUtil().setWidth(750),
        height: ScreenUtil().setHeight(70),
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(50)),
        child: Text(
          'BIKA 公告',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 19.0,
            color: Color(0Xff4c4c4c),
            decoration: TextDecoration.none,
          ),
        ),
        decoration: BoxDecoration(
          border: new Border(
            bottom: Divider.createBorderSide(
              context,
              color: Color.fromRGBO(229, 229, 231, 1),
              width: 2,
            ),
          ), // 边色与边宽度
        ),
      ),
    );
  }
}
