/*
 * @Date: 2019-12-27 23:30:17
 * @名称: 公告列表 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-09 10:19:31
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:common_utils/common_utils.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';
import 'package:pica_acg/tools/extends/Json/Home/HomeAnnouncements/home_announcements_json.dart';
import 'package:dio/dio.dart';

class AnnouncementsControllerAnnouncementsList extends StatefulWidget {
  AnnouncementsControllerAnnouncementsList({Key key, this.height})
      : super(key: key);
  final double height;
  @override
  _AnnouncementsControllerAnnouncementsListState createState() =>
      _AnnouncementsControllerAnnouncementsListState();
}

var dataJson = HomeAnnouncementsJson.fromJson($netWorkReturnAnnouncements);
CancelToken announcementsListRequest = CancelToken();
bool announcementsListRequestStop = false;
bool announcementsListRequestSuccess = false;

class _AnnouncementsControllerAnnouncementsListState
    extends State<AnnouncementsControllerAnnouncementsList> {
  @override
  void initState() {
    NetWorkController.get(
      getAnnouncementsList(),
      (data) {
        print(data);
        announcementsListRequestSuccess = true;

        if (data['code'] == "200") {
          dataJson = HomeAnnouncementsJson.fromJson(data);
          setState(() {});
        } else if (data['code'] == "-1") {
          if (announcementsListRequestStop == false) {
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                  height: ScreenUtil().setHeight(900),
                ),
                opaque: false,
              ),
            );
          } else {}
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        } else {
          if (announcementsListRequestStop == false) {
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                  height: ScreenUtil().setHeight(900),
                ),
                opaque: false,
              ),
            );
          } else {}
          requestErrorParsing(data, $context: context);
        }
      },
      requestName: announcementsListRequest,
      headers: getApiRequestHeadrs("Announcements"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Stack(
        children: [
          MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView.builder(
              itemCount: dataJson.data.announcements.docs.length,
              itemBuilder: (BuildContext context, int index) {
                String time = DateTime.parse(
                        dataJson.data.announcements.docs[index].createdAt)
                    .add(new Duration(hours: 0))
                    .toString();
                time = DateUtil.formatDateMs(DateUtil.getDateMsByTimeStr(time),
                    format: DataFormats.full);
                return new GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (context, _, __) => GlobalTipsExtend(
                          body: 'a',
                          height: 1000,
                          logo: {
                            "isLogo": false,
                          },
                          expand: true,
                          expandBody: Container(
                            margin: EdgeInsets.only(
                              top: ScreenUtil().setHeight(119),
                            ),
                            height: ScreenUtil().setHeight(900),
                            alignment: Alignment.topCenter,
                            child: ClipRRect(
                              borderRadius: new BorderRadius.vertical(
                                top: Radius.elliptical(20, 20),
                              ),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Image(
                                      width: ScreenUtil().setWidth(546),
                                      image: NetworkImage(
                                        springboard() +
                                            dataJson.data.announcements
                                                .docs[index].thumb.fileServer +
                                            '/static/' +
                                            dataJson.data.announcements
                                                .docs[index].thumb.path,
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        'Waifu新面孔！',
                                        style: TextStyle(
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFFed97b7),
                                          decoration: TextDecoration.none,
                                        ),
                                      ),
                                      margin: EdgeInsets.only(
                                        top: ScreenUtil().setHeight(5),
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                    Container(
                                      child: Text(
                                        time,
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFFaeafad),
                                          decoration: TextDecoration.none,
                                        ),
                                      ),
                                      margin: EdgeInsets.only(
                                        top: ScreenUtil().setHeight(5),
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                    SizedBox(
                                      width: ScreenUtil().setWidth(506),
                                      child: Text(
                                        dataJson.data.announcements.docs[index]
                                            .content,
                                        softWrap: true,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 17.0,
                                          color: Color.fromRGBO(79, 79, 79, 1),
                                          decoration: TextDecoration.none,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          expButton: {
                            'isExpButton': true,
                            'expButtonSuccessName': '不要再说了',
                            'expButtonSuccessOntab': () => {
                                  showDialog<Null>(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) {
                                      return new AlertDialog(
                                        title: new Text('提示'),
                                        content: new SingleChildScrollView(
                                          child: new ListBody(
                                            children: <Widget>[
                                              new Text('你点了 - 不要����说了'),
                                            ],
                                          ),
                                        ),
                                        actions: <Widget>[
                                          new FlatButton(
                                            child: new Text('确定'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                },
                            'expButtonErrorName': '确定',
                            'expButtonErrorOntab': () => {
                                  Navigator.pop(context),
                                },
                          },
                        ),
                        opaque: false,
                      ),
                    );
                  },
                  child: Container(
                    height: ScreenUtil().setHeight(250),
                    child: Container(
                      margin: EdgeInsets.all(
                        ScreenUtil().setHeight(20.0),
                      ),
                      child: Row(
                        children: [
                          ListImage(
                            src: springboard() +
                                dataJson.data.announcements.docs[index].thumb
                                    .fileServer +
                                '/static/' +
                                dataJson
                                    .data.announcements.docs[index].thumb.path,
                          ),
                          Stack(
                            children: <Widget>[
                              /**标题 */
                              Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(20.0),
                                  top: ScreenUtil().setHeight(20.0),
                                ),
                                alignment: Alignment.topLeft,
                                child: SizedBox(
                                  width: ScreenUtil().setWidth(470),
                                  child: RollWidget(
                                    direction: Axis.horizontal,
                                    child: RollWidget(
                                      direction: Axis.horizontal,
                                      child: Text(
                                        time +
                                            '/' +
                                            dataJson.data.announcements
                                                .docs[index].title,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14.0,
                                          color: Color.fromRGBO(79, 79, 79, 1),
                                          decoration: TextDecoration.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              /**父标题 */
                              Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(20.0),
                                  top: ScreenUtil().setHeight(100.0),
                                ),
                                alignment: Alignment.topLeft,
                                child: SizedBox(
                                  width: ScreenUtil().setWidth(470),
                                  child: Text(
                                    dataJson
                                        .data.announcements.docs[index].content,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(189, 189, 189, 1),
                                      decoration: TextDecoration.none,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          width: 1,
                        ),
                      ), // 边色与边宽度
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class ListImage extends StatelessWidget {
  const ListImage({Key key, this.src = $global_ImagesUrlUndefined})
      : super(key: key);
  final String src;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Image.network(
        this.src,
        height: ScreenUtil().setHeight(200),
        fit: BoxFit.cover,
        width: ScreenUtil().setWidth(200),
        // width: ScreenUtil().setWidth(50.0),
      ),
    );
  }
}
