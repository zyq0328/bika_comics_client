/*
 * @Date: 2019-12-27 22:17:35
 * @名称: 公告页面的背景
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2019-12-27 22:40:49
 */
import 'package:flutter/material.dart';

class AnnouncementsControllerAnnouncementsBackGround extends StatefulWidget {
  AnnouncementsControllerAnnouncementsBackGround({Key key}) : super(key: key);

  @override
  _AnnouncementsControllerAnnouncementsBackGroundState createState() =>
      _AnnouncementsControllerAnnouncementsBackGroundState();
}

class _AnnouncementsControllerAnnouncementsBackGroundState
    extends State<AnnouncementsControllerAnnouncementsBackGround> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF2F2F7),
    );
  }
}
