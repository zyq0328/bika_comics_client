import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class BottomControllerBottomBox extends StatefulWidget {
  BottomControllerBottomBox({Key key}) : super(key: key);

  @override
  _BottomControllerBottomBoxState createState() =>
      _BottomControllerBottomBoxState();
}

int currentPosition = 0;

class _BottomControllerBottomBoxState extends State<BottomControllerBottomBox> {
  @override
  Widget build(BuildContext context) {
    BottomNavigationBarType animType = BottomNavigationBarType.fixed;
    return BottomNavigationBar(
      items: [
        new BottomNavigationBarItem(
          icon: new Icon(
            IconFont.iconHome,
            color: currentPosition == 0 ? Color(0xffeba4be) : Color(0xffacacac),
          ),
          title: new Text(
            "主页",
            style: new TextStyle(
              color:
                  currentPosition == 0 ? Color(0xffeba4be) : Color(0xffacacac),
            ),
          ),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(
            IconFont.iconcategory,
            color: currentPosition == 1 ? Color(0xffeba4be) : Color(0xffacacac),
          ),
          title: new Text(
            "分类",
            style: new TextStyle(
              color:
                  currentPosition == 1 ? Color(0xffeba4be) : Color(0xffacacac),
            ),
          ),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(
            IconFont.iconProfile,
            color: currentPosition == 2 ? Color(0xffeba4be) : Color(0xffacacac),
          ),
          title: new Text(
            "我的",
            style: new TextStyle(
              color:
                  currentPosition == 2 ? Color(0xffeba4be) : Color(0xffacacac),
            ),
          ),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(
            IconFont.iconsetting,
            color: currentPosition == 3 ? Color(0xffeba4be) : Color(0xffacacac),
          ),
          title: new Text(
            "设定",
            style: new TextStyle(
              color:
                  currentPosition == 3 ? Color(0xffeba4be) : Color(0xffacacac),
            ),
          ),
        ),
      ],
      currentIndex: currentPosition,
      type: animType,
      onTap: (index) {
        setState(
          () {
            currentPosition = index;
            /*  Widget widget;
            switch (index) {
              case 0:
                widget = LoadingHomeAnime();
                break;
              case 1:
                widget = Category();
                break;
              case 2:
                widget = My();
                break;
              case 3:
                widget = SettingContorller();
                break;
              default:
            } */
            mainHomeState.currentState.toPage(index);
          },
        );
      },
    );
  }
}
