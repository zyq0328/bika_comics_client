import 'dart:ui';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/ServiceList/ServiceList.dart';
import 'package:pica_acg/anime/LiuDong.dart';
import 'package:pica_acg/anime/anime.dart';

import '../../anime/HuaDong.anime.dart';

/*
 * 入口文件
 */
class ServiceBranch extends StatelessWidget {
  const ServiceBranch({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Stack(
      children: <Widget>[
        Background(),
        Tisp(),
        Container(
          margin: EdgeInsets.only(top: ScreenUtil().setHeight(580)),
          alignment: Alignment.center,
          child: Container(
            width: ScreenUtil().setWidth(600),
            child: AnimeServiceBranchList(),
          ),
        ),
      ],
    );
  }
}

/*
 * 背景图像
 */
class Background extends StatelessWidget {
  const Background({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Image.asset(
          'images/ServiceBranch/ServiceBranchBG.jpg',
          fit: BoxFit.cover,
          width: ScreenUtil().setWidth(750),
          height: ScreenUtil().setHeight(1334),
        ),
      ),
    );
  }
}

/*
 * 服务器列表
 */
class ServiceBranchList extends StatefulWidget {
  ServiceBranchList({Key key}) : super(key: key);

  @override
  _ServiceBranchListState createState() => _ServiceBranchListState();
}

class _ServiceBranchListState extends State<ServiceBranchList> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(10.0),
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(5.0),
          height: ScreenUtil().setHeight(90),
          child: MaterialButton(
            child: Container(
              height: ScreenUtil().setHeight(80),
              alignment: Alignment.center,
              child: new Text(
                '分流1',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(40),
                ),
              ),
            ),
            color: Colors.grey[50],
            textColor: Colors.pinkAccent,
            onPressed: () {
              Navigator.of(context).push(
                PageRouteBuilder(
                  pageBuilder: (context, _, __) => AnimeLiuDong(
                    widget:
                        /*  GlobalTipsExtend(
                      body: 'a',
                      height: ScreenUtil().setHeight(900),
                      // islogo: false,
                      logo: {
                        "isLogo": true,
                        "logoSrc": "images/Tisp/UnknownError.png",
                      },
                      expand: true,
                      expButton: {
                        "isExpButton": false,
                        "expButtonSuccessName": 'aaaa',
                        "expButtonSuccessOntab": () {
                          Navigator.pop(context);
                        },
                        "expButtonErrorName": 'aaaa',
                        "expButtonErrorOntab": () {
                          Navigator.pop(context);
                        },
                      },
                      expandBody: Container(
                        //文本
                        margin: EdgeInsets.only(
                          right: ScreenUtil().setWidth(10),
                          left: ScreenUtil().setWidth(10),
                          // top: ScreenUtil().setHeight(50),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          '咔哔咔被玩坏了!\n这肯定不是哔咔的问题!\n绝对不是!',
                          style: TextStyle(
                            fontSize: 14 /* ScreenUtil().setWidth(30) */,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF000000),
                            decoration: TextDecoration.none,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ), */

                        GlobalTips(
                      body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                      height: ScreenUtil().setHeight(900),
                      context: context,
                    ),
                  ),
                  opaque: false,
                ),
              );
              return;
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Scaffold(
                    body: ServiceList(
                        login: true,
                        key: serviceListState,
                        serviceUrl: 'https://picaapi.picacomic.com/'),
                  ),
                ),
              );
            },
          ),
        ),
        Container(
          margin: const EdgeInsets.all(5.0),
          height: ScreenUtil().setHeight(90),
          child: MaterialButton(
            child: Container(
              height: ScreenUtil().setHeight(80),
              alignment: Alignment.center,
              child: new Text(
                '分流2',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(40),
                ),
              ),
            ),
            color: Colors.grey[50],
            textColor: Colors.pinkAccent,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Scaffold(
                    body: ServiceList(
                        login: false,
                        key: serviceListState,
                        serviceUrl: 'https://picaapi.picacomic.com/'),
                  ),
                ),
              );
            },
          ),
        ),
        Container(
          margin: const EdgeInsets.all(5.0),
          height: ScreenUtil().setHeight(90),
          child: MaterialButton(
            child: Container(
              height: ScreenUtil().setHeight(80),
              alignment: Alignment.center,
              child: new Text(
                'localhost',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(40),
                ),
              ),
            ),
            color: Colors.grey[50],
            textColor: Colors.pinkAccent,
            onPressed: () {
              Navigator.of(context).push(
                AnimeZuoYouHuaDong(
                  widget: Scaffold(
                    body: ServiceList(
                        login: false,
                        key: serviceListState,
                        serviceUrl: 'http://localhost/'),
                  ),
                ),
              );
              /* Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Login(serviceUrl: 'http://localhost/'))); */
            },
          ),
        ),
      ],
      physics: const NeverScrollableScrollPhysics(),
    );
  }
}

/*
 * 加载服务器列表的动画
 */
class AnimeServiceBranchList extends StatefulWidget {
  _AnimeServiceBranchListState createState() =>
      new _AnimeServiceBranchListState();
}

class _AnimeServiceBranchListState extends State<AnimeServiceBranchList>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  initState() {
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
// 非线性动画
    final CurvedAnimation curve =
        CurvedAnimation(parent: controller, curve: Curves.elasticOut);
    animation = Tween(begin: 0.0, end: 300.0).animate(curve)
      ..addListener(() {
        setState(() {});
      });
    controller.forward();
  }

  Widget build(BuildContext context) {
    return new Center(
      child: new Container(
        margin: new EdgeInsets.symmetric(vertical: 10.0),
        width: animation.value,
        child: new ServiceBranchList(),
      ),
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}

/*
 * 服务器列表上面的字
 */
class Tisp extends StatelessWidget {
  const Tisp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: ScreenUtil().setHeight(330)),
          child: Text(
            '请选择分流',
            style: TextStyle(
              color: Colors.grey[50],
              fontSize: ScreenUtil().setSp(30),
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          child: Text(
            '(分流一看不了的图的人请去分流而，两个都不行请vpn，之后会再增加分流)',
            style: TextStyle(
              color: Colors.grey[50],
              fontSize: ScreenUtil().setSp(30),
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
