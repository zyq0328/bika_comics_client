import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Comment/NullComment/MyMyBannerMyBannerNullCommentImages.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Comment/NullComment/MyMyBannerMyBannerNullCommentText.dart';

class MyMyBannerMyBannerNullCommentController extends StatefulWidget {
  MyMyBannerMyBannerNullCommentController({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerNullCommentControllerState createState() =>
      _MyMyBannerMyBannerNullCommentControllerState();
}

class _MyMyBannerMyBannerNullCommentControllerState
    extends State<MyMyBannerMyBannerNullCommentController> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          MyMyBannerMyBannerNullCommentImages(),
          MyMyBannerMyBannerNullCommentText(),
        ],
      ),
    );
  }
}
