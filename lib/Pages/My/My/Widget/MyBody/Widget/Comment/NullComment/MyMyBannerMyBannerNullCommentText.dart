/*
 * @Date: 2020-01-17 21:16:43
 * @名称: 我的 - 评论 - 没有评论 - 文本
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-17 21:27:05
 */
import 'package:flutter/material.dart';

class MyMyBannerMyBannerNullCommentText extends StatefulWidget {
  MyMyBannerMyBannerNullCommentText({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerNullCommentTextState createState() =>
      _MyMyBannerMyBannerNullCommentTextState();
}

class _MyMyBannerMyBannerNullCommentTextState
    extends State<MyMyBannerMyBannerNullCommentText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('没有任何评论哦！'),
    );
  }
}
