/*
 * @Date: 2020-01-17 21:16:54
 * @名称: 我的 - 评论 - 没有评论 - 图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-17 21:25:47
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyMyBannerMyBannerNullCommentImages extends StatefulWidget {
  MyMyBannerMyBannerNullCommentImages({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerNullCommentImagesState createState() =>
      _MyMyBannerMyBannerNullCommentImagesState();
}

class _MyMyBannerMyBannerNullCommentImagesState
    extends State<MyMyBannerMyBannerNullCommentImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(600),
      child: Image.asset(
        'images/My/NoComment.png',
      ),
    );
  }
}
