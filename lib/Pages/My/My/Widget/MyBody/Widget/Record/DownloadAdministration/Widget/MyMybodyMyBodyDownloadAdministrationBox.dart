/*
 * @Date: 2020-01-15 21:38:47
 * @名称: 我的 - 下载管理框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:11:24
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/DownloadAdministration/Widget/MyMybodyMybodyDownloadAdministrationList.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class MyMybodyMyBodyDownloadAdministrationBox extends StatefulWidget {
  MyMybodyMyBodyDownloadAdministrationBox({Key key}) : super(key: key);

  @override
  _MyMybodyMyBodyDownloadAdministrationBoxState createState() =>
      _MyMybodyMyBodyDownloadAdministrationBoxState();
}

class _MyMybodyMyBodyDownloadAdministrationBoxState
    extends State<MyMybodyMyBodyDownloadAdministrationBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(
          20.0,
        ),
      ),
      alignment: Alignment.topCenter,
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(40),
          top: ScreenUtil().setHeight(
            20.0,
          ),
        ),
        child: Column(
          children: <Widget>[
            Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Icon(
                    IconFont.iconProfile,
                    color: Color(0xffe386a9),
                  ),
                ),
                Expanded(
                  flex: 20,
                  child: Texts(),
                ),
                Expanded(
                  flex: 6,
                  child: Texts2(),
                ),
                Expanded(
                  flex: 3,
                  child: Icon(
                    IconFont.iconRight,
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 1),
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}

class Texts extends StatelessWidget {
  const Texts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(10.0)),
      child: Text(
        '下载管理',
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 19.0,
          color: Color.fromRGBO(79, 79, 79, 1),
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}

class Texts2 extends StatelessWidget {
  const Texts2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: new GestureDetector(
        onTap: () {
          /*  Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => AnnouncementsAnime(),
              opaque: false,
            ),
          ); */
        },
        child: Text(
          $downloadCount.toString(),
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 19.0,
            color: Color.fromRGBO(189, 189, 189, 1),
            decoration: TextDecoration.none,
          ),
        ),
      ),
    );
  }
}
