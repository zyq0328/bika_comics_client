/*
 * @Date: 2020-01-17 20:08:24
 * @名称: 我的 - 最近观看
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 20:47:02
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/RecentlyWatch/Widget/MyMyBodyMyBodyRecentlyWatchBox.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/RecentlyWatch/Widget/MyMyBodyMyBodyRecentlyWatchList.dart';
import 'package:pica_acg/tools/extends/Json/User/UserFavourite.dart';

class MyMyBodyMyBodyRecentlyWatchContorller extends StatefulWidget {
  MyMyBodyMyBodyRecentlyWatchContorller({Key key, this.favourite})
      : super(key: key);
  final UserFavourite favourite;
  @override
  _MyMyBodyMyBodyRecentlyWatchContorllerState createState() =>
      _MyMyBodyMyBodyRecentlyWatchContorllerState();
}

class _MyMyBodyMyBodyRecentlyWatchContorllerState
    extends State<MyMyBodyMyBodyRecentlyWatchContorller> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        MyMyBodyMyBodyRecentlyWatchBox(
            limit: widget.favourite.data.comics.docs.length),
        MyMyBodyMyBodyRecentlyWatchList(list: widget.favourite.data.comics.docs)
      ],
    );
  }
}
