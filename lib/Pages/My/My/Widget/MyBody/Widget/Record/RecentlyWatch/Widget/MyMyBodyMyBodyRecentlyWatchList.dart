/*
 * @Date: 2020-01-15 22:52:27
 * @名称: 我的 - 最近观看 - 列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 20:38:56
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/tools/extends/Json/User/UserFavourite.dart';

class MyMyBodyMyBodyRecentlyWatchList extends StatefulWidget {
  MyMyBodyMyBodyRecentlyWatchList({Key key, this.list}) : super(key: key);
  final List<Docs> list;
  @override
  _MyMyBodyMyBodyRecentlyWatchListState createState() =>
      _MyMyBodyMyBodyRecentlyWatchListState();
}

class _MyMyBodyMyBodyRecentlyWatchListState
    extends State<MyMyBodyMyBodyRecentlyWatchList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(20),
        right: ScreenUtil().setWidth(20),
      ),
      child: Container(
        height: ScreenUtil().setHeight(450),
        width: ScreenUtil().setHeight(750),
        child: ListView.builder(
          itemCount: widget.list.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: ScreenUtil().setWidth(180),
              child: Column(
                //!上下排列
                children: <Widget>[
                  Container(
                    //?元素1 图片
                    margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                    child: Image.network(
                      springboard() +
                          widget.list[index].thumb.fileServer +
                          '/static/' +
                          widget.list[index].thumb.path,
                      width: ScreenUtil().setWidth(150),
                      height: ScreenUtil().setWidth(250),
                    ),
                  ),
                  Container(
                    //?元素1 标题
                    margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                    child: Text(
                      widget.list[index].title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: ScreenUtil().setSp(27),
                        color: Color(0xffaaaaaa),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),

      /* Flex(//!不可以用
        direction: Axis.horizontal,
        children: List.generate(
          widget.list.length,
          (index) {
            return Expanded(
              flex: 4,
              child: Column(
                //!上下排列
                children: <Widget>[
                  Container(
                    //?元素1 图片
                    margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                    child: Image.network(
                      'https://tva2.sinaimg.cn/large/006ZEs2Bly1gaxn0wbjsej30ci0go3yz.jpg',
                      width: ScreenUtil().setWidth(150),
                      height: ScreenUtil().setWidth(250),
                    ),
                  ),
                  Container(
                    //?元素1 标题
                    margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                    child: Text(
                      'と兄と兄と兄と兄と兄と兄',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: ScreenUtil().setSp(27),
                        color: Color(0xffaaaaaa),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ), */
      /* Flex(//!可以用
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Column(
              //!上下排列
              children: <Widget>[
                Container(
                  //?元素1 图片
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                  child: Image.network(
                    'https://tva2.sinaimg.cn/large/006ZEs2Bly1gaxn0wbjsej30ci0go3yz.jpg',
                    width: ScreenUtil().setWidth(150),
                    height: ScreenUtil().setWidth(250),
                  ),
                ),
                Container(
                  //?元素1 标题
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: Text(
                    'と兄と兄と兄と兄と兄と兄',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(27),
                      color: Color(0xffaaaaaa),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                  child: Image.network(
                    'https://tva2.sinaimg.cn/large/006ZEs2Bly1gaxn0wbjsej30ci0go3yz.jpg',
                    width: ScreenUtil().setWidth(150),
                    height: ScreenUtil().setWidth(250),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: Text(
                    'と兄と兄と兄と兄と兄と兄',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(27),
                      color: Color(0xffaaaaaa),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                  child: Image.network(
                    'https://tva2.sinaimg.cn/large/006ZEs2Bly1gaxn0wbjsej30ci0go3yz.jpg',
                    width: ScreenUtil().setWidth(150),
                    height: ScreenUtil().setWidth(250),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: Text(
                    'と兄と兄と兄と兄と兄と兄',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(27),
                      color: Color(0xffaaaaaa),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(130)),
                  child: Image.network(
                    'https://tva2.sinaimg.cn/large/006ZEs2Bly1gaxn0wbjsej30ci0go3yz.jpg',
                    width: ScreenUtil().setWidth(150),
                    height: ScreenUtil().setWidth(250),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                  child: Text(
                    'と兄と兄と兄と兄と兄と兄',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(27),
                      color: Color(0xffaaaaaa),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ), */
    );
  }
}
