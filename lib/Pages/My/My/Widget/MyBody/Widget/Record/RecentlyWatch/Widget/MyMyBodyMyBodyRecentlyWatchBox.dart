/*
 * @Date: 2020-01-17 20:09:42
 * @名称: 我的 最近观看 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 20:49:43
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class MyMyBodyMyBodyRecentlyWatchBox extends StatefulWidget {
  MyMyBodyMyBodyRecentlyWatchBox({Key key, this.limit}) : super(key: key);
  int limit;
  @override
  _MyMyBodyMyBodyRecentlyWatchBoxState createState() =>
      _MyMyBodyMyBodyRecentlyWatchBoxState();
}

class _MyMyBodyMyBodyRecentlyWatchBoxState
    extends State<MyMyBodyMyBodyRecentlyWatchBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(450.0),
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(
          20.0,
        ),
      ),
      alignment: Alignment.topCenter,
      child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(40),
            top: ScreenUtil().setHeight(
              20.0,
            ),
          ),
          child: Column(
            children: <Widget>[
              Flex(
                direction: Axis.horizontal,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Icon(
                      IconFont.iconProfile,
                      color: Color(0xffe386a9),
                    ),
                  ),
                  Expanded(
                    flex: 20,
                    child: Texts(),
                  ),
                  Expanded(
                    flex: 2,
                    child: Texts2(limit: widget.limit),
                  ),
                  Expanded(
                    flex: 3,
                    child: Icon(
                      IconFont.iconRight,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  border: new Border(
                    top: Divider.createBorderSide(
                      context,
                      color: Color(0xffc8c7cc),
                      width: 1,
                    ),
                  ), // 边色与边宽度
                ),
              ),
            ],
          )),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 1),
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}

class Texts extends StatelessWidget {
  const Texts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(10.0)),
      child: Text(
        '我的收藏',
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 19.0,
          color: Color.fromRGBO(79, 79, 79, 1),
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}

class Texts2 extends StatelessWidget {
  const Texts2({Key key, this.limit}) : super(key: key);
  final int limit;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: new GestureDetector(
        onTap: () {
          /*  Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => AnnouncementsAnime(),
              opaque: false,
            ),
          ); */
        },
        child: Text(
          limit.toString(),
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 19.0,
            color: Color.fromRGBO(189, 189, 189, 1),
            decoration: TextDecoration.none,
          ),
        ),
      ),
    );
  }
}
