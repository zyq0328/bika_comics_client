/*
 * @Date: 2020-01-17 21:09:51
 * @名称: 个人 - 记录 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 22:11:11
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/DownloadAdministration/MyMyBodyMyBodyDownloadAdministrationController.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/RecentlyWatch/MyMyBodyMyBodyRecentlyWatchContorller.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/User/UserFavourite.dart';

class MyMyBodyControllerRecordContorller extends StatefulWidget {
  MyMyBodyControllerRecordContorller({Key key}) : super(key: key);

  @override
  _MyMyBodyControllerRecordContorllerState createState() =>
      _MyMyBodyControllerRecordContorllerState();
}

var $userFavourite = UserFavourite.fromJson($netWorkUserFavourite);

class _MyMyBodyControllerRecordContorllerState
    extends State<MyMyBodyControllerRecordContorller> {
  @override
  void initState() {
    super.initState();
    NetWorkController.post(getUserFavourite(), (callBack) {
      if (callBack['code'] == "200" && callBack['message'] == 'success') {
        $userFavourite = UserFavourite.fromJson(callBack);
        setState(() {});
      } else {
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, _, __) => GlobalTips(
              body: '获取我的收藏息失败',
              height: ScreenUtil().setHeight(900),
            ),
            opaque: false,
          ),
        );
      }
    }, headers: getApiRequestHeadrs('User-Favourite'));
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          MyMyBodyMyBodyDownloadAdministrationController(),
          MyMyBodyMyBodyRecentlyWatchContorller(favourite: $userFavourite),
        ],
      ),
    );
  }
}
