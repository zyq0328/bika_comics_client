/*
 * @Date: 2020-01-15 21:37:06
 * @名称: 我的 - 下载管理 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 00:19:27
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/DownloadAdministration/Widget/MyMybodyMyBodyDownloadAdministrationBox.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/DownloadAdministration/Widget/MyMybodyMybodyDownloadAdministrationList.dart';

class MyMyBodyMyBodyDownloadAdministrationController extends StatefulWidget {
  MyMyBodyMyBodyDownloadAdministrationController({Key key}) : super(key: key);

  @override
  _MyMyBodyMyBodyDownloadAdministrationControllerState createState() =>
      _MyMyBodyMyBodyDownloadAdministrationControllerState();
}

class _MyMyBodyMyBodyDownloadAdministrationControllerState
    extends State<MyMyBodyMyBodyDownloadAdministrationController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          MyMybodyMyBodyDownloadAdministrationBox(),
          MyMybodyMybodyDownloadAdministrationList(),
        ],
      ),
    );
  }
}
