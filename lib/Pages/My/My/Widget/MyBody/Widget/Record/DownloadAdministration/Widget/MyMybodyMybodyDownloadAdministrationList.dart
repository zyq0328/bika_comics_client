/*
 * @Date: 2020-01-15 22:52:27
 * @名称: 我的 - 下载管理 - 列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:16:51
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/DataBaseFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';

class MyMybodyMybodyDownloadAdministrationList extends StatefulWidget {
  MyMybodyMybodyDownloadAdministrationList({Key key}) : super(key: key);

  @override
  _MyMybodyMybodyDownloadAdministrationListState createState() =>
      _MyMybodyMybodyDownloadAdministrationListState();
}

var downloadList = [];
var $downloadCount = 0;

class _MyMybodyMybodyDownloadAdministrationListState
    extends State<MyMybodyMybodyDownloadAdministrationList> {
  @override
  void initState() {
    super.initState();
    dataBaseSelect("DownloadList", null, null).then(
      (value) {
        downloadList = value;
        $downloadCount = value.length;
      },
    );
  }

  Widget build(BuildContext context) {
    print(downloadList);
    return Container(
      child: Container(
        margin: EdgeInsets.all(
          ScreenUtil().setHeight(20),
        ),
        child: Container(
          width: ScreenUtil.screenWidth,
          height: ScreenUtil().setHeight(260),
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: downloadList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.only(right: ScreenUtil().setHeight(10)),
                width: ScreenUtil.screenWidth * 0.08,
                child: Container(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: LocalCustomImage(
                              src: downloadList[index]['LocalCoverUrl']),

                          /* Image.network(
                            downloadList[index]['FileServer'],
                          ), */
                          height: ScreenUtil().setHeight(200),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: ScreenUtil().setHeight(15)),
                          child: Text(
                            downloadList[index]['Title'],
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: ScreenUtil().setSp(27),
                              color: Color(0xffaaaaaa),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 1),
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
