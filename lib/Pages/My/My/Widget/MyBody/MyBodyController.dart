/*
 * @Date: 2020-01-15 21:32:16
 * @名称: 个人 - 身体
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 21:53:33
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/Widget/Record/MyMyBodyControllerRecordContorller.dart';

class MyMyBodyController extends StatefulWidget {
  MyMyBodyController({Key key}) : super(key: key);

  @override
  _MyMyBodyControllerState createState() => _MyMyBodyControllerState();
}

class _MyMyBodyControllerState extends State<MyMyBodyController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: MyMyBodyControllerRecordContorller(),
      // child: MyMyBannerMyBannerNullCommentController(),
    );
  }
}
