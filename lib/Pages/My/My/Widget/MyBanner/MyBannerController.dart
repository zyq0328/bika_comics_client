/*
 * @Date: 2020-01-13 20:47:10
 * @名称: 我的界面 - 头像 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 17:30:50
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerPunchTheClock.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserAutograph.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserDressUp.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserImages.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserImagesBox.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserLevel.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserName.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/Widget/MyBannerUserRank.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyMyBannerController extends StatefulWidget {
  MyMyBannerController({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyMyBannerControllerState createState() => _MyMyBannerControllerState();
}

class _MyMyBannerControllerState extends State<MyMyBannerController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffed97b7),
      height: ScreenUtil().setHeight(ScreenUtil.screenHeight / 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  child: GestureDetector(
                    child: Stack(
                      children: <Widget>[
                        MyMyBannerMyBannerUserImages(), //用户头像
                        MyBannerUserImagesBox(
                          experience: widget.userProfile.data.user.exp,
                        ), //用户等级 - 经验框
                        Visibility(
                          child: MyMyBannerMyBannerUserDressUp(
                            character: widget.userProfile.data.user
                                .character, //这里应该是png图片 例如https://i0.hdslb.com/bfs/album/2c13e920ce82fa31425aceecad31e144e220fa85.png
                          ), //用户装饰 / 头像框
                          visible: $hiddenHeadImagesBox,
                        ),
                      ],
                    ),
                    onTap: () {
                      $hiddenHeadImagesBox = !$hiddenHeadImagesBox;
                      setState(() {});
                    },
                  ),
                ),
                MyMyBannerMyBannerIconPunchTheClock(), //打卡
              ],
            ),
          ),
          MyMyBannerMyBannerUserLevel(
            level: widget.userProfile.data.user.level,
          ), //用户 等级
          MyMyBannerMyBannerUserName(
              name: widget.userProfile.data.user.name), // 用户名
          MyMyBannerMyBannerUserRank(
              rank: widget.userProfile.data.user.title), //用户头衔
          MyMyBannerMyBannerUserAutograph(
            autograph: widget.userProfile.data.user.slogan,
          ), //个性签名
        ],
      ),
    );
  }
}
