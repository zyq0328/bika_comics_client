/*
 * @Date: 2020-01-15 10:41:11
 * @名称: 用户 用户等级 - 经验框
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 17:16:49
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class MyBannerUserImagesBox extends StatefulWidget {
  MyBannerUserImagesBox({Key key, this.experience}) : super(key: key);
  final double experience;
  @override
  _MyBannerUserImagesBoxState createState() => _MyBannerUserImagesBoxState();
}

class _MyBannerUserImagesBoxState extends State<MyBannerUserImagesBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: new CircularPercentIndicator(
              radius: 100.0,
              lineWidth: 10.0,
              animation: true,
              percent: widget.experience,
              backgroundColor: Color(0xff555555),
              progressColor: Color(0xffce4e74),
            ),
            width: ScreenUtil().setWidth(100),
          ),
        ],
      ),
    );
  }
}
