/*
 * @Date: 2020-01-17 20:22:33
 * @名称: 个人 - 头像 - 头像框 / 装扮
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 17:05:42
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyMyBannerMyBannerUserDressUp extends StatefulWidget {
  MyMyBannerMyBannerUserDressUp({Key key, this.character}) : super(key: key);
  final String character;
  @override
  _MyMyBannerMyBannerUserDressUpState createState() =>
      _MyMyBannerMyBannerUserDressUpState();
}

class _MyMyBannerMyBannerUserDressUpState
    extends State<MyMyBannerMyBannerUserDressUp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Image.network(
        widget.character,
        width: ScreenUtil().setWidth(200.0),
      ),
    );
  }
}
