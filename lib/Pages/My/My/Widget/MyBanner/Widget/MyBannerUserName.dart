/*
 * @Date: 2020-01-13 20:51:37
 * @名称: 用户界面 - 用户 用户名
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 16:41:26
 */
import 'package:flutter/material.dart';

class MyMyBannerMyBannerUserName extends StatefulWidget {
  MyMyBannerMyBannerUserName({Key key, this.name}) : super(key: key);
  final String name;
  @override
  _MyMyBannerMyBannerUserNameState createState() =>
      _MyMyBannerMyBannerUserNameState();
}

class _MyMyBannerMyBannerUserNameState
    extends State<MyMyBannerMyBannerUserName> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        widget.name,
        style: TextStyle(
          color: Color(0xffffffff),
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
