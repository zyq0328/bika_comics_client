/*
 * @Date: 2020-01-13 20:51:37
 * @名称: 用户界面 - 用户头像
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:16:41
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyMyBannerMyBannerUserImages extends StatefulWidget {
  MyMyBannerMyBannerUserImages({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerUserImagesState createState() =>
      _MyMyBannerMyBannerUserImagesState();
}

class _MyMyBannerMyBannerUserImagesState
    extends State<MyMyBannerMyBannerUserImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(22),
      ),
      child: new Container(
        child: ClipOval(
          child: Image.network(
            'https://tvax1.sinaimg.cn/large/006ZEs2Bly1gav8kgr82dj308c08cq42.jpg',
          ),
        ),
        width: ScreenUtil().setWidth(146),
      ),
    );
  }
}
