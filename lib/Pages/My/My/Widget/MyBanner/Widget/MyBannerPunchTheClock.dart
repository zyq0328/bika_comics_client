/*
 * @Date: 2020-01-17 20:56:04
 * @名称: 打卡
 * @描述: 签到
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 17:30:04
 * @FilePath: /bika_comics_client/lib/Pages/My/My/Widget/MyBanner/Widget/MyBannerPunchTheClock.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class MyMyBannerMyBannerIconPunchTheClock extends StatefulWidget {
  MyMyBannerMyBannerIconPunchTheClock({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerIconPunchTheClockState createState() =>
      _MyMyBannerMyBannerIconPunchTheClockState();
}

class _MyMyBannerMyBannerIconPunchTheClockState
    extends State<MyMyBannerMyBannerIconPunchTheClock> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(18),
        left: ScreenUtil().setWidth(600),
      ),
      child: Column(
        children: <Widget>[
          Icon(
            IconFont.iconPunchTheClock,
            color: Color(0xffffffff),
            size: ScreenUtil().setWidth(100),
          ),
          Text(
            '打卡',
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: ScreenUtil().setSp(40),
            ),
          ),
        ],
      ),
    );
  }
}
