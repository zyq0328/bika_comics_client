/*
 * @Date: 2020-01-15 21:07:20
 * @名称: 用户 - 个性签名
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:10:51
 */

import 'package:flutter/material.dart';

class MyMyBannerMyBannerUserAutograph extends StatefulWidget {
  MyMyBannerMyBannerUserAutograph({Key key, this.autograph}) : super(key: key);
  final String autograph;
  @override
  _MyMyBannerMyBannerUserAutographState createState() =>
      _MyMyBannerMyBannerUserAutographState();
}

class _MyMyBannerMyBannerUserAutographState
    extends State<MyMyBannerMyBannerUserAutograph> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        widget.autograph,
        style: TextStyle(
          color: Color(0xfffdf4f7),
        ),
      ),
    );
  }
}
