/*
 * @Date: 2020-01-13 20:51:37
 * @名称: 用户界面 - 用户 等级
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:11:15
 */
import 'package:flutter/material.dart';

class MyMyBannerMyBannerUserLevel extends StatefulWidget {
  MyMyBannerMyBannerUserLevel({Key key, this.level}) : super(key: key);
  final int level;
  @override
  _MyMyBannerMyBannerUserLevelState createState() =>
      _MyMyBannerMyBannerUserLevelState();
}

class _MyMyBannerMyBannerUserLevelState
    extends State<MyMyBannerMyBannerUserLevel> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        'lv. ' + widget.level.toString(),
        style: TextStyle(
          color: Color(0xffffffff),
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
