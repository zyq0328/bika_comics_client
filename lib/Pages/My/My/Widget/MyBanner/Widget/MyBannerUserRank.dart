/*
 * @Date: 2020-01-15 20:34:29
 * @名称: 用户 - 用户头衔
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 16:41:40
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyMyBannerMyBannerUserRank extends StatefulWidget {
  MyMyBannerMyBannerUserRank({Key key, this.rank}) : super(key: key);
  final String rank;
  @override
  _MyMyBannerMyBannerUserRankState createState() =>
      _MyMyBannerMyBannerUserRankState();
}

class _MyMyBannerMyBannerUserRankState
    extends State<MyMyBannerMyBannerUserRank> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Container(
          height: ScreenUtil().setHeight(40),
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(10),
            right: ScreenUtil().setWidth(10),
            top: ScreenUtil().setHeight(5),
            bottom: ScreenUtil().setHeight(5),
          ),
          child: Text(
            widget.rank,
            style: TextStyle(
              height: ScreenUtil().setHeight(2.75), //!设置高度字体
              // backgroundColor: Color(0xffff0000),
              color: Color.fromRGBO(255, 124, 0, 1), // 字体颜色
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        decoration: new BoxDecoration(
          color: Color(0xffffe3a6), // 底色
          border:
              new Border.all(color: Color(0xFFd69e6b), width: 0.5), // 边色与边宽度
          //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
          borderRadius: new BorderRadius.vertical(
            top: Radius.elliptical(20, 20),
            bottom: Radius.elliptical(20, 20),
          ), // 也可控件一边圆角大小
        ),
      ),
    );
  }
}
