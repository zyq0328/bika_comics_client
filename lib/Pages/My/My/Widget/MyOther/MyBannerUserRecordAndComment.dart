/*
 * @Date: 2020-01-15 21:13:49
 * @名称: 个人界面 - 评论和记录
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 16:46:36
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';

class MyMyBannerMyBannerUserRecordAndComment extends StatefulWidget {
  MyMyBannerMyBannerUserRecordAndComment({Key key}) : super(key: key);

  @override
  _MyMyBannerMyBannerUserRecordAndCommentState createState() =>
      _MyMyBannerMyBannerUserRecordAndCommentState();
}

var select01 = Color(0xffed97b7);
var select02 = Color(0xffffffff);

class _MyMyBannerMyBannerUserRecordAndCommentState
    extends State<MyMyBannerMyBannerUserRecordAndComment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          GestureDetector(
            child: Container(
              height: ScreenUtil().setHeight(70),
              width: ScreenUtil().setWidth($global_My_MyBannerWidth / 2),
              alignment: Alignment.center,
              child: Text(
                '记录',
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: select01,
                    width: 5,
                  ),
                ), //
              ),
            ),
            onTap: () {
              select01 = Color(0xffed97b7);
              select02 = Color(0xffffffff);
              setState(() {});
            },
          ),
          GestureDetector(
            child: Container(
              height: ScreenUtil().setHeight(70),
              width: ScreenUtil().setWidth($global_My_MyBannerWidth / 2),
              alignment: Alignment.center,
              child: Text(
                '评论',
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: select02,
                    width: 5,
                  ),
                ), //
              ),
            ),
            onTap: () {
              select01 = Color(0xffffffff);
              select02 = Color(0xffed97b7);
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
