/*
 * @Date: 2020-01-13 20:31:03
 * @名称: 我的页面 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-20 12:57:09
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBox/Widget/MyboxHead.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyMyboxContorller extends StatefulWidget {
  MyMyboxContorller({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyMyboxContorllerState createState() => _MyMyboxContorllerState();
}

class _MyMyboxContorllerState extends State<MyMyboxContorller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: MyMyboxMyboxHead(userProfile: widget.userProfile),
    );
  }
}
