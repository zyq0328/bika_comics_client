/*
 * @Date: 2020-01-13 20:38:10
 * @名称: 我的框架头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:51:25
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/My/EditMy/EdirMy.dart';
import 'package:pica_acg/anime/LiuDong.dart';
import 'package:pica_acg/anime/anime.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyMyboxMyboxHead extends StatefulWidget {
  MyMyboxMyboxHead({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyMyboxMyboxHeadState createState() => _MyMyboxMyboxHeadState();
}

class _MyMyboxMyboxHeadState extends State<MyMyboxMyboxHead> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      child: Container(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                width: ScreenUtil().setWidth(750),
                height: ScreenUtil().setHeight(70),
                child: Text(
                  '我的',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 19.0,
                    color: Color(0xFF434343),
                    decoration: TextDecoration.none,
                  ),
                ),
                decoration: BoxDecoration(
                  border: new Border(
                    bottom: Divider.createBorderSide(
                      context,
                      color: Color.fromRGBO(229, 229, 231, 1),
                      width: 1,
                    ),
                  ), // 边色与边宽度
                ),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerRight,
                width: ScreenUtil().setWidth(750),
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.only(right: ScreenUtil().setHeight(40)),
                child: GestureDetector(
                  child: Text(
                    '编辑',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 19.0,
                      color: Color(0xFF434343),
                      decoration: TextDecoration.none,
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (context, _, __) => AnimeLiuDong(
                          widget: EditMy(
                            context: context,
                            userProfile: widget.userProfile,
                          ),
                        ),
                        opaque: false,
                      ),
                    );
                  },
                ),
                decoration: BoxDecoration(
                  border: new Border(
                    bottom: Divider.createBorderSide(
                      context,
                      color: Color.fromRGBO(229, 229, 231, 1),
                      width: 1,
                    ),
                  ), // 边色与边宽度
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
