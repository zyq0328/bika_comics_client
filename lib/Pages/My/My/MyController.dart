/*
 * @Date: 2020-01-13 20:25:18
 * @名称: 我的页面总控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:11:01
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBanner/MyBannerController.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyOther/MyBannerUserRecordAndComment.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBody/MyBodyController.dart';
import 'package:pica_acg/Pages/My/My/Widget/MyBox/MyboxContorller.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyController extends StatefulWidget {
  MyController({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyControllerState createState() => _MyControllerState();
}

class _MyControllerState extends State<MyController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          MyMyboxContorller(userProfile: widget.userProfile), //框架

          Expanded(
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              shrinkWrap: true,
              children: <Widget>[
                MyMyBannerController(userProfile: widget.userProfile), //头部
                MyMyBannerMyBannerUserRecordAndComment(), //记录和评论
                MyMyBodyController(), //身体
              ],
            ),
          ),
        ],
      ),
    );
  }
}
