/*
 * @Date: 2020-01-13 20:24:10
 * @名称: 我的页面
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 22:10:41
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/My/My/MyController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class My extends StatefulWidget {
  My({Key key}) : super(key: key);

  @override
  _MyState createState() => _MyState();
}

var $userProfile = UserProfile.fromJson($netWorkUserProfile);

class _MyState extends State<My> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  initState() {
    super.initState();
    NetWorkController.post(
      getUserProfile(),
      (callBack) {
        print(callBack);
        if (callBack['code'] == "200" && callBack['message'] == 'success') {
          $userProfile = UserProfile.fromJson(callBack);
          setState(() {});
        } else {
          Navigator.of(context).push(
            PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                      body: '获取用户信息失败',
                      height: ScreenUtil().setHeight(900),
                    ),
                opaque: false),
          );
        }
      },
      headers: getApiRequestHeadrs('User-Profile'),
    );
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: MyController(
            userProfile: $userProfile,
          ),
          // height: ScreenUtil().setHeight(1098),
          height: ScreenUtil().setHeight(ScreenUtil.screenHeight / 2.221),
        ),
      ],
    );
  }
}
