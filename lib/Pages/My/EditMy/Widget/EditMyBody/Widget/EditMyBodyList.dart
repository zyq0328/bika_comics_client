import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyEditMyBodyEditMyBodyList extends StatefulWidget {
  MyEditMyBodyEditMyBodyList({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyEditMyBodyEditMyBodyListState createState() =>
      _MyEditMyBodyEditMyBodyListState();
}

class _MyEditMyBodyEditMyBodyListState
    extends State<MyEditMyBodyEditMyBodyList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(1210),
      child: ListView(
        children: <Widget>[
          Container(
            color: Color(0xffffffff),
            child: HeadImages(),
            margin: EdgeInsets.only(
              bottom: ScreenUtil().setHeight(30),
            ),
          ),
          Basic(userProfile: widget.userProfile),
          SelfIntroduction(),
        ],
      ),
    );
  }
}

class Basic extends StatefulWidget {
  Basic({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _BasicState createState() => _BasicState();
}

class _BasicState extends State<Basic> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ),
        color: Color(0xffffffff),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Nickname(name: widget.userProfile.data.user.name),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                LandingID(email: widget.userProfile.data.user.email),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                DateOfBirth(
                  birthday: widget.userProfile.data.user.birthday,
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Gender(
                  gender: widget.userProfile.data.user.gender,
                ),
                Container(
                  child: Container(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

/*
 * 头像
 */
class HeadImages extends StatefulWidget {
  HeadImages({Key key}) : super(key: key);

  @override
  _HeadImagesState createState() => _HeadImagesState();
}

class _HeadImagesState extends State<HeadImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '头像',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 15,
            child: Container(
              child: ClipOval(
                child: Image.network(
                  'https://tvax1.sinaimg.cn/large/006ZEs2Bly1gav8kgr82dj308c08cq42.jpg',
                  height: ScreenUtil().setHeight(150),
                  fit: BoxFit.cover,
                ),
              ),
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Color(0xFFFF0000),
                  width: 0.5,
                ), // 边色与边宽度
                color: Color(0xFF9E9E9E), // 底色
                //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                borderRadius: new BorderRadius.vertical(
                  top: Radius.elliptical(50, 50),
                  bottom: Radius.elliptical(50, 50),
                ), // 也可控件一边圆角大小
              ),
            ),
          ),
          Expanded(flex: 10, child: Icon(Icons.chevron_right)),
        ],
      ),
      height: ScreenUtil().setHeight(175),
      decoration: BoxDecoration(
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ),
      ),
    );
  }
}

/*
 * 昵称
 */
class Nickname extends StatefulWidget {
  Nickname({Key key, this.name}) : super(key: key);
  final String name;
  @override
  _NicknameState createState() => _NicknameState();
}

class _NicknameState extends State<Nickname> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '昵称',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(30)),
              alignment: Alignment.centerRight,
              child: Text(
                widget.name,
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(30),
                  color: Color(0xffaaaaaa),
                ),
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(125),
    );
  }
}

/*
 * 登陆id
 */
class LandingID extends StatefulWidget {
  LandingID({Key key, this.email}) : super(key: key);
  final String email;
  @override
  _LandingIDState createState() => _LandingIDState();
}

class _LandingIDState extends State<LandingID> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '登陆 ID',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 30,
            child: Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(30)),
              child: Text(
                widget.email,
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(30),
                  color: Color(0xffaaaaaa),
                ),
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(125),
    );
  }
}

/*
 * 出生日期
 */
class DateOfBirth extends StatefulWidget {
  DateOfBirth({Key key, this.birthday}) : super(key: key);
  final String birthday;
  @override
  _DateOfBirthState createState() => _DateOfBirthState();
}

class _DateOfBirthState extends State<DateOfBirth> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '出生日期',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 20,
            child: Container(
              margin: EdgeInsets.only(right: ScreenUtil().setWidth(30)),
              alignment: Alignment.centerRight,
              child: Text(
                iSO_8601ToNewData(widget.birthday, true),
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(30),
                  color: Color(0xffaaaaaa),
                ),
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(125),
    );
  }
}

/*
 * 性别
 */
class Gender extends StatefulWidget {
  Gender({Key key, this.gender}) : super(key: key);
  final String gender;
  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  @override
  var gender = '绅士';
  Widget build(BuildContext context) {
    switch (widget.gender) {
      case 'm':
        gender = '绅士';
        break;

      case 'bot':
        gender = '机器人';
        break;

      default:
    }
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 50,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '性别',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(
              child: Text(
                '$gender',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(30),
                  color: Color(0xffaaaaaa),
                ),
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(125),
    );
  }
}

/*
 * 自我介绍
 */
class SelfIntroduction extends StatefulWidget {
  SelfIntroduction({Key key}) : super(key: key);

  @override
  _SelfIntroductionState createState() => _SelfIntroductionState();
}

FocusNode _commentFocus = FocusNode();

class _SelfIntroductionState extends State<SelfIntroduction> {
  @override
  void initState() {
    _commentFocus.addListener(() {
      //获取焦点
      setState(() {});
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              '自我介绍',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenUtil().setSp(30),
                color: Color(0xffaaaaaa),
              ),
            ),
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(30),
              left: ScreenUtil().setHeight(30),
              bottom: ScreenUtil().setHeight(20),
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(300),
            child: TextField(
              autofocus: false,
              obscureText: false,
              textInputAction: TextInputAction.done,
              maxLines: 10,
              focusNode: _commentFocus,
              decoration: InputDecoration(
                border: InputBorder.none, //去掉输入框的下滑线
                fillColor: Colors.white,
                filled: true,
                hintText: "写一下阁下想表达的绅士之情或对哔咔娘的爱意之类什么的（最大20字）",
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 13.0,
                ),
                // contentPadding: EdgeInsets.all(5.0),
                enabledBorder: null,
                disabledBorder: null,
              ),
              onSubmitted: (value) {
                print("onSubmitted 点击了键盘的确定按钮，输出的信息是：${value}");
                setState(() {});
              },
            ),
            decoration: BoxDecoration(
              border: new Border(
                top: Divider.createBorderSide(
                  context,
                  color: Color(0xffc8c7cc),
                  width: 1,
                ),
                bottom: Divider.createBorderSide(
                  context,
                  color: Color(0xffc8c7cc),
                  width: 1,
                ),
              ),
              color: Color(0xffffffff),
            ),
          ),
        ],
      ),
    );
  }
}
