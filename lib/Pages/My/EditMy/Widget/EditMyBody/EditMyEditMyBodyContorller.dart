import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/EditMy/Widget/EditMyBody/Widget/EditMyBodyList.dart'; 
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class MyEditMyEditMyBodyContorller extends StatefulWidget {
  MyEditMyEditMyBodyContorller({Key key, this.userProfile}) : super(key: key);
  final UserProfile userProfile;
  @override
  _MyEditMyEditMyBodyContorllerState createState() =>
      _MyEditMyEditMyBodyContorllerState();
}

class _MyEditMyEditMyBodyContorllerState
    extends State<MyEditMyEditMyBodyContorller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(248, 248, 248, 1),
      child: Column(
        children: <Widget>[
          MyEditMyBodyEditMyBodyList(userProfile: widget.userProfile),
        ],
      ),
    );
  }
}
