/*
 * @Date: 2020-01-19 23:57:42
 * @名称: 个人 - 编辑 - 框架 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 20:54:45
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class MyEditMyEditMyBoxEditMyBoxHead extends StatefulWidget {
  MyEditMyEditMyBoxEditMyBoxHead({Key key, this.context}) : super(key: key);
  final BuildContext context;

  @override
  _MyEditMyEditMyBoxEditMyBoxHeadState createState() =>
      _MyEditMyEditMyBoxEditMyBoxHeadState();
}

class _MyEditMyEditMyBoxEditMyBoxHeadState
    extends State<MyEditMyEditMyBoxEditMyBoxHead> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      height: ScreenUtil().setHeight(124),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            child: GestureDetector(
              child: Text(
                languAre().$languAgeOrdinary["basic"]["return"],
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 19.0,
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
              onTap: () {
                Navigator.pop(widget.context);
              },
            ),
          ),
          Container(
            child: Text(
              '编辑绅士资料',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
          Container(
            child: Text(
              '保存',
              textAlign: TextAlign.right,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
