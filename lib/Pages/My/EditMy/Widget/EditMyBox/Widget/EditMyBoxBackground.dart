/*
 * @Date: 2020-01-19 23:57:42
 * @名称: 个人 - 编辑 - 框架 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-20 13:41:50
 */
import 'package:flutter/material.dart';

class MyEditMyEditMyBoxEditMyBoxBackground extends StatefulWidget {
  MyEditMyEditMyBoxEditMyBoxBackground({Key key}) : super(key: key);

  @override
  _MyEditMyEditMyBoxEditMyBoxBackgroundState createState() =>
      _MyEditMyEditMyBoxEditMyBoxBackgroundState();
}

class _MyEditMyEditMyBoxEditMyBoxBackgroundState
    extends State<MyEditMyEditMyBoxEditMyBoxBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(255, 254, 254, 254),
    );
  }
}
