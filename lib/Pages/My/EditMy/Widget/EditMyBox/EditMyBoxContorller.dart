/*
 * @Date: 2020-01-19 23:55:12
 * @名称: 个人 - 编辑 - 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-20 14:15:20
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/EditMy/Widget/EditMyBox/Widget/EditMyBoxBackground.dart';
import 'package:pica_acg/Pages/My/EditMy/Widget/EditMyBox/Widget/EditMyBoxHead.dart';

class MyEdirMyMyEditMyBoxContorller extends StatefulWidget {
  MyEdirMyMyEditMyBoxContorller({Key key, this.context}) : super(key: key);
  final BuildContext context;

  @override
  _MyEdirMyMyEditMyBoxContorllerState createState() =>
      _MyEdirMyMyEditMyBoxContorllerState();
}

class _MyEdirMyMyEditMyBoxContorllerState
    extends State<MyEdirMyMyEditMyBoxContorller> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        MyEditMyEditMyBoxEditMyBoxBackground(),
        MyEditMyEditMyBoxEditMyBoxHead(context: widget.context),
      ],
    );
  }
}
