/*
 * @Date: 2020-01-19 23:54:11
 * @名称: 我的 - 编辑
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-29 17:05:26
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/My/EditMy/Widget/EditMyBody/EditMyEditMyBodyContorller.dart';
import 'package:pica_acg/Pages/My/EditMy/Widget/EditMyBox/EditMyBoxContorller.dart';
import 'package:pica_acg/tools/extends/Json/User/UserProfile.dart';

class EditMy extends StatefulWidget {
  EditMy({Key key, this.userProfile, this.context}) : super(key: key);
  final UserProfile userProfile;
  final BuildContext context;
  @override
  _EditMyState createState() => _EditMyState();
}

class _EditMyState extends State<EditMy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            MyEdirMyMyEditMyBoxContorller(context: widget.context),
            MyEditMyEditMyBodyContorller(userProfile: widget.userProfile),
          ],
        ),
      ),
    );
  }
}
