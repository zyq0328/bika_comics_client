/*
 * @Date: 2020-01-19 23:57:42
 * @名称: 设置 - 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-21 20:27:52
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingsSettingsBoxContorllerSettingsBox extends StatefulWidget {
  SettingsSettingsBoxContorllerSettingsBox({Key key, this.context})
      : super(key: key);
  final BuildContext context;

  @override
  _SettingsSettingsBoxContorllerSettingsBoxState createState() =>
      _SettingsSettingsBoxContorllerSettingsBoxState();
}

class _SettingsSettingsBoxContorllerSettingsBoxState
    extends State<SettingsSettingsBoxContorllerSettingsBox> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      height: ScreenUtil().setHeight(124),
      child: Container(
        width: ScreenUtil().setWidth(1334),
        margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(50),
        ),
        child: Text(
          'BIKA 设置',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 19.0,
            color: Color(0xFF434343),
            decoration: TextDecoration.none,
          ),
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
