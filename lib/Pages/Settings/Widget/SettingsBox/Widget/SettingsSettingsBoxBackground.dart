/*
 * @Date: 2020-01-19 23:57:42
 * @名称: 设置 - 框架 - 背景颜色
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-29 18:10:22
 */
import 'package:flutter/material.dart';

class SettingsSettingsBoxContorllerSettingsSettingsBoxBackground extends StatefulWidget {
  SettingsSettingsBoxContorllerSettingsSettingsBoxBackground({Key key}) : super(key: key);

  @override
  _SettingsSettingsBoxContorllerSettingsSettingsBoxBackgroundState createState() =>
      _SettingsSettingsBoxContorllerSettingsSettingsBoxBackgroundState();
}

class _SettingsSettingsBoxContorllerSettingsSettingsBoxBackgroundState
    extends State<SettingsSettingsBoxContorllerSettingsSettingsBoxBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffffffff),
    );
  }
}
