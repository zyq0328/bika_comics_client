/*
 * @Date: 2020-01-19 23:55:12
 * @名称: 设置 - 框架 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-21 20:28:46
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBox/Widget/SettingsSettingsBox.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBox/Widget/SettingsSettingsBoxBackground.dart';

class SettingSettingBoxContorller extends StatefulWidget {
  SettingSettingBoxContorller({
    Key key,
  }) : super(key: key);
  @override
  _SettingSettingBoxContorllerState createState() =>
      _SettingSettingBoxContorllerState();
}

class _SettingSettingBoxContorllerState
    extends State<SettingSettingBoxContorller> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SettingsSettingsBoxContorllerSettingsSettingsBoxBackground(),
        SettingsSettingsBoxContorllerSettingsBox(),
      ],
    );
  }
}
