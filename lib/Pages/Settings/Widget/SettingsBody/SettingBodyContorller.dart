/*
 * @Date: 2020-01-29 17:59:50
 * @名称: 设置 - 内容 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-04-22 13:39:11
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/Exit/SettingSettingBodyExitContorller.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/OtherSettings/SettingsSettingsBodySettingsBodyOtherSettingsContorller.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/ReadingSettings/SettingsSettingsBodySettingsBodyReadingSettingsContorller.dart';

class SettingSettingBodyContorller extends StatefulWidget {
  SettingSettingBodyContorller({Key key})
      : super(key: settingSettingBodyContorllerState);

  @override
  SettingSettingBodyContorllerState createState() =>
      SettingSettingBodyContorllerState();
}

class SettingSettingBodyContorllerState
    extends State<SettingSettingBodyContorller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SettingsSettingsBodySettingsBodyReadingSettingsContorller(),
        SettingsSettingsBodySettingsBodyOtherSettingsContorller(),
        SettingSettingBodyExitContorller(),
      ],
    );
  }

  updateView() {
    setState(() {});
  }
}
