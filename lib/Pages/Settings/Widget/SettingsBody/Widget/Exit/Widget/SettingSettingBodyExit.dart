/*
 * @Date: 2020-01-30 18:37:36
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-31 13:48:41
 * @FilePath: /pica_acg/lib/Pages/Settings/Widget/SettingsBody/Widget/Exit/Widget/SettingSettingBodyExit.dart
 */
/*
 * @Date: 2020-01-30 18:37:36
 * @名称: 设置 - 退出登录
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-30 18:44:37
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingSettingBodyExit extends StatefulWidget {
  SettingSettingBodyExit({Key key}) : super(key: key);

  @override
  _SettingSettingBodyExitState createState() => _SettingSettingBodyExitState();
}

class _SettingSettingBodyExitState extends State<SettingSettingBodyExit> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffed5545),
      width: ScreenUtil().setWidth(700),
      height: ScreenUtil().setHeight(120),
      alignment: Alignment.center,
      child: Text(
        'BIKA 登出',
        style: TextStyle(
          fontSize: ScreenUtil().setSp(40),
          fontWeight: FontWeight.w700,
          color: Color(0xffffffff)
        ),
      ),
    );
  }
}
