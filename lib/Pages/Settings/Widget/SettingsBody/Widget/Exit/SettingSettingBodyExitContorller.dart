import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/Exit/Widget/SettingSettingBodyExit.dart';

class SettingSettingBodyExitContorller extends StatefulWidget {
  SettingSettingBodyExitContorller({Key key}) : super(key: key);

  @override
  _SettingSettingBodyExitContorllerState createState() =>
      _SettingSettingBodyExitContorllerState();
}

class _SettingSettingBodyExitContorllerState
    extends State<SettingSettingBodyExitContorller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SettingSettingBodyExit(),
    );
  }
}
