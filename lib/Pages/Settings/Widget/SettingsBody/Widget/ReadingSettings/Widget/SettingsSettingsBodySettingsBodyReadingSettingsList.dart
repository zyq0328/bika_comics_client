/*
 * @Date: 2020-01-29 18:15:26
 * @名称: 设置 - 阅读设置 - 阅读设置 字
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:18:02
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kumi_popup_window/kumi_popup_window.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalVarlable.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList
    extends StatefulWidget {
  SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList(
      {Key key})
      : super(key: key);

  @override
  _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState
      createState() =>
          _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState();
}

class _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState
    extends State<
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList> {
  @override
  Widget build(BuildContext context) {
    return ReadingSettings();
  }
}

class ReadingSettings extends StatefulWidget {
  ReadingSettings({Key key}) : super(key: key);
  @override
  _ReadingSettingsState createState() => _ReadingSettingsState();
}

class _ReadingSettingsState extends State<ReadingSettings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ),
        color: Color(0xffffffff),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                ScreenDirection(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                PageDirection(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                AutoFlippingInterval(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                NightMode(
                  gender: 'widget.userProfile.data.user.gender',
                ),
                Container(
                  child: Container(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

/*
 * 屏幕方向
 */
class ScreenDirection extends StatefulWidget {
  ScreenDirection({
    Key key,
  }) : super(key: key);
  @override
  _ScreenDirectionState createState() => _ScreenDirectionState();
}

class _ScreenDirectionState extends State<ScreenDirection> {
  @override
  Widget build(BuildContext context) {
    KumiPopupWindow popupWindow; //弹窗
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '屏幕方向',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: GestureDetector(
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  (setting.screenOrientation == 0) ? '垂直阅读' : "横屏阅读",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: ScreenUtil().setSp(35),
                    color: Color(0xffed97b7),
                  ),
                ),
              ),
              onTap: () {
//showPopupWindow 和 createPopupWindow 均可
                popupWindow = showPopupWindow(
                  context,
                  // gravity: KumiPopupGravity.leftTop,
                  bgColor: Colors.grey.withOpacity(0.5),
                  clickOutDismiss: true,
                  clickBackDismiss: true,
                  customAnimation: false,
                  customPop: false,
                  customPage: false,
                  //targetRenderBox: (btnKey.currentContext.findRenderObject() as RenderBox),
                  //childSize: null,
                  underStatusBar: false,
                  underAppBar: true,
                  offsetX: 0,
                  offsetY: 0,
                  duration: Duration(milliseconds: 200),
                  onShowStart: (pop) {
                    print("showStart");
                  },
                  onShowFinish: (pop) {
                    print("showFinish");
                  },
                  onDismissStart: (pop) {
                    print("dismissStart");
                  },
                  onDismissFinish: (pop) {
                    print("dismissFinish");
                  },
                  onClickOut: (pop) {
                    print("onClickOut");
                  },
                  onClickBack: (pop) {
                    print("onClickBack");
                  },
                  childFun: (pop) {
                    return Container(
                      key: GlobalKey(),
                      child: TispList(
                        {
                          "title": "选择阅读本子时的屏幕方向",
                          "titleSize": ScreenUtil().setSp(40),
                          "a_list": [
                            {
                              "text": "垂直阅读",
                              "color": "0xffed98b8",
                              "fontSize": ScreenUtil().setSp(40),
                              'function': () => {
                                    setting.screenOrientation = 0,
                                    updateSetting("ScreenOrientation", false),
                                    popupWindow.dismiss(context),
                                  }
                            },
                            {
                              "text": "横向阅读",
                              "color": "0xffed98b8",
                              'function': () => {
                                    setting.screenOrientation = 1,
                                    updateSetting("ScreenOrientation", true),
                                    popupWindow.dismiss(context),
                                  }
                            },
                          ],
                          "exitTitle": "取消",
                          "exitFuncton": () => {
                                // Navigator.pop(context),

                                popupWindow.dismiss(context),
                              },
                        },
                      ),
                    );
                  },
                );

                /* return;
                Navigator.of(context).push(
                  PageRouteBuilder(
                    pageBuilder: (context, _, __) => GlobalList(
                      {
                        "title": "选择阅读本子时的屏幕方向",
                        "titleSize": ScreenUtil().setSp(30),
                        "a_list": [
                          {
                            "text": "垂直阅读",
                            "color": "0xffed98b8",
                            "fontSize": ScreenUtil().setSp(40),
                            'function': () => {
                                  setting.screenOrientation = 0,
                                  settingSettingBodyContorllerState.currentState
                                      .updateSetting(
                                          "ScreenOrientation", false),
                                  Navigator.pop(context),
                                }
                          },
                          {
                            "text": "横向阅读",
                            "color": "0xffed98b8",
                            'function': () => {
                                  setting.screenOrientation = 1,
                                  settingSettingBodyContorllerState.currentState
                                      .updateSetting("ScreenOrientation", true),
                                  Navigator.pop(context),
                                }
                          },
                        ],
                        "exitTitle": "取消",
                        "exitFuncton": () => {
                              Navigator.pop(context),
                            },
                      },
                    ),
                    opaque: false,
                  ),
                ); */
                setState(() {});
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: Icon(
              IconFont.iconRight,
              color: Color(0xffc4c4c6),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 翻页方向
 */
class PageDirection extends StatefulWidget {
  PageDirection({
    Key key,
  }) : super(key: key);
  @override
  _PageDirectionState createState() => _PageDirectionState();
}

class _PageDirectionState extends State<PageDirection> {
  @override
  Widget build(BuildContext context) {
    KumiPopupWindow popupWindow; //弹窗
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '翻页方向',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: GestureDetector(
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  (setting.pageTurningMode == 0) ? '左右翻页' : "上下翻页",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: ScreenUtil().setSp(35),
                    color: Color(0xffed97b7),
                  ),
                ),
              ),
              onTap: () {
                popupWindow = showPopupWindow(
                  context,
                  // gravity: KumiPopupGravity.leftTop,
                  bgColor: Colors.grey.withOpacity(0.5),
                  clickOutDismiss: true,
                  clickBackDismiss: true,
                  customAnimation: false,
                  customPop: false,
                  customPage: false,
                  //targetRenderBox: (btnKey.currentContext.findRenderObject() as RenderBox),
                  //childSize: null,
                  underStatusBar: false,
                  underAppBar: true,
                  offsetX: 0,
                  offsetY: 0,
                  duration: Duration(milliseconds: 200),
                  onShowStart: (pop) {
                    print("showStart");
                  },
                  onShowFinish: (pop) {
                    print("showFinish");
                  },
                  onDismissStart: (pop) {
                    print("dismissStart");
                  },
                  onDismissFinish: (pop) {
                    print("dismissFinish");
                  },
                  onClickOut: (pop) {
                    print("onClickOut");
                  },
                  onClickBack: (pop) {
                    print("onClickBack");
                  },
                  childFun: (pop) {
                    return Container(
                      key: GlobalKey(),
                      child: TispList(
                        {
                          "title": "选择阅读本子时翻页方向",
                          "titleSize": ScreenUtil().setSp(40),
                          "a_list": [
                            {
                              "text": "上下翻页",
                              "color": "0xffed98b8",
                              "fontSize": ScreenUtil().setSp(40),
                              'function': () => {
                                    setting.pageTurningMode = 1,
                                    updateSetting("PageTurningMode", true),
                                    popupWindow.dismiss(context),
                                  }
                            },
                            {
                              "text": "左右翻页",
                              "color": "0xffed98b8",
                              'function': () => {
                                    setting.pageTurningMode = 0,
                                    updateSetting("PageTurningMode", false),
                                    popupWindow.dismiss(context),
                                  }
                            },
                          ],
                          "exitTitle": "取消",
                          "exitFuncton": () => {
                                popupWindow.dismiss(context),
                              },
                        },
                      ),
                    );
                  },
                );
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: Icon(
              IconFont.iconRight,
              color: Color(0xffc4c4c6),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 自动翻页间隔
 */
class AutoFlippingInterval extends StatefulWidget {
  AutoFlippingInterval({
    Key key,
  }) : super(key: key);
  @override
  _AutoFlippingIntervalState createState() => _AutoFlippingIntervalState();
}

class _AutoFlippingIntervalState extends State<AutoFlippingInterval> {
  @override
  Widget build(BuildContext context) {
    KumiPopupWindow popupWindow; //弹窗
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '自动翻页间隔',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                child: Text(
                  '${setting.pageTurningInterval} 秒',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: ScreenUtil().setSp(35),
                    color: Color(0xffed97b7),
                  ),
                ),
                onTap: () {
                  popupWindow = showPopupWindow(
                    context,
                    gravity: KumiPopupGravity.center,
                    bgColor: Colors.grey.withOpacity(0.5),
                    clickOutDismiss: true,
                    clickBackDismiss: true,
                    customAnimation: false,
                    customPop: false,
                    customPage: false,
                    //targetRenderBox: (btnKey.currentContext.findRenderObject() as RenderBox),
                    //childSize: null,
                    underStatusBar: false,
                    underAppBar: true,
                    offsetX: 0,
                    offsetY: -ScreenUtil().setHeight(50),
                    duration: Duration(milliseconds: 200),
                    onShowStart: (pop) {
                      print("showStart");
                    },
                    onShowFinish: (pop) {
                      print("showFinish");
                    },
                    onDismissStart: (pop) {
                      print("dismissStart");
                    },
                    onDismissFinish: (pop) {
                      print("dismissFinish");
                    },
                    onClickOut: (pop) {
                      print("onClickOut");
                    },
                    onClickBack: (pop) {
                      print("onClickBack");
                    },
                    childFun: (pop) {
                      return Container(
                        key: GlobalKey(),
                        child: TispList(
                          {
                            "title": "选择阅读本子时翻页方向",
                            "titleSize": ScreenUtil().setSp(40),
                            "a_list": [
                              {
                                "text": "0.5 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 0.5,
                                      updateSetting("PageTurningInterval", 0.5),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "1.0 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 1.0,
                                      updateSetting("PageTurningInterval", 1.0),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "1.5 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 1.5,
                                      updateSetting("PageTurningInterval", 1.5),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "2.0 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 2.0,
                                      updateSetting("PageTurningInterval", 2.0),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "2.5 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 2.5,
                                      updateSetting("PageTurningInterval", 2.5),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "3.0 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 3.0,
                                      updateSetting("PageTurningInterval", 3.0),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "3.5 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 3.5,
                                      updateSetting("PageTurningInterval", 3.5),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "4.0 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 4.0,
                                      updateSetting("PageTurningInterval", 4.0),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "4.5 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 4.5,
                                      updateSetting("PageTurningInterval", 4.5),
                                      popupWindow.dismiss(context),
                                    }
                              },
                              {
                                "text": "5.0 秒",
                                "color": "0xffed98b8",
                                "fontSize": ScreenUtil().setSp(40),
                                'function': () => {
                                      setting.pageTurningInterval = 5.0,
                                      updateSetting("PageTurningInterval", 5.0),
                                      popupWindow.dismiss(context),
                                    }
                              }
                            ],
                            "exitTitle": "取消",
                            "exitFuncton": () => {
                                  popupWindow.dismiss(context),
                                },
                          },
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Icon(
              IconFont.iconRight,
              color: Color(0xffc4c4c6),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 夜间模式
 */
class NightMode extends StatefulWidget {
  NightMode({Key key, this.gender}) : super(key: key);
  final String gender;
  @override
  _NightModeState createState() => _NightModeState();
}

class _NightModeState extends State<NightMode> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '夜间模式',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: CupertinoSwitch(
                activeColor: Color(0xffed97b7),
                value: (setting.nightMode == 0) ? false : true,
                onChanged: (bool value) {
                  setState(() {
                    if (value == true) {
                      setting.nightMode = 1;
                    } else {
                      setting.nightMode = 0;
                    }
                    updateSetting('NightMode', value);
                  });
                },
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/* class GlobalList extends StatefulWidget {
  /*
   * 列表
   */
  GlobalList(this.list, {this.widghtWidth, Key key}) : super(key: key);
  final Map list;
  num widghtWidth; //宽度
  @override
  _GlobalListState createState() => _GlobalListState();
}

class _GlobalListState extends State<GlobalList> {
  @override
  void initState() {
    super.initState();
    if (widget.widghtWidth == null) {
      widget.widghtWidth = ScreenUtil().setWidth(550);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listBuildSuccess = List.generate(
      widget.list['a_list'].length,
      (index) {
        return Container(
          width: widget.widghtWidth,
          height: ScreenUtil().setWidth(80),
          child: GestureDetector(
            child: Container(
              height: ScreenUtil().setHeight(52),
              alignment: Alignment.center,
              child: Text(
                widget.list['a_list'][index]['text'],
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: HexColor(widget.list['a_list'][index]['color']),
                  fontSize: (widget.list['a_list'][index]['fontSize'] != null)
                      ? widget.list['a_list'][index]['fontSize']
                      : ScreenUtil().setSp(40),
                ),
              ),
            ),
            onTap: () {
              widget.list['a_list'][index]['function']();
            },
          ),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 0.5,
                color: Color(0xffb2b1b1),
              ),
            ),
          ),
        );
      },
    );
    return ClipRRect(
      borderRadius: new BorderRadius.vertical(
        top: Radius.elliptical(20, 20),
        bottom: Radius.elliptical(20, 20),
      ),
      child: Container(
        color: Colors.red,
        child: Container(
          //一个盒子
          color: Color(0xffffffff), //盒子背景颜色
          // height: ScreenUtil().setHeight(930),
          child: Column(
            // 竖排列表
            children: <Widget>[
              Container(
                //标题
                width: widget.widghtWidth,
                height: ScreenUtil().setHeight(100),
                alignment: Alignment.center,
                child: Text(
                  widget.list['title'],
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Color(0xff000000),
                      fontSize: (widget.list['titleSize'] != null)
                          ? widget.list['titleSize']
                          : ScreenUtil().setSp(65),
                      fontWeight: FontWeight.w700),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xFFb1b0b2),
                      width: 0.5,
                    ),
                  ),
                ),
              ),
              Column(
                children: listBuildSuccess,
              ),
              Container(
                //按钮
                height: ScreenUtil().setHeight(100),
                child: GestureDetector(
                  child: Text(
                    widget.list['exitTitle'],
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Color(0xfffe4439),
                        fontSize: ScreenUtil().setSp(45),
                        fontWeight: FontWeight.w700),
                  ),
                  onTap: () {
                    widget.list['exitFuncton']();
                  },
                ),
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
 */
