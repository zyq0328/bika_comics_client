/*
 * @Date: 2020-01-29 18:05:43
 * @名称: 设置 - 其他设置 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:11:47
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/ReadingSettings/Widget/SettingsSettingsBodySettingsBodyReadingSettingsList.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/ReadingSettings/Widget/SettingsSettingsBodySettingsBodyReadingSettingsText.dart';

class SettingsSettingsBodySettingsBodyReadingSettingsContorller
    extends StatefulWidget {
  SettingsSettingsBodySettingsBodyReadingSettingsContorller({Key key})
      : super(key: key);

  @override
  _SettingsSettingsBodySettingsBodyReadingSettingsContorllerState
      createState() =>
          _SettingsSettingsBodySettingsBodyReadingSettingsContorllerState();
}

class _SettingsSettingsBodySettingsBodyReadingSettingsContorllerState
    extends State<SettingsSettingsBodySettingsBodyReadingSettingsContorller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingTexts(),
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList(),
      ],
    );
  }
}
