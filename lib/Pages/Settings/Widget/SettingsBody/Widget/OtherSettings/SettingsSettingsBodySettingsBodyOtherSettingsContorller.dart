/*
 * @Date: 2020-01-29 18:05:43
 * @名称: 设置 - 阅读设置 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:17:51
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/OtherSettings/Widget/SettingsSettingsBodySettingsBodyOtherSettingsList.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/Widget/OtherSettings/Widget/SettingsSettingsBodySettingsBodyOtherSettingsText.dart';

class SettingsSettingsBodySettingsBodyOtherSettingsContorller
    extends StatefulWidget {
  SettingsSettingsBodySettingsBodyOtherSettingsContorller({Key key})
      : super(key: key);

  @override
  _SettingsSettingsBodySettingsBodyOtherSettingsContorllerState createState() =>
      _SettingsSettingsBodySettingsBodyOtherSettingsContorllerState();
}

class _SettingsSettingsBodySettingsBodyOtherSettingsContorllerState
    extends State<SettingsSettingsBodySettingsBodyOtherSettingsContorller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingText(),
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList(),
      ],
    );
  }
}
