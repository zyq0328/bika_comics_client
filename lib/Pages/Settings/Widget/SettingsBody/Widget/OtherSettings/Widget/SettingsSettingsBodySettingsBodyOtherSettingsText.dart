/*
 * @Date: 2020-01-29 18:15:26
 * @名称: 设置 - 阅读设置 - 阅读设置 字
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-29 19:11:53
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingText
    extends StatefulWidget {
  SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingText(
      {Key key})
      : super(key: key);

  @override
  _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingTextState
      createState() =>
          _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingTextState();
}

class _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingTextState
    extends State<
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(100),
      width: ScreenUtil().setWidth(1334),
      color: Color(0xffe9e9eb),
      child: Container(
        margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(35),
          left: ScreenUtil().setWidth(35),
        ),
        child: Text(
          '其他',
          style: TextStyle(
            color: Color(
              0xff77777c,
            ),
            fontSize: ScreenUtil().setSp(
              30,
            ),
          ),
        ),
      ),
    );
  }
}
