/*
 * @Date: 2020-01-29 18:15:26
 * @名称: 设置 - 其他设置 - 其他设置 字
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:19:12
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalVarlable.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList
    extends StatefulWidget {
  SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList(
      {Key key})
      : super(key: key);

  @override
  _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState
      createState() =>
          _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState();
}

class _SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingListState
    extends State<
        SettingSettingBodySettingBodyReadingSettingSettingBodySettingBoxReadingSettingList> {
  @override
  Widget build(BuildContext context) {
    return ReadingSettings();
  }
}

class ReadingSettings extends StatefulWidget {
  ReadingSettings({Key key}) : super(key: key);
  @override
  _ReadingSettingsState createState() => _ReadingSettingsState();
}

class _ReadingSettingsState extends State<ReadingSettings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffc8c7cc),
            width: 1,
          ),
        ),
        color: Color(0xffffffff),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Account(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                ClearCache(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                AddPostDownloadPrompt(),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(30),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: new Border(
                        bottom: Divider.createBorderSide(
                          context,
                          color: Color.fromRGBO(229, 229, 231, 1),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                OpenAppAndContinueDownloading(),
                Container(
                  child: Container(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

/*
 * 账户
 */
class Account extends StatefulWidget {
  Account({
    Key key,
  }) : super(key: key);
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '账户',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                '变更密码',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(35),
                  color: Color(0xffed97b7),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Icon(
              IconFont.iconRight,
              color: Color(0xffc4c4c6),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 清除缓存
 */
class ClearCache extends StatefulWidget {
  ClearCache({
    Key key,
  }) : super(key: key);
  @override
  _ClearCacheState createState() => _ClearCacheState();
}

class _ClearCacheState extends State<ClearCache> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '清除缓存',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                '检测及清除',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(35),
                  color: Color(0xffed97b7),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Icon(
              IconFont.iconRight,
              color: Color(0xffc4c4c6),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 添加下载后提示
 */
class AddPostDownloadPrompt extends StatefulWidget {
  AddPostDownloadPrompt({
    Key key,
  }) : super(key: key);
  @override
  _AddPostDownloadPromptState createState() => _AddPostDownloadPromptState();
}

class _AddPostDownloadPromptState extends State<AddPostDownloadPrompt> {
  var valueb = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '添加下载后提示',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: CupertinoSwitch(
                activeColor: Color(0xffed97b7),
                value: (setting.addDownloadTips == 0) ? false : true,
                onChanged: (bool value) {
                  if (value == true) {
                    setting.addDownloadTips = 1;
                  } else {
                    setting.addDownloadTips = 0;
                  }
                  setState(() {});
                },
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 打开App后继续下载
 */
class OpenAppAndContinueDownloading extends StatefulWidget {
  OpenAppAndContinueDownloading({
    Key key,
  }) : super(key: key);
  @override
  _OpenAppAndContinueDownloadingState createState() =>
      _OpenAppAndContinueDownloadingState();
}

class _OpenAppAndContinueDownloadingState
    extends State<OpenAppAndContinueDownloading> {
  var valueb = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: Container(
              margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
              child: Text(
                '打开App后继续下载',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(95, 95, 95, 1),
                  fontSize: ScreenUtil().setSp(35),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: CupertinoSwitch(
                activeColor: Color(0xffed97b7),
                value:
                    (setting.openAppToContinueDownloading == 0) ? false : true,
                onChanged: (bool value) {
                  if (value == true) {
                    setting.openAppToContinueDownloading = 1;
                  } else {
                    setting.openAppToContinueDownloading = 0;
                  }
                  setState(() {});
                },
              ),
            ),
          ),
        ],
      ),
      height: ScreenUtil().setHeight(90),
    );
  }
}

/*
 * 自我介绍
 */
class SelfIntroduction extends StatefulWidget {
  SelfIntroduction({Key key}) : super(key: key);

  @override
  _SelfIntroductionState createState() => _SelfIntroductionState();
}

FocusNode _commentFocus = FocusNode();

class _SelfIntroductionState extends State<SelfIntroduction> {
  @override
  void initState() {
    _commentFocus.addListener(() {
      //获取焦点
      setState(() {});
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              '自我介绍',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenUtil().setSp(30),
                color: Color(0xffaaaaaa),
              ),
            ),
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(30),
              left: ScreenUtil().setHeight(30),
              bottom: ScreenUtil().setHeight(20),
            ),
          ),
          Container(
            height: ScreenUtil().setWidth(300),
            child: TextField(
              autofocus: false,
              obscureText: false,
              textInputAction: TextInputAction.done,
              maxLines: 10,
              focusNode: _commentFocus,
              decoration: InputDecoration(
                border: InputBorder.none, //去掉输入框的下滑线
                fillColor: Colors.white,
                filled: true,
                hintText: "写一下阁下想表达的绅士之情或对哔咔娘的爱意之类什么的（最大20字）",
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 13.0,
                ),
                // contentPadding: EdgeInsets.all(5.0),
                enabledBorder: null,
                disabledBorder: null,
              ),
              onSubmitted: (value) {
                print("onSubmitted 点击了键盘的确定按钮，输出的信息是：${value}");
                setState(() {});
              },
            ),
            decoration: BoxDecoration(
              border: new Border(
                top: Divider.createBorderSide(
                  context,
                  color: Color(0xffc8c7cc),
                  width: 1,
                ),
                bottom: Divider.createBorderSide(
                  context,
                  color: Color(0xffc8c7cc),
                  width: 1,
                ),
              ),
              color: Color(0xffffffff),
            ),
          ),
        ],
      ),
    );
  }
}
