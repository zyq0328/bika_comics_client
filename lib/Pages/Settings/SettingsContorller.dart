/*
 * @Date: 2020-01-29 17:29:38
 * @名称: 设置 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-21 20:28:39
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/SettingBodyContorller.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBox/SettingsSettingsBoxContorller.dart';

class SettingContorller extends StatefulWidget {
  SettingContorller({
    Key key,
  }) : super(key: key);
  @override
  _SettingContorllerState createState() => _SettingContorllerState();
}

class _SettingContorllerState extends State<SettingContorller>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            SettingSettingBoxContorller(),
            SettingSettingBodyContorller(),
          ],
        ),
      ],
    );
  }
}
