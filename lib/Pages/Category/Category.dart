/*
 * @Date: 2020-01-02 09:54:42
 * @名称: 分类页面
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 21:33:57
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryController.dart';

class Category extends StatefulWidget {
  Category({Key key}) : super(key: key);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category>
    with AutomaticKeepAliveClientMixin {
  bool get wantKeepAlive => true;
  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return Container(
      child: CategoryController(),
    );
  }
}
