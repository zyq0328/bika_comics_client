/*
 * @Date: 2020-08-22 23:24:39
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 00:12:37
 * @FilePath: /bica_acg/lib/Pages/Category/Widget/CategoryBox/CategoryBoxController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryBox/Widget/CategoryBoxSelect.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryBox/Widget/CategoryBoxTitle.dart';

class CategoryCategoryBoxController extends StatefulWidget {
  CategoryCategoryBoxController({Key key}) : super(key: key);

  @override
  _CategoryCategoryBoxControllerState createState() =>
      _CategoryCategoryBoxControllerState();
}

class _CategoryCategoryBoxControllerState
    extends State<CategoryCategoryBoxController> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return Container(
      // color: Color(0xFFFF0000),
      height: ScreenUtil().setHeight(1334),
      child: Column(
        children: <Widget>[
          CategoryCategoryBoxControllerCategoryBoxTitle(), //标题
          CategoryCategoryBoxControllerCategorySelect(), //搜索框
        ],
      ),
    );
  }
}
