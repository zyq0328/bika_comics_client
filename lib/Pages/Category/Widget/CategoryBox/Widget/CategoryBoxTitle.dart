/*
 * @Date: 2020-01-02 10:01:46
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 19:14:26
 * @FilePath: /bika_comics_client/lib/Pages/Category/Widget/CategoryBox/Widget/CategoryBoxTitle.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoryCategoryBoxControllerCategoryBoxTitle extends StatefulWidget {
  CategoryCategoryBoxControllerCategoryBoxTitle({Key key}) : super(key: key);

  @override
  _CategoryCategoryBoxControllerCategoryBoxTitleState createState() =>
      _CategoryCategoryBoxControllerCategoryBoxTitleState();
}

class _CategoryCategoryBoxControllerCategoryBoxTitleState
    extends State<CategoryCategoryBoxControllerCategoryBoxTitle> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(70),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      alignment: Alignment.center,
      child: Text(
        '分类',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 19.0,
          color: Color.fromRGBO(237, 153, 184, 1),
          decoration: TextDecoration.none,
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
