/*
 * @Date: 2020-01-02 10:16:05
 * @名称:搜索框
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 19:25:28
 * @FilePath: /bika_comics_client/lib/Pages/Category/Widget/CategoryBox/Widget/CategoryBoxSelect.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

/*
 * 分裂页面的搜索框
 */
class CategoryCategoryBoxControllerCategorySelect extends StatefulWidget {
  CategoryCategoryBoxControllerCategorySelect({Key key}) : super(key: key);

  @override
  _CategoryCategoryBoxControllerCategorySelectState createState() =>
      _CategoryCategoryBoxControllerCategorySelectState();
}

class _CategoryCategoryBoxControllerCategorySelectState
    extends State<CategoryCategoryBoxControllerCategorySelect> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(98), //设置内容高度
      // color: Color(0xffff0000),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(15.0),
        right: ScreenUtil().setWidth(15.0),
        top: ScreenUtil().setWidth(15.0),
        bottom: ScreenUtil().setWidth(15.0),
      ),
      child: TextField(
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            /*边角*/
            borderRadius: BorderRadius.all(
              Radius.circular(30), //边角为30
            ),
            borderSide: BorderSide(
              color: Colors.amber, //边线颜色为黄色
              width: 2, //边线宽度为2
            ),
          ),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.green, //边框颜色为绿色
            width: 5, //宽度为5
          )),
          labelText: "搜索作品、作者、标记、上传者",
          // errorText: "errorText",
          // hintText: "hintText",
          prefixIcon: Icon(
            IconFont.iconSearch,
          ),
        ),
      ),
      // Row(
      //   children: <Widget>[
      //     Icon(
      //       IconFont.iconSearch,
      //       color: Color(0xFFacabaf),
      //     ),
      //     Container(
      //       width: ScreenUtil().setWidth(687),
      //       color: Color(0xFFFF00),
      //       child: TextField(
      //         decoration: InputDecoration(
      //           labelText: '搜索作品、作者、标记、上传者',
      //         ),
      //         /* style: TextStyle(
      //           fontSize: 17,
      //           color: Color(0xFFacabaf),
      //         ), */
      //       ),
      //       // margin: EdgeInsets.only(left: ScreenUtil().setWidth(10.0)),
      //     ),
      //   ],
      // ),
    );
  }
}
