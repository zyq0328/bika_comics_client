/*
 * @Date: 2020-01-02 11:22:35
 * @名称: 分类页面关键字 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 11:17:36
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryEveryoneSearch/Widget/CategoryEveryoneSearchBox.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryEveryoneSearch/Widget/CategoryEveryoneSearchKeyword.dart';

class CategoryCategoryEveryoneSearchContraller extends StatefulWidget {
  CategoryCategoryEveryoneSearchContraller({Key key}) : super(key: key);

  @override
  _CategoryCategoryEveryoneSearchContrallerState createState() =>
      _CategoryCategoryEveryoneSearchContrallerState();
}

class _CategoryCategoryEveryoneSearchContrallerState
    extends State<CategoryCategoryEveryoneSearchContraller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CategoryCategoryBoxCategoryEveryoneSearchBox(),
        CategoryCategoryEveryoneSearchKeyword(),
      ],
    );
  }
}
