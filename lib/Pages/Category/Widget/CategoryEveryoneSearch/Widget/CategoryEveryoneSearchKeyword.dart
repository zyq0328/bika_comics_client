/*
 * @Date: 2020-01-09 12:58:22
 * @名称: 分类页面关键字
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 14:08:59
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Category/CategoryEveryoneSearch/category_everyone_search.dart';

class CategoryCategoryEveryoneSearchKeyword extends StatefulWidget {
  CategoryCategoryEveryoneSearchKeyword({Key key}) : super(key: key);

  @override
  _CategoryCategoryEveryoneSearchKeywordState createState() =>
      _CategoryCategoryEveryoneSearchKeywordState();
}

CategoryEveryoneSearch categoryEveryoneSearchJson =
    CategoryEveryoneSearch.fromJson($getCategoryEveryoneSearch);

class _CategoryCategoryEveryoneSearchKeywordState
    extends State<CategoryCategoryEveryoneSearchKeyword> {
  @override
  initState() {
    super.initState();
    NetWorkController.get(
      getCategoryEveryoneSearch(),
      (data) {
        if (data['code'] == "200" && data['message'] == 'success') {
          categoryEveryoneSearchJson = CategoryEveryoneSearch.fromJson(data);
          setState(() {});
        } else if (data['code'] == "-1") {
          Navigator.of(context).push(
            PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                      body: '获取关键字失败',
                      height: ScreenUtil().setHeight(900),
                    ),
                opaque: false),
          );
        } else {
          requestErrorParsing(data, $context: context);
        }
      },
      headers: getApiRequestHeadrs("CategoryEveryoneSearch"),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      child:
          /* ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: ModuleText(),
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey))),
          );
        },
      ), */
          WrapWidget(categoryEveryoneSearchJson: categoryEveryoneSearchJson),
    );
  }
}

class WrapWidget extends StatefulWidget {
  WrapWidget({Key key, this.categoryEveryoneSearchJson}) : super(key: key);
  final CategoryEveryoneSearch categoryEveryoneSearchJson;
  @override
  _WrapWidgetState createState() => _WrapWidgetState();
}

class _WrapWidgetState extends State<WrapWidget> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 10, //主轴上子控件的间距
      runSpacing: 5, //交叉轴上子控件之间的间距
      children: List.generate(
        categoryEveryoneSearchJson.data.keywords.length,
        (index) {
          return SizedBox(
            child: ModuleText(
              keyWord: categoryEveryoneSearchJson.data.keywords[index],
            ),
          );
        },
      ),
    );
  }
}

class ModuleText extends StatefulWidget {
  ModuleText({Key key, this.keyWord}) : super(key: key);
  final keyWord;
  @override
  _ModuleTextState createState() => _ModuleTextState();
}

class _ModuleTextState extends State<ModuleText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: ScreenUtil().setHeight(40),
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(10),
          right: ScreenUtil().setWidth(10),
          top: ScreenUtil().setHeight(5),
          bottom: ScreenUtil().setHeight(5),
        ),
        child: Text(
          widget.keyWord,
          style: TextStyle(
            height: ScreenUtil().setHeight(2.75), //!设置高度字体
            // backgroundColor: Color(0xffff0000),
            color: Color.fromRGBO(255, 124, 0, 1), // 字体颜色
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      decoration: new BoxDecoration(
        color: Color(0xffffe3a6), // 底色
        border: new Border.all(color: Color(0xFFd69e6b), width: 0.5), // 边色与边宽度
        //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
        borderRadius: new BorderRadius.vertical(
          top: Radius.elliptical(20, 20),
          bottom: Radius.elliptical(20, 20),
        ), // 也可控件一边圆角大小
      ),
    );
  }
}
