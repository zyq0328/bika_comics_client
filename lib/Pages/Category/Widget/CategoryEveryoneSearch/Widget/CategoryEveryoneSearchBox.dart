/*
 * @Date: 2020-01-09 12:14:13
 * @名称: 分类页面-大家都在搜索的关键字 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-09 12:36:16
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class CategoryCategoryBoxCategoryEveryoneSearchBox extends StatefulWidget {
  CategoryCategoryBoxCategoryEveryoneSearchBox({Key key}) : super(key: key);

  @override
  _CategoryCategoryBoxCategoryEveryoneSearchBoxState createState() =>
      _CategoryCategoryBoxCategoryEveryoneSearchBoxState();
}

class _CategoryCategoryBoxCategoryEveryoneSearchBoxState
    extends State<CategoryCategoryBoxCategoryEveryoneSearchBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //把整个内容包起来
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        bottom: ScreenUtil().setHeight(15),
      ),
      child: Row(
        //横向排列
        children: <Widget>[
          Icon(
            //logo
            IconFont.iconProfile,
            color: Color(0xFFe486aa),
            size: 30,
          ),
          Text(
            //文字
            '大家都在搜索的关键字',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFFe486aa),
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}
