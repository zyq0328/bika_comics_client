/*
 * @Date: 2020-01-02 09:57:26
 * @名称: 分类页面总控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:13:23
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryBox/CategoryBoxController.dart';
import 'package:pica_acg/Pages/Category/Widget/CategoryEveryoneSearch/CategoryEveryoneSearchContraller.dart';
import 'package:pica_acg/Pages/Category/Widget/PopularClassification/PopularClassificationController.dart';

class CategoryController extends StatefulWidget {
  CategoryController({Key key}) : super(key: key);

  @override
  _CategoryControllerState createState() => _CategoryControllerState();
}

class _CategoryControllerState extends State<CategoryController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              child: CategoryCategoryBoxController(), //标题 搜索框
              height: ScreenUtil()
                  .setHeight($global_Category_PopularClassificationBoxHeight),
            ), //框架
            Expanded(
              child: MediaQuery.removePadding(
                removeTop: true,
                context: context,
                child: Container(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          CategoryCategoryEveryoneSearchContraller(), //大家都在搜
                          PopularClassificationController(), //热门分类
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
