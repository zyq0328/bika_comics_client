/*
 * @Date: 2020-01-11 11:27:44
 * @名称: 分类 热门分类 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 11:36:33
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class PopularClassificationBox extends StatefulWidget {
  PopularClassificationBox({Key key}) : super(key: key);

  @override
  _PopularClassificationBoxState createState() =>
      _PopularClassificationBoxState();
}

class _PopularClassificationBoxState extends State<PopularClassificationBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //把整个内容包起来
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        bottom: ScreenUtil().setHeight(15),
      ),
      child: Row(
        //横向排列
        children: <Widget>[
          Icon(
            //logo
            IconFont.iconProfile,
            color: Color(0xFFe486aa),
            size: 30,
          ),
          Text(
            //文字
            '热门分类',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFFe486aa),
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}
