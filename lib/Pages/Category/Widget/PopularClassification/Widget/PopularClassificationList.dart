/*
 * @Date: 2020-01-11 11:36:12
 * @名称: 分类 热门分类 列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 19:15:25
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Comics/ComicsContorller.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Category/PopularClassification/popular_classification.dart';

class PopularClassificationPopularClassificationList extends StatefulWidget {
  PopularClassificationPopularClassificationList({Key key}) : super(key: key);

  @override
  _PopularClassificationPopularClassificationListState createState() =>
      _PopularClassificationPopularClassificationListState();
}

class _PopularClassificationPopularClassificationListState
    extends State<PopularClassificationPopularClassificationList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: OneView(),
    );
  }
}

class OneView extends StatefulWidget {
  OneView({Key key}) : super(key: key);

  @override
  _OneViewState createState() => _OneViewState();
}

PopularClassification classificationList =
    PopularClassification.fromJson($getPopularClassification);

class _OneViewState extends State<OneView> {
  @override
  initState() {
    super.initState();
    NetWorkController.get(
      getPopularClassificationList(),
      (data) {
        if (data['code'] == "200" && data['message'] == 'success') {
          classificationList = PopularClassification.fromJson(data);
          setState(() {});
        } else if (data['code'] == "-1") {
          Navigator.of(context).push(
            PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                      body: '获取关键字失败',
                      height: ScreenUtil().setHeight(900),
                    ),
                opaque: false),
          );
        } else {
          requestErrorParsing(data, $context: context);
        }
      },
      headers: getApiRequestHeadrs("PopularClassification"),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(15)),
      // height: ScreenUtil().setHeight(830),
      child: Wrap(
        spacing: 20, //主轴上子控件的间距
        runSpacing: 10, //交叉轴上子控件之间的间距
        children: List.generate(
          classificationList.data.categories.length,
          (index) {
            return SizedBox(
              child: GestureDetector(
                child: ModuleImagesText(
                  images: springboard() +
                      classificationList
                          .data.categories[index].thumb.fileServer +
                      "/static/" +
                      classificationList.data.categories[index].thumb.path,
                  text: classificationList.data.categories[index].title,
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => ComicsContorller(
                          title:
                              classificationList.data.categories[index].title),
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}

class ModuleImagesText extends StatefulWidget {
  ModuleImagesText({
    Key key,
    this.images,
    this.text,
  }) : super(key: key);
  final String images;
  final String text;
  @override
  _ModuleImagesTextState createState() => _ModuleImagesTextState();
}

class _ModuleImagesTextState extends State<ModuleImagesText> {
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new ClipRRect(
          borderRadius: new BorderRadius.vertical(
            top: Radius.elliptical(20, 20),
            bottom: Radius.elliptical(20, 20),
          ),
          child: Image.network(
            widget.images,
            height: ScreenUtil().setWidth(214),
            width: ScreenUtil().setWidth(214),
            fit: BoxFit.fill,
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
          child: Text(
            widget.text,
            style: new TextStyle(
              fontSize: 17.0,
              color: const Color(0xFFeea1be),
              fontFamily: "Roboto",
            ),
          ),
        ),
      ],
    );
  }
}
