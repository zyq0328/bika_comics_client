/*
 * @Date: 2020-01-11 11:19:16
 * @名称: 分类页面 热门分类 - 总控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-11 11:42:24
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Category/Widget/PopularClassification/Widget/PopularClassificationBox.dart';
import 'package:pica_acg/Pages/Category/Widget/PopularClassification/Widget/PopularClassificationList.dart';

class PopularClassificationController extends StatefulWidget {
  PopularClassificationController({Key key}) : super(key: key);

  @override
  _PopularClassificationControllerState createState() =>
      _PopularClassificationControllerState();
}

class _PopularClassificationControllerState
    extends State<PopularClassificationController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        PopularClassificationBox(),
        PopularClassificationPopularClassificationList(),
      ],
    );
  }
}
