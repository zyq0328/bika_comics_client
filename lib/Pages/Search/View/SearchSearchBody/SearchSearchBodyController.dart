import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalVarlableJson.dart';
import 'package:pica_acg/Pages/ComicsInfo/ComicsInfoController.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyAuthor/SearchSearchBodySearchBodyAuthor.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyChineseTeam/SearchSearchBodySearchBodyChineseTeam.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyClass/SearchSearchBodySearchBodyClass.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyImages/SearchSearchBodySearchBodyImages.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyLike/SearchSearchBodySearchBodyLike.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodySpecialLabel/SearchSearchBodySearchBodySpecialLabel.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyTitle/SearchSearchBodySearchBodyTitle.dart';
import 'package:pica_acg/tools/extends/Json/Comics/comics_list.dart'
    as comicsList;
import 'package:pica_acg/tools/extends/Json/Search/Search.dart';

class SearchBodyController extends StatefulWidget {
  SearchBodyController({Key key, this.list}) : super(key: key);
  final SearchJson list;
  @override
  SearchBodyControllerState createState() => SearchBodyControllerState();
}

List<Docs> hiddenData = [];

class SearchBodyControllerState extends State<SearchBodyController> {
  @override
  Widget build(BuildContext context) {
    aaaaaa();
    return Expanded(
      child: MediaQuery.removePadding(
        removeTop: true,
        removeLeft: true,
        removeRight: true,
        removeBottom: true,
        context: context,
        child: Container(
          child: ListView.separated(
            itemCount: widget.list.data.comics.docs.length,
            itemBuilder: (BuildContext context, int index) {
              var info = widget.list.data.comics.docs[index];
              return GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(25),
                    bottom: ScreenUtil().setHeight(10),
                  ),
                  child: Row(
                    children: <Widget>[
                      SearchSearchBodyComicsBodyImages(
                        src: springboard() +
                            info.thumb.fileServer +
                            info.thumb.path,
                      ),
                      Container(
                        height: ScreenUtil().setWidth(252),
                        width: ScreenUtil().setWidth(440),
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SearchSearchBodyComicsBodyTitle(
                              text: info.title,
                              page: info.pagesCount,
                            ),
                            SearchSearchBodyComicsBodyChineseTeam(
                              chineseTeam: info.chineseTeam,
                            ),
                            SearchSearchBodyComicsBodyAuthor(
                              author: info.author,
                            ),
                            SearchSearchBodyComicsBodyClass(
                              list: info.categories,
                              comicsId: info.id,
                            ),
                            SearchSearchBodyComicsBodyLike(
                              likeLength: info.likesCount,
                            ),
                          ],
                        ),
                      ),
                      SearchSearchBodyCOmicsBodySpecialLabel(),
                    ],
                  ),
                  /* decoration: BoxDecoration(
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: Color.fromRGBO(229, 229, 231, 1),
                    width: 2,
                  ),
                ), // 边色与边宽度
              ), */
                ),
                onTap: () {
                  var $aaa = info.toJson();
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => ComicsInfoController(
                        title: info.title.toString(),
                        author: comicsList.Docs.fromJson($aaa).author,
                        categories: info.categories,
                        likeNumber: info.likesCount,
                        comicsId: info.id,
                      ),
                    ),
                  );
                },
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                color: Colors.grey,
              );
            },
          ),
        ),
      ),
    );
  }

  void classificationUpdate() {
    //更新视图 隐藏了分类或者显示了分类在调用
    widget.list.data.comics.docs.addAll(hiddenData);
    hiddenData = [];
    aaaaaa();
  }

  bool aaaaaa() {
    for (var i = 0; i < widget.list.data.comics.docs.length; i++) {
      var list = widget.list.data.comics.docs[i].categories;
      for (var categoriesKeyi = 0;
          categoriesKeyi < list.length;
          categoriesKeyi++) {
        if ($GlobalFilterClassInfo[list[categoriesKeyi]] != null &&
            $GlobalFilterClassInfo[list[categoriesKeyi]]["Selection"] ==
                false) {
          hiddenData.add(widget.list.data.comics.docs[i]);
          widget.list.data.comics.docs.removeAt(i);
        }
      }
    }
    setState(() {});
  }
}
