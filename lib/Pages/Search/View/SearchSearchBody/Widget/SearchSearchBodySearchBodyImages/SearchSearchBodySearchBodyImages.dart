/*
 * @Date: 2020-01-31 17:47:57
 * @名称: 搜索 - 列表 - 图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 20:55:36
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyImages/SearchSearchBodySearchBodyImages.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchSearchBodyComicsBodyImages extends StatefulWidget {
  SearchSearchBodyComicsBodyImages({Key key, this.src}) : super(key: key);
  final String src;
  @override
  _SearchSearchBodyComicsBodyImagesState createState() =>
      _SearchSearchBodyComicsBodyImagesState();
}

class _SearchSearchBodyComicsBodyImagesState
    extends State<SearchSearchBodyComicsBodyImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(188),
      height: ScreenUtil().setHeight(250),
      child: Image.network(
        widget.src,
        fit: BoxFit.cover,
      ),
    );
  }
}
