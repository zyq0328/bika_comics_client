/*
 * @Date: 2020-01-31 18:00:52
 * @名称: 分类 - 漫画分类 - 列表 - 协作者
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 21:10:47
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyChineseTeam/SearchSearchBodySearchBodyChineseTeam.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/tools/extends/Json/Search/Search.dart';

class SearchSearchBodyComicsBodyChineseTeam extends StatefulWidget {
  SearchSearchBodyComicsBodyChineseTeam({Key key, this.chineseTeam})
      : super(key: key);
  final List<ChineseTeam> chineseTeam;
  @override
  _SearchSearchBodyComicsBodyChineseTeamState createState() =>
      _SearchSearchBodyComicsBodyChineseTeamState();
}

class _SearchSearchBodyComicsBodyChineseTeamState
    extends State<SearchSearchBodyComicsBodyChineseTeam> {
  @override
  Widget build(BuildContext context) {
    if (widget.chineseTeam == null) {
      return Row();
    } else {
      return Row(
        children: <Widget>[
          Text('协作者:'),
          Container(
            child: Wrap(
              spacing: 5.0,
              runSpacing: 0.0,
              children: List.generate(
                (widget.chineseTeam != null) ? widget.chineseTeam.length : 0,
                (index) {
                  return Text(
                    widget.chineseTeam[index].name,
                    style: TextStyle(
                      color: Color(0xff4b8bf5),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      );
    }
  }
}
