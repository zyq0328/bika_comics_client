/*
 * @Date: 2020-01-31 18:00:52
 * @名称: 分类 - 漫画分类 - 列表 - 作者
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 21:10:59
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyAuthor/SearchSearchBodySearchBodyAuthor.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/tools/extends/Json/Search/Search.dart';

class SearchSearchBodyComicsBodyAuthor extends StatefulWidget {
  SearchSearchBodyComicsBodyAuthor({Key key, this.author}) : super(key: key);
  final List<Author> author;
  @override
  _SearchSearchBodyComicsBodyAuthorState createState() =>
      _SearchSearchBodyComicsBodyAuthorState();
}

class _SearchSearchBodyComicsBodyAuthorState
    extends State<SearchSearchBodyComicsBodyAuthor> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('创作者: '),
        Container(
          child: Wrap(
            spacing: 5.0,
            runSpacing: 0.0,
            children: List.generate(
              (widget.author != null) ? widget.author.length : 0,
              (index) {
                return Text(
                  widget.author[index].name,
                  style: TextStyle(
                    color: Color(0xffee9ab9),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
