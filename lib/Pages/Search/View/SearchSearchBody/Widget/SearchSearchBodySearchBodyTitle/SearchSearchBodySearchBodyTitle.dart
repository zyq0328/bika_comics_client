/*
 * @Date: 2020-01-31 17:43:22
 * @名称: 搜索 - 列表 - 标题
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 20:55:29
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBody/Widget/SearchSearchBodySearchBodyTitle/SearchSearchBodySearchBodyTitle.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';

class SearchSearchBodyComicsBodyTitle extends StatefulWidget {
  SearchSearchBodyComicsBodyTitle({Key key, this.text, this.page})
      : super(key: key);
  final String text;
  final int page;
  @override
  _SearchSearchBodyComicsBodyTitleState createState() =>
      _SearchSearchBodyComicsBodyTitleState();
}

class _SearchSearchBodyComicsBodyTitleState
    extends State<SearchSearchBodyComicsBodyTitle> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      // height: ScreenUtil().setHeight(120),
      child: SizedBox(
        child: RollWidget(
          direction: Axis.horizontal,
          child: RollWidget(
            direction: Axis.horizontal,
            child: Text(
              widget.text + ' (${widget.page}P)',
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenUtil().setSp(30),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
