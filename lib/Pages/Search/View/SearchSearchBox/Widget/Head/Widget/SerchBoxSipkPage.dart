/*
 * @Date: 2020-08-22 22:07:58
 * @名称: 搜索页面 - 搜索控制器 - 搜索框架控制器 - 跳页按钮
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-22 22:31:30
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBox/Widget/SerchBoxSipkPage.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class SerchBoxSipkPage extends StatelessWidget {
  const SerchBoxSipkPage({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Visibility(
        visible: true,
        child: GestureDetector(
          child: Container(
            child: Text(
              languAre().$languAgeSearchController["box"]["skip_page"],
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
          onTap: () {},
        ),
      ),
    );
  }
}
