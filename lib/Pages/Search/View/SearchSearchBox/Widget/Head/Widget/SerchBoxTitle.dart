/*
 * @Date: 2020-08-22 22:07:58
 * @名称: 搜索页面 - 搜索控制器 - 搜索框架控制器 - 标题
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-22 22:30:37
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBox/Widget/SerchBoxTitle.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class SerchBoxTitle extends StatelessWidget {
  const SerchBoxTitle({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 6,
      child: Text(
        languAre().$languAgeSearchController["box"]["title"] + title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 19.0,
          color: Color(0xFF434343),
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}
