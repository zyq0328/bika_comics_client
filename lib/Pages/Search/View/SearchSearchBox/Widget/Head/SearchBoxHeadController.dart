/*
 * @Date: 2020-08-22 22:04:20
 * @名称: 搜索 - 框架 - 头部控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 00:17:13
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBox/Widget/Head/SearchBoxHeadController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/Head/Widget/SerchBoxBack.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/Head/Widget/SerchBoxSipkPage.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/Head/Widget/SerchBoxTitle.dart';

class SearchBoxHeadController extends StatefulWidget {
  SearchBoxHeadController({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _SearchBoxHeadControllerState createState() =>
      _SearchBoxHeadControllerState();
}

class _SearchBoxHeadControllerState extends State<SearchBoxHeadController> {
  @override
  Widget build(BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      height: ScreenUtil().setHeight(70),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          SerchBoxBack(),
          SerchBoxTitle(title: widget.title),
          SerchBoxSipkPage(),
        ],
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffbfbfbf),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
