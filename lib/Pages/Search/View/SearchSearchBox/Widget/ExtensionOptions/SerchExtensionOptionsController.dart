/*
 * @Date: 2020-08-22 22:40:51
 * @名称: 搜索 - 框架 - 扩展选项
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-22 22:45:39
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBox/Widget/ExtensionOptions/SerchExtensionOptionsController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/ExtensionOptions/Widget/ExtensionOptionsOrder.dart';

class SerchExtensionOptionsController extends StatefulWidget {
  SerchExtensionOptionsController({Key key}) : super(key: key);

  @override
  _SerchExtensionOptionsControllerState createState() =>
      _SerchExtensionOptionsControllerState();
}

class _SerchExtensionOptionsControllerState
    extends State<SerchExtensionOptionsController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          ExtensionOptionsOrder(),
        ],
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color(0x5fbfbfbf),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
