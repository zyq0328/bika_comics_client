/*
 * @Date: 2020-08-22 22:04:20
 * @名称: 搜索 - 框架控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-23 00:04:42
 * @FilePath: /bica_acg/lib/Pages/Search/View/SearchSearchBox/SerchBoxController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/ExtensionOptions/SerchExtensionOptionsController.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/Widget/Head/SearchBoxHeadController.dart';
import 'package:pica_acg/tools/Widget/FilterClass/FilterClass.dart';

class SerchBoxController extends StatefulWidget {
  SerchBoxController({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _SerchBoxControllerState createState() => _SerchBoxControllerState();
}

class _SerchBoxControllerState extends State<SerchBoxController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SearchBoxHeadController(
            title: widget.title,
          ),
          FilterClass(),
          SerchExtensionOptionsController(),
        ],
      ),
    );
  }
}
