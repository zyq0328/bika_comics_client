/*
 * @Date: 2020-04-07 21:19:06
 * @名称: 搜索控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-24 22:49:44
 * @FilePath: /bica_acg/lib/Pages/Search/SearchController.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBody/SearchSearchBodyController.dart';
import 'package:pica_acg/Pages/Search/View/SearchSearchBox/SerchBoxController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Search/Search.dart';

class SearchController extends StatefulWidget {
  SearchController({Key key}) : super(key: key);

  @override
  _SearchControllerState createState() => _SearchControllerState();
}

class _SearchControllerState extends State<SearchController> {
  @override
  SearchJson $search = SearchJson.fromJson($netWorkSearch);
  void initState() {
    super.initState();
    NetWorkController.post(
      getSearch(),
      (callBack) {
        if (callBack["code"] == "200") {
          $search = SearchJson.fromJson(callBack);
          setState(() {});
        } else {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '获取目录失败',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        }
      },
      data: {
        "keyword": "暗杀教室",
        "sort": "ld",
        "categories": "",
      },
      headers: getApiRequestHeadrs('Comment/search'),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SerchBoxController(title: "暗杀教室"),
          SearchBodyController(
            list: $search,
          ),
        ],
      ),
    );
  }
}
