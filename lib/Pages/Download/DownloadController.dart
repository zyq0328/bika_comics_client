/*
 * @Date: 2020-07-15 19:50:04
 * @名称: 下载页面控制器
 * @描述: 下载页面的控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 20:07:52
 * @FilePath: /bika_comics_client/lib/Pages/Download/DownloadController.dart
 * @最后编辑: 初雪桜
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/DownloadDownloadBodyDownloadDownloadListController.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBox/DownloadDownloadBoxController.dart';

class DownloadController extends StatefulWidget {
  DownloadController({Key key}) : super(key: key);

  @override
  _DownloadControllerState createState() => _DownloadControllerState();
}

class _DownloadControllerState extends State<DownloadController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        DownloadDownloadBoxController(),
        DownloadDownloadBodyDownloadDownloadListController(),
      ],
    );
  }
}
