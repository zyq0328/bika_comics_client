/*
 * @Date: 2020-07-15 19:52:33
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 19:54:04
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBox/DownloadDownloadBoxController.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBox/Widget/DownloadDownloadBoxDownloadBoxTitle.dart';

class DownloadDownloadBoxController extends StatefulWidget {
  DownloadDownloadBoxController({Key key}) : super(key: key);

  @override
  _DownloadDownloadBoxControllerState createState() =>
      _DownloadDownloadBoxControllerState();
}

class _DownloadDownloadBoxControllerState
    extends State<DownloadDownloadBoxController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DownloadDownloadBoxDownloadBoxTitle(),
    );
  }
}
