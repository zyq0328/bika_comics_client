/*
 * @Date: 2020-01-02 10:01:46
 * @名称: 下载页面标题
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 19:53:47
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBox/Widget/DownloadDownloadBoxDownloadBoxTitle.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DownloadDownloadBoxDownloadBoxTitle extends StatefulWidget {
  DownloadDownloadBoxDownloadBoxTitle({Key key}) : super(key: key);

  @override
  _DownloadDownloadBoxDownloadBoxTitleState createState() =>
      _DownloadDownloadBoxDownloadBoxTitleState();
}

class _DownloadDownloadBoxDownloadBoxTitleState
    extends State<DownloadDownloadBoxDownloadBoxTitle> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight(70),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      alignment: Alignment.center,
      child: Text(
        '下载详情',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 19.0,
          color: Color.fromRGBO(237, 153, 184, 1),
          decoration: TextDecoration.none,
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
