/*
 * @Date: 2020-07-15 22:58:33
 * @名称: 下载列表 - 漫画详细信息
 * @描述: 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 22:59:06
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadComicsInfo/DownloadDownloadBodyDownloadDownloadComicsInfoController.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
