/*
 * @Date: 2020-07-15 19:59:33
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-16 22:40:08
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/DownloadDownloadBodyDownloadDownloadListController.dart
 * @最后编辑: 初雪桜
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/DataBaseFunction.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListAuthor/DownloadListAuthor.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListAuthorClass/DownloadListAuthorClass.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListChineseTeam/DownloadListChineseTeam.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListImages/DownloadListImages.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListLike/DownloadListLike.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListProgressBar/DownloadListProgressBar.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListTitle/DownloadListTitle.dart';

class DownloadDownloadBodyDownloadDownloadListController
    extends StatefulWidget {
  DownloadDownloadBodyDownloadDownloadListController({Key key})
      : super(key: downloadDownloadBodyDownloadDownloadListControllerState);

  @override
  DownloadDownloadBodyDownloadDownloadListControllerState createState() =>
      DownloadDownloadBodyDownloadDownloadListControllerState();
}

class DownloadDownloadBodyDownloadDownloadListControllerState
    extends State<DownloadDownloadBodyDownloadDownloadListController> {
  @override
  var $downloadList = [];
  double $subVolumeProgress = 0; //分卷进度
  Map<String, Map<String, dynamic>> $downloadImagesList = {};
  /* {
    "xxx":{//文章id
      "xxx":{
        "success":10;//下载完成数量
        "downloadMax":100;//总需要数量
        "list":[];//总需要数量
      }//章节id
    }
  } */
  void initState() {
    super.initState();

    dataBaseSelect("BookDownloadChapterImagesList", null, null).then(
      (value) {
        for (var item in value) {
          if ($downloadImagesList[item['BookId']] == null) {
            //创建漫画
            $downloadImagesList[item['BookId']] = {};
          }
          if ($downloadImagesList[item['BookId']]['success'] == null) {
            $downloadImagesList[item['BookId']]['success'] = 0;
          }
          if ($downloadImagesList[item['BookId']][item['ChapterId']] == null) {
            //创建漫画分卷
            $downloadImagesList[item['BookId']][item['ChapterId']] = {};
          }
          if ($downloadImagesList[item['BookId']][item['ChapterId']]
                  ['success'] ==
              null) {
            $downloadImagesList[item['BookId']][item['ChapterId']]['success'] =
                0;
          }
          if ($downloadImagesList[item['BookId']][item['ChapterId']]
                  ['downloadMax'] ==
              null) {
            $downloadImagesList[item['BookId']][item['ChapterId']]
                ['downloadMax'] = 0;
          }
          if ($downloadImagesList[item['BookId']][item['ChapterId']]['list'] ==
              null) {
            $downloadImagesList[item['BookId']][item['ChapterId']]['list'] = [];
          }

          $downloadImagesList[item['BookId']][item['ChapterId']]['list'].addAll(
            [item],
          );
          if (item['DownloadSuccess'] == true) {
            //判断是否下完了
            $downloadImagesList[item['BookId']][item['ChapterId']]['success']++;
          }
          $downloadImagesList[item['BookId']][item['ChapterId']]
              ['downloadMax']++;
          //获取总进度
        }
        setState(() {});
      },
    );
    dataBaseSelect("BookDownloadChapterList", null, null).then(
      (value) {
        for (var item in value) {
          if ($downloadImagesList[item['BookId']]['success'] == null) {
            $downloadImagesList[item['BookId']]['success'] = 0;
          }
          if (item['Success'] == 1) {
            $downloadImagesList[item['BookId']]['success']++;
          }
        }
        setState(() {});
      },
    );
    dataBaseSelect("DownloadList", null, null).then(
      (value) {
        $downloadList = value;
        setState(() {});
      },
    );
  }

  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(10),
          right: ScreenUtil().setWidth(10),
        ),
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView.separated(
            itemCount: $downloadList.length,
            separatorBuilder: (BuildContext context, int index) {
              return Divider(height: 1.0, color: Colors.black45);
            },
            itemBuilder: (BuildContext context, int index) {
              var bookid = $downloadList[index]['BookId'];
              return Container(
                child: Row(
                  children: <Widget>[
                    DownloadListImages(
                      src: $downloadList[index]['FileServer'] +
                          $downloadList[index]['Path'],
                    ),
                    Container(
                      height: ScreenUtil().setHeight(250),
                      margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          // DownloadListSpecialLabel(),
                          DownloadListTitle(
                            text: $downloadList[index]['Title'],
                            page: ($downloadImagesList[bookid] == null)
                                ? 0
                                : $downloadImagesList[bookid].length,
                          ),
                          DownloadListChineseTeam(
                            chineseTeam: $downloadList[index]['ChineseTeam'],
                          ),
                          DownloadListAuthor(
                            author: $downloadList[index]['Author'],
                          ),
                          DownloadListLike(
                            likeLength: $downloadList[index]['Like'],
                          ),
                          DownloadListAuthorClass(
                            list: $downloadList[index]['Categories'],
                          ),
                          DownloadListProgressBar(
                            overallProgress: $downloadImagesList[bookid]
                                    ['success'] /
                                $downloadImagesList[bookid].length *
                                100, //总进度
                            subVolumeProgress: $subVolumeProgress, //分卷进度
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                margin: EdgeInsets.all(10),
              );
            },
          ),
        ),
      ),
    );
  }

  updataDownloadProgress($bookId, $chapterId) {
    $downloadImagesList[$bookId][$chapterId]['success']++;
    if ($downloadImagesList[$bookId][$chapterId]['success'] ==
        $downloadImagesList[$bookId][$chapterId]['downloadMax']) {
      $downloadImagesList[$bookId]['success']++;
    }
    $subVolumeProgress = $downloadImagesList[$bookId][$chapterId]['success'] /
        $downloadImagesList[$bookId][$chapterId]['downloadMax'] *
        100;
    setState(() {});
  }
}
