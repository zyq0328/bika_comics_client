/*
 * @Date: 2020-01-31 18:00:52
 * @名称: 下载列表 - 列表 - 协作者
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 22:26:04
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListChineseTeam/DownloadListChineseTeam.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';

class DownloadListChineseTeam extends StatefulWidget {
  DownloadListChineseTeam({Key key, this.chineseTeam}) : super(key: key);
  String chineseTeam;
  @override
  _DownloadListChineseTeamState createState() =>
      _DownloadListChineseTeamState();
}

class _DownloadListChineseTeamState extends State<DownloadListChineseTeam> {
  @override
  Widget build(BuildContext context) {
    if (widget.chineseTeam == null) {
      return Row();
    } else {
      var list = widget.chineseTeam.split(',');
      return Container(
        width: ScreenUtil().setWidth(430),
        child: RollWidget(
          direction: Axis.horizontal,
          child: RollWidget(
            direction: Axis.horizontal,
            child: Container(
              child: Row(
                children: <Widget>[
                  Text('协作者:'),
                  Container(
                    child: Wrap(
                      spacing: 5.0,
                      runSpacing: 0.0,
                      children: List.generate(
                        (list != null) ? list.length : 0,
                        (index) {
                          return Text(
                            list[index],
                            style: TextStyle(
                              color: Color(0xff4b8bf5),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
}
