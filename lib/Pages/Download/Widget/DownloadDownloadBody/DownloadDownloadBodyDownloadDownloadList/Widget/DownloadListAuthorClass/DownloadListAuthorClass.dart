/*
 * @Date: 2020-01-31 18:19:27
 * @名称: 分类 - 漫画分类 - 列表 - 分类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:15:27
 * @FilePath: /bica_acg/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListAuthorClass/DownloadListAuthorClass.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';

class DownloadListAuthorClass extends StatefulWidget {
  DownloadListAuthorClass({Key key, this.list, this.comicsId})
      : super(key: key);
  final String list;
  final String comicsId;
  @override
  _DownloadListAuthorClassState createState() =>
      _DownloadListAuthorClassState();
}

class _DownloadListAuthorClassState extends State<DownloadListAuthorClass> {
  @override
  Widget build(BuildContext context) {
    if (widget.list != null) {
      var $list = widget.list.split(',');
      return Container(
        width: ScreenUtil().setWidth(430),
        child: SizedBox(
          child: RollWidget(
            direction: Axis.horizontal,
            child: RollWidget(
              direction: Axis.horizontal,
              child: Text(
                djawiodjwai($list, widget.comicsId),
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}

String djawiodjwai(List<String> list, String comicsId) {
  var a = '分类：';
  for (var i = 0; i < list.length; i++) {
    if (list.length == i + 1) {
      a += list[i];
    } else {
      a += list[i] + '、';
    }
  }
  return a;
}
