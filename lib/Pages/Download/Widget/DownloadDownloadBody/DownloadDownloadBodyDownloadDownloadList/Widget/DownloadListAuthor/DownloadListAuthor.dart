/*
 * @Date: 2020-01-31 18:00:52
 * @名称: 下载列表 - 列表 - 作者
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:10:43
 * @FilePath: /bica_acg/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListAuthor/DownloadListAuthor.dart
 */
import 'package:flutter/material.dart';

class DownloadListAuthor extends StatefulWidget {
  DownloadListAuthor({Key key, this.author}) : super(key: key);
  String author;
  @override
  _DownloadListAuthorState createState() => _DownloadListAuthorState();
}

class _DownloadListAuthorState extends State<DownloadListAuthor> {
  @override
  Widget build(BuildContext context) {
    if (widget.author != null) {
      var $author = widget.author.split(',');
      return Row(
        children: <Widget>[
          Text('创作者: '),
          Container(
            child: Wrap(
              spacing: 5.0,
              runSpacing: 0.0,
              children: List.generate(
                ($author != null) ? $author.length : 0,
                (index) {
                  return Text(
                    $author[index],
                    style: TextStyle(
                      color: Color(0xffee9ab9),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}
