/*
 * @Date: 2020-01-31 20:05:56
 * @名称: 下载列表 - 列表 - 特殊标签
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 20:06:06
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListSpecialLabel/DownloadListSpecialLabel.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DownloadListSpecialLabel extends StatelessWidget {
  const DownloadListSpecialLabel({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(250),
      width: ScreenUtil().setWidth(50),
      color: Color(0xffed97b7),
      alignment: Alignment.center,
      child: Text(
        '禁\n书',
      ),
    );
  }
}
