/*
 * @Date: 2020-07-15 23:01:50
 * @名称: 下载管理 - 列表 - 进度条
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:16:14
 * @FilePath: /bica_acg/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListProgressBar/DownloadListProgressBar.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_rounded_progress_bar/flutter_icon_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DownloadListProgressBar extends StatefulWidget {
  DownloadListProgressBar({
    Key key,
    this.overallProgress = 0, //0-100
    this.subVolumeProgress = 0, //0-100
  }) : super(key: key);
  final double overallProgress;
  final double subVolumeProgress;
  @override
  _DownloadListProgressBarState createState() =>
      _DownloadListProgressBarState();
}

class _DownloadListProgressBarState extends State<DownloadListProgressBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      child: Container(
        child: IconRoundedProgressBar(
          widthIconSection:
              ScreenUtil().setWidth((430 * widget.overallProgress) / 100),
          icon: Padding(
            padding: EdgeInsets.all(8),
            /*  child: Icon(
              Icons.airline_seat_flat,
              color: Colors.white,
            ), */
          ),
          style: RoundedProgressBarStyle(
            colorBackgroundIcon: Color(0xff2ecc71),
            colorProgress: Color(0xaf2ecc71),
            colorProgressDark: Color(0xff4a627a),
            colorBorder: Color(0xff2c3e50),
            backgroundProgress: Color(0xff4a627a),
            borderWidth: 4,
            widthShadow: 6,
          ),
          borderRadius: BorderRadius.circular(6),
          percent: ScreenUtil().setWidth(
            (100 - widget.overallProgress) * widget.subVolumeProgress / 100,
          ),
        ),
      ),
    );
  }
}
