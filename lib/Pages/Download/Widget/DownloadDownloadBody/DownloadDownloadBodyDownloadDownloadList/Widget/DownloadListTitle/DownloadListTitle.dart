/*
 * @Date: 2020-01-31 17:43:22
 * @名称: 下载列表 - 列表 - 标题
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-15 21:49:13
 * @FilePath: /bika_comics_client/lib/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/Widget/DownloadListTitle/DownloadListTitle.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';

class DownloadListTitle extends StatefulWidget {
  DownloadListTitle({Key key, this.text, this.page}) : super(key: key);
  final String text;
  final int page;
  @override
  _DownloadListTitleState createState() => _DownloadListTitleState();
}

class _DownloadListTitleState extends State<DownloadListTitle> {
  @override
  var $page = '';
  void initState() {
    super.initState();
    if (widget.page == 0) {
      $page = "(没有下载章节)";
    } else {
      $page = " (${widget.page}卷)";
    }
  }

  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      // height: ScreenUtil().setHeight(120),
      child: SizedBox(
        child: RollWidget(
          direction: Axis.horizontal,
          child: RollWidget(
            direction: Axis.horizontal,
            child: Text(
              widget.text + $page,
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: ScreenUtil().setSp(30),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
