/*
 * @Date: 2020-03-20 22:36:55
 * @名称: 遮罩层动画 所属与列表加载前
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:11:34
 * @FilePath: /bica_acg/lib/Pages/ServiceList/Widget/MaskController.dart
 */
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/LogoController.dart';

var maskAnime; //遮罩层
AnimationController serviceListMaskAnimeController; //遮罩层
double mask = 0.0; //白色遮罩层
double splashBGTransparency = 0.0; //高斯模糊

class Mask extends StatefulWidget {
  Mask({
    Key key,
  }) : super(key: severListMaskState);

  MaskState createState() => new MaskState();
}

BuildContext context;
var request = false;

class MaskState extends State<Mask> with SingleTickerProviderStateMixin {
  initState() {
    super.initState();
    serviceListMaskAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    maskAnime =
        new Tween(begin: 0.0, end: 1.0).animate(serviceListMaskAnimeController)
          ..addListener(
            () {
              if (maskAnime.value > 0.3) {
                splashBGTransparency = (maskAnime.value - 0.3) * 12;
              }
              if (maskAnime.value < 0.5) {
                mask = (maskAnime.value) * 10 * 0.1;
              }
              if (maskAnime.value > 0.6) {
                mask = 0.6 - (maskAnime.value - 0.6);
              }
              setState(
                () {
                  // the state that has changed here is the MaskAnime object’s value
                },
              );
            },
          );
    maskAnime.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //当这个动画执行完毕 执行 logo上移
        logoAnimeController.forward();
      }
      if (status == AnimationStatus.dismissed) {
        //倒放
        logoAnimeController.reverse();
      }
    });
  }

  Widget build(BuildContext context) {
    return new Container(
      child: BackdropFilter(
        filter: new ImageFilter.blur(
          sigmaX: splashBGTransparency,
          sigmaY: splashBGTransparency,
        ),
        child: new Container(
          color: Colors.white.withOpacity(mask),
        ),
      ),
    );
  }

  dispose() {
    super.dispose();
  }
}
