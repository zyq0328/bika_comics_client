/*
 * @Date: 2019-11-16 22:26:39
 * @名称: 登陆控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @LastEditTime: 2020-08-23 00:17:30
 * @FilePath: /bica_acg/lib/Pages/ServiceList/Widget/LoginController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/DataBaseFunction.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';
import 'package:pica_acg/Pages/ServiceList/ServiceList.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/User/UserLoginJsonAnalysis.dart';

class LoginController extends StatefulWidget {
  LoginController({Key key, this.login}) : super(key: key);
  static String tag = 'login-page';
  final bool login;
  @override
  _LoginControllerState createState() => new _LoginControllerState();
}

String userName = "202184199";
String passWord = "12345678";

class _LoginControllerState extends State<LoginController> {
  @override
  void initState() {
    super.initState();
    if (widget.login) {
      dataBaseDelete("UserInfo", "id!=90909090", []);
    }
  }

  Widget build(BuildContext context) {
    var email = new TextField(
      autofocus: false,
      // initialValue: '202184199', //默认文字
      onChanged: (value) {
        userName = value;
      },
      decoration: new InputDecoration(
        icon: Icon(
          Icons.supervised_user_circle,
          color: Colors.pink[200],
        ),
        hintText: '登入id',
        contentPadding: new EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
    );

    TextFormField password = new TextFormField(
      autofocus: false,
      obscureText: true,
      onChanged: (value) {
        passWord = value;
      },
      decoration: new InputDecoration(
        hintText: '密码',
        contentPadding: new EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        icon: Icon(
          Icons.lock_outline,
          color: Colors.pink[200],
        ),
      ),
    );

    final loginButton = new Container(
      padding: new EdgeInsets.symmetric(vertical: 12.0),
      child: new FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(50),
          ),
        ),
        /*     minWidth: 200.0,
          height: 42.0, */
        onPressed: () async {
          if (userName == '' || passWord == '') {
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: languAre().$languAgeLoginController["userTips"]
                      ["A-00001"],
                  height: ScreenUtil().setHeight(900),
                  imageAsset: 'images/Tisp/Error.png',
                ),
                opaque: false,
              ),
            );
            return;
          }
          NetWorkController.post(
            getUserLogin(),
            (data) {
              switch (data["code"]) {
                case "200":
                  var returnm = UserLoginJsonAnalysis.fromJson(data);
                  $tempUserToKen = returnm.data.token;
                  print($tempUserToKen);
                  // print(returnm.data.token);
                  //等于 200 登陆成功
                  dataBaseInsert("UserInfo", {
                    "UserName": userName,
                    "PassWord": passWord,
                    "ToKen": returnm.data.token,
                  });
                  serviceListState.currentState.loginState(false, context);
                  break;

                default:
                  requestErrorParsing(data, $context: context);
                  break;
              }
            },
            data: {"email": userName, "password": passWord},
            headers: getApiRequestHeadrs("UserLogin"),
          );
          // Navigator.of(context).pushNamed(HomePage.tag);
        },
        color: Colors.pink[200],
        child: new Container(
          width: ScreenUtil().setWidth(300),
          height: ScreenUtil().setHeight(100),
          alignment: Alignment.center,
          child: Text(
            'BIKA 登陆',
            style: new TextStyle(
              color: Colors.white,
              fontSize: ScreenUtil().setSp(40),
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    Widget freeRegistration = new GestureDetector(
      onTap: () {},
      child: new Text(
        '免费注册',
        style: new TextStyle(color: Colors.pink[200]),
      ),
    );

    Widget forgetLabel = new GestureDetector(
      onTap: () {},
      child: new Text(
        '忘记密码',
        style: new TextStyle(color: Colors.pink[200]),
      ),
    );
    Widget termsOfUse = new GestureDetector(
      onTap: () {},
      child: new Text(
        '使用条款',
        style: new TextStyle(color: Colors.pink[200]),
      ),
    );
    Widget buttonAll = new Container(
      child: Column(
        children: <Widget>[
          loginButton,
          Wrap(
            spacing: ScreenUtil().setWidth(80), //主轴上子控件的间距
            children: [
              freeRegistration,
              forgetLabel,
              termsOfUse,
            ], //要显示的子控件集合
          ),
        ],
      ),
    );
    return new Container(
      height: ScreenUtil().setHeight(400),
      child: new ListView(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: new EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          email,
          SizedBox(
            height: 8.0,
          ),
          password,
          buttonAll,
        ],
      ),
    );
  }
}
