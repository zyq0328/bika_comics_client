/*
 * @Date: 2020-03-20 22:18:27
 * @名称: 服务器选择页面 - 服务器列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-06-17 23:06:02
 * @FilePath: /bika_comics_client/lib/Pages/ServiceList/Widget/Mask/ServiceListBranchListViewController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListView.dart';
import 'package:pica_acg/tools/extends/Json/ServiceList/service_list_json.dart';

class ServiceListBranchListViewController extends StatefulWidget {
  ServiceListBranchListViewController({Key key, this.data, this.height})
      : super(key: key);
  final ServiceListJson data;
  final double height;
  @override
  _ServiceListBranchListViewControllerState createState() =>
      _ServiceListBranchListViewControllerState();
}

class _ServiceListBranchListViewControllerState
    extends State<ServiceListBranchListViewController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ServiceListBranchListViewBranchListView(
        data: widget.data,
        height: widget.height,
      ),
    );
  }
}
