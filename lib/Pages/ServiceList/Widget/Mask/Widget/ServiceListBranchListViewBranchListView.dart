/*
 * @Date: 2020-03-20 22:21:30
 * @名称: 服务器选择页面 - 服务器列表 - 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-08 12:26:30
 * @FilePath: /bika_comics_client/lib/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListView.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewListController.dart';
import 'package:pica_acg/tools/extends/Json/ServiceList/service_list_json.dart';

class ServiceListBranchListViewBranchListView extends StatefulWidget {
  ServiceListBranchListViewBranchListView({Key key, this.data, this.height})
      : super(key: key);
  final ServiceListJson data;
  final double height;
  @override
  _ServiceListBranchListViewBranchListViewState createState() =>
      _ServiceListBranchListViewBranchListViewState();
}

class _ServiceListBranchListViewBranchListViewState
    extends State<ServiceListBranchListViewBranchListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Container(
          child: new ServiceListBranchListViewBranchListViewList(
            data: widget.data,
          ),
          width: ScreenUtil().setWidth(600),
          height: ScreenUtil().setHeight(widget.height),
          decoration: new BoxDecoration(
            border: new Border.all(
                color: Color.fromRGBO(237, 169, 185, 1), width: 1), // 边色与边宽度
            color: Color.fromARGB(255, 255, 255, 255), // 底色
            //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
            borderRadius: new BorderRadius.vertical(
                top: Radius.elliptical(20, 20),
                bottom: Radius.elliptical(20, 20)), // 也可控件一边圆角大小
          ),
          // alignment: const Alignment(0.0,0.0),
        ),
      ),
    );
  }
}
