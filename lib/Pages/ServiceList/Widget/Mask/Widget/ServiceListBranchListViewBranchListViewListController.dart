/*
 * @Date: 2020-03-20 22:22:04
 * @名称: 服务器选择页面 - 服务器列表 - 列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-06-17 23:09:58
 * @FilePath: /bika_comics_client/lib/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewListController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/LoadingController.dart';
import 'package:pica_acg/tools/extends/Json/ServiceList/service_list_json.dart';

class ServiceListBranchListViewBranchListViewList extends StatefulWidget {
  ServiceListBranchListViewBranchListViewList(
      {Key key, this.data, this.closeContext})
      : super(key: key);
  final ServiceListJson data;
  final BuildContext closeContext;
  @override
  _ServiceListBranchListViewBranchListViewListState createState() =>
      _ServiceListBranchListViewBranchListViewListState();
}

class _ServiceListBranchListViewBranchListViewListState
    extends State<ServiceListBranchListViewBranchListViewList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemCount: widget.data.data.sources.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                PageRouteBuilder(
                  pageBuilder: (context, _, __) =>
                      Loading2(url: widget.data.data.sources[index].url),
                  opaque: false,
                ),
              );
              // Navigator.pop(context);
              // Navigator.pushAndRemoveUntil(context, new MaterialPageRoute(
              //   builder: (BuildContext context) {
              //     return LoadingHomeAnime(title: widget.data.data.sources[index].url);
              //   },
              // ), (route) => route == null);

              // Navigator.of(context).push(
              //   PageRouteBuilder(
              //     pageBuilder: (context, _, __) => LoadingHomeAnime(title: widget.data.data.sources[index].url),
              //     opaque: false,
              //   ),
              // );
              // Navigator.of(context).push(
              //   new MaterialPageRoute(
              //     builder: (context) {
              //       return new LoadingHomeAnime(title: "传递的参数");
              //     },
              //   ),
              // );
            },
            child: Container(
              child: Container(
                child: Text(
                  widget.data.data.sources[index].title,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFF000000),
                    decoration: TextDecoration.none,
                  ),
                  textAlign: TextAlign.center,
                ),
                alignment: Alignment.center,
                height: ScreenUtil().setHeight(80),
              ),
              decoration: BoxDecoration(
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: Color.fromRGBO(229, 229, 231, 1),
                    width: 1,
                  ),
                ), // 边色与边宽度
              ),
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
            ),
          );
        },
      ),
      /* child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              child: Container(
                child: Text(
                  '冷火服务器',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFF000000),
                    decoration: TextDecoration.none,
                  ),
                  textAlign: TextAlign.center,
                ),
                alignment: Alignment.center,
                height: ScreenUtil().setHeight(80),
              ),
              decoration: BoxDecoration(
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: Color.fromRGBO(229, 229, 231, 1),
                    width: 1,
                  ),
                ), // 边色与边宽度
              ),
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
            ),
          ],
        ) */
    );
  }
}
