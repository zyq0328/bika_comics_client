/*
 * @Date: 2020-03-20 22:54:32
 * @名称: 当用户点击列表以后的加载动画
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:51:54
 * @FilePath: /bica_acg/lib/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Mask2Controller.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Home/Home.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Widget/Loading2Images.dart';
import 'package:pica_acg/anime/LiuDong.dart';
import 'package:pica_acg/anime/anime.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/GlobalFunction.dart';

AnimationController mask2AnimeController; //遮罩层
Animation<double> mask2Anime; //遮罩层
var mask2State = new GlobalKey<Mask2State>();

class Mask2 extends StatefulWidget {
  Mask2({Key key, this.url}) : super(key: key);
  final String url;
  @override
  Mask2State createState() => Mask2State();
}

class Mask2State extends State<Mask2> with SingleTickerProviderStateMixin {
  @override
  initState() {
    super.initState();
    mask2AnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    mask2Anime = new Tween(begin: 0.0, end: 0.6).animate(mask2AnimeController)
      ..addListener(() {
        setState(() {
          // the state that has changed here is the Mask2Anime object’s value
        });
      });
    mask2AnimeController.forward();
  }

  Widget build(BuildContext context) {
    if (mask2Anime.value == 0.6) {
      NetWorkController.get(
        widget.url,
        (data) {
          if (data['code'] == "-1" || data['code'] == "-1") {
            mask2AnimeController.dispose();
            loading2AnimeController.dispose();
            Navigator.of(context).pop(); //关闭加载框
            Navigator.of(context).push(
              PageRouteBuilder(
                  pageBuilder: (context, _, __) => GlobalTips(
                        body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                        height: ScreenUtil().setHeight(900),
                      ),
                  opaque: false),
            );
            return;
          }
          if (data['code'] == "200") {
            mask2AnimeController.dispose(); // 调试时候使用 正式版本用 dispose()
            loading2AnimeController.dispose(); // 调试时候使用 正式版本用 dispose()

            Navigator.pushAndRemoveUntil(context,
                new MaterialPageRoute(builder: (BuildContext context) {
              return Scaffold(
                body: AnimeLiuDong(
                  widget: MainHome(),
                ),
              );
            }), (route) => route == null);
          } else {
            mask2AnimeController.dispose();
            loading2AnimeController.dispose();
            Navigator.of(context).pop(); //关闭加载框
            Navigator.of(context).push(
              PageRouteBuilder(
                  pageBuilder: (context, _, __) => GlobalTips(
                        body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                        height: ScreenUtil().setHeight(900),
                      ),
                  opaque: false),
            );
          }
        },
        headers: getApiRequestHeadrs('Index'),
      );
    }
    return AnimatedOpacity(
      child: Container(
        child: Container(
          color: Color(0xFF000000),
        ),
      ),
      duration: Duration(milliseconds: 1),
      opacity: mask2Anime.value,
    );
  }

  dispose() {
    super.dispose();
  }
}
