import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Mask2Controller.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Widget/Loading2Images.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Widget/Loading2Text.dart';

class Loading2 extends StatefulWidget {
  Loading2({Key key, this.url}) : super(key: key);
  final url;

  @override
  _Loading2State createState() => _Loading2State();
}

class _Loading2State extends State<Loading2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Mask2(
            url: widget.url,
            key: mask2State,
          ),
          Loading2Images(),
          Loading2Text(),
        ],
      ),
    );
  }
}
