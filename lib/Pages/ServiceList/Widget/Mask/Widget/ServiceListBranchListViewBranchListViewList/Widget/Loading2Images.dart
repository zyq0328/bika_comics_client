/*
 * @Date: 2020-03-20 22:57:16
 * @名称: 加载动画2 的加载图
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-20 22:58:03
 * @FilePath: /pica_acg/lib/Pages/ServiceList/Widget/Mask/Widget/ServiceListBranchListViewBranchListViewList/Widget/Loading2Images.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Animation<double> loading2Anime; //服务器列表
AnimationController loading2AnimeController; //服务器列表
double $loading2GifState = 0.0; //logo加载层透明度 最大1.0

class Loading2Images extends StatefulWidget {
  Loading2Images({Key key}) : super(key: key);
  @override
  _Loading2ImagesState createState() => _Loading2ImagesState();
}

class _Loading2ImagesState extends State<Loading2Images>
    with SingleTickerProviderStateMixin {
  var imgSrc = 'images/Loading/loading_1.png';
  @override
  initState() {
    super.initState();
    loading2AnimeController = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    loading2Anime =
        new Tween(begin: 1.0, end: 10.0).animate(loading2AnimeController)
          ..addListener(() {
            setState(() {
              // the state that has changed here is the serviceListAnime object’s value
            });
          });
    loading2AnimeController.repeat();
    $loading2GifState = 1.0;
  }

  Widget build(BuildContext context) {
    imgSrc = 'images/Loading/loading_' +
        double.parse(loading2Anime.value.toString()).floor().toString() +
        '.png';
    return AnimatedOpacity(
      child: Container(
        margin: EdgeInsets.only(top: ScreenUtil().setWidth(430)),
        child: Image.asset(
          imgSrc,
          fit: BoxFit.contain,
          alignment: Alignment.topCenter,
          width: ScreenUtil().setWidth(750),
          height: ScreenUtil().setHeight(300),
        ),
      ),
      duration: Duration(milliseconds: 1),
      opacity: $loading2GifState,
    );
  }

  dispose() {
    // loadingAnimeController.dispose();
    super.dispose();
  }
}
