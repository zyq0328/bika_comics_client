import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Loading2Text extends StatefulWidget {
  Loading2Text({Key key}) : super(key: key);

  @override
  _Loading2TextState createState() => _Loading2TextState();
}

class _Loading2TextState extends State<Loading2Text> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(750),
        left: ScreenUtil().setWidth(250),
      ),
      child: Text(
        '正在BIKA界取得更新...',
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 15,
          color: Color(0xffffffff),
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}
