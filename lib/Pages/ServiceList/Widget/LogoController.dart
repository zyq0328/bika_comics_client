/*
 * @Date: 2020-03-20 22:46:20
 * @名称: logo动画控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-21 22:05:21
 * @FilePath: /bica_acg/lib/Pages/ServiceList/Widget/LogoController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/ServiceList/ServiceList.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/Mask/ServiceListBranchListViewController.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/MaskController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/ServiceList/service_list_json.dart';

Animation<double> logoAnime; //logo图标
AnimationController logoAnimeController; //logo图标
double $logoTransparency = 0.0;
double isLoading = 0; //是否显示 最顶上小人

class LogoController extends StatefulWidget {
  LogoController({
    Key key,
    this.context,
  });
  final BuildContext context;
  _LogoControllerState createState() => new _LogoControllerState();
}

class _LogoControllerState extends State<LogoController>
    with SingleTickerProviderStateMixin {
  initState() {
    super.initState();
    logoAnimeController = new AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    logoAnime = new Tween(begin: 0.0, end: 1.0).animate(logoAnimeController)
      ..addListener(() {
        setState(() {
          $logoTransparency = logoAnime.value;
          // the state that has changed here is the logoAnime object’s value
        });
      });
    logoAnime.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //当这个动画执行完毕 执行 logo上移
        isLoading = 1;
        requestSeverList(context);
      }
      if (status == AnimationStatus.reverse) {
        //当这个动画执行完毕 执行 logo上移
        isLoading = 0;
      }
    });
  }

  Widget build(BuildContext context) {
    return new Stack(
      children: [
        Container(
          width: ScreenUtil().setWidth(ScreenUtil.screenWidth),
          height: ScreenUtil().setWidth(ScreenUtil.screenHeight),
          color: Colors.white.withOpacity(logoAnime.value * 0.6),
        ),
        Container(
          child: Column(
            children: [
              Opacity(
                opacity: isLoading,
                child: Container(
                  margin: EdgeInsets.only(top: ScreenUtil().setHeight(60)),
                  child: Image.asset(
                    "images/Loading/loading.gif",
                    fit: BoxFit.contain,
                    alignment: Alignment.topCenter,
                    height: ScreenUtil().setHeight(200),
                  ),
                ),
              ),
              Opacity(
                opacity: $logoTransparency,
                child: Center(
                  child: new Container(
                    width: ScreenUtil().setWidth(650),
                    margin: EdgeInsets.only(
                        top: ScreenUtil()
                            .setHeight(300 - (logoAnime.value * 270))),
                    child: Image.asset(
                      'images/ServiceList/splashTitle_bikacg.png',
                      fit: BoxFit.contain,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  requestSeverList(BuildContext context) {
    NetWorkController.get(
      getServiceList(),
      (callBack) {
        setState(() {
          isLoading = 0;
        });
        switch (callBack['code']) {
          case "-1":
            Navigator.of(widget.context).push(
              PageRouteBuilder(
                  pageBuilder: (context, _, __) => GlobalTips(
                        body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                        height: ScreenUtil().setHeight(900),
                      ),
                  opaque: false),
            );
            break;
          case "200":
            var $callBack = ServiceListJson.fromJson(callBack);
            Navigator.of(widget.context).push(
              PageRouteBuilder(
                  pageBuilder: (context, _, __) =>
                      ServiceListBranchListViewController(
                        //服务器列表
                        data: $callBack,
                        height: ScreenUtil().setHeight(1344),
                      ),
                  opaque: false),
            );
            break;
          case "T-10000":
            serviceListMaskAnimeController.reverse(); //调用这个关闭 动画
            serviceListState.currentState.login();
            break;
          case "T-10002":
            serviceListMaskAnimeController.reverse(); //调用这个关闭 动画
            serviceListState.currentState.login();
            break;
          case "T-10001":
            serviceListMaskAnimeController.reverse(); //调用这个关闭 动画
            serviceListState.currentState.login();
            break;
          default:
            print(callBack);
            Navigator.of(widget.context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: '哔咔哔咔被玩坏了！\n这肯定不是哔咔的问题！\n绝对不是！',
                  height: ScreenUtil().setHeight(900),
                ),
                opaque: false,
              ),
            );
            break;
        }
      },
      headers: getApiRequestHeadrs("ServiceList"),
    );
  }

  dispose() {
    logoAnimeController.dispose();
    super.dispose();
  }
}
