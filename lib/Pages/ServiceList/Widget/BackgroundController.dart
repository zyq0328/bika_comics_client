/*
 * @Date: 2020-03-20 22:49:39
 * @名称: 背景图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-21 00:33:17
 * @FilePath: /bica_acg/lib/Pages/ServiceList/Widget/BackgroundController.dart
 */

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Animation<double> animation; //背景
AnimationController controller; //背景

class BackgroundController extends StatefulWidget {
  _BackgroundControllerState createState() => new _BackgroundControllerState();
}

class _BackgroundControllerState extends State<BackgroundController>
    with SingleTickerProviderStateMixin {
  Widget build(BuildContext context) {
    return new Stack(
      children: [
        Center(
          child: new Container(
            child: Image.asset(
              'images/ServiceList/splashBG.jpg',
              fit: BoxFit.cover,
              width: ScreenUtil().setWidth(750),
              height: ScreenUtil().setHeight(1334),
            ),
          ),
        ),
      ],
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}
