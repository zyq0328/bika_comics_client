/*
 * @Date: 2019-11-19 18:03:00
 * @名称: 选择节点页面 包括登陆
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:17:20
 * @FilePath: /bica_acg/lib/Pages/ServiceList/ServiceList.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/BackgroundController.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/LoginController.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/LogoController.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/MaskController.dart';

var loadingDoubel; //登陆动画过程数值
AnimationController serviceListLoadingAnime; //登陆动画
var serviceListState = new GlobalKey<ServiceListState>();

class ServiceList extends StatefulWidget {
  ServiceList({Key key, @required this.serviceUrl, @required this.login})
      : super(key: key);
  final String serviceUrl;
  bool login = false;

  @override
  ServiceListState createState() => ServiceListState();
}

class ServiceListState extends State<ServiceList>
    with SingleTickerProviderStateMixin {
  initState() {
    super.initState();
    serviceListLoadingAnime = new AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    loadingDoubel =
        new Tween(begin: 0.0, end: 400).animate(serviceListLoadingAnime)
          ..addListener(() {
            setState(() {});
          });

    // serviceListMaskAnimeController.forward();
    Future.delayed(Duration(seconds: 1), () {
      //因为载入动画是一秒所以要设置一秒延迟开始播放动画
      if (widget.login) {
        // 判断是否需要登陆
        serviceListLoadingAnime.forward(); //调用这个打开登陆
      } else {
        serviceListMaskAnimeController.forward();
      }
    });
    loadingDoubel.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        serviceListMaskAnimeController.forward();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(1334),
      child: new MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: ListView(
          children: <Widget>[
            Container(
              height: ScreenUtil().setHeight(1334 - loadingDoubel.value),
              child: Stack(
                children: <Widget>[
                  BackgroundController(),
                  Mask(),
                  LogoController(context: context),
                  // LoadingAnime(),
                  // Loading2(),
                ],
              ),
            ),
            LoginController(login: widget.login),
          ],
        ),
      ),
    );
  }

  loginState(bool login, context) {
    if (!login) {
      widget.login = login;
      serviceListLoadingAnime.reverse(); //控制 加载动画（收起输入框）
      // severListMaskState.currentState.requestSeverList(context);
      setState(() {});
    }
  }

  void login() {
    serviceListLoadingAnime.forward(); //调用这个打开登陆
    setState(() {});
  }
}
