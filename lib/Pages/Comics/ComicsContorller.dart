/*
 * @Date: 2020-01-31 14:25:33
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-01 20:18:37
 * @FilePath: /bica_acg/lib/Pages/Comics/ComicsContorller.dart
 * @最后编辑: 初雪桜
 */
/*
 * @Date: 2020-01-31 14:25:33
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-06-17 22:44:53
 * @FilePath: /bika_comics_client/lib/Pages/Comics/ComicsContorller.dart
 */
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/ComicsComicsBodyComicsBodyContorller.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBox/ComicsComicsBoxContorller.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Comics/comics_list.dart';

class ComicsContorller extends StatefulWidget {
  ComicsContorller({Key key, this.title}) : super(key: key);
  final title;
  @override
  _ComicsContorllerState createState() => _ComicsContorllerState();
}

class _ComicsContorllerState extends State<ComicsContorller> {
  @override
  ComicsList $comics = ComicsList.fromJson($netWorkComics);
  void initState() {
    super.initState();
    NetWorkController.post(
      getComicBookList(),
      (callBack) {
        if (callBack["code"] == "200") {
          $comics = ComicsList.fromJson(callBack);
          setState(() {});
        } else {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '获取目录失败',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        }
      },
      data: {
        "classified": widget.title,
      },
      headers: getApiRequestHeadrs('ComicsList'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            ConicsComicsBoxContorller(
              title: widget.title,
            ),
            ComicsComicsBodyComicsBodyContorller(list: $comics),
          ],
        ),
        color: Color(0xffffffff),
      ),
    );
  }
}
