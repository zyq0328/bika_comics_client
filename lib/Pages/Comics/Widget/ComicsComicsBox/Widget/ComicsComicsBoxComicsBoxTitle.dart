/*
 * @Date: 2020-01-31 14:30:55
 * @名称: 分类 - 漫画分类 - 列表 - 框架 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 19:18:03
 * @FilePath: /bika_comics_client/lib/Pages/Comics/Widget/ComicsComicsBox/Widget/ComicsComicsBoxComicsBoxTitle.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class ComicsComicsBoxComicsBoxTitle extends StatefulWidget {
  ComicsComicsBoxComicsBoxTitle({Key key, this.context, this.title})
      : super(key: key);
  final String title;
  final BuildContext context;

  @override
  _ComicsComicsBoxComicsBoxTitleState createState() =>
      _ComicsComicsBoxComicsBoxTitleState();
}

class _ComicsComicsBoxComicsBoxTitleState
    extends State<ComicsComicsBoxComicsBoxTitle> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      height: ScreenUtil().setHeight(70),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      alignment: Alignment.center,
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: GestureDetector(
              child: Text(
                languAre().$languAgeOrdinary["basic"]["return"],
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 19.0,
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
              onTap: () {
                Navigator.pop(widget.context);
              },
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              '跳页',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffbfbfbf),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}
