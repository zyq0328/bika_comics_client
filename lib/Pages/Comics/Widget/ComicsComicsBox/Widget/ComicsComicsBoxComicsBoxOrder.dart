/*
 * @Date: 2020-01-31 17:22:32
 * @名称: 分类 - 漫画分类 - 列表 - 排序
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-03 16:31:11
 * @FilePath: /pica_acg/lib/Pages/Comics/Widget/ComicsComicsBox/Widget/ComicsComicsBoxComicsBoxOrder.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsComicsBoxComicsBoxOrder extends StatefulWidget {
  ComicsComicsBoxComicsBoxOrder({Key key}) : super(key: key);

  @override
  _ComicsComicsBoxComicsBoxOrderState createState() =>
      _ComicsComicsBoxComicsBoxOrderState();
}

class _ComicsComicsBoxComicsBoxOrderState
    extends State<ComicsComicsBoxComicsBoxOrder> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(14),
        ),
        child: Container(
          margin: EdgeInsets.only(
            bottom: ScreenUtil().setHeight(14),
            left: ScreenUtil().setWidth(50),
          ),
          child: Row(
            children: <Widget>[
              Text(
                '默认排序',
                style: TextStyle(
                  color: Color(0xffb4b4b4),
                  fontSize: ScreenUtil().setSp(36),
                  fontWeight: FontWeight.w700,
                ),
              ),
              Icon(
                IconFont.iconDown,
                color: Color(0xffed97b7),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          border: new Border(
            bottom: Divider.createBorderSide(
              context,
              color: Color(0xfff2f2f7),
              width: 1,
            ),
          ), // 边色与边宽度
        ),
      ),
      onTap: () => {
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, _, __) => GlobalList(
              {
                "title": "请选择排序",
                "a_list": [
                  {
                    "text": "默认排序",
                    "color": "0xffed98b8",
                    'function': () => {
                          print('你点击了 ‘默认排序’'),
                        }
                  },
                  {
                    "text": "从旧到新",
                    "color": "0xffed98b8",
                    'function': () => {
                          print('你点击了 ‘从旧到新’'),
                        }
                  },
                  {
                    "text": "爱心满满",
                    "color": "0xffed98b8",
                    'function': () => {
                          print('你点击了 ‘爱心满满’'),
                        }
                  },
                  {
                    "text": "观看人数",
                    "color": "0xffed98b8",
                    'function': () => {
                          print('你点击了 ‘观看人数’'),
                        }
                  },
                ],
                "exitTitle": "取消",
                "exitFuncton": () => {
                      Navigator.pop(context),
                    },
              },
            ),
            opaque: false,
          ),
        ),
      },
    );
  }
}
