/*
 * @Date: 2020-01-19 23:57:42
 * @名称: 分类 - 漫画分类 - 列表 - 背景颜色
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-31 14:45:03
 */
import 'package:flutter/material.dart';

class ComicsComicsBoxComicsBoxBackground extends StatefulWidget {
  ComicsComicsBoxComicsBoxBackground({Key key}) : super(key: key);

  @override
  _ComicsComicsBoxComicsBoxBackgroundState createState() =>
      _ComicsComicsBoxComicsBoxBackgroundState();
}

class _ComicsComicsBoxComicsBoxBackgroundState
    extends State<ComicsComicsBoxComicsBoxBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffffffff),
    );
  }
}
