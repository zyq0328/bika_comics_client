/*
 * @Date: 2020-01-31 14:29:48
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-22 23:44:42
 * @FilePath: /pica_acg/lib/Pages/Comics/Widget/ComicsComicsBox/ComicsComicsBoxContorller.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBox/Widget/ComicsComicsBoxComicsBoxBackground.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBox/Widget/ComicsComicsBoxComicsBoxOrder.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBox/Widget/ComicsComicsBoxComicsBoxTitle.dart';

import '../../../../tools/Widget/FilterClass/FilterClass.dart';

class ConicsComicsBoxContorller extends StatefulWidget {
  ConicsComicsBoxContorller({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ConicsComicsBoxContorllerState createState() =>
      _ConicsComicsBoxContorllerState();
}

class _ConicsComicsBoxContorllerState extends State<ConicsComicsBoxContorller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ComicsComicsBoxComicsBoxTitle(
          title: widget.title,
          context: context,
        ),
        ComicsComicsBoxComicsBoxBackground(),
        FilterClass(),
        ComicsComicsBoxComicsBoxOrder(),
      ],
    );
  }
}
