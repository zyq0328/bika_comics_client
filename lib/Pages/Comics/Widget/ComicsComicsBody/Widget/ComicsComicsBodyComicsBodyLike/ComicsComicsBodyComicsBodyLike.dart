/*
 * @Date: 2020-01-31 18:54:31
 * @名称: 分类 - 漫画分类 - 列表 - 喜欢
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-31 21:03:48
 * @FilePath: /pica_acg/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyLike/ComicsComicsBodyComicsBodyLike.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsComicsBodyComicsBodyLike extends StatelessWidget {
  const ComicsComicsBodyComicsBodyLike({Key key, this.likeLength})
      : super(key: key);
  final int likeLength;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Icon(
            IconFont.iconiconLike,
            color: Color(0xffed97b7),
          ),
          Container(
            margin: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            child: Text(
              likeLength.toString(),
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(35), color: Color(0xffbdbdbd)),
            ),
          ),
        ],
      ),
    );
  }
}
