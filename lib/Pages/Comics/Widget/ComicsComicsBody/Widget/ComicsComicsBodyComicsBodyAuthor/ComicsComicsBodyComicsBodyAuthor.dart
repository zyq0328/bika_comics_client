/*
 * @Date: 2020-01-31 18:00:52
 * @名称: 分类 - 漫画分类 - 列表 - 作者
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-10 21:28:23
 * @FilePath: /bika_comics_client/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyAuthor/ComicsComicsBodyComicsBodyAuthor.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsComicsBodyComicsBodyAuthor extends StatefulWidget {
  ComicsComicsBodyComicsBodyAuthor({Key key, this.author}) : super(key: key);
  final List<Author> author;
  @override
  _ComicsComicsBodyComicsBodyAuthorState createState() =>
      _ComicsComicsBodyComicsBodyAuthorState();
}

class _ComicsComicsBodyComicsBodyAuthorState
    extends State<ComicsComicsBodyComicsBodyAuthor> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text('创作者: '),
        Container(
          child: Wrap(
            spacing: 5.0,
            runSpacing: 0.0,
            children: List.generate(
              (widget.author != null) ? widget.author.length : 0,
              (index) {
                return Text(
                  widget.author[index].name,
                  style: TextStyle(
                    color: Color(0xffee9ab9),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
