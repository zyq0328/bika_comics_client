/*
 * @Date: 2020-01-31 20:05:56
 * @名称: 分类 - 漫画分类 - 列表 - 特殊标签
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-31 20:08:36
 * @FilePath: /pica_acg/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyCOmicsBodySpecialLabel/ComicsComicsBodyCOmicsBodySpecialLabel.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsComicsBodyCOmicsBodySpecialLabel extends StatelessWidget {
  const ComicsComicsBodyCOmicsBodySpecialLabel({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(250),
      width: ScreenUtil().setWidth(50),
      color: Color(0xffed97b7),
      alignment: Alignment.center,
      child: Text(
        '禁\n书',
      ),
    );
  }
}
