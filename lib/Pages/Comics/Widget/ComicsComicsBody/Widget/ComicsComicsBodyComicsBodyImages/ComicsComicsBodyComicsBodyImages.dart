/*
 * @Date: 2020-01-31 17:47:57
 * @名称: 分类 - 漫画分类 - 列表 - 图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-01-31 20:08:52
 * @FilePath: /pica_acg/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyImages/ComicsComicsBodyComicsBodyImages.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsComicsBodyComicsBodyImages extends StatefulWidget {
  ComicsComicsBodyComicsBodyImages({Key key, this.src}) : super(key: key);
  final String src;
  @override
  _ComicsComicsBodyComicsBodyImagesState createState() =>
      _ComicsComicsBodyComicsBodyImagesState();
}

class _ComicsComicsBodyComicsBodyImagesState
    extends State<ComicsComicsBodyComicsBodyImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(188),
      height: ScreenUtil().setHeight(250),
      child: Image.network(
        widget.src,
        fit: BoxFit.cover,
      ),
    );
  }
}
