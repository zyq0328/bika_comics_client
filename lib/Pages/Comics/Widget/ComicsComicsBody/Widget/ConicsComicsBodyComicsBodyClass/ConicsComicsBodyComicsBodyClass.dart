/*
 * @Date: 2020-01-31 18:19:27
 * @名称: 分类 - 漫画分类 - 列表 - 分类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:10:11
 * @FilePath: /bica_acg/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ConicsComicsBodyComicsBodyClass/ConicsComicsBodyComicsBodyClass.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/tools/Widget/RollWidget/RollWidget.dart';

class ConicsComicsBodyComicsBodyClass extends StatefulWidget {
  ConicsComicsBodyComicsBodyClass({Key key, this.list, this.comicsId})
      : super(key: key);
  final List<String> list;
  final String comicsId;
  @override
  _ConicsComicsBodyComicsBodyClassState createState() =>
      _ConicsComicsBodyComicsBodyClassState();
}

class _ConicsComicsBodyComicsBodyClassState
    extends State<ConicsComicsBodyComicsBodyClass> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: RollWidget(
        direction: Axis.horizontal,
        child: RollWidget(
          direction: Axis.horizontal,
          child: Text(
            djawiodjwai(widget.list, widget.comicsId),
          ),
        ),
      ),
    );
  }
}

String djawiodjwai(List<String> list, String comicsId) {
  var a = '分类：';
  for (var i = 0; i < list.length; i++) {
    if (list.length == i + 1) {
      a += list[i];
    } else {
      a += list[i] + '、';
    }
  }
  return a;
}
