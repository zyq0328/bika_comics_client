import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/GlobalVarlableJson.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyCOmicsBodySpecialLabel/ComicsComicsBodyCOmicsBodySpecialLabel.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyAuthor/ComicsComicsBodyComicsBodyAuthor.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyChineseTeam/ComicsComicsBodyComicsBodyChineseTeam.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyImages/ComicsComicsBodyComicsBodyImages.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyLike/ComicsComicsBodyComicsBodyLike.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ComicsComicsBodyComicsBodyTitle/ComicsComicsBodyComicsBodyTitle.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/Widget/ConicsComicsBodyComicsBodyClass/ConicsComicsBodyComicsBodyClass.dart';
import 'package:pica_acg/Pages/ComicsInfo/ComicsInfoController.dart';
import 'package:pica_acg/tools/extends/Json/Comics/comics_list.dart';

class ComicsComicsBodyComicsBodyContorller extends StatefulWidget {
  ComicsComicsBodyComicsBodyContorller({Key key, this.list})
      : super(key: comicsComicsBodyComicsBodyContorllerState);
  final ComicsList list;
  @override
  ComicsComicsBodyComicsBodyContorllerState createState() =>
      ComicsComicsBodyComicsBodyContorllerState();
}

List<Docs> hiddenData = [];

class ComicsComicsBodyComicsBodyContorllerState
    extends State<ComicsComicsBodyComicsBodyContorller> {
  @override
  Widget build(BuildContext context) {
    aaaaaa();
    return Expanded(
      child: MediaQuery.removePadding(
        removeTop: true,
        removeLeft: true,
        removeRight: true,
        removeBottom: true,
        context: context,
        child: Container(
          child: ListView.separated(
            itemCount: widget.list.data.comics.docs.length,
            itemBuilder: (BuildContext context, int index) {
              var info = widget.list.data.comics.docs[index];
              return GestureDetector(
                child: Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(25),
                    bottom: ScreenUtil().setHeight(10),
                  ),
                  child: Row(
                    children: <Widget>[
                      ComicsComicsBodyComicsBodyImages(
                        src: springboard() +
                            info.thumb.fileServer +
                            info.thumb.path,
                      ),
                      Container(
                        height: ScreenUtil().setWidth(252),
                        width: ScreenUtil().setWidth(440),
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ComicsComicsBodyComicsBodyTitle(
                              text: info.title,
                              page: info.pagesCount,
                            ),
                            ComicsComicsBodyComicsBodyChineseTeam(
                              chineseTeam: info.chineseTeam,
                            ),
                            ComicsComicsBodyComicsBodyAuthor(
                              author: info.author,
                            ),
                            ConicsComicsBodyComicsBodyClass(
                              list: info.categories,
                              comicsId: info.id,
                            ),
                            ComicsComicsBodyComicsBodyLike(
                              likeLength: info.likesCount,
                            ),
                          ],
                        ),
                      ),
                      ComicsComicsBodyCOmicsBodySpecialLabel(),
                    ],
                  ),
                  /* decoration: BoxDecoration(
                border: new Border(
                  bottom: Divider.createBorderSide(
                    context,
                    color: Color.fromRGBO(229, 229, 231, 1),
                    width: 2,
                  ),
                ), // 边色与边宽度
              ), */
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => ComicsInfoController(
                        title: info.title,
                        author: info.author,
                        categories: info.categories,
                        likeNumber: info.likesCount,
                        comicsId: info.id,
                      ),
                    ),
                  );
                },
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                color: Colors.grey,
              );
            },
          ),
        ),
      ),
    );
  }

  void classificationUpdate() {
    //更新视图 隐藏了分类或者显示了分类在调用
    widget.list.data.comics.docs.addAll(hiddenData);
    hiddenData = [];
    aaaaaa();
  }

  bool aaaaaa() {
    for (var i = 0; i < widget.list.data.comics.docs.length; i++) {
      var list = widget.list.data.comics.docs[i].categories;
      for (var categoriesKeyi = 0;
          categoriesKeyi < list.length;
          categoriesKeyi++) {
        if ($GlobalFilterClassInfo[list[categoriesKeyi]] != null &&
            $GlobalFilterClassInfo[list[categoriesKeyi]]["Selection"] ==
                false) {
          hiddenData.add(widget.list.data.comics.docs[i]);
          widget.list.data.comics.docs.removeAt(i);
        }
      }
    }
    setState(() {});
  }
}
