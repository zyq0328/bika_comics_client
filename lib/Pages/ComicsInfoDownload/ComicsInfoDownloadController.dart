/*
 * @Date: 2020-02-06 18:35:24
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:14:27
 * @FilePath: /bica_acg/lib/Pages/ComicsInfoDownload/ComicsInfoDownloadController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/ComicsInfoDownloadComicsInfoDownloadBodyController.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/ComicsInfoDownloadComicsInfoDownloadBottomController.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/ComicsInfoDownloadComicsInfoDownloadBoxController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';
import 'package:pica_acg/GlobalFunction.dart';

class ComicsInfoDownloadController extends StatefulWidget {
  ComicsInfoDownloadController({
    Key key,
    this.comicsId,
    this.chapter,
    this.title,
  }) : super(key: key);
  final String comicsId;
  final AllChapterList chapter;
  final String title;
  @override
  _ComicsInfoDownloadControllerState createState() =>
      _ComicsInfoDownloadControllerState();
}

class _ComicsInfoDownloadControllerState
    extends State<ComicsInfoDownloadController> {
  @override
  List<Docs> newAllChapter = new List(); //显示的列表

  void initState() {
    super.initState();
    var downloadList = widget.chapter.data.eps.docs;
    for (var i = 0; i < downloadList.length; i++) {
      newAllChapter.add(downloadList[i]);
      setState(() {});
    }
    next($page) {
      NetWorkController.get(
        getAllChapter(
          widget.comicsId,
          page: $page,
        ),
        (data) {
          // temp['0'] = widget.chapter;
          downloadList = AllChapterList.fromJson(data).data.eps.docs;
          if ($page <= widget.chapter.data.eps.pages) {
            next($page + 1);
          }
          for (var i = 0; i < downloadList.length; i++) {
            newAllChapter.add(downloadList[i]);
            setState(() {});
          }
        },
        headers: getApiRequestHeadrs("ComicsAllChapter"),
      );
    }

    next(2);
  }
  /* 异步有很多问题 */
  /* if (widget.chapter.data.eps.page != widget.chapter.data.eps.pages) {
      for (var i = 2; i < widget.chapter.data.eps.pages + 1; i++) {
        NetWork.get(
            getAllChapter(
              widget.comicsId,
              page: i,
            ), (data) {
          // temp['0'] = widget.chapter;
          temp['$i'] = AllChapterList.fromJson(data).data.eps.docs;
          List<String> keys = temp.keys.toList();
          // key排序
          keys.sort(
            (a, b) => (int.parse(
              b,
            )).compareTo(
              int.parse(
                a,
              ),
            ),
          );
          if (i == widget.chapter.data.eps.pages - 1) {
          } else {
            newAllChapter = new List();
          }
          for (var i = 0; i < keys.length; i++) {
            for (var ai = 0; ai < temp[keys[i]].length; ai++) {
              newAllChapter.add(temp[keys[i]][temp[keys[i]].length - (ai + 1)]);
            }
            setState(() {});
          }
        });
      }
    } 
  }*/

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            ComicsInfoDownloadComicsInfoDownloadBoxController(),
            ComicsInfoDownloadComicsInfoDownloadBodyController(
              allChapter: newAllChapter,
              comicsId: widget.comicsId,
              title: widget.title,
            ),
            ComicsInfoDownloadComicsInfoDownloadBottomController(
              bookId: widget.comicsId,
            ),
          ],
        ),
      ],
    );
  }
}
