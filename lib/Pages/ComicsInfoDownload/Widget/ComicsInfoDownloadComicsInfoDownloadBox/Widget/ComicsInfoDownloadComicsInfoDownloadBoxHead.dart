/*
 * @Date: 2020-02-06 18:37:46
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 18:15:07
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/Widget/ComicsInfoDownloadComicsInfoDownloadBoxHead.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class ComicsInfoDownloadComicsInfoDownloadBoxHead extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBoxHead({Key key}) : super(key: key);

  @override
  _ComicsInfoDownloadComicsInfoDownloadBoxHeadState createState() =>
      _ComicsInfoDownloadComicsInfoDownloadBoxHeadState();
}

class _ComicsInfoDownloadComicsInfoDownloadBoxHeadState
    extends State<ComicsInfoDownloadComicsInfoDownloadBoxHead> {
  @override
  Widget build(BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;
    return Container(
      child: Container(
        height: ScreenUtil().setHeight(70),
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
        child: Flex(
          direction: Axis.horizontal,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                child: Text(
                  languAre().$languAgeOrdinary["basic"]["return"],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFF434343),
                    decoration: TextDecoration.none,
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Expanded(
              flex: 6,
              child: Text(
                '下载页面',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 19.0,
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(
                '下载',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 19.0,
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          border: new Border(
            bottom: Divider.createBorderSide(
              context,
              color: Color(0xffbfbfbf),
              width: 1,
            ),
          ), // 边色与边宽度
        ),
      ),
    );
  }
}
