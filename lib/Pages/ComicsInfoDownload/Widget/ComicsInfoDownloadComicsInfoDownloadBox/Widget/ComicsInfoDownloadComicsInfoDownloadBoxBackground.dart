/*
 * @Date: 2020-02-06 18:40:16
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 背景色
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-06 18:41:08
 * @FilePath: /pica_acg/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/Widget/ComicsInfoDownloadComicsInfoDownloadBoxBackground.dart
 */

import 'package:flutter/material.dart';

class ComicsInfoDownloadComicsInfoDownloadBoxBackground extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBoxBackground({Key key})
      : super(key: key);

  @override
  _ComicsInfoDownloadComicsInfoDownloadBoxBackgroundState createState() =>
      _ComicsInfoDownloadComicsInfoDownloadBoxBackgroundState();
}

class _ComicsInfoDownloadComicsInfoDownloadBoxBackgroundState
    extends State<ComicsInfoDownloadComicsInfoDownloadBoxBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffffffff),
    );
  }
}
