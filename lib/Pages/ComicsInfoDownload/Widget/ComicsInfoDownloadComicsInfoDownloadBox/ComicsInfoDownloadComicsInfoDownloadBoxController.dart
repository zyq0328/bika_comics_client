/*
 * @Date: 2020-02-06 18:42:23
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 框架控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-06 18:54:25
 * @FilePath: /pica_acg/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/ComicsInfoDownloadComicsInfoDownloadBoxController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/Widget/ComicsInfoDownloadComicsInfoDownloadBoxBackground.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBox/Widget/ComicsInfoDownloadComicsInfoDownloadBoxHead.dart';

class ComicsInfoDownloadComicsInfoDownloadBoxController extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBoxController({Key key})
      : super(key: key);

  @override
  _ComicsInfoDownloadComicsInfoDownloadBoxControllerState createState() =>
      _ComicsInfoDownloadComicsInfoDownloadBoxControllerState();
}

class _ComicsInfoDownloadComicsInfoDownloadBoxControllerState
    extends State<ComicsInfoDownloadComicsInfoDownloadBoxController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ComicsInfoDownloadComicsInfoDownloadBoxBackground(),
        ComicsInfoDownloadComicsInfoDownloadBoxHead(),
      ],
    );
  }
}
