/*
 * @Date: 2020-02-06 18:55:03
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 内容 - 列表 - 下载的进度条
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-15 15:35:58
 * @FilePath: /pica_acg/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/Widget/ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress
    extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress(
      {Key key, this.progress})
      : super(key: key);
  final double progress;
  @override
  _ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgressState
      createState() =>
          _ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgressState();
}

class _ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgressState
    extends State<
        ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(10),
      width: ScreenUtil().setWidth(168),
      child: LinearProgressIndicator(
        backgroundColor: Colors.blue,
        value: widget.progress,
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
      ),
    );
  }
}
