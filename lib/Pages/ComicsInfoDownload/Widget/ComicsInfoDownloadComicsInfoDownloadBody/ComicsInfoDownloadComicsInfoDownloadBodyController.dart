/*
 * @Date: 2020-02-06 18:52:20
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 内容控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 22:48:06
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/ComicsInfoDownloadComicsInfoDownloadBodyController.dart
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/Widget/ComicsInfoDownloadComicsInfoDownloadBodyList.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomButton/ComicsInfoDownloadComicsInfoDownloadBottomButton.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';

class ComicsInfoDownloadComicsInfoDownloadBodyController
    extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBodyController(
      {Key key, this.allChapter, this.comicsId, this.title})
      : super(key: key);
  final List<Docs> allChapter;
  final String comicsId;
  final String title;

  @override
  _ComicsInfoDownloadComicsInfoDownloadBodyControllerState createState() =>
      _ComicsInfoDownloadComicsInfoDownloadBodyControllerState();
}

class _ComicsInfoDownloadComicsInfoDownloadBodyControllerState
    extends State<ComicsInfoDownloadComicsInfoDownloadBodyController> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ComicsInfoDownloadComicsInfoDownloadBodyList(
          allChapter: widget.allChapter,
          comicsId: widget.comicsId,
          title: widget.title,
          key: listKey,
        ),
      ],
    );
  }
}
