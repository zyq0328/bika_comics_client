/*
 * @Date: 2020-02-07 15:52:05
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 底部 - 点击下载按钮
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:14:38
 * @FilePath: /bica_acg/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomButton/ComicsInfoDownloadComicsInfoDownloadBottomButton.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/Widget/ComicsInfoDownloadComicsInfoDownloadBodyList.dart';
import 'package:pica_acg/tools/Controller/Download/AddDownload.dart';
import 'package:pica_acg/tools/Controller/Download/PlayDownload.dart';

var listKey = new GlobalKey<
    ComicsInfoDownloadComicsInfoDownloadBodyListState>(); //引入 下载页面列表

class ComicsInfoDownloadComicsInfoDownloadBottomButton extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBottomButton({Key key, this.bookId})
      : super(key: key);
  final String bookId;
  @override
  ComicsInfoDownloadComicsInfoDownloadBottomButtonState createState() =>
      ComicsInfoDownloadComicsInfoDownloadBottomButtonState();
}

Map buttonStyle = {
  "buttonColor": Color(0xffFFFFFF),
  "TextColor": Color(0xff575757),
  "Text": '请选择下载章节',
  'Function': () => {},
};

class ComicsInfoDownloadComicsInfoDownloadBottomButtonState
    extends State<ComicsInfoDownloadComicsInfoDownloadBottomButton> {
  @override
  Widget build(BuildContext context) {
    PlayDownload.getDownLoad(); //开始下载
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
      ),
      width: ScreenUtil().setWidth(720),
      height: ScreenUtil().setHeight(100),
      child: MaterialButton(
        color: buttonStyle['buttonColor'],
        textColor: Colors.white,
        child: new Text(
          buttonStyle['Text'],
          style: TextStyle(
            fontSize: ScreenUtil().setSp(40),
            color: buttonStyle['TextColor'],
          ),
        ),
        onPressed: () {
          buttonStyle['Function']();
          // key.currentState.dawda123();
        },
      ),
    );
  }

  void updateState(
    bool state, {
    bookId,
  }) {
    if (state) {
      buttonStyle = {
        "buttonColor": Color(0xffec97b6),
        "TextColor": Color(0xffffffff),
        "Text": '开始下载',
        "Function": () {
          listKey.currentState.startDownload((returnm) {
            //插入数据库
            //先添加数据库 到下载列表
            loadingDialog(context, "正在努力加载");

            AddDownload.addDownloadList(widget.bookId, (returm) {
              //在进行解析
              Navigator.pop(context);

              Navigator.of(context).push(
                PageRouteBuilder(
                  pageBuilder: (context, _, __) => GlobalTips(
                    body: '已加入下载列表准备下载',
                    imageAsset: "images/Tisp/Success.png",
                    height: ScreenUtil().setHeight(900),
                  ),
                  opaque: false,
                ),
              );
              PlayDownload.getDownLoad(); //开始下载
            });
          });
        },
      };
      if (bookId == null) {
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, _, __) => GlobalTips(
              body: '您尚未选择任何章节',
              height: ScreenUtil().setHeight(900),
            ),
            opaque: false,
          ),
        );
      } else {}
    } else {
      buttonStyle = {
        "buttonColor": Color(0xffFFFFFF),
        "TextColor": Color(0xff575757),
        "Text": '请选择下载章节',
        "Function": () {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '您尚未选择任何章节',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        },
      };
    }
    setState(() {});
  }
}
