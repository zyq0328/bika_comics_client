/*
 * @Date: 2020-02-07 15:46:43
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 底部 - 选择的信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-04-06 00:28:08
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomText/ComicsInfoDownloadComicsInfoDownloadBottomText.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

final key = GlobalKey<ComicsInfoDownloadComicsInfoDownloadBottomTextState>();

class ComicsInfoDownloadComicsInfoDownloadBottomText extends StatefulWidget {
  var downloaded;
  var downloadedLoading;
  var haveChosen;
  ComicsInfoDownloadComicsInfoDownloadBottomText({
    Key key,
    this.haveChosen,
    this.downloaded,
    this.downloadedLoading,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return ComicsInfoDownloadComicsInfoDownloadBottomTextState();
  }

/*   ComicsInfoDownloadComicsInfoDownloadBottomTextState createState() =>
      ComicsInfoDownloadComicsInfoDownloadBottomTextState(); */
}

class ComicsInfoDownloadComicsInfoDownloadBottomTextState
    extends State<ComicsInfoDownloadComicsInfoDownloadBottomText> {
  @override

/*     widget.haveChosen = haveChosen;
    widget.downloaded = downloaded; */
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        left: ScreenUtil().setWidth(15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('已选择章节:${widget.haveChosen}'),
          Text('下载中章节:${widget.downloadedLoading}'),
          Text('已下载章节:${widget.downloaded}'),
        ],
      ),
    );
  }

//更新下载数量
  void updateDownloadValue(
      {int haveChosen, int downloaded, int downloadedLoading}) {
    if (haveChosen != null) {
      widget.haveChosen = haveChosen;
    }
    if (downloaded != null) {
      widget.downloaded = downloaded;
    }
    if (downloadedLoading != null) {
      widget.downloadedLoading = downloadedLoading;
    }
    setState(() {});
  }
}
