/*
 * @Date: 2020-02-07 15:45:14
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 底部控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 22:46:46
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/ComicsInfoDownloadComicsInfoDownloadBottomController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomButton/ComicsInfoDownloadComicsInfoDownloadBottomButton.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomText/ComicsInfoDownloadComicsInfoDownloadBottomText.dart';

class ComicsInfoDownloadComicsInfoDownloadBottomController
    extends StatefulWidget {
  ComicsInfoDownloadComicsInfoDownloadBottomController({Key key, this.bookId})
      : super(key: key);
  final String bookId;
  @override
  _ComicsInfoDownloadComicsInfoDownloadBottomControllerState createState() =>
      _ComicsInfoDownloadComicsInfoDownloadBottomControllerState();
}

class _ComicsInfoDownloadComicsInfoDownloadBottomControllerState
    extends State<ComicsInfoDownloadComicsInfoDownloadBottomController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ComicsInfoDownloadComicsInfoDownloadBottomText(
          key: refreshKey,
          haveChosen: null,
          downloaded: null,
          downloadedLoading: null,
        ),
        ComicsInfoDownloadComicsInfoDownloadBottomButton(
            key: buttonKey, bookId: widget.bookId),
      ],
    );
  }
}
