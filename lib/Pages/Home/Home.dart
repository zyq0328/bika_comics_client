/*
 * @Date: 2019-11-28 10:11:53
 * @名称: 主页
 * @描述: 主页 带着 bottom 切换 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 19:07:40
 * @FilePath: /bika_comics_client/lib/Pages/Home/Home.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/Bottom/Bottom.dart';
import 'package:pica_acg/Pages/Category/Category.dart';
import 'package:pica_acg/Pages/Home/Widget/AnnouncementsController.dart';
import 'package:pica_acg/Pages/Home/Widget/BackGround.dart';
import 'package:pica_acg/Pages/Home/Widget/Banner/Banner.dart';
import 'package:pica_acg/Pages/Home/Widget/Title.dart';
import 'package:pica_acg/Pages/My/My.dart';
import 'package:pica_acg/Pages/Settings/SettingsContorller.dart';

class MainHome extends StatefulWidget {
  MainHome({
    Key key,
  }) : super(key: mainHomeState);
  @override
  MainHomeState createState() => MainHomeState();
}

class MainHomeState extends State<MainHome> {
  @override
  final List<Widget> _pages = List();
  PageController _controller;

  void initState() {
    super.initState();
    _pages
      ..add(HomePage1View())
      ..add(Category())
      ..add(My())
      ..add(SettingContorller());
    _controller = PageController(initialPage: 0);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildBottomNavigationBarWidget(),
      body: _buildBodyWidget(),
    );
  }

  Widget _buildBodyWidget() {
    return PageView.builder(
      controller: _controller,
      itemCount: _pages.length,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return _pages[index];
      },
      onPageChanged: (index) {
        print('您切换了页面 $index');
      },
    );
  }

  toPage(index) {
    _controller.jumpToPage(index);
  }

  Widget _buildBottomNavigationBarWidget() {
    return BottomControllerBottomBox();
  }
}

class HomePage1View extends StatefulWidget {
  HomePage1View({Key key}) : super(key: key);
  @override
  _HomePage1ViewState createState() => _HomePage1ViewState();
}

class _HomePage1ViewState extends State<HomePage1View> {
  bool get wantKeepAlive => true;
  Animation<double> upInsertAnime; //服务器列表
  AnimationController upInsertAnimeController; //logo图标
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          HomeBackGround(), //背景
          Column(
            children: <Widget>[
              Container(
                child: HomeTitle(), //标题
              ),
              Container(
                child: HomeBanner(), //横幅
                decoration: new BoxDecoration(
                  border: new Border(
                    top: BorderSide(
                      color: Color(0xFF9E9E9E),
                      width: 0.5,
                    ),
                    bottom: BorderSide(
                      color: Color(0xFF9E9E9E),
                      width: 0.5,
                    ),
                  ), // 边色与边宽度
                  color: Color(0xFF9E9E9E), // 底色
                  //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                ),
              ),
              Container(
                decoration: new BoxDecoration(
                  border: new Border(
                    top: BorderSide(
                      color: Color(0xFF9E9E9E),
                      width: 0.5,
                    ),
                    bottom: BorderSide(
                      color: Color(0xFF9E9E9E),
                      width: 0.5,
                    ),
                  ), // 边色与边宽度
                  color: Color(0xFF9E9E9E), // 底色
                  //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                ),
                margin: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
                child: AnnouncementsController(), //公告
              ),
            ],
          ),
        ],
      ),
    );
  }
}
