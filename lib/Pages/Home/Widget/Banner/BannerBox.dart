import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';

class HomeBannerBannerBox extends StatefulWidget {
  HomeBannerBannerBox({Key key}) : super(key: key);

  @override
  _HomeBannerBannerBoxState createState() => _HomeBannerBannerBoxState();
}

class _HomeBannerBannerBoxState extends State<HomeBannerBannerBox> {
  @override
  Widget build(BuildContext context) {
    
    return Container(
      // width: ScreenUtil().setWidth(750),
      height: ScreenUtil().setHeight($global_HomeBannerBannerImagesAndBoxHeight),
      width: ScreenUtil().setWidth($global_HomeBannerBannerImagesAndBoxWidth),
      color: Color(0xFFFFFFFF),
    );
  }
}
