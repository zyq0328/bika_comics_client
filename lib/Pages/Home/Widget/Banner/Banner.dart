/*
 * @Date: 2019-11-29 22:58:51
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-09 00:46:43
 * @FilePath: /bika_comics_client/lib/Pages/Home/Widget/Banner/Banner.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/Pages/Home/Widget/Banner/BannerBox.dart';
import 'package:pica_acg/Pages/Home/Widget/Banner/BannerImages.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Home/HomeBanner/home_banner_json.dart';

class HomeBanner extends StatefulWidget {
  HomeBanner({Key key}) : super(key: key);

  @override
  _HomeBannerState createState() => _HomeBannerState();
}

class _HomeBannerState extends State<HomeBanner> {
  @override
  void initState() {
    super.initState();
    if ($networkRequest == true) {
      $networkRequest = false;
      NetWorkController.get(
        getBannerList(),
        (data) {
          bannerData = HomeBannerJson.fromJson(data);
          if (bannerData.code == "200") {
            setState(() {});
          } else {
            requestErrorParsing(data, $context: context);
          }
        },
        headers: getApiRequestHeadrs("Banner"),
      );
    }
  }

  Widget build(BuildContext context) {
    return Container(
      child: HomeBannerBannerController(),
    );
  }
}

class HomeBannerBannerController extends StatefulWidget {
  HomeBannerBannerController({Key key}) : super(key: key);

  @override
  _HomeBannerBannerControllerState createState() =>
      _HomeBannerBannerControllerState();
}

var $networkRequest = true;
var networkRequest = {
  "code": "200",
  "message": "success",
  "data": {
    "banners": [
      /*  {
        "_id": "3df0efef",
        "title": "BIKACG 是一個萌萌的地方",
        "shortDescription": "0.0",
        "type": "web",
        "link": "https:\/\/www.hahatai.com\/global-buzz\/articles\/8803",
        "thumb": {
          "fileServer":
              "https://tvax4.sinaimg.cn/large/006wvbvUly1g9rgpmlyhqj313a0rs1kx.jpg",
          "path": "",
          "originalName": "image.jpg"
        }
      },
      {
        "_id": "3df0efef",
        "title": "BIKACG 是一個萌萌的地方",
        "shortDescription": "0.0",
        "type": "web",
        "link": "https:\/\/www.hahatai.com\/global-buzz\/articles\/8803",
        "thumb": {
          "fileServer":
              "https://tvax4.sinaimg.cn/large/006wvbvUly1g9rgpmlyhqj313a0rs1kx.jpg",
          "path": "",
          "originalName": "image.jpg"
        }
      } */
    ]
  }
};
HomeBannerJson bannerData = HomeBannerJson.fromJson(networkRequest);

class _HomeBannerBannerControllerState
    extends State<HomeBannerBannerController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: ScreenUtil().setHeight(120)),
      color: Colors.red,
      child: Stack(
        children: <Widget>[
          HomeBannerBannerBox(),
          HomeBannerBannerImages(data: bannerData),
        ],
      ),
    );
  }
}
