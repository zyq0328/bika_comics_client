import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';

class HomeBannerBannerText extends StatelessWidget {
  const HomeBannerBannerText({Key key, this.text}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.only(left: ScreenUtil().setWidth(30)),
        child: Text(
          this.text,
          style: TextStyle(
            fontSize: 14 /* ScreenUtil().setWidth(30) */,
            fontWeight: FontWeight.w600,
            color: Color(0xFF000000),
            decoration: TextDecoration.none,
          ),
        ),
      ),
      color: Color($global_HomeBannerBannerTextBackgroundColor),
      height: ScreenUtil().setHeight($global_HomeBannerBannerTextAndBoxHeight),
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(
          top: ScreenUtil()
              .setHeight($global_HomeBannerBannerImagesAndBoxHeight - $global_HomeBannerBannerTextAndBoxHeight)),
    );
  }
}
