/*
 * @Date: 2019-11-29 23:13:43
 * @名称: 轮播
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-19 01:30:35
 * @FilePath: /bica_acg/lib/Pages/Home/Widget/Banner/BannerImages.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Home/Widget/Banner/BannerText.dart';
import 'package:pica_acg/tools/extends/Json/Home/HomeBanner/home_banner_json.dart';

class HomeBannerBannerImages extends StatefulWidget {
  HomeBannerBannerImages({
    Key key,
    this.data,
  }) : super(key: key);
  final HomeBannerJson data;
  @override
  _HomeBannerBannerImagesState createState() => _HomeBannerBannerImagesState();
}

var itemCount = 0;

class _HomeBannerBannerImagesState extends State<HomeBannerBannerImages> {
  @override
  Widget build(BuildContext context) {
    if (widget.data == null || widget.data.code == "-1") {
    } else {
      // print(widget.data.data.banners[0].thumb.fileServer);
      itemCount = widget.data.data.banners.length;
    }

    return Container(
      width: ScreenUtil().setWidth($global_HomeBannerBannerImagesAndBoxWidth),
      height:
          ScreenUtil().setHeight($global_HomeBannerBannerImagesAndBoxHeight),
      child: Swiper(
        itemCount: itemCount,
        autoplay: true,
        //是否自动播放
        autoplayDelay: 3000,
        //自动播放延迟
        autoplayDisableOnInteraction: true,
        //触发时是否停止播放
        duration: 600,
        //动画时间
        pagination: new SwiperPagination(
          alignment: Alignment.bottomRight,
          builder: new DotSwiperPaginationBuilder(
              size: 8,
              activeSize: 12,
              color: Color.fromARGB(30, 255, 255, 255),
              activeColor: Color.fromARGB(255, 255, 255, 255)),
        ), //默认指示器
        //item数量
        itemBuilder: (BuildContext context, int index) {
          //item构建
          return new Stack(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                // child: Image.network(
                //   widget.data.data.banners[index].thumb.fileServer,
                // ),
                child: Container(
                  child: NetworkCustomImage(
                    url: widget.data.data.banners[index].thumb.fileServer,
                  ),
                ),
              ),
              HomeBannerBannerText(text: widget.data.data.banners[index].title),
            ],
          );
        },
      ),
    );
  }
}
