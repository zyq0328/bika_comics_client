import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Announcements/Announcements.dart';

class HomeAnnouncementsAnnouncementsBox extends StatefulWidget {
  HomeAnnouncementsAnnouncementsBox({Key key}) : super(key: key);

  @override
  _HomeAnnouncementsAnnouncementsBoxState createState() =>
      _HomeAnnouncementsAnnouncementsBoxState();
}

class _HomeAnnouncementsAnnouncementsBoxState
    extends State<HomeAnnouncementsAnnouncementsBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(90.0),
      child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: new Row(
          children: [
            Icon(),
            Texts(),
            Texts2(),
          ],
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color.fromRGBO(229, 229, 231, 1),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}

class Icon extends StatelessWidget {
  const Icon({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(
        'images/Home/Announcements/icon.png',
        height: ScreenUtil().setHeight(70.0),
      ),
    );
  }
}

class Texts extends StatelessWidget {
  const Texts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(10.0)),
      child: Text(
        '公告',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 19.0,
          color: Color.fromRGBO(79, 79, 79, 1),
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}

class Texts2 extends StatelessWidget {
  const Texts2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: ScreenUtil().setWidth(450.0)),
      child: new GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => AnnouncementsAnime(),
              opaque: false,
            ),
          );
        },
        child: Text(
          '更多   >',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 19.0,
            color: Color.fromRGBO(189, 189, 189, 1),
            decoration: TextDecoration.none,
          ),
        ),
      ),
    );
  }
}
