import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Home/Widget/Announcements/AnnouncementsBox.dart';
import 'package:pica_acg/Pages/Home/Widget/Announcements/AnnouncementsBoxList.dart';

class HomeAnnouncementsAnnouncements extends StatefulWidget {
  HomeAnnouncementsAnnouncements({
    Key key,
  }) : super(key: key);
  @override
  _HomeAnnouncementsAnnouncementsState createState() =>
      _HomeAnnouncementsAnnouncementsState();
}

class _HomeAnnouncementsAnnouncementsState
    extends State<HomeAnnouncementsAnnouncements> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:
          /* ListView.builder(
        itemCount: 3000,
        itemBuilder: (BuildContext context, int index) {
          return Text('$index');
        },
      ), */
          Stack(
        children: <Widget>[
          HomeAnnouncementsAnnouncementsBoxList(),
          HomeAnnouncementsAnnouncementsBox(),
        ],
      ),
    );
  }
}
