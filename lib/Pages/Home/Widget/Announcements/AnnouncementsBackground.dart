import 'package:flutter/cupertino.dart';

/*
 * 这是 home页面的 公告背景
 */
class HomeAnnouncementsBackgroundBackground extends StatefulWidget {
  HomeAnnouncementsBackgroundBackground({Key key}) : super(key: key);

  @override
  _HomeAnnouncementsBackgroundBackgroundState createState() =>
      _HomeAnnouncementsBackgroundBackgroundState();
}

class _HomeAnnouncementsBackgroundBackgroundState
    extends State<HomeAnnouncementsBackgroundBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(0, 242, 242, 247),
    );
  }
}
