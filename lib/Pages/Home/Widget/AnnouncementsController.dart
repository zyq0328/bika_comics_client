/*
 * @Date: 2019-12-05 22:44:34
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-09 00:07:17
 * @FilePath: /bika_comics_client/lib/Pages/Home/Widget/AnnouncementsController.dart
 * @最后编辑: 初雪桜
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Home/Widget/Announcements/AnnouncementsBackground.dart';
import 'package:pica_acg/Pages/Home/Widget/Announcements/Announcements.dart';

class AnnouncementsController extends StatefulWidget {
  AnnouncementsController({Key key}) : super(key: key);
  @override
  _AnnouncementsControllerState createState() =>
      _AnnouncementsControllerState();
}

class _AnnouncementsControllerState extends State<AnnouncementsController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: Stack(
        children: <Widget>[
          HomeAnnouncementsBackgroundBackground(),
          HomeAnnouncementsAnnouncements(),
        ],
      ),
    );
  }
}
