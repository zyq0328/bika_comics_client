/*
 * @Date: 2020-02-19 18:03:25
 * @名称: 阅读漫画页面 - 内容 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-02-21 18:51:33
 * @FilePath: /pica_acg/lib/Pages/ReadingComics/Widget/ReadingComicsBody/ReadingComicsBodyController copy.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ReadingComics/Widget/ReadingComicsBox/ReadingComicsBoxController.dart';
import 'package:yin_drag_sacle/yin_drag_sacle.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

var globalKeyReadingComicsReadingComicsBodyControllerState =
    new GlobalKey<ReadingComicsReadingComicsBodyControllerState>(); //引入 下载页面列表

class ReadingComicsReadingComicsBodyController extends StatefulWidget {
  ReadingComicsReadingComicsBodyController({Key key, this.allPage})
      : super(key: key);
  int allPage;
  @override
  ReadingComicsReadingComicsBodyControllerState createState() =>
      ReadingComicsReadingComicsBodyControllerState();
}

double topDistance = 0.0;
double leftDistance = 0.0;
Axis $pageTurningMode = Axis.horizontal;

class ReadingComicsReadingComicsBodyControllerState
    extends State<ReadingComicsReadingComicsBodyController> {
  @override
  Widget build(BuildContext context) {
    return Swiper(
      onIndexChanged: (e) {
        globalKeyReadingComicsReadingComicsBoxControllerState.currentState
            .setProgress(currentProgress: e + 1);
      },
      index: 10,
      loop: false,
      itemCount: widget.allPage,
      autoplay: false,
      //是否自动播放
      autoplayDelay: 3000,
      //自动播放延迟
      autoplayDisableOnInteraction: true,
      //触发时是否停止播放
      duration: 600,
      //动画时间
      pagination: new SwiperCustomPagination(
        builder: (BuildContext context, SwiperPluginConfig config) {
          return Text('data');
        },
      ), //默认指示器
      //item数量
      itemBuilder: (BuildContext context, int index) {
        //item构建
        return new Stack(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              // child: Image.network(
              //   widget.data.data.banners[index].thumb.fileServer,
              // ),
              child: Container(
                child: DragScaleContainer(
                  child: Image.network(
                      'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                  doubleTapStillScale: false,
                  maxScale: 4, //放大最大倍数
                ),
              ),
            ),
          ],
        );
      },
    );
    return Container(
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(790),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(50)),
      child: GestureDetector(
        child: Container(
          child: PageView(
            scrollDirection: $pageTurningMode,
            //翻页模式 有 Axis.vertical 纵向 和 Axis.horizontal 横向
            children: <Widget>[
              DragScaleContainer(
                child: Image.network(
                    'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                doubleTapStillScale: false,
                maxScale: 4, //放大最大倍数
              ),
              DragScaleContainer(
                child: Image.network(
                    'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                doubleTapStillScale: false,
                maxScale: 4, //放大最大倍数
              ),
              DragScaleContainer(
                child: Image.network(
                    'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                doubleTapStillScale: false,
                maxScale: 4, //放大最大倍数
              ),
              DragScaleContainer(
                child: Image.network(
                    'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                doubleTapStillScale: false,
                maxScale: 4, //放大最大倍数
              ),
              DragScaleContainer(
                child: Image.network(
                    'https://i0.hdslb.com/bfs/album/4fd2594d3da5c47b6dd45c146c38efa8f14047db.jpg'),
                doubleTapStillScale: false,
                maxScale: 4, //放大最大倍数
              ),
            ],
          ),
        ),
      ),

    );
  }

  pageTurningMode(Axis scrollDirection) {
    $pageTurningMode = scrollDirection;
    setState(() {});
  }
}
