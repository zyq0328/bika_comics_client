/*
 * @Date: 2020-02-19 13:39:48
 * @名称: 阅读漫画页面 - 提示 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-08 22:12:00
 * @FilePath: /pica_acg/lib/Pages/ReadingComics/Widget/Tips/ReadingComicsTipsController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ReadingComics/ReadingComicsController.dart';

class ReadingComicsReadingComicsTipsController extends StatefulWidget {
  ReadingComicsReadingComicsTipsController({Key key}) : super(key: key);

  @override
  _ReadingComicsReadingComicsTipsControllerState createState() =>
      _ReadingComicsReadingComicsTipsControllerState();
}

bool visual = true;

class _ReadingComicsReadingComicsTipsControllerState
    extends State<ReadingComicsReadingComicsTipsController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Color(0xff050000),
      child: Container(
        margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(50),
        ),
        child: Column(
          children: <Widget>[
            Container(
              //头部
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(80),
                    height: ScreenUtil().setHeight(80),
                    color: Color(0xfff2ab00),
                    alignment: Alignment.center,
                    child: Text(
                      '返回',
                      style: TextStyle(fontSize: ScreenUtil().setSp(30)),
                    ),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(660),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(10),
                    ),
                    height: ScreenUtil().setHeight(80),
                    alignment: Alignment.center,
                    color: Color(0xffa2e98f),
                    child: Text(
                      '标题',
                      style: TextStyle(fontSize: ScreenUtil().setSp(30)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
              ),
              //身体
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(80),
                    height: ScreenUtil().setHeight(1104),
                    color: Color(0xffe190ae),
                    alignment: Alignment.center,
                    child: Text(
                      '上\n一\n步',
                      style: TextStyle(
                        color: Color(0xffffffff),
                        fontSize: ScreenUtil().setSp(40),
                      ),
                    ),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(570),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(10),
                    ),
                    height: ScreenUtil().setHeight(1104),
                    alignment: Alignment.center,
                    color: Color(0xcf62c9ff),
                    child: GestureDetector(
                      onTap: () {
                        readingComicsState.currentState.isItHiddenTips();
                      },
                      child: Text(
                        '手势区域 (点一下关闭提示)',
                        style: TextStyle(
                          color: Color(0xffffffff),
                          fontSize: ScreenUtil().setSp(40),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(80),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(10),
                    ),
                    height: ScreenUtil().setHeight(1104),
                    color: Color(0xffe392b0),
                    alignment: Alignment.center,
                    child: Text(
                      '下\n一\n步',
                      style: TextStyle(
                        color: Color(0xffffffff),
                        fontSize: ScreenUtil().setSp(40),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
              ),
              //底部
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(80),
                    height: ScreenUtil().setHeight(80),
                    color: Color(0xffa2e98f),
                    alignment: Alignment.center,
                    child: Text(
                      '设置',
                      style: TextStyle(fontSize: ScreenUtil().setSp(30)),
                    ),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(660),
                    margin: EdgeInsets.only(
                      left: ScreenUtil().setHeight(10),
                    ),
                    height: ScreenUtil().setHeight(80),
                    alignment: Alignment.center,
                    color: Color(0xffe18fae),
                    child: Text(
                      '下一步',
                      style: TextStyle(
                        color: Color(0xffffffff),
                        fontSize: ScreenUtil().setSp(40),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
