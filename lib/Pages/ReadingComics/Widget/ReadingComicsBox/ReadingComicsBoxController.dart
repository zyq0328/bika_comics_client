/*
 * @Date: 2020-02-19 14:55:32
 * @名称: 阅读漫画页面 - 框架 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-21 20:36:16
 * @FilePath: /bica_acg/lib/Pages/ReadingComics/Widget/ReadingComicsBox/ReadingComicsBoxController.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalVarlable.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';
import 'package:pica_acg/Pages/Comment/CommentController.dart';
import 'package:pica_acg/Pages/ReadingComics/ReadingComicsController.dart';
import 'package:pica_acg/Pages/ReadingComics/Widget/ReadingComicsBody/ReadingComicsBodyController.dart';
import 'package:pica_acg/Pages/Settings/SettingsContorller.dart';
import 'package:pica_acg/tools/icon/icon.dart';
import 'package:screen/screen.dart';
import 'package:share_extend/share_extend.dart';

var globalKeyReadingComicsReadingComicsBoxControllerState =
    new GlobalKey<ReadingComicsReadingComicsBoxControllerState>(); //引入 下载页面列表
var screenDirection = {
  "text": (setting.screenOrientation == 1) ? '设定竖屏' : "设定横屏",
  "icon": (setting.screenOrientation == 1)
      ? IconFont.iconDirectionPortrait
      : IconFont.iconDirectionTransverse,
  "state": (setting.screenOrientation == 1) ? true : false, //竖屏模式的话 他就是假
};
var pageTurningModeJson = {
  "text": (setting.pageTurningMode == 1) ? '上下翻页' : '左右翻页',
  "icon": (setting.pageTurningMode == 1)
      ? IconFont.iconPageUpAndDown
      : IconFont.iconPanToNextPage,
  "state": (setting.pageTurningMode == 1) ? true : false, //竖屏模式的话 他就是假
};
var autoPlayPage = {
  "text": '自动翻页',
  "textColor": Color(0xffffffff),
  "icon": IconFont.iconAutoPage_,
  "iconColor": Color(0xffffffff),
  "state": false, //是否开启自动播放
};

class ReadingComicsReadingComicsBoxController extends StatefulWidget {
  ReadingComicsReadingComicsBoxController({
    Key key,
    this.allPage,
    this.context,
    this.pageName,
    this.comicsName,
    this.comicsId,
    this.subsectionId,
  }) : super(key: key);
  int allPage;
  final String pageName;
  final String comicsName;
  final BuildContext context;
  final String comicsId; // 漫画id
  final String subsectionId; // 分卷id
  @override
  ReadingComicsReadingComicsBoxControllerState createState() =>
      ReadingComicsReadingComicsBoxControllerState();
}

int currentPageNumber = 1; //当前页数

class ReadingComicsReadingComicsBoxControllerState
    extends State<ReadingComicsReadingComicsBoxController> {
  @override
  void initState() {
    super.initState();
    currentPageNumber = 1; //当前页数
    globalKeyReadingComicsReadingComicsBoxControllerState.currentState
        .setProgress(currentProgress: whatPageDoYouSee);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(1334),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            //头部
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(50),
            ),
            color: Color(0xaf000000),
            child: Row(
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(150),
                  height: ScreenUtil().setHeight(80),
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: Text(
                      languAre().$languAgeOrdinary["basic"]["return"],
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(30),
                        color: Color(0xffffffff),
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(widget.context);
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setHeight(10),
                  ),
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(420),
                  alignment: Alignment.center,
                  child: Text(
                    '${widget.comicsName} - ${widget.pageName}',
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(30),
                      color: Color(0xffffffff),
                    ),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    width: ScreenUtil().setWidth(80),
                    child: Column(
                      children: <Widget>[
                        Icon(
                          IconFont.iconShare,
                          size: ScreenUtil().setSp(50),
                          color: Color(0xffffffff),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(80),
                          alignment: Alignment.center,
                          child: Text(
                            '分享',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(30),
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    ShareExtend.share("share text", "text",
                        sharePanelTitle: "android share panel title",
                        subject: "share subject");
                    print('object');
                  },
                ),
                GestureDetector(
                  child: Container(
                    width: ScreenUtil().setWidth(80),
                    child: Column(
                      children: <Widget>[
                        Icon(
                          IconFont.iconHelp,
                          size: ScreenUtil().setSp(50),
                          color: Color(0xffffffff),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(80),
                          alignment: Alignment.center,
                          child: Text(
                            '帮助',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(30),
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    readingComicsState.currentState.isItHiddenTips();
                  },
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                //身体
                /*  margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(350),
                ), */
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(400),
                color: Color(0xaf000000),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: FlutterSlider(
                        values: [brightness],
                        axis: Axis.vertical,
                        rtl: true,
                        hatchMark: FlutterSliderHatchMark(
                          distanceFromTrackBar: 5,
                          density: 0.5,
                          labels: [
                            /* FlutterSliderHatchMarkLabel(
                                percent: 0,
                                label: Text(
                                  '0%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 10,
                                label: Text(
                                  '10%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 20,
                                label: Text(
                                  '20%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 30,
                                label: Text(
                                  '30%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 40,
                                label: Text(
                                  '40%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 50,
                                label: Text(
                                  '50%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 60,
                                label: Text(
                                  '60%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 70,
                                label: Text(
                                  '70%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 80,
                                label: Text(
                                  '80%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 90,
                                label: Text(
                                  '90%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )),
                            FlutterSliderHatchMarkLabel(
                                percent: 100,
                                label: Text(
                                  '100%',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )), */
                          ],
                        ),
                        max: 100.0,
                        min: 0.0,
                        onDragging: (handlerIndex, lowerValue, upperValue) {
                          brightness = lowerValue;
                          Screen.setBrightness(lowerValue / 100);
                          setState(() {});
                        },
                      ),
                      height: ScreenUtil().setHeight(300),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setHeight(80),
                      alignment: Alignment.center,
                      child: Text(
                        '亮度',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(30),
                          color: Color(0xffffffff),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                //身体
                /* margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(350),
                ), */
                width: ScreenUtil().setWidth(100),
                height: ScreenUtil().setHeight(400),
                color: Color(0xaf000000),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Icon(
                          IconFont.iconList,
                          size: ScreenUtil().setSp(80),
                          color: Color(0xffffffff),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(100),
                          alignment: Alignment.center,
                          child: Text(
                            '列表',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(30),
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Icon(
                          IconFont.iconAtNight,
                          size: ScreenUtil().setSp(80),
                          color: Color(0xffffffff),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(100),
                          alignment: Alignment.center,
                          child: Text(
                            '夜间',
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(30),
                              color: Color(0xffffffff),
                            ),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            IconFont.iconComment,
                            size: ScreenUtil().setSp(80),
                            color: Color(0xffffffff),
                          ),
                          Container(
                            width: ScreenUtil().setWidth(100),
                            alignment: Alignment.center,
                            child: Text(
                              '评论',
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(30),
                                color: Color(0xffffffff),
                              ),
                            ),
                          ),
                        ],
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          PageRouteBuilder(
                            pageBuilder: (context, _, __) => CommentController(
                              bookId: widget.comicsId,
                              subsectionId: widget.subsectionId,
                              bookName: widget.comicsName,
                            ),
                            opaque: false,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            color: Color(0xaf000000),
            child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerRight,
                        width: ScreenUtil().setWidth(120),
                        child: Text(
                          "$currentPageNumber/${widget.allPage}",
                          style: TextStyle(
                            color: Color(0xffffffff),
                          ),
                        ),
                      ),
                      Container(
                        width: ScreenUtil().setWidth(630),
                        child: Slider(
                          label: '当前页 $currentPageNumber',
                          max: (widget.allPage.toDouble() != 0)
                              ? widget.allPage.toDouble()
                              : 1,
                          min: 1,
                          activeColor: Color(0xffffffff),
                          inactiveColor: Color(0x0fffffff),
                          value: currentPageNumber.toDouble(),
                          onChanged: (double v) {
                            setState(() {
                              currentPageNumber = v.floor();
                              globalKeyReadingComicsReadingComicsBodyControllerState
                                  .currentState
                                  .toPage(currentPageNumber);
                            });
                          },
                          /* onChangeStart: (double startValue) {
                      print('Started change at $startValue');
                    },
                    onChangeEnd: (double newValue) {
                      print('Ended change on $newValue');
                    }, */
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      child: GestureDetector(
                        child: Column(
                          children: <Widget>[
                            Icon(
                              screenDirection['icon'],
                              size: ScreenUtil().setSp(60),
                              color: Color(0xffffffff),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(10),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                screenDirection['text'],
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(30),
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          ],
                        ),
                        onTap: () {
                          if (screenDirection['state'] == false) {
                            screenDirection = {
                              "text": '设定竖屏',
                              "icon": IconFont.iconDirectionPortrait,
                              "state": true,
                            };
                            SystemChrome.setPreferredOrientations([
                              DeviceOrientation.landscapeRight,
                              DeviceOrientation.landscapeRight,
                            ]);
                            setting.screenOrientation = 1;
                            updateSetting("ScreenOrientation", true);
                          } else {
                            //设置横向并且修改图标 改为关闭的
                            screenDirection = {
                              "text": '设定横屏',
                              "icon": IconFont.iconDirectionTransverse,
                              "state": false,
                            };
                            SystemChrome.setPreferredOrientations([
                              DeviceOrientation.portraitUp,
                            ]);
                            setting.screenOrientation = 0;
                            updateSetting("ScreenOrientation", false);
                          }

                          setState(() {});
                        },
                      ),
                    ),
                    Container(
                      child: GestureDetector(
                        onTap: () {
                          if (pageTurningModeJson['state'] == false) {
                            globalKeyReadingComicsReadingComicsBodyControllerState
                                .currentState
                                .pageTurningMode(
                              Axis.vertical,
                            );
                            pageTurningModeJson = {
                              "text": '左右翻页',
                              "icon": IconFont.iconPanToNextPage,
                              "state": true, //竖屏模式的话 他就是假
                            };
                            setting.pageTurningMode = 0;
                            updateSetting("PageTurningMode", false);
                          } else {
                            pageTurningModeJson = {
                              "text": '上下翻页',
                              "icon": IconFont.iconPageUpAndDown,
                              "state": false, //竖屏模式的话 他就是假
                            };
                            globalKeyReadingComicsReadingComicsBodyControllerState
                                .currentState
                                .pageTurningMode(
                              Axis.horizontal,
                            );
                            setting.pageTurningMode = 1;
                            updateSetting("PageTurningMode", true);
                          }
                          setState(() {});
                        },
                        child: Column(
                          children: <Widget>[
                            Icon(
                              pageTurningModeJson['icon'],
                              size: ScreenUtil().setSp(60),
                              color: Color(0xffffffff),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(10),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                pageTurningModeJson['text'],
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(30),
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: GestureDetector(
                        onTap: () {
                          if (autoPlayPage['state'] == false) {
                            globalKeyReadingComicsReadingComicsBodyControllerState
                                .currentState
                                .aotuPlay(true);
                            autoPlayPage = {
                              "text": '自动翻页',
                              "textColor": Color(0xffc73379),
                              "icon": IconFont.iconAutoPage_,
                              "iconColor": Color(0xffc73379),
                              "state": true, //是否开启自动播放
                            };
                          } else {
                            globalKeyReadingComicsReadingComicsBodyControllerState
                                .currentState
                                .aotuPlay(false);
                            autoPlayPage = {
                              "text": '自动翻页',
                              "textColor": Color(0xffffffff),
                              "icon": IconFont.iconAutoPage_,
                              "iconColor": Color(0xffffffff),
                              "state": false, //是否开启自动播放
                            };
                          }
                          setState(() {});
                        },
                        child: Column(
                          children: <Widget>[
                            Icon(
                              autoPlayPage["icon"],
                              size: ScreenUtil().setSp(60),
                              color: autoPlayPage["iconColor"],
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: ScreenUtil().setHeight(10),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                autoPlayPage["text"],
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(30),
                                  color: autoPlayPage['textColor'],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Icon(
                          IconFont.iconsetting,
                          size: ScreenUtil().setSp(60),
                          color: Color(0xffffffff),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: ScreenUtil().setHeight(10),
                          ),
                          alignment: Alignment.center,
                          child: GestureDetector(
                            child: Text(
                              '详细设置',
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(30),
                                color: Color(0xffffffff),
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(
                                CupertinoPageRoute(
                                  builder: (BuildContext context) {
                                    // 每次点击再打开一次当前的页面
                                    return Scaffold(
                                      body: SettingContorller(),
                                    );
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  setProgress({
    int currentProgress,
    int allProgress,
  }) {
    if (currentProgress != null) {
      currentPageNumber = currentProgress;
    }
    if (allProgress != null) {
      widget.allPage = allProgress;
    }
    setState(() {});
  }
}
