/*
 * @Date: 2020-02-23 20:15:05
 * @名称: 评论 - 框架 - 底部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-02 20:16:24
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBottom/CommentCommentBottomController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/tools/Controller/NetWork/Get.dart';
import 'package:pica_acg/tools/NetWorkController.dart';

final globalKeyCommentCommentBottomController =
    new GlobalKey<CommentCommentBottomControllerState>(); //评论区key

class CommentCommentBottomController extends StatefulWidget {
  CommentCommentBottomController({
    Key key,
    this.bookId,
    this.subsectionId,
    this.commentTableName,
    this.commentId,
    this.isReplyComment,
  }) : super(key: key);
  final String bookId;
  final String subsectionId;
  final String commentTableName;
  final String commentId;
  final bool isReplyComment;
  @override
  CommentCommentBottomControllerState createState() =>
      CommentCommentBottomControllerState();
}

var inputText = '';
String floor = "null";
String commentId = ""; //主评论id
String tableName = ""; //主评论的表的名称
bool publishCommentState = true;
bool peplyCommentState = false;

class CommentCommentBottomControllerState
    extends State<CommentCommentBottomController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(20),
        right: ScreenUtil().setWidth(20),
      ),
      child: Container(
        height: ScreenUtil().setHeight(100),
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
        child: Stack(
          children: <Widget>[
            Visibility(
              child: PublishComment(
                bookId: widget.bookId,
                subsectionId: widget.subsectionId,
              ),
              visible: !widget.isReplyComment,
            ),
            Visibility(
              child: ReplyComment(
                bookId: widget.bookId,
                commentTableName: widget.commentTableName,
                commentId: widget.commentId,
              ),
              visible: widget.isReplyComment,
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        border: new Border(
          top: Divider.createBorderSide(
            context,
            color: Color(0xffbfbfbf),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }
}

class PublishComment extends StatefulWidget {
  PublishComment({Key key, this.bookId, this.subsectionId}) : super(key: key);
  final String bookId;
  final String subsectionId;

  @override
  _PublishCommentState createState() => _PublishCommentState();
}

class _PublishCommentState extends State<PublishComment> {
  @override
  Widget build(BuildContext context) {
    var inputValue = '';
    return Container(
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Container(
              child: TextField(
                textInputAction: TextInputAction.send,
                decoration: InputDecoration(
                  hintText: "在这写下你的想法吧",
                  // errorText: "errorText",
                  // hintText: "hintText",
                ),
                onChanged: (val) {
                  //当内容改变时
                  inputValue = val;
                },
                onSubmitted: (val) {
                  //当点击键盘的回车时
                  sendComment(widget.bookId, widget.subsectionId, val, context);
                },
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
              child: Container(
                height: ScreenUtil().setHeight(80),
                alignment: Alignment.center,
                child: Text(
                  '发表',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFFffffff),
                    decoration: TextDecoration.none,
                  ),
                ),
                decoration: new BoxDecoration(
                  color: Color(0xffed97b7),
                  border: new Border.all(
                      color: Color(0xFFf092b5), width: 1.0), // 边色与边宽度
                  //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                  borderRadius: new BorderRadius.vertical(
                      top: Radius.elliptical(20, 20),
                      bottom: Radius.elliptical(20, 20)), // 也可控件一边圆角大小
                ),
              ),
              onTap: () {
                sendComment(
                    widget.bookId, widget.subsectionId, inputValue, context);

                /*  Navigator.of(context).push(
                    PageRouteBuilder(
                      pageBuilder: (context, _, __) => GlobalTips(
                        body: '抱歉，您不能发布评论 因为未完善',
                        height: ScreenUtil().setHeight(900),
                        imageAsset: 'images/Tisp/Error.png',
                      ),
                      opaque: false,
                    ),
                  ); */
              },
            ),
          ),
        ],
      ),
    );
  }

  sendComment(bookId, projectId, inputValue, context) {
    loadingDialog(context, "正在提交数据");
    NetWorkController.post(
      operationCommentSendComment(),
      (callBack) {
        Navigator.of(context).pop();
      },
      data: {
        "book_id": bookId,
        "project_id": projectId,
        "content": inputValue,
      },
      headers: getApiRequestHeadrs("Comment-SendComment"),
    );
  }
}

//回复评论
class ReplyComment extends StatefulWidget {
  ReplyComment({
    Key key,
    this.bookId,
    this.commentTableName,
    this.commentId,
  }) : super(key: key);
  final String bookId;
  final String commentTableName;
  final String commentId;

  @override
  _ReplyCommentState createState() => _ReplyCommentState();
}

class _ReplyCommentState extends State<ReplyComment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Container(
              child: TextField(
                textInputAction: TextInputAction.send,
                decoration: InputDecoration(
                  hintText: "请吐槽一下楼主吧",
                  // errorText: "errorText",
                ),
                onSubmitted: (val) {
                  NetWorkController.post(
                    operationCommentSendComment(),
                    (callBack) {
                      print({
                        "book_id": widget.bookId,
                        "content": val,
                        "comment_table_name": widget.commentTableName,
                        "comment_id": widget.commentId,
                      });
                    },
                    data: {
                      "book_id": widget.bookId,
                      "content": val,
                      "comment_table_name": widget.commentTableName,
                      "comment_id": widget.commentId,
                    },
                    headers: getApiRequestHeadrs("Comment-SendComment"),
                  );
                },
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
              child: Container(
                height: ScreenUtil().setHeight(80),
                alignment: Alignment.center,
                child: Text(
                  '发表',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFFffffff),
                    decoration: TextDecoration.none,
                  ),
                ),
                decoration: new BoxDecoration(
                  color: Color(0xffed97b7),
                  border: new Border.all(
                      color: Color(0xFFf092b5), width: 1.0), // 边色与边宽度
                  //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                  borderRadius: new BorderRadius.vertical(
                      top: Radius.elliptical(20, 20),
                      bottom: Radius.elliptical(20, 20)), // 也可控件一边圆角大小
                ),
              ),
              onTap: () {
                NetWork.post("http://sakura.nat300.top/Test1/", (data) {
                  print(data);
                }, parameter: {"aaa": "bbb"});

                /*  Navigator.of(context).push(
                    PageRouteBuilder(
                      pageBuilder: (context, _, __) => GlobalTips(
                        body: '抱歉，您不能发布评论 因为未完善',
                        height: ScreenUtil().setHeight(900),
                        imageAsset: 'images/Tisp/Error.png',
                      ),
                      opaque: false,
                    ),
                  ); */
              },
            ),
          ),
        ],
      ),
    );
  }
}
