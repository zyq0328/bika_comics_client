/*
 * @Date: 2020-02-23 20:25:59
 * @名称: 评论 - 内容
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 22:05:44
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/CommentCommentBodyController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentCommentModuleController.dart';
import 'package:pica_acg/tools/Controller/NetWork/Get.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/SonComment.dart'
    as sonComment;
import 'package:pica_acg/tools/icon/icon.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBottom/CommentCommentBottomController.dart'; //评论回复key

var globalKeyCommentCommentBodyController =
    new GlobalKey<CommentCommentBodyControllerState>();

class CommentCommentBodyController extends StatefulWidget {
  CommentCommentBodyController({
    Key key,
    this.comment,
    this.bookId,
    this.subsectionId,
  }) : super(key: key);
  final CommentInfoJson comment;
  final String bookId;
  final String subsectionId;
  @override
  CommentCommentBodyControllerState createState() =>
      CommentCommentBodyControllerState();
}

class CommentCommentBodyControllerState
    extends State<CommentCommentBodyController> {
  @override
  void initState() {
    super.initState();
    /*  NetWorkController.post(
      getCommentSonComment(
        widget.commentId,
        page: 1,
      ),
      (callBack) {
        if (callBack['code'] == "200") {
          print(callBack);
          globalKeyCommentCommentBodyController.currentState.loadSubComments(
            widget.commentId,
            sonComment.SonComment.fromJson(callBack),
          );
        } else {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '获取分类失败 \n这肯定不是哔咔的问题！\n绝对不是！',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        }
      },
      data: {
        "book_id": widget.bookId,
        "comment_table_name": widget.sonTableName,
      },
      // headers: getApiRequestHeadrs("CommentSonComment"),
    ); */

    // 监听现在的位置是否下滑到了底部
    _scrollController.addListener(
      () {
        if (_scrollController.offset ==
            _scrollController.position.maxScrollExtent) {
          // 加载更多数据
          if (int.parse(widget.comment.data.comments.page) ==
              widget.comment.data.comments.pages) {
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: '已经全部加载完了，下次再来看看吧',
                  height: ScreenUtil().setHeight(900),
                  imageAsset: 'images/Tisp/Error.png',
                ),
                opaque: false,
              ),
            );
          } else {
            loadingDialog(context, "正在获取更多");
            NetWork.get(
              getComment(widget.bookId,
                  page: int.parse(widget.comment.data.comments.page) + 1,
                  chapterId: (widget.subsectionId == null)
                      ? null
                      : widget.subsectionId),
              (callBack) {
                Navigator.pop(context);
                var temp = CommentInfoJson.fromJson(callBack);
                widget.comment.data.comments.docs
                    .addAll(temp.data.comments.docs);
                widget.comment.data.comments.limit = temp.data.comments.limit;
                widget.comment.data.comments.page = temp.data.comments.page;
                widget.comment.data.comments.pages = temp.data.comments.pages;
                widget.comment.data.comments.total = temp.data.comments.total;
                setState(() {});
              },
            );
            setState(() {});
          }
        }
      },
    );
  }

  final _scrollController = ScrollController();
  Map<String, dynamic> commentComment = Map(); // 这里记录回复了某个评论的列表 里面的数据是
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Color(0xffffffff),
          height: ScreenUtil().setHeight(1094),
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView.builder(
              controller: _scrollController,
              itemCount: widget.comment.data.comments.docs.length,
              itemBuilder: (BuildContext context, int index) {
                var info = widget.comment.data.comments.docs[index];
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: CommentCommentCommentModuleController(
                    userInfo: info.uUser, //用户信息
                    comment: info.content, //评论
                    allFloor: widget.comment.data.comments.total,
                    time: info.createdAt, //发布时间
                    index: index, //key
                    likeNumber: info.likesCount,
                    isLike: info.isLiked,
                    isTop: info.isTop,
                    commentNumber: info.commentsCount,
                    // childrensList: commentComment[info.id],
                    page: 1,
                    commentId: info.id, //获取下一页子评论 需要提交父评论id
                    tableName: info.tableName,
                    sonTableName: info.sonTableName,
                    bookId: widget.bookId,
                  ),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: '举报',
                      color: Colors.red,
                      icon: IconFont.icontanhao,
                      onTap: () => {
                        print(
                            '您正在举报${widget.comment.data.comments.docs[index].content}')
                      },
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  void loadSubComments(
    //更新评论数据
    String commentId,
    sonComment.SonComment sonComment,
  ) {
    commentComment[commentId] = sonComment;
    setState(() {});
  }

  void updateFavoriteData(
    int key,
    String commentId,
    bool state,
  ) {
    widget.comment.data.comments.docs[key].isLiked = state;
    if (state) {
      widget.comment.data.comments.docs[key].likesCount++;
    } else {
      widget.comment.data.comments.docs[key].likesCount--;
    }
    setState(() {});
  }
}
