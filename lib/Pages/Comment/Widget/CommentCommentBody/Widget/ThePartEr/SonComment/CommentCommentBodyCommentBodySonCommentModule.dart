/*
 * @Date: 2020-08-22 23:24:39
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-06 21:30:44
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/ThePartEr/SonComment/CommentCommentBodyCommentBodySonCommentModule.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Comment/InfiniteSonComment/InfiniteSonCommentController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentTheParErController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/ConmentCommentTheFirstPartController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/SonComment.dart'
    as sonComment;

class SonCommentModule extends StatefulWidget {
  SonCommentModule({
    Key key,
    this.userInfo,
    this.comment,
    this.allFloor,
    this.time,
    this.index,
    this.likeNumber,
    this.isLike,
    this.isTop,
    this.childrensList,
    this.commentNumber,
    this.bookId,
    this.commentId,
    this.commentTableName,
  }) : super(key: key);
  final User userInfo;
  final String comment;
  final int allFloor;
  final String time;
  final int index;
  final int likeNumber;
  final int commentNumber;
  final bool isLike;
  final bool isTop;
  final String bookId;
  final String commentId;
  final String commentTableName;
  final sonComment.Comments childrensList;
  @override
  _SonCommentModuleState createState() => _SonCommentModuleState();
}

class _SonCommentModuleState extends State<SonCommentModule> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Container(
          child: Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(20),
              right: ScreenUtil().setWidth(20),
              top: ScreenUtil().setHeight(20),
            ),
            child: GestureDetector(
              child: Column(
                children: <Widget>[
                  ConmentCommentTheFirstPartController(
                    userInfo: widget.userInfo,
                    comment: widget.comment,
                  ),
                  /* 第一部分 */
                  CommentCommentTheParErController(
                    allFloor: widget.allFloor,
                    time: widget.time,
                    index: widget.index,
                    likeNumber: widget.likeNumber,
                    isLike: widget.isLike,
                    commentNumber: widget.commentNumber,
                    /* 第二部分 */
                  ),
                ],
              ),
              onTap: () {
                /* Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => Scaffold(
                      body: InfiniteSonCommentController(
                        bookId: widget.bookId,
                        title: '正在查看:${widget.userInfo.name}的评论',
                        commentId: widget.commentId,
                        commentTableName:
                            "book_comment_son_kzrp4pg4puf5fxkjioemgbwbe5y6fwhg",
                        initiaWidget: Container(
                          child: Container(
                            child: Container(
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(20),
                                  right: ScreenUtil().setWidth(20),
                                  top: ScreenUtil().setHeight(20),
                                ),
                                child: GestureDetector(
                                  child: Column(
                                    children: <Widget>[
                                      ConmentCommentTheFirstPartController(
                                        userInfo: widget.userInfo,
                                        comment: widget.comment,
                                      ),
                                      /* 第一部分 */
                                      CommentCommentTheParErController(
                                        allFloor: widget.allFloor,
                                        time: widget.time,
                                        index: widget.index,
                                        likeNumber: widget.likeNumber,
                                        isLike: widget.isLike,
                                        commentNumber: widget.commentNumber,
                                        /* 第二部分 */
                                      ),
                                    ],
                                  ),
                                ),
                                decoration: new BoxDecoration(
                                  color: Color(0xfff8f8f8),
                                  border: Border(
                                      bottom: BorderSide(
                                    color: Color(0x5F000000),
                                    width: 0.5,
                                  )),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ); */
              },
            ),
            decoration: new BoxDecoration(
              color: Color(0xfff8f8f8),
              border: Border(
                  bottom: BorderSide(
                color: Color(0x5F000000),
                width: 0.5,
              )),
            ),
          ),
        ),
      ),
    );
  }
}
