/*
 * @Date: 2020-02-23 23:31:31
 * @名称: 评论 - 内容 - 第二部分 - 楼层 和 评论时间 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-02-24 21:37:52
 * @FilePath: /pica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/ThePartEr/CommentCommentBodyCommentBodyFloorCommentaryTime.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';

class CommentCommentBodyCommentBodyFloorCommentaryTime extends StatefulWidget {
  CommentCommentBodyCommentBodyFloorCommentaryTime({
    Key key,
    this.floor,
    this.time,
  }) : super(key: key);
  final int floor;
  final String time;

  @override
  _CommentCommentBodyCommentBodyFloorCommentaryTimeState createState() =>
      _CommentCommentBodyCommentBodyFloorCommentaryTimeState();
}

class _CommentCommentBodyCommentBodyFloorCommentaryTimeState
    extends State<CommentCommentBodyCommentBodyFloorCommentaryTime> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Text(
          "${widget.floor} 楼 / " +
              iSO_8601ToNewData(widget.time, false, supers: true),
          style: TextStyle(
            color: Color(0x5f000000),
            fontSize: ScreenUtil().setSp(27),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}
