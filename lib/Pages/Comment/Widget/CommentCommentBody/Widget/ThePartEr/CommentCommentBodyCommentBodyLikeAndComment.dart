/*
 * @Date: 2020-02-23 23:31:31
 * @名称: 评论 - 内容 - 第二部分 - 喜欢 喜欢人数 和 评论 评论人数 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-26 20:38:22
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/ThePartEr/CommentCommentBodyCommentBodyLikeAndComment.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/tools/Controller/NetWork/Get.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentLikeAndUnLike..dart';
import 'package:pica_acg/tools/icon/icon.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/CommentCommentBodyController.dart'; //更新数据用

class CommentCommentBodyCommentBodyLikeAndComment extends StatefulWidget {
  CommentCommentBodyCommentBodyLikeAndComment({
    Key key,
    this.likeNumber,
    this.isLike,
    this.commentNumber,
    this.commentId,
    this.bookId,
    this.index,
  }) : super(key: key);
  final int likeNumber;
  final bool isLike;
  final int commentNumber;
  final int index;
  final String commentId; //评论id
  final String bookId; //书籍id
  @override
  _CommentCommentBodyCommentBodyLikeAndCommentState createState() =>
      _CommentCommentBodyCommentBodyLikeAndCommentState();
}

class _CommentCommentBodyCommentBodyLikeAndCommentState
    extends State<CommentCommentBodyCommentBodyLikeAndComment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Row(
          children: <Widget>[
            Container(
              child: GestureDetector(
                child: Row(
                  children: [
                    Icon(
                      IconFont.iconCommentLike,
                      color: Color(
                          (widget.isLike == true) ? 0xffed98b8 : 0x5f000000),
                    ),
                    Container(
                      child: Text(
                        widget.likeNumber.toString(),
                        style: TextStyle(
                          color: Color((widget.isLike == true)
                              ? 0xffed98b8
                              : 0x5f000000),
                        ),
                      ),
                      margin: EdgeInsets.only(
                        top: ScreenUtil().setHeight(5),
                        left: ScreenUtil().setWidth(10),
                        right: ScreenUtil().setWidth(10),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  NetWorkController.post(
                    operationCommentLikeAndNoLike(widget.commentId),
                    (callBack) {
                      var likeState = CommentLikeAndUnLike.fromJson(callBack);
                      if (likeState.code == "200") {
                        //判断 返回的code是不是200 200为正常 其他的请参考服务器
                        globalKeyCommentCommentBodyController.currentState
                            .updateFavoriteData(
                          widget.index,
                          widget.commentId,
                          (likeState.data.action == "like") ? true : false,
                        );
                      } else {
                        Navigator.of(context).push(
                          PageRouteBuilder(
                            pageBuilder: (context, _, __) => GlobalTips(
                              body: '发生意外错误:错误代码${likeState.code}具体错误请参考官网',
                              height: ScreenUtil().setHeight(900),
                            ),
                            opaque: false,
                          ),
                        );
                      }
                    },
                    data: {
                      "book_id": widget.bookId,
                    },
                  );
                },
              ),
            ),
            Visibility(
              child: Row(
                children: <Widget>[
                  Icon(
                    IconFont.iconCommentReply,
                    color: Color(0x5f000000),
                  ),
                  Container(
                    child: Text(
                      widget.commentNumber.toString(),
                      style: TextStyle(
                        color: Color(0x5f000000),
                      ),
                    ),
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(5),
                      left: ScreenUtil().setWidth(10),
                    ),
                  ),
                ],
              ),
              visible: (widget.commentNumber != null),
            ),
          ],
        ),
      ),
    );
  }
}
