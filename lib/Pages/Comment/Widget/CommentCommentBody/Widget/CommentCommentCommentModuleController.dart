/*
 * @Date: 2020-03-18 22:35:38
 * @名称: 评论 - 内容 - 子评论控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 22:02:18
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentCommentModuleController.dart
 */

//评论模型
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/Comment/InfiniteSonComment/InfiniteSonCommentController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/CommentCommentBodyController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentTheParErController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleIsTop.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleLoadMore.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleLoadNoReply.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/ConmentCommentTheFirstPartController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBottom/CommentCommentBottomController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart'; //主要是用 这里面的user模型
import 'package:pica_acg/tools/extends/Json/Comment/Bika/SonComment.dart'
    as sonComment;

class CommentCommentCommentModuleController extends StatefulWidget {
  CommentCommentCommentModuleController({
    Key key,
    this.userInfo,
    this.comment,
    this.allFloor,
    this.time,
    this.index,
    this.likeNumber,
    this.isLike,
    this.isTop,
    this.commentNumber,
    this.page,
    this.commentId,
    this.bookId,
    this.tableName,
    this.sonTableName,
    this.comicsInfo,
  }) : super(key: key);
  final Docs comicsInfo;
  final User userInfo;
  final String comment;
  final int allFloor;
  final String time;
  final int index;
  final int likeNumber;
  final bool isLike;
  final bool isTop;
  final int commentNumber;
  final int page;
  final String commentId;
  final String bookId; //书籍id
  final String tableName; //这是我的评论的表的名称
  final String sonTableName; //这是我的评论的表的名称
  @override
  _CommentCommentCommentModuleControllerState createState() =>
      _CommentCommentCommentModuleControllerState();
}

Map hidden = Map();
Map isRequest = Map();

class _CommentCommentCommentModuleControllerState
    extends State<CommentCommentCommentModuleController> {
  @override
  Widget build(BuildContext context) {
    if (hidden[widget.commentId] == null) {
      hidden[widget.commentId] = true; //子评论是否隐藏 总控制
      isRequest[widget.commentId] = false; //是否请求过第一次评论
    }

    return Container(
      child: Container(
        color: Colors.white,
        child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(20),
            right: ScreenUtil().setWidth(20),
            top: ScreenUtil().setHeight(20),
          ),
          child: Column(
            children: <Widget>[
              Visibility(
                visible: widget.isTop,
                child: CommentCommentCommentModuleIsTop(
                  //置顶🔝显示
                  allFloor: widget.allFloor,
                  index: widget.index,
                ),
              ),
              GestureDetector(
                child: ConmentCommentTheFirstPartController(
                  userInfo: widget.userInfo,
                  comment: widget.comment,
                ),
                onTap: () {
                  print(widget.comment);
                  print(widget.tableName);
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: InfiniteSonCommentController(
                          bookId: widget.bookId,
                          title: '正在查看:${widget.userInfo.name}的评论',
                          commentId: widget.commentId,
                          commentTableName: widget.tableName,
                          sonTableName: widget.sonTableName,
                          initiaWidget: Container(
                            child: Container(
                              child: Container(
                                child: Container(
                                  margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(20),
                                    right: ScreenUtil().setWidth(20),
                                    top: ScreenUtil().setHeight(20),
                                  ),
                                  child: GestureDetector(
                                    child: Column(
                                      children: <Widget>[
                                        ConmentCommentTheFirstPartController(
                                          userInfo: widget.userInfo,
                                          comment: widget.comment,
                                        ),
                                        /* 第一部分 */
                                        CommentCommentTheParErController(
                                          allFloor: widget.allFloor,
                                          time: widget.time,
                                          index: widget.index,
                                          likeNumber: widget.likeNumber,
                                          isLike: widget.isLike,
                                          commentNumber: widget.commentNumber,
                                          /* 第二部分 */
                                        ),
                                      ],
                                    ),
                                  ),
                                  decoration: new BoxDecoration(
                                    color: Color(0xfff8f8f8),
                                    border: Border(
                                        bottom: BorderSide(
                                      color: Color(0x5F000000),
                                      width: 0.5,
                                    )),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                  isRequest[widget.commentId] = true;
                  setState(() {});
                },
              ),
              /* 第一部分 */
              CommentCommentTheParErController(
                allFloor: widget.allFloor,
                time: widget.time,
                index: widget.index,
                likeNumber: widget.likeNumber,
                isLike: widget.isLike,
                commentNumber: widget.commentNumber,
                commentId: widget.commentId,
                bookId: widget.bookId,
              ),
            ],
          ),
          decoration: new BoxDecoration(
            border: Border(
                bottom: BorderSide(
              color: Color(0x5F000000),
              width: 0.5,
            )),
            color: Color(0xFFFFFFFF), // 底色
          ),
        ),
      ),
    );
  }
}
