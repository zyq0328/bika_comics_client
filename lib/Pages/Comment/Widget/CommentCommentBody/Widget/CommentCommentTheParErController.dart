/*
 * @Date: 2020-02-23 23:29:23
 * @名称: 评论 - 内容 - 第二部分控制器 //?包含了 楼层 评论时间 喜欢 喜欢人数 和 评论 评论人数 六部分
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-20 22:00:23
 * @FilePath: /pica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentTheParErController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/ThePartEr/CommentCommentBodyCommentBodyFloorCommentaryTime.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/ThePartEr/CommentCommentBodyCommentBodyLikeAndComment.dart';

class CommentCommentTheParErController extends StatefulWidget {
  CommentCommentTheParErController({
    Key key,
    this.allFloor,
    this.likeNumber,
    this.index,
    this.isLike,
    this.commentNumber,
    this.time,
    this.commentId,
    this.bookId,
  }) : super(key: key);
  final int allFloor;
  final int likeNumber;
  final int index;
  final bool isLike;
  final int commentNumber;
  final String time;
  final String commentId; //评论id
  final String bookId; //书籍id
  @override
  _CommentCommentTheParErControllerState createState() =>
      _CommentCommentTheParErControllerState();
}

class _CommentCommentTheParErControllerState
    extends State<CommentCommentTheParErController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(680),
      //下半部分 发布楼层 发布时间 喜欢 评论 和分界线
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        left: ScreenUtil().setWidth(10),
        right: ScreenUtil().setWidth(10),
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // 下半部分 发布楼层 发布时间 喜欢 评论
            children: <Widget>[
              CommentCommentBodyCommentBodyFloorCommentaryTime(
                floor: widget.allFloor - widget.index,
                time: widget.time,
              ),
              CommentCommentBodyCommentBodyLikeAndComment(
                index: widget.index, //楼层也是之前数组的序列号
                likeNumber: widget.likeNumber,
                isLike: widget.isLike,
                commentNumber: widget.commentNumber,
                commentId: widget.commentId,
                bookId: widget.bookId,
              ),
            ],
          ),
        ],
      ),

      alignment: Alignment.topCenter,
      height: ScreenUtil().setHeight(70),
    );
  }
}
