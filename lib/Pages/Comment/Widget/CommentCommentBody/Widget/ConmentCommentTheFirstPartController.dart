/*
 * @Date: 2020-02-23 23:25:43
 * @名称: 评论 - 内容 - 第一部分控制器 //?包含了 用户头像 用户昵称 用户等级 评论四个模块
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 23:47:19
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/ConmentCommentTheFirstPartController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalSettings.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserHeadImages.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserHeadImagesframe.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserLevel.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart';

class ConmentCommentTheFirstPartController extends StatefulWidget {
  ConmentCommentTheFirstPartController({Key key, this.userInfo, this.comment})
      : super(key: key);
  final User userInfo;
  final String comment;
  @override
  _ConmentCommentTheFirstPartControllerState createState() =>
      _ConmentCommentTheFirstPartControllerState();
}

class _ConmentCommentTheFirstPartControllerState
    extends State<ConmentCommentTheFirstPartController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        //上半部分 名称 评论 等级 头像 头像框
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                CommentCommentBodyCommentBodyUserHeadImages(
                  src: (widget.userInfo.avatar != null)
                      ? springboard() +
                          widget.userInfo.avatar.fileServer +
                          widget.userInfo.avatar.path
                      : $global_ImagesUrlUndefined,
                ),
                CommentCommentBodyCommentBodyUserHeadImagesframe(
                  src: widget.userInfo.character,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(10),
            ),
            constraints: BoxConstraints(
              minHeight: ScreenUtil().setHeight(150),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.userInfo.name,
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(30),
                      fontWeight: FontWeight.w700),
                ), //昵称
                CommentCommentBodyCommentBodyUserLevel(
                  level: widget.userInfo.level,
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(500),
                  child: Text(
                    widget.comment,
                    softWrap: true,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(30),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
