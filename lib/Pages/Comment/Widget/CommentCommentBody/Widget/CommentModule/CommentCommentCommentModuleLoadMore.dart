/*
 * @Date: 2020-03-18 22:37:58
 * @名称: 评论 - 内容 - 子评论 - 加载更多 - 组建
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 21:18:07
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleLoadMore.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/tools/NetWorkController.dart';

class CommentCommentCommentModuleLoadMore extends StatelessWidget {
  const CommentCommentCommentModuleLoadMore({Key key, this.text})
      : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(750),
      child: Center(
        child: Text(text),
      ),
      color: Color(0xff49b8a1),
    );
  }
}
