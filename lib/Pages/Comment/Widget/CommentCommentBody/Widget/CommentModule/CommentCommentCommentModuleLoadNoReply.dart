/*
 * @Date: 2020-03-18 22:37:58
 * @名称: 评论 - 内容 - 子评论 - 没有任何回复 - 组建
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-19 00:19:32
 * @FilePath: /pica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleLoadNoReply.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CommentCommentCommentModuleLoadNoReply extends StatelessWidget {
  const CommentCommentCommentModuleLoadNoReply({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(50),
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(750),
      child: Text(
        '还没有任何回复!要吐槽一下么...?',
        style: TextStyle(color: Color(0xffffffff)),
      ),
      color: Color(0xffff5f69),
    );
  }
}
