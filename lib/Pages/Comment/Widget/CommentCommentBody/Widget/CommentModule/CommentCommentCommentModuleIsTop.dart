/*
 * @Date: 2020-03-18 22:37:58
 * @名称: 评论 - 内容 - 子评论 - 加载更多 - 组建
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-19 01:02:45
 * @FilePath: /pica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/CommentModule/CommentCommentCommentModuleIsTop.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CommentCommentCommentModuleIsTop extends StatelessWidget {
  const CommentCommentCommentModuleIsTop({Key key, this.index, this.allFloor})
      : super(key: key);
  final int index;
  final int allFloor;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(40),
      alignment: Alignment.center,
      width: ScreenUtil().setWidth(750),
      child: Text(
        '${allFloor - index}楼的留言已置顶🔝',
        style: TextStyle(
            color: Color(0xffffffff), fontSize: ScreenUtil().setSp(20)),
      ),
      color: Color(0xffffce42),
    );
  }
}
