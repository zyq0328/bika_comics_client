/*
 * @Date: 2020-02-23 20:28:34
 * @名称: 评论 - 内容 - 第一部分 - 用户头像框
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 01:30:46
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserHeadImagesframe.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';

class CommentCommentBodyCommentBodyUserHeadImagesframe extends StatefulWidget {
  CommentCommentBodyCommentBodyUserHeadImagesframe({Key key, this.src})
      : super(key: key);
  final String src;
  @override
  _CommentCommentBodyCommentBodyUserHeadImagesframeState createState() =>
      _CommentCommentBodyCommentBodyUserHeadImagesframeState();
}

class _CommentCommentBodyCommentBodyUserHeadImagesframeState
    extends State<CommentCommentBodyCommentBodyUserHeadImagesframe> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: NetworkCustomImage(
        url: widget.src,
        fit: BoxFit.fill,
      ),
      width: ScreenUtil().setWidth(150),
      height: ScreenUtil().setWidth(150),
    );
  }
}
