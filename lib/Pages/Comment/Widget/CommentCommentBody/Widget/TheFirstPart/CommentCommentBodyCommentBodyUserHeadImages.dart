/*
 * @Date: 2020-02-23 20:28:34
 * @名称: 评论 - 内容 - 第一部分 - 用户头像
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 01:32:40
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserHeadImages.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';

class CommentCommentBodyCommentBodyUserHeadImages extends StatefulWidget {
  CommentCommentBodyCommentBodyUserHeadImages({Key key, this.src})
      : super(key: key);
  final String src;
  @override
  _CommentCommentBodyCommentBodyUserHeadImagesState createState() =>
      _CommentCommentBodyCommentBodyUserHeadImagesState();
}

class _CommentCommentBodyCommentBodyUserHeadImagesState
    extends State<CommentCommentBodyCommentBodyUserHeadImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(15),
        left: ScreenUtil().setWidth(15),
      ),
      child: ClipOval(
        child: NetworkCustomImage(
          url: widget.src,
          fit: BoxFit.cover,
        ),
      ),
      width: ScreenUtil().setWidth(130),
      height: ScreenUtil().setWidth(130),
      decoration: new BoxDecoration(
        color: Color(0xffffe3a6), // 底色
        border: new Border.all(color: Color(0xFFed98b8), width: 2), // 边色与边宽度
        //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
        borderRadius: new BorderRadius.vertical(
          top: Radius.elliptical(50, 50),
          bottom: Radius.elliptical(50, 50),
        ), // 也可控件一边圆角大小
      ),
    );
  }
}
