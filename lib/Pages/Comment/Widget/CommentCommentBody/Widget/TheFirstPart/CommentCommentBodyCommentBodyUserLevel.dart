/*
 * @Date: 2020-02-23 20:28:34
 * @名称: 评论 - 内容 - 第一部分 - 用户的等级
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-02-27 21:37:17
 * @FilePath: /pica_acg/lib/Pages/Comment/Widget/CommentCommentBody/Widget/TheFirstPart/CommentCommentBodyCommentBodyUserLevel.dart
 */
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CommentCommentBodyCommentBodyUserLevel extends StatefulWidget {
  CommentCommentBodyCommentBodyUserLevel({Key key, this.level})
      : super(key: key);
  final int level;
  @override
  _CommentCommentBodyCommentBodyUserLevelState createState() =>
      _CommentCommentBodyCommentBodyUserLevelState();
}

class _CommentCommentBodyCommentBodyUserLevelState
    extends State<CommentCommentBodyCommentBodyUserLevel> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        //等级
        children: [
          Text(
            'Lv.${widget.level}',
            style: TextStyle(
                color: Color(0xffed9bb9),
                fontSize: ScreenUtil().setSp(30),
                fontWeight: FontWeight.w700),
          ),
          Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(10),
            ),
            child: Container(
              child: Container(
                margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(10),
                  right: ScreenUtil().setWidth(10),
                ),
                child: Text(
                  "萌新",
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontSize: ScreenUtil().setSp(30),
                  ),
                ),
              ),
              decoration: new BoxDecoration(
                color: Color(0xfffebc11),
                border: new Border.all(
                    color: Color(0xFFd69e6b), width: 0.5), // 边色与边宽度
                //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                borderRadius: new BorderRadius.vertical(
                  top: Radius.elliptical(20, 20),
                  bottom: Radius.elliptical(20, 20),
                ), // 也可控件一边圆角大小
              ),
            ),
          ),
        ],
      ),
    );
  }
}
