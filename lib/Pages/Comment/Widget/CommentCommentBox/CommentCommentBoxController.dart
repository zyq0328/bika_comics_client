/*
 * @Date: 2020-02-23 20:15:05
 * @名称: 评论 - 框架
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-02 20:16:32
 * @FilePath: /bica_acg/lib/Pages/Comment/Widget/CommentCommentBox/CommentCommentBoxController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';

class CommentCommentBoxController extends StatefulWidget {
  CommentCommentBoxController({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _CommentCommentBoxControllerState createState() =>
      _CommentCommentBoxControllerState();
}

class _CommentCommentBoxControllerState
    extends State<CommentCommentBoxController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: ScreenUtil().setHeight(100),
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
        child: Flex(
          direction: Axis.horizontal,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                child: Text(
                  languAre().$languAgeOrdinary["basic"]["return"],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFF434343),
                    decoration: TextDecoration.none,
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Expanded(
              flex: 6,
              child: Text(
                ' ${widget.title}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(
                '回复',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 19.0,
                  color: Color(0xFF434343),
                  decoration: TextDecoration.none,
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          border: new Border(
            bottom: Divider.createBorderSide(
              context,
              color: Color(0xffbfbfbf),
              width: 1,
            ),
          ), // 边色与边宽度
        ),
      ),
    );
  }
}
