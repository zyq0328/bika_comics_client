/*
 * @Date: 2020-02-22 21:03:39
 * @名称: 评论控制器 - 子评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-02 20:36:23
 * @FilePath: /bica_acg/lib/Pages/Comment/InfiniteSonComment/InfiniteSonCommentController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/Pages/Comment/InfiniteSonComment/CommentCommentBodyController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBottom/CommentCommentBottomController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBox/CommentCommentBoxController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/SonComment.dart';

var comment = SonComment.fromJson({
  "message": "success",
  "data": {
    "comments": {"page": 1, "pages": 1, "docs": [], "total": 5, "limit": 50}
  },
  "code": "200"
});

class InfiniteSonCommentController extends StatefulWidget {
  InfiniteSonCommentController({
    Key key,
    this.bookId,
    this.title,
    this.commentId,
    this.initiaWidget,
    this.commentTableName,
    this.sonTableName,
  }) : super(key: key);
  final String bookId; //书籍id
  final String commentId; //评论id
  final String title; //书籍名字
  final String commentTableName; //自己评论的表的名称
  final String sonTableName; //子评论表的名字
  final Widget initiaWidget; //默认的 widget

  @override
  _InfiniteSonCommentControllerState createState() =>
      _InfiniteSonCommentControllerState();
}

List<Docs> topList = List();
List<Docs> noTopList = List();
int $page = 0;
var childrensList = SonComment.fromJson($netWorkSonComment);
var $hidden = true;

class _InfiniteSonCommentControllerState
    extends State<InfiniteSonCommentController> {
  @override
  void initState() {
    super.initState();
    NetWorkController.post(
        getCommentSonComment(
          widget.commentId,
          page: $page + 1,
        ), (callBack) {
      $hidden = false;
      comment = SonComment.fromJson(callBack);
      setState(() {});
    }, data: {
      "book_id": widget.bookId,
      "comment_table_name": widget.sonTableName,
    });
    setState(() {});
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CommentCommentBoxController(
            title: widget.title,
          ),
          widget.initiaWidget,
          Expanded(
            child: Scaffold(
              resizeToAvoidBottomPadding: true,
              body: Column(
                children: [
                  Visibility(
                    child: Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("images/Loading/loading.gif"),
                            Text(
                              "正在获取漫画评论",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(40),
                                color: Color(0xffe19eb6),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    visible: $hidden,
                  ),
                  Visibility(
                    child: Expanded(
                      child: InfiniteSonCommentBody(
                        bookId: widget.bookId,
                        comment: comment,
                        commentId: widget.commentId,
                        commentTableName:
                            widget.sonTableName, //这里应该穿入 子评论 所在额表的名称
                      ),
                    ),
                    visible: !$hidden,
                  ),
                ],
              ),
            ),
          ),
          CommentCommentBottomController(
            bookId: widget.bookId,
            commentTableName: widget.commentTableName,
            commentId: widget.commentId,
            isReplyComment: true,
          ),
        ],
      ),
    );
  }
}
