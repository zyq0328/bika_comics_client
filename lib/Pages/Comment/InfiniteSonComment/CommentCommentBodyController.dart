/*
 * @Date: 2020-09-02 21:57:50
 * @名称: 评论中的子评论 渲染器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-10-12 21:25:48
 * @FilePath: /bica_acg/lib/Pages/Comment/InfiniteSonComment/CommentCommentBodyController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentCommentModuleController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/SonComment.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart'
    as commentInfoJson; //主要是用 这里面的user模型

class InfiniteSonCommentBody extends StatefulWidget {
  InfiniteSonCommentBody({
    Key key,
    this.bookId,
    this.comment,
    this.commentId,
    this.commentTableName,
  }) : super(key: key);
  final String bookId;
  final SonComment comment;
  final String commentId;
  final String commentTableName;
  @override
  _InfiniteSonCommentBodyState createState() => _InfiniteSonCommentBodyState();
}

class _InfiniteSonCommentBodyState extends State<InfiniteSonCommentBody> {
  @override
  Widget build(BuildContext context) {
    // Map<String, dynamic> commentComment = Map(); // 这里记录回复了某个评论的列表 里面的数据是
    return MediaQuery.removePadding(
      removeTop: true,
      context: context,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: widget.comment.data.comments.docs.length,
        itemBuilder: (BuildContext context, int index) {
          return CommentCommentCommentModuleController(
            userInfo: commentInfoJson.User.fromJson(
              widget.comment.data.comments.docs[index].uUser.toJson(),
            ),
            comment: widget.comment.data.comments.docs[index].content,
            allFloor: widget.comment.data.comments.total,
            time: widget.comment.data.comments.docs[index].createdAt,
            index: index,
            likeNumber: widget.comment.data.comments.docs[index].likesCount,
            isLike: widget.comment.data.comments.docs[index].isLiked,
            isTop: widget.comment.data.comments.docs[index].isTop,
            // childrensList: commentComment[index],
            commentNumber:
                widget.comment.data.comments.docs[index].commentCount,
            page: widget.comment.data.comments.page,
            commentId: widget.comment.data.comments.docs[index].commentId,
            bookId: widget.bookId,
            tableName: widget.commentTableName,
            sonTableName: widget.comment.data.comments.docs[index].sonTableName,
          );
        },
      ),
    );
  }
}
