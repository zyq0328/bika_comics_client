/*
 * @Date: 2020-02-22 21:03:39
 * @名称: 评论控制器 - 主评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 21:33:37
 * @FilePath: /bica_acg/lib/Pages/Comment/CommentController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/CommentCommentBodyController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBottom/CommentCommentBottomController.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/Comment/Bika/CommentInfoJson.dart';
import 'Widget/CommentCommentBox/CommentCommentBoxController.dart';

var comment = CommentInfoJson.fromJson($netWorkComment);

class CommentController extends StatefulWidget {
  CommentController({
    Key key,
    this.bookId,
    this.bookName,
    this.subsectionId,
  }) : super(key: key);
  final String bookId; //书籍id
  final String subsectionId; //分卷id
  final String bookName; //书籍名字
  @override
  _CommentControllerState createState() => _CommentControllerState();
}

List<Docs> topList = List();
List<Docs> noTopList = List();

class _CommentControllerState extends State<CommentController> {
  @override
  void initState() {
    //对数据进行重新排列
    super.initState();

    NetWorkController.get(
      getComment(widget.bookId,
          chapterId:
              (widget.subsectionId == null) ? null : widget.subsectionId),
      (callBack) {
        comment = CommentInfoJson.fromJson(callBack);
        setState(() {});
      },
      headers: getApiRequestHeadrs('Comment-GetComment'),
    );

    for (var i = 0; i < comment.data.comments.docs.length; i++) {
      if (comment.data.comments.docs[i].isTop == true) {
        /* comment.data.comments.docs.insert(0, comment.data.comments.docs[i]);
        comment.data.comments.docs.removeAt(i); */
        topList.insert(0, comment.data.comments.docs[i]);
      } else {
        noTopList.insert(0, comment.data.comments.docs[i]);
      }
    }
    comment.data.comments.docs = List();
    for (var i = 0; i < noTopList.length; i++) {
      comment.data.comments.docs.insert(0, noTopList[i]);
    }
    for (var i = 0; i < topList.length; i++) {
      comment.data.comments.docs.insert(0, topList[i]);
    }
  }

  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
          child: Container(
            child: Stack(
              children: [
                Column(
                  children: <Widget>[
                    CommentCommentBoxController(
                      title: widget.bookName,
                    ),
                    Visibility(
                      child: CommentCommentBodyController(
                        key: globalKeyCommentCommentBodyController,
                        comment: comment,
                        bookId: widget.bookId,
                        subsectionId: widget.subsectionId,
                      ),
                      visible: (comment.data.comments.docs.length > 0),
                    ),
                    Visibility(
                      child: Container(
                        height: ScreenUtil().setHeight(1094),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("images/Other/Other2.png"),
                            Text(
                              "暂时没有评论 ，来评论一个吧",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(40),
                                color: Color(0xffe19eb6),
                              ),
                            ),
                          ],
                        ),
                      ),
                      visible: (comment.data.comments.docs.length == 0),
                    ),
                    CommentCommentBottomController(
                      //漫画主评论 的 发布评论
                      key: globalKeyCommentCommentBottomController,
                      bookId: widget.bookId,
                      subsectionId: widget.subsectionId,
                      isReplyComment: false,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
