/*
 * @Date: 2020-02-04 23:32:58
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块3 - 评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 17:05:02
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3Comment.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/Comment/CommentController.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsInfoComicsInfoBodyModule3Comment extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule3Comment(
      {Key key, this.comment, this.bookId, this.bookName})
      : super(key: key);
  final int comment;
  final String bookId;
  final String bookName;
  @override
  _ComicsInfoComicsInfoBodyModule3CommentState createState() =>
      _ComicsInfoComicsInfoBodyModule3CommentState();
}

class _ComicsInfoComicsInfoBodyModule3CommentState
    extends State<ComicsInfoComicsInfoBodyModule3Comment> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(3),
              ),
              child: Icon(
                IconFont.iconiconComment,
                size: ScreenUtil().setSp(100),
                color: Color(0xffaaaaaa),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(80),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(5),
                  bottom: ScreenUtil().setHeight(5),
                  left: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                ),
                child: Text(widget.comment.toString()),
              ),
              decoration: new BoxDecoration(
                color: Color(0xffff5f69),
                border: new Border.all(
                    color: Color(0xFFd69e6b), width: 0.5), // 边色与边宽度
                //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                borderRadius: BorderRadius.only(
                  topLeft: Radius.elliptical(30, 30),
                  topRight: Radius.elliptical(30, 30),
                  bottomRight: Radius.elliptical(30, 30),
                ),
                /* new BorderRadius.vertical(
              top: Radius.elliptical(
                ScreenUtil().setHeight(30),
                ScreenUtil().setHeight(30),
              ),
              bottom: Radius.elliptical(
                ScreenUtil().setHeight(30),
                ScreenUtil().setHeight(30),
              ),
            ), // 也可控件一边圆角大小
          ), */
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) => CommentController(
              bookId: widget.bookId,
              subsectionId: null,
              bookName: widget.bookName,
            ),
          ),
        );
      },
    );
  }
}
