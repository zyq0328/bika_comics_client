/*
 * @Date: 2020-02-04 23:09:27
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块3 - 开始阅读
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 13:07:33
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3StartReading.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule3StartReading extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule3StartReading({Key key}) : super(key: key);

  @override
  _ComicsInfoComicsInfoBodyModule3StartReadingState createState() =>
      _ComicsInfoComicsInfoBodyModule3StartReadingState();
}

class _ComicsInfoComicsInfoBodyModule3StartReadingState
    extends State<ComicsInfoComicsInfoBodyModule3StartReading> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(100),
      width: ScreenUtil().setWidth(300),
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
      ),
      alignment: Alignment.center,
      child: Text(
        '开始阅读',
        style: TextStyle(
          color: Color(0xfffdf9fa),
          fontSize: ScreenUtil().setSp(40),
        ),
      ),
      decoration: new BoxDecoration(
        border: new Border.all(color: Color(0xFFf2bacf), width: 1.0), // 边色与边宽度
        color: Color(0xffec97b6),
        //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
        borderRadius: new BorderRadius.vertical(
            top: Radius.elliptical(30, 30),
            bottom: Radius.elliptical(30, 30)), // 也可控件一边圆角大小
      ),
    );
  }
}
