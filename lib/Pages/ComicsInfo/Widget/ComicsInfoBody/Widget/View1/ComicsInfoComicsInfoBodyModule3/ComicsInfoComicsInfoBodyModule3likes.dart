/*
 * @Date: 2020-02-05 12:20:16
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块3 - 喜欢
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-01 21:10:07
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3likes.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/tools/NetWorkController.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/ComicsInfoLikeAndUnLike.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsInfoComicsInfoBodyModule3likes extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule3likes(
      {Key key, this.likes, this.islike, this.bookId})
      : super(key: key);
  int likes;
  String bookId;
  bool islike;
  @override
  _ComicsInfoComicsInfoBodyModule3likesState createState() =>
      _ComicsInfoComicsInfoBodyModule3likesState();
}

class _ComicsInfoComicsInfoBodyModule3likesState
    extends State<ComicsInfoComicsInfoBodyModule3likes> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //喜欢图标点击
      onTap: () {
        NetWorkController.post(
          operationComicsLikeAndNoLike(),
          (callBack) {
            var $data = ComicsInfoLikeAndUnLike.fromJson(callBack);
            switch ($data.data.action) {
              case "unlike":
                widget.likes = widget.likes - 1;
                widget.islike = false;
                setState(() {});
                break;
              case "like":
                widget.likes = widget.likes + 1;
                widget.islike = true;
                setState(() {});
                break;
              default:
            }
          },
          data: {"book_id": widget.bookId},
          headers: getApiRequestHeadrs('ComicsLike'),
        );
      },
      child: Container(
        margin: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(3),
              ),
              child: Icon(
                (widget.islike)
                    ? IconFont.iconiconLike
                    : IconFont.iconiconLikeNull,
                size: ScreenUtil().setSp(100),
                color: Color((widget.islike) ? 0xffff5f69 : 0xffaaaaaa),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: ScreenUtil().setWidth(80),
              ),
              child: Container(
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(5),
                  bottom: ScreenUtil().setHeight(5),
                  left: ScreenUtil().setWidth(20),
                  right: ScreenUtil().setWidth(20),
                ),
                child: Text(widget.likes.toString()),
              ),
              decoration: new BoxDecoration(
                color: Color(0xffff5f69),
                border: new Border.all(
                    color: Color(0xFFd69e6b), width: 0.5), // 边色与边宽度
                //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
                borderRadius: BorderRadius.only(
                  topLeft: Radius.elliptical(30, 30),
                  topRight: Radius.elliptical(30, 30),
                  bottomRight: Radius.elliptical(30, 30),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
