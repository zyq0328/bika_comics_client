/*
 * @Date: 2020-02-03 17:41:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块3
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-06-20 23:46:36
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3Controller.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3Comment.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3StartReading.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3/ComicsInfoComicsInfoBodyModule3likes.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoComicsInfoBodyModule3Controller extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule3Controller({Key key, this.comicsInfo})
      : super(key: key);
  final ComicsInfo comicsInfo;
  @override
  _ComicsInfoComicsInfoBodyModule3ControllerState createState() =>
      _ComicsInfoComicsInfoBodyModule3ControllerState();
}

class _ComicsInfoComicsInfoBodyModule3ControllerState
    extends State<ComicsInfoComicsInfoBodyModule3Controller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setHeight(10),
        right: ScreenUtil().setHeight(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          ComicsInfoComicsInfoBodyModule3StartReading(),
          ComicsInfoComicsInfoBodyModule3Comment(
            comment: widget.comicsInfo.data.comic.commentsCount,
            bookId: widget.comicsInfo.data.comic.sId,
            bookName: widget.comicsInfo.data.comic.title,
          ),
          ComicsInfoComicsInfoBodyModule3likes(
            likes: widget.comicsInfo.data.comic.likesCount,
            islike: widget.comicsInfo.data.comic.isLiked,
            bookId: widget.comicsInfo.data.comic.sId,
          ),
        ],
      ),
    );
  }
}
