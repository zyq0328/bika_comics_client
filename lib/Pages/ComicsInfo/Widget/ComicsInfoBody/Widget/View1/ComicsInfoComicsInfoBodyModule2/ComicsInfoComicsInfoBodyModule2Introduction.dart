/*
 * @Date: 2020-02-04 22:35:25
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块2 - 简介
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 17:01:42
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2/ComicsInfoComicsInfoBodyModule2Introduction.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsInfoComicsInfoBodyModule2Introduction extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule2Introduction({Key key, this.introduction})
      : super(key: key);
  final String introduction;
  @override
  _ComicsInfoComicsInfoBodyModule2IntroductionState createState() =>
      _ComicsInfoComicsInfoBodyModule2IntroductionState();
}

double widgetMaxHeight = 0; //组建 最大 高度
double height = ScreenUtil.screenHeight; //组建高度
double singleRowHeight = ScreenUtil().setHeight(34.5); //单行高度
Animation<double> widgetAngle;

var animeState;
AnimationController controller;

class _ComicsInfoComicsInfoBodyModule2IntroductionState
    extends State<ComicsInfoComicsInfoBodyModule2Introduction>
    with TickerProviderStateMixin {
  @override
  void dispose() {
    // 释放资源
    controller.dispose();
    super.dispose();
  }

  void initState() {
    super.initState();
    widgetMaxHeight = 0; //组建 最大 高度
    height = ScreenUtil.screenHeight; //组建高度
    singleRowHeight = ScreenUtil().setHeight(34.5); //单行高度

    WidgetsBinding.instance.addPostFrameCallback((mag) {
      widgetMaxHeight = context.size.height - singleRowHeight;
      controller.reverse(from: controller.upperBound); //反转
      setState(() {});
    });
    controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addListener(() {
        height = singleRowHeight + (widgetMaxHeight * widgetAngle.value);
        this.setState(() {});
      });
    widgetAngle = new Tween(begin: 0.0, end: 1.0).animate(controller);
    controller.addStatusListener((status) {
      animeState = status;
    });
  }

  Widget build(BuildContext context) {
    var introduction =
        (widget.introduction == null) ? "没有简介" : widget.introduction;

    return Row(
      children: <Widget>[
        Container(
          constraints: BoxConstraints(
            maxHeight: height,
          ),
          margin: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
          width: ScreenUtil().setWidth(684),
          child: Text(
            introduction,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(30),
            ),
          ),
        ),
        Visibility(
          child: GestureDetector(
            child: ExpandingAndShrinking(),
            onTap: () {
              if (animeState == AnimationStatus.completed) {
                controller.reverse(from: controller.upperBound); //反转
              } else {
                controller.forward(); //正传
              }
            },
          ),
          visible: (widgetMaxHeight <= 0) ? false : true,
        ),
      ],
    );
  }
}

class TagsModule extends StatefulWidget {
  TagsModule({Key key, this.title, this.color, this.borderRadius})
      : super(key: key);
  final String title;
  final Color color;
  final BorderRadius borderRadius;
  @override
  _TagsModuleState createState() => _TagsModuleState();
}

class _TagsModuleState extends State<TagsModule> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(10),
          top: ScreenUtil().setHeight(10),
          bottom: ScreenUtil().setHeight(10),
        ),
        child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(22),
            right: ScreenUtil().setWidth(20),
          ),
          height: ScreenUtil().setHeight(40),
          child: Text(
            widget.title,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(30),
              // backgroundColor: Color(0xffff0000),
              color: Color(0xffdf6c8e), // 字体颜色
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        decoration: new BoxDecoration(
          color: widget.color, // 底色
          border:
              new Border.all(color: Color(0xFFe68ea9), width: 0.5), // 边色与边宽度
          //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
          borderRadius: widget.borderRadius,
        ),
      ),
    );
  }
}

class ExpandingAndShrinking extends StatefulWidget {
  ExpandingAndShrinking({Key key}) : super(key: key);

  @override
  _ExpandingAndShrinkingState createState() => _ExpandingAndShrinkingState();
}

class _ExpandingAndShrinkingState extends State<ExpandingAndShrinking>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: 3.1 * widgetAngle.value,
      child: Icon(IconFont.iconDown),
    );
  }
}
