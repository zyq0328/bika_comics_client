/*
 * @Date: 2020-02-03 17:43:37
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块2 - 标签
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 17:01:14
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2/ComicsInfoComicsInfoBodyModule2Tags.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalVarlableJson.dart';
import 'package:pica_acg/tools/icon/icon.dart';

class ComicsInfoComicsInfoBodyModule2Tags extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule2Tags({Key key, this.tags}) : super(key: key);
  final List<String> tags;
  @override
  _ComicsInfoComicsInfoBodyModule2TagsState createState() =>
      _ComicsInfoComicsInfoBodyModule2TagsState();
}

double height = ScreenUtil.screenHeight;
double maximumAltitudeRange = 0; //最大高度范围 用总高度减去一行高度
double singleRowHeight = ScreenUtil().setHeight(62); //单行高度

var animeState;
Animation<double> widgetAngle; //遮罩层
AnimationController controller;

class _ComicsInfoComicsInfoBodyModule2TagsState
    extends State<ComicsInfoComicsInfoBodyModule2Tags>
    with TickerProviderStateMixin {
  @override
  void dispose() {
    // 释放资源
    controller.dispose();
    super.dispose();
  }

  void initState() {
    super.initState();
    height = ScreenUtil.screenHeight;
    maximumAltitudeRange = 0; //最大高度范围 用总高度减去一行高度
    singleRowHeight = ScreenUtil().setHeight(62); //单行高度

    WidgetsBinding.instance.addPostFrameCallback((mag) {
      maximumAltitudeRange = context.size.height - ScreenUtil().setHeight(62);
      controller.reverse(from: controller.upperBound); //反转
      setState(() {});
    });
    controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addListener(() {
        height = singleRowHeight + (maximumAltitudeRange * widgetAngle.value);
        this.setState(() {});
      });
    widgetAngle = new Tween(begin: 0.0, end: 1.0).animate(controller);
    controller.addStatusListener((status) {
      animeState = status;
    });
  }

  Widget build(BuildContext context) {
    @override
    var tags = widget.tags;
    if (tags.length > 0) {
      return Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(696),
            constraints: BoxConstraints(
              maxHeight: height,
            ),
            child: Wrap(
              children: List.generate(
                tags.length,
                (index) {
                  return SizedBox(
                    child: TagsModule(
                      title: tags[index],
                      color: Color(
                        0x3fe68ea9,
                      ),
                      borderRadius: comicsComicsBoxComicsBoxClassBox(
                        true,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          GestureDetector(
            child: ExpandingAndShrinking(),
            onTap: () {
              if (animeState == AnimationStatus.completed) {
                controller.reverse(from: controller.upperBound); //反转
              } else {
                controller.forward(); //正传
              }
            },
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}

class TagsModule extends StatefulWidget {
  TagsModule({Key key, this.title, this.color, this.borderRadius})
      : super(key: key);
  final String title;
  final Color color;
  final BorderRadius borderRadius;
  @override
  _TagsModuleState createState() => _TagsModuleState();
}

class _TagsModuleState extends State<TagsModule> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(10),
          top: ScreenUtil().setHeight(10),
          bottom: ScreenUtil().setHeight(10),
        ),
        child: Container(
          margin: EdgeInsets.only(
            left: ScreenUtil().setWidth(22),
            right: ScreenUtil().setWidth(20),
          ),
          height: ScreenUtil().setHeight(40),
          child: Text(
            widget.title,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(30),
              color: Color(0xffdf6c8e), // 字体颜色
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        decoration: new BoxDecoration(
          color: widget.color, // 底色
          border:
              new Border.all(color: Color(0xFFe68ea9), width: 0.5), // 边色与边宽度
          borderRadius: widget.borderRadius,
        ),
      ),
    );
  }
}

class ExpandingAndShrinking extends StatefulWidget {
  ExpandingAndShrinking({Key key}) : super(key: key);

  @override
  _ExpandingAndShrinkingState createState() => _ExpandingAndShrinkingState();
}

class _ExpandingAndShrinkingState extends State<ExpandingAndShrinking>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: 3.1 * widgetAngle.value,
      child: Icon(IconFont.iconDown),
    );
  }
}
