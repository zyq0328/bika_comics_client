/*
 * @Date: 2020-02-03 17:17:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 - 分类 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 18:26:19
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Class.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule1Class extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Class({
    Key key,
    this.categories,
  }) : super(key: key);
  final List<String> categories;
  @override
  _ComicsInfoComicsInfoBodyModule1ClassState createState() =>
      _ComicsInfoComicsInfoBodyModule1ClassState();
}

class _ComicsInfoComicsInfoBodyModule1ClassState
    extends State<ComicsInfoComicsInfoBodyModule1Class> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      child: Text(
        djawiodjwai(widget.categories),
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: Color(0xffc5c5c5),
          fontSize: ScreenUtil().setSp(30),
        ),
      ),
    );
  }
}

String djawiodjwai(List<String> list) {
  var a = '分类：';
  for (var i = 0; i < list.length; i++) {
    if (list.length == i + 1) {
      a += list[i];
    } else {
      a += list[i] + '、';
    }
  }
  return a;
}
