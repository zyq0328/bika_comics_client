/*
 * @Date: 2020-02-03 16:58:47
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 - 作者 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-05-24 21:22:30
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Author.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoComicsInfoBodyModule1Author extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Author({Key key, this.author})
      : super(key: key);
  final List<Author> author;

  @override
  _ComicsInfoComicsInfoBodyModule1AuthorState createState() =>
      _ComicsInfoComicsInfoBodyModule1AuthorState();
}

class _ComicsInfoComicsInfoBodyModule1AuthorState
    extends State<ComicsInfoComicsInfoBodyModule1Author> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(34),
      child: Wrap(
        spacing: 5.0,
        runSpacing: 0.0,
        children: List.generate(
          widget.author.length,
          (index) {
            return Text(
              widget.author[index].name,
              style: TextStyle(
                color: Color(0xffee9ab9),
              ),
            );
          },
        ),
      ),
    );
  }
}
