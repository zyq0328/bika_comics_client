/*
 * @Date: 2020-02-03 17:17:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 - 查看人数 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 19:34:05
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Number.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule1Number extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Number({Key key, this.totalViews})
      : super(key: key);
  final int totalViews;
  @override
  _ComicsInfoComicsInfoBodyModule1NumberState createState() =>
      _ComicsInfoComicsInfoBodyModule1NumberState();
}

class _ComicsInfoComicsInfoBodyModule1NumberState
    extends State<ComicsInfoComicsInfoBodyModule1Number> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      child: Text(
        '绅士指明次数：${widget.totalViews}',
        softWrap: true,
        style: TextStyle(
          color: Color(0xffc5c5c5),
          fontSize: ScreenUtil().setSp(30),
        ),
      ),
    );
  }
}
