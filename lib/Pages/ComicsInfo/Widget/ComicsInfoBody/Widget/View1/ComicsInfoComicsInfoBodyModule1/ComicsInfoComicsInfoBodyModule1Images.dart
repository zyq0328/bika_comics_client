/*
 * @Date: 2020-02-03 16:58:47
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 - 图片 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 01:37:39
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Images.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';

class ComicsInfoComicsInfoBodyModule1Images extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Images({Key key, this.imageSrc})
      : super(key: key);
  final String imageSrc;
  @override
  _ComicsInfoComicsInfoBodyModule1ImagesState createState() =>
      _ComicsInfoComicsInfoBodyModule1ImagesState();
}

class _ComicsInfoComicsInfoBodyModule1ImagesState
    extends State<ComicsInfoComicsInfoBodyModule1Images> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(188),
      height: ScreenUtil().setHeight(250),
      child: NetworkCustomImage(
        url: widget.imageSrc,
      ),
    );
  }
}
