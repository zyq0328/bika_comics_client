/*
 * @Date: 2020-02-03 17:17:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 - 标题 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 16:07:28
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Title.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule1Title extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Title({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ComicsInfoComicsInfoBodyModule1TitleState createState() =>
      _ComicsInfoComicsInfoBodyModule1TitleState();
}

class _ComicsInfoComicsInfoBodyModule1TitleState
    extends State<ComicsInfoComicsInfoBodyModule1Title> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(430),
      child: Text(
        widget.title,
        softWrap: true,
        maxLines: 3,
        textAlign: TextAlign.left,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: ScreenUtil().setSp(30),
        ),
      ),
    );
  }
}
