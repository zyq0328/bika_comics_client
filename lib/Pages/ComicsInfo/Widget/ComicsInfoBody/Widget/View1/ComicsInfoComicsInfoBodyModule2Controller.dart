/*
 * @Date: 2020-02-03 17:41:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块2
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-10 22:00:35
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2Controller.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2/ComicsInfoComicsInfoBodyModule2Introduction.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2/ComicsInfoComicsInfoBodyModule2Tags.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoComicsInfoBodyModule2Controller extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule2Controller({Key key, this.comicsInfo})
      : super(key: key);
  final ComicsInfo comicsInfo;
  @override
  _ComicsInfoComicsInfoBodyModule2ControllerState createState() =>
      _ComicsInfoComicsInfoBodyModule2ControllerState();
}

class _ComicsInfoComicsInfoBodyModule2ControllerState
    extends State<ComicsInfoComicsInfoBodyModule2Controller> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ComicsInfoComicsInfoBodyModule2Tags(
          tags: widget.comicsInfo.data.comic.tags,
        ),
        ComicsInfoComicsInfoBodyModule2Introduction(
          introduction: widget.comicsInfo.data.comic.description,
        ),
      ],
    );
  }
}
