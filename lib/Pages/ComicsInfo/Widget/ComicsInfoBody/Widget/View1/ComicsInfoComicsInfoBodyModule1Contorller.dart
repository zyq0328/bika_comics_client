/*
 * @Date: 2020-02-03 16:57:44
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块1 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 17:28:32
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1Contorller.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalVarlable.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Author.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1ChineseTeam.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Class.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Images.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Number.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1/ComicsInfoComicsInfoBodyModule1Title.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoComicsInfoBodyModule1Contorller extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule1Contorller({
    Key key,
    this.title,
    this.author,
    this.categories,
    this.likeNumber,
    this.comicsInfo,
  }) : super(key: key);
  final String title;
  final List<Author> author;
  final List<String> categories;
  final int likeNumber;
  final ComicsInfo comicsInfo;
  @override
  _ComicsInfoComicsInfoBodyModule1ContorllerState createState() =>
      _ComicsInfoComicsInfoBodyModule1ContorllerState();
}

class _ComicsInfoComicsInfoBodyModule1ContorllerState
    extends State<ComicsInfoComicsInfoBodyModule1Contorller> {
  @override
  Widget build(BuildContext context) {
    $globalVarlableBookCover["Src"] = springboard() +
        widget.comicsInfo.data.comic.thumb.fileServer +
        widget.comicsInfo.data.comic.thumb.path;
    $globalVarlableBookCover["SeverName"] =
        widget.comicsInfo.data.comic.thumb.originalName;
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(10),
        right: ScreenUtil().setWidth(10),
      ),
      child: Row(
        children: <Widget>[
          ComicsInfoComicsInfoBodyModule1Images(
            imageSrc: $globalVarlableBookCover["Src"],
          ),
          Container(
            width: ScreenUtil().setWidth(530),
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ComicsInfoComicsInfoBodyModule1Title(title: widget.title),
                Container(
                  height: ScreenUtil().setHeight(34),
                  child: Row(
                    children: <Widget>[
                      Text(
                        '作者:',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(28),
                          color: Color(0xffee9ab9),
                        ),
                      ),
                      ComicsInfoComicsInfoBodyModule1Author(
                        author: widget.author,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: ScreenUtil().setHeight(34),
                  child: Row(
                    children: <Widget>[
                      Text(
                        '汉化:',
                        style: TextStyle(
                          fontSize: ScreenUtil().setSp(28),
                          color: Color(0xff4b8bf5),
                        ),
                      ),
                      ComicsInfoComicsInfoBodyModule1ChineseTeam(
                        author: widget.author,
                      ),
                    ],
                  ),
                ),
                ComicsInfoComicsInfoBodyModule1Number(
                  totalViews: widget.comicsInfo.data.comic.totalViews,
                ),
                ComicsInfoComicsInfoBodyModule1Class(
                  categories: widget.categories,
                ),
              ],
            ),
            height: ScreenUtil().setHeight(250),
          ),
        ],
      ),
    );
  }
}
