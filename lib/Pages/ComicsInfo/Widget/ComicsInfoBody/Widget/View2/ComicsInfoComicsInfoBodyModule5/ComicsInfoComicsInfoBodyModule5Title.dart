/*
 * @Date: 2020-02-05 14:34:57
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块5 - 大家都在看的 - 标题
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 20:45:43
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5/ComicsInfoComicsInfoBodyModule5Title.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule5Title extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule5Title({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ComicsInfoComicsInfoBodyModule5TitleState createState() =>
      _ComicsInfoComicsInfoBodyModule5TitleState();
}

class _ComicsInfoComicsInfoBodyModule5TitleState
    extends State<ComicsInfoComicsInfoBodyModule5Title> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(170),
      child: Text(
        widget.title,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(),
      ),
    );
  }
}
