/*
 * @Date: 2020-02-05 14:34:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块5 - 大家都在看的 图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 01:31:08
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5/ComicsInfoComicsInfoBodyModule5Images.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalWidget.dart';

class ComicsInfoComicsInfoBodyModule5Images extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule5Images({Key key, this.image})
      : super(key: key);
  final String image;
  @override
  _ComicsInfoComicsInfoBodyModule5ImagesState createState() =>
      _ComicsInfoComicsInfoBodyModule5ImagesState();
}

class _ComicsInfoComicsInfoBodyModule5ImagesState
    extends State<ComicsInfoComicsInfoBodyModule5Images> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(170),
      height: ScreenUtil().setHeight(230),
      child: NetworkCustomImage(
        url: widget.image,
        fit: BoxFit.cover,
      ),
    );
  }
}
