/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 分卷
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:51:19
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4Subsection.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalVarlable.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBody/Widget/ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress.dart';
import 'package:pica_acg/Pages/ReadingComics/ReadingComicsController.dart';
import 'package:pica_acg/anime/LiuDong.dart';
import 'package:pica_acg/anime/anime.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';

class ComicsInfoComicsInfoBodyModule4Subsection extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4Subsection(
      {Key key, this.allChapter, this.comicsName, this.comicsId})
      : super(key: key);
  final AllChapterList allChapter;
  final String comicsName;
  final String comicsId;
  @override
  ComicsInfoComicsInfoBodyModule4SubsectionState createState() =>
      ComicsInfoComicsInfoBodyModule4SubsectionState();
}

class ComicsInfoComicsInfoBodyModule4SubsectionState
    extends State<ComicsInfoComicsInfoBodyModule4Subsection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        spacing: 5, //主轴上子控件的间距
        runSpacing: 10, //交叉轴上子控件之间的间距
        children: List.generate(
          widget.allChapter.data.eps.docs.length,
          (index) {
            var $fenJuanId = widget.allChapter.data.eps.docs[index].id;
            return SizedBox(
              child: GestureDetector(
                child: Stack(
                  alignment: const FractionalOffset(0.1, 1.0),
                  children: [
                    Container(
                      child: Container(
                        width: ScreenUtil().setWidth(120),
                        margin: EdgeInsets.only(
                          left: ScreenUtil().setWidth(24),
                          right: ScreenUtil().setWidth(24),
                          top: ScreenUtil().setHeight(24),
                          bottom: ScreenUtil().setHeight(24),
                        ),
                        child: Text(
                          widget.allChapter.data.eps.docs[index].title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(30),
                            fontWeight: FontWeight.w700,
                            color: (downList[widget.comicsId] != null &&
                                    downList[widget.comicsId][$fenJuanId] !=
                                        null)
                                ? downList[widget.comicsId][$fenJuanId]['Color']
                                    ['TextColor']
                                : Color(0xff000000),
                            shadows: [
                              BoxShadow(
                                  color: Colors.grey, offset: Offset(0.2, 0.2))
                            ],
                          ),
                        ),
                      ),
                      decoration: new BoxDecoration(
                        color: (downList[widget.comicsId] != null &&
                                downList[widget.comicsId][$fenJuanId] != null)
                            ? downList[widget.comicsId][$fenJuanId]['Color']
                                ['Color']
                            : Color(0xffffffff),
                        border: new Border.all(
                            color: Color(0xFFf0f0f0), width: 1.0), // 边色与边宽度
                        //        borderRadius: new BorderRadius.circular((20.0)), // 圆���度
                      ),
                    ),
                    Visibility(
                      visible: (downList[widget.comicsId] != null &&
                              downList[widget.comicsId][$fenJuanId] != null &&
                              downList[widget.comicsId][$fenJuanId]['Extends']
                                      ["Progress"] !=
                                  1)
                          ? true
                          : false,
                      child: Container(
                        margin: EdgeInsets.only(
                          // top: ScreenUtil().setHeight(124),
                          left: ScreenUtil().setWidth(2),
                        ),
                        child:
                            ComicsInfoDownloadComicsInfoDownloadBodyListDownloadProgress(
                          progress: (downList[widget.comicsId] != null &&
                                  downList[widget.comicsId][$fenJuanId] != null)
                              ? downList[widget.comicsId][$fenJuanId]['Extends']
                                  ["Progress"]
                              : 0,
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      pageBuilder: (context, _, __) => AnimeLiuDong(
                        widget: ReadingComicsController(
                          key: readingComicsState,
                          context: context,
                          pageName:
                              widget.allChapter.data.eps.docs[index].title,
                          comicsName: widget.comicsName,
                          comicsId: widget.comicsId, //漫画id
                          subsectionId: widget
                              .allChapter.data.eps.docs[index].id, //漫画的分卷id
                        ),
                      ),
                      opaque: false,
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  //更新下载位置
  bool modifyBlockLocation() {
    //首先判断是不是当前漫画信息
    /**
       * 正常情况数组是这样的
       *      {
       *        "Title": '${item["Title"]}', //分卷名
       *        "Revised": true,
       *        "Color": {
       *          'Color': Color(0xffff9a05),
       *          'TextColor': Color(0xffffffff),
       *        },
       *        "Extends": {
       *          "Download": false,
       *          "Success": false,
       *          "Progress": item["Progress"],
       *          "SetProgress": false,
       *        }
       *      };
       */
//挖坑这里会有问题
//为什么屏蔽下面 因为下载列表开始下载会自动按照下面的进行赋值
    /*  if (downList[$comicsId] == null) {
          print(downList);
        }
        downList[$comicsId][$chapterId] = {
          "Title": downList[$comicsId][$chapterId]["Title"], //分卷名
          "Revised": downList[$comicsId][$chapterId]["Revised"],
          "Color": {
            'Color': Color(0xffff9a05),
            'TextColor': Color(0xffffffff),
          },
          "Extends": {
            "Download": downList[$comicsId][$chapterId]["Download"],
            "Success": downList[$comicsId][$chapterId]["Success"],
            "Progress": $progress,
            "SetProgress": downList[$comicsId][$chapterId]["SetProgress"],
          }
        }; */
    setState(() {});

    return true;
  }
}
