/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 更多章节
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-03-02 19:37:15
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4More.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalKey.dart';

class ComicsInfoComicsInfoBodyModule4More extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4More(
      {Key key, this.allPage, this.page, this.comicsId})
      : super(key: key);
  final String comicsId;
  final int allPage;
  final int page;
  @override
  _ComicsInfoComicsInfoBodyModule4MoreState createState() =>
      _ComicsInfoComicsInfoBodyModule4MoreState();
}

var $state = true;

class _ComicsInfoComicsInfoBodyModule4MoreState
    extends State<ComicsInfoComicsInfoBodyModule4More> {
  @override
  Widget build(BuildContext context) {
    if (widget.allPage == widget.page) {
      $state = false;
    } else {
      $state = true;
    }
    setState(() {});
    return Visibility(
      visible: $state,
      child: Container(
        margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(30),
          right: ScreenUtil().setWidth(30),
          top: ScreenUtil().setHeight(20),
        ),
        child: GestureDetector(
          child: Container(
            margin: EdgeInsets.only(
              left: ScreenUtil().setWidth(30),
              right: ScreenUtil().setWidth(30),
              top: ScreenUtil().setHeight(20),
              bottom: ScreenUtil().setHeight(20),
            ),
            child: Text('更多章节'),
          ),
          onTap: () {
            $state = false;
            comicsInfoComicsInfoBodyControllerState.currentState
                .netWorkGetAllChapter(widget.comicsId, page: (widget.page + 1));
            print(widget.page);
            setState(() {});
          },
        ),
        decoration: new BoxDecoration(
          border:
              new Border.all(color: Color(0xffaaaaaa), width: 0.5), // 边色与边宽度
          //        borderRadius: new BorderRadius.circular((20.0)), // 圆角度
          // 也可控件一边圆角大小
        ),
      ),
    );
  }
}
