/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 其他人也在看
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-13 17:11:31
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4OthersAreWatching.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule4OthersAreWatching extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4OthersAreWatching({Key key}) : super(key: key);

  @override
  _ComicsInfoComicsInfoBodyModule4OthersAreWatchingState createState() =>
      _ComicsInfoComicsInfoBodyModule4OthersAreWatchingState();
}

class _ComicsInfoComicsInfoBodyModule4OthersAreWatchingState
    extends State<ComicsInfoComicsInfoBodyModule4OthersAreWatching> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(20),
        bottom: ScreenUtil().setHeight(20),
        left: ScreenUtil().setWidth(10),
      ),
      child: Text(
        '相关推荐',
        style: TextStyle(
          color: Color(0xff656565),
          fontSize: ScreenUtil().setSp(40),
        ),
      ),
    );
  }
}
