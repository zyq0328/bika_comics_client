/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 用户名称 和 更新时间
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 13:10:31
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4UserNameAndTime.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule4UserNameAndTime extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4UserNameAndTime({Key key, this.name})
      : super(key: key);
  final String name;
  @override
  _ComicsInfoComicsInfoBodyModule4UserNameAndTimeState createState() =>
      _ComicsInfoComicsInfoBodyModule4UserNameAndTimeState();
}

class _ComicsInfoComicsInfoBodyModule4UserNameAndTimeState
    extends State<ComicsInfoComicsInfoBodyModule4UserNameAndTime> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.name,
            style: TextStyle(
              color: Color(0xffee9ebc),
              fontSize: ScreenUtil().setSp(40),
            ),
          ),
          Text(
            '今天 01:11',
            style: TextStyle(
              color: Color(0xffb0b0b0),
              fontSize: ScreenUtil().setSp(30),
            ),
          ),
        ],
      ),
    );
  }
}
