/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 用户头像
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-05 20:43:30
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4UserHead.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyModule4UserHead extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4UserHead({Key key, this.headSrc})
      : super(key: key);
  final String headSrc;
  @override
  _ComicsInfoComicsInfoBodyModule4UserHeadState createState() =>
      _ComicsInfoComicsInfoBodyModule4UserHeadState();
}

class _ComicsInfoComicsInfoBodyModule4UserHeadState
    extends State<ComicsInfoComicsInfoBodyModule4UserHead> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipOval(
        child: Image.network(
          widget.headSrc,
          width: ScreenUtil().setHeight(100),
          height: ScreenUtil().setHeight(100),
          fit: BoxFit.cover,
        ),
      ),
      decoration: new BoxDecoration(
        border: new Border.all(color: Color(0xFFf092b5), width: 3.0), // 边色与边宽度
        color: Color(0xFFFFFFFF), // 底色
        //        borderRadius: new BorderRadius.circular((20.0)), // 圆���度
        borderRadius: new BorderRadius.vertical(
            top: Radius.elliptical(
                ScreenUtil().setHeight(180), ScreenUtil().setHeight(180)),
            bottom: Radius.elliptical(ScreenUtil().setHeight(180),
                ScreenUtil().setHeight(180))), // 也可控件���边圆角大小
      ),
    );
  }
}
