/*
 * @Date: 2020-02-05 12:37:57
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-13 20:33:19
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4Controller.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4More.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4OthersAreWatching.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4Subsection.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4UserHead.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4UserNameAndTime.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoComicsInfoBodyModule4Controller extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule4Controller(
      {Key key, this.comicsInfo, this.allChapter})
      : super(key: key);
  final ComicsInfo comicsInfo;
  final AllChapterList allChapter;
  @override
  _ComicsInfoComicsInfoBodyModule4ControllerState createState() =>
      _ComicsInfoComicsInfoBodyModule4ControllerState();
}

class _ComicsInfoComicsInfoBodyModule4ControllerState
    extends State<ComicsInfoComicsInfoBodyModule4Controller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xfff8f8f8),
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
      ),
      child: Container(
        margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(10),
          left: ScreenUtil().setWidth(10),
          right: ScreenUtil().setWidth(10),
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                ComicsInfoComicsInfoBodyModule4UserHead(
                  headSrc: springboard() +
                      widget.comicsInfo.data.comic.cCreator.avatar.fileServer +
                      widget.comicsInfo.data.comic.cCreator.avatar.path,
                ),
                ComicsInfoComicsInfoBodyModule4UserNameAndTime(
                  name: widget.comicsInfo.data.comic.cCreator.name,
                ),
              ],
            ),
            ComicsInfoComicsInfoBodyModule4Subsection(
              allChapter: widget.allChapter,
              comicsName: widget.comicsInfo.data.comic.title,
              comicsId: widget.comicsInfo.data.comic.sId, //漫画id
              key: comicsInfoComicsInfoBodyModule4SubsectionState,
            ),
            ComicsInfoComicsInfoBodyModule4More(
              page: widget.allChapter.data.eps.page,
              allPage: widget.allChapter.data.eps.pages,
              comicsId: widget.comicsInfo.data.comic.sId, //漫画id
            ),
            ComicsInfoComicsInfoBodyModule4OthersAreWatching(),
          ],
        ),
      ),
    );
  }
}
