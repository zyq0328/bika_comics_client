/*
 * @Date: 2020-02-05 12:37:57
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块5
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:14:07
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5Controller.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5/ComicsInfoComicsInfoBodyModule5Images.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5/ComicsInfoComicsInfoBodyModule5Title.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/recommendation.dart';

class ComicsInfoComicsInfoBodyModule5Controller extends StatefulWidget {
  ComicsInfoComicsInfoBodyModule5Controller({
    Key key,
    this.recommendation,
  }) : super(key: key);
  final Recommendation recommendation;
  @override
  _ComicsInfoComicsInfoBodyModule5ControllerState createState() =>
      _ComicsInfoComicsInfoBodyModule5ControllerState();
}

class _ComicsInfoComicsInfoBodyModule5ControllerState
    extends State<ComicsInfoComicsInfoBodyModule5Controller> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(272),
      margin: EdgeInsets.only(
        right: ScreenUtil().setWidth(10),
      ),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.recommendation.data.comics.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Container(
              width: ScreenUtil().setWidth(192),
              height: ScreenUtil().setHeight(270),
              child: Column(
                children: <Widget>[
                  ComicsInfoComicsInfoBodyModule5Images(
                    image: springboard() +
                        widget.recommendation.data.comics[index].thumb
                            .fileServer +
                        widget.recommendation.data.comics[index].thumb.path,
                  ),
                  ComicsInfoComicsInfoBodyModule5Title(
                    title: widget.recommendation.data.comics[index].title,
                  ),
                ],
              ),
            ),
            onTap: () {
              comicsInfoControllerState.currentState.uodateView(
                widget.recommendation.data.comics[index].title,
                widget.recommendation.data.comics[index].author,
                widget.recommendation.data.comics[index].categories.toList(),
                widget.recommendation.data.comics[index].likesCount,
                widget.recommendation.data.comics[index].id,
              );
              /*  Navigator.of(context).push(
                PageRouteBuilder(
                    pageBuilder: (context1, _, __) => ComicsInfoController(
                          title: widget.recommendation.data.comics[index].title,
                          author:
                              widget.recommendation.data.comics[index].author,
                          categories: [],
                          likeNumber: widget
                              .recommendation.data.comics[index].likesCount,
                          comicsId: widget.recommendation.data.comics[index].id,
                        ),
                    opaque: false),
              ); */
            },
          );
        },
      ),
    );
  }
}
