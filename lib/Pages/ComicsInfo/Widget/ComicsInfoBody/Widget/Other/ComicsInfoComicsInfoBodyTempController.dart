/*
 * @Date: 2020-04-19 23:20:51
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 显示页面的
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-07-14 18:10:13
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyTempController.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule1Contorller.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule2Controller.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View1/ComicsInfoComicsInfoBodyModule3Controller.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4Controller.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule5Controller.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/recommendation.dart'
    as recommendationJson;

class ComicsInfoComicsInfoBodyTempController extends StatefulWidget {
  ComicsInfoComicsInfoBodyTempController({
    Key key,
    this.title,
    this.author,
    this.categories,
    this.likeNumber,
    this.comicsInfo,
    this.allChapter,
    this.recommendation,
  }) : super(key: key);
  final String title;
  final List<Author> author;
  final List<String> categories;
  final int likeNumber;
  final ComicsInfo comicsInfo;
  final AllChapterList allChapter;
  final recommendationJson.Recommendation recommendation;
  @override
  _ComicsInfoComicsInfoBodyTempControllerState createState() =>
      _ComicsInfoComicsInfoBodyTempControllerState();
}

class _ComicsInfoComicsInfoBodyTempControllerState
    extends State<ComicsInfoComicsInfoBodyTempController> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        color: Color(0xffffffff),
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      bottom: ScreenUtil().setHeight(3),
                    ),
                    child: Column(
                      children: <Widget>[
                        ComicsInfoComicsInfoBodyModule1Contorller(
                          title: widget.title,
                          author: widget.author,
                          categories: widget.categories,
                          likeNumber: widget.likeNumber,
                          comicsInfo: widget.comicsInfo,
                        ),
                        ComicsInfoComicsInfoBodyModule2Controller(
                          comicsInfo: widget.comicsInfo,
                        ),
                        ComicsInfoComicsInfoBodyModule3Controller(
                          comicsInfo: widget.comicsInfo,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        ComicsInfoComicsInfoBodyModule4Controller(
                          //列表
                          comicsInfo: widget.comicsInfo,
                          allChapter: widget.allChapter,
                        ),
                        ComicsInfoComicsInfoBodyModule5Controller(
                          recommendation: widget.recommendation, //大家都在看
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
