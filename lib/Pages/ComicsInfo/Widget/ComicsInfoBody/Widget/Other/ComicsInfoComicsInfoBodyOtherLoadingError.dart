/*
 * @Date: 2020-04-19 21:23:05
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 如果获取信息失败
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-07-14 18:12:54
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyOtherLoadingError.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ComicsInfoComicsInfoBodyOtherLoadingError extends StatefulWidget {
  ComicsInfoComicsInfoBodyOtherLoadingError({Key key, this.errorMsg})
      : super(key: key);
  final String errorMsg;
  @override
  _ComicsInfoComicsInfoBodyOtherLoadingErrorState createState() =>
      _ComicsInfoComicsInfoBodyOtherLoadingErrorState();
}

class _ComicsInfoComicsInfoBodyOtherLoadingErrorState
    extends State<ComicsInfoComicsInfoBodyOtherLoadingError> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: ScreenUtil().setHeight(600),
                child: Image.asset(
                  "images/Tisp/Error.png",
                  fit: BoxFit.contain,
                  alignment: Alignment.topCenter,
                  height: ScreenUtil().setHeight(350),
                ),
              ),
              Container(
                margin: EdgeInsets.all(ScreenUtil().setWidth(10)),
                child: Text(
                  "获取漫画信息失败错误信息为 - ${widget.errorMsg}",
                  maxLines: 2, //不限制行数
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(50),
                    color: Color(0xffc73379),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
