/*
 * @Date: 2020-04-19 21:23:05
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 率先加载的 获取详细信息页面 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-07-14 18:11:18
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyOtherLoading.dart
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

bool $loadingGifHide = false; //加载动画
Animation<double> loadingAnime; //当前数值
AnimationController
    loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading; //动画控制器

class ComicsInfoComicsInfoBodyOtherLoading extends StatefulWidget {
  ComicsInfoComicsInfoBodyOtherLoading({Key key, this.tips}) : super(key: key);
  final String tips;
  @override
  _ComicsInfoComicsInfoBodyOtherLoadingState createState() =>
      _ComicsInfoComicsInfoBodyOtherLoadingState();
}

class _ComicsInfoComicsInfoBodyOtherLoadingState
    extends State<ComicsInfoComicsInfoBodyOtherLoading>
    with SingleTickerProviderStateMixin {
  var imgSrc = 'images/Loading/loading_1.png';
  @override
  initState() {
    super.initState();
    loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading =
        new AnimationController(
            duration: const Duration(milliseconds: 1000), vsync: this);
    loadingAnime = new Tween(begin: 1.0, end: 10.0)
        .animate(loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading)
          ..addListener(
            () {
              if (mounted) {
                setState(
                  () {
                    // the state that has changed here is the serviceListAnime object’s value
                  },
                );
              }
            },
          );
    loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading.repeat();
  }

  Widget build(BuildContext context) {
    imgSrc = 'images/Loading/loading_' +
        double.parse(loadingAnime.value.toString()).floor().toString() +
        '.png';
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        child: Container(
          height: ScreenUtil().setHeight(450),
          child: Column(
            children: <Widget>[
              Container(
                child: Image.asset(
                  imgSrc,
                  fit: BoxFit.contain,
                  alignment: Alignment.topCenter,
                  height: ScreenUtil().setHeight(350),
                ),
              ),
              Text(
                "正在获取详细信息",
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(60),
                  color: Color(0xffc73379),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  dispose() {
    super.dispose();
  }
}
