/*
 * @Date: 2020-02-03 16:40:17
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-23 00:13:56
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart
 * @info 这是详细信息 其中 又一个变量 view 是视图 载入以后先进行获取漫画信息 判断有没有漫画 没有的话 在调用方法修改 view这个变量 变成其他的
 */

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalFunction.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/GlobalNetWorkReturnDefind.dart';
import 'package:pica_acg/GlobalWidget.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyOtherLoading.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyOtherLoadingError.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/Other/ComicsInfoComicsInfoBodyTempController.dart';
import 'package:pica_acg/tools/Controller/NetWork/Get.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/all_chapter_list.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/recommendation.dart'
    as recommendationJson;

class ComicsInfoComicsInfoBodyController extends StatefulWidget {
  ComicsInfoComicsInfoBodyController({
    Key key,
    this.title,
    this.author,
    this.categories,
    this.likeNumber,
    this.comicsId,
  }) : super(key: key);
  String title;
  List<Author> author;
  List<String> categories;
  int likeNumber;
  String comicsId;
  @override
  ComicsInfoComicsInfoBodyControllerState createState() =>
      ComicsInfoComicsInfoBodyControllerState();
}

var comicsInfo = ComicsInfo.fromJson($netWorkComicsInfo);
var allChapter = AllChapterList.fromJson($netWorkAllChapter);
var recommendation =
    recommendationJson.Recommendation.fromJson($netWorkRecommendation);
Widget view; //整个窗口的视图

class ComicsInfoComicsInfoBodyControllerState
    extends State<ComicsInfoComicsInfoBodyController> {
  initState() {
    super.initState();
    view = ComicsInfoComicsInfoBodyOtherLoading(); //加载窗口
    updateDateComicsInfoComicsInfoBodyController(
      title: widget.title,
      author: widget.author,
      categories: widget.categories,
      likeNumber: widget.likeNumber,
      comicsId: widget.comicsId,
    );
  }

  newpage() {
    view = ComicsInfoComicsInfoBodyOtherLoading(); //加载窗口
    loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading.reset(); //播放动画
    comicsInfoBoxTitleState.currentState.downLoadButtonSetIsHide(false);
    // allChapter = AllChapterList.fromJson($netWorkAllChapter);
    recommendation =
        recommendationJson.Recommendation.fromJson($netWorkRecommendation);
    setState(() {});
  }

//更新数据 单独写一个方法方便调用
  updateDateComicsInfoComicsInfoBodyController(
      //更新视图
      {
    String title,
    List<Author> author,
    List<String> categories,
    int likeNumber,
    String comicsId,
  }) {
    widget.title = title;
    widget.author = author;
    widget.categories = categories;
    widget.likeNumber = likeNumber;
    widget.comicsId = comicsId;
    allChapter = AllChapterList.fromJson($netWorkAllChapter); //重置章节信息
    netWorkGetComicsInfo(widget.comicsId); //获取漫画信息 -> 当获取成功 在获取章节和大家都在看
  }

  @override
  Widget build(BuildContext context) {
    return view;
  }

  netWorkGetComicsInfo(comicsId) {
    NetWork.get(
      getComicsInfo(comicsId),
      (callBack) {
        loadingAnimeControllerComicsInfoComicsInfoBodyOtherLoading
            .stop(); //关闭动画
        if (ComicsInfo.fromJson(callBack).code == "200") {
          comicsInfo = ComicsInfo.fromJson(callBack);
          widget.author = comicsInfo.data.comic.author;
          netWorkGetAllChapter(widget.comicsId); //获取全部章节
          netWorkGetRecommendation(widget.comicsId); //获取大家都在看
          updateView();
        } else {
          //如果获取失败
          view = Container(
            child: ComicsInfoComicsInfoBodyOtherLoadingError(
              errorMsg: ComicsInfo.fromJson(callBack).message,
            ),
          );
        }
        setState(() {});
      },
      headers: getApiRequestHeadrs("ComicsInfo"),
    );
  }

  netWorkGetAllChapter(comicsId, {page = 1}) {
    NetWork.get(
      getAllChapter(comicsId, page: page),
      (callBack) {
        var temp = AllChapterList.fromJson(callBack);
        if (temp.code == "200") {
          allChapter.data.eps.limit += temp.data.eps.limit;
          allChapter.data.eps.page = temp.data.eps.page;
          allChapter.data.eps.pages = temp.data.eps.pages;
          allChapter.data.eps.total = temp.data.eps.total;
          allChapter.data.eps.docs.addAll(temp.data.eps.docs);
          if (temp.data.eps.docs.length > 0) {
            comicsInfoBoxTitleState.currentState.downLoadButtonSetIsHide(true);
            updateView();
          } else {
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, _, __) => GlobalTips(
                  body: '该漫画还没有章节先看别的吧',
                  height: ScreenUtil().setHeight(900),
                ),
                opaque: false,
              ),
            );
          }
        } else {
          Navigator.of(context).push(
            PageRouteBuilder(
              pageBuilder: (context, _, __) => GlobalTips(
                body: '发生意外错误，获取漫画信息列表错误 ， 请反馈或稍后再试',
                height: ScreenUtil().setHeight(900),
              ),
              opaque: false,
            ),
          );
        }
        setState(() {});
      },
      headers: getApiRequestHeadrs("ComicsAllChapter"),
    );
  }

  netWorkGetRecommendation(comicsId) {
    NetWork.get(
      getRecommendation(comicsId),
      (callBack) {
        recommendation = recommendationJson.Recommendation.fromJson(callBack);
        updateView(); //更新视图
      },
      headers: getApiRequestHeadrs('Recommendation'),
    );
  }

  updateView() {
    // 更新视图
    view = ComicsInfoComicsInfoBodyTempController(
      title: widget.title,
      author: widget.author,
      categories: widget.categories,
      likeNumber: widget.likeNumber,
      comicsInfo: comicsInfo,
      allChapter: allChapter,
      recommendation: recommendation,
    );

    setState(() {});
  }
}
