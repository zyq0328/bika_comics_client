/*
 * @Date: 2020-01-31 14:32:57
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 框架 -背景颜色
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime : 2020-02-03 16:48:48
 * @FilePath: /pica_acg/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxBackground.dart
 */
import 'package:flutter/material.dart';

class ComicsInfoBoxBackground extends StatefulWidget {
  ComicsInfoBoxBackground({Key key}) : super(key: key);

  @override
  _ComicsInfoBoxBackgroundState createState() =>
      _ComicsInfoBoxBackgroundState();
}

class _ComicsInfoBoxBackgroundState extends State<ComicsInfoBoxBackground> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xffffffff),
    );
  }
}
