/*
 * @Date: 2020-01-31 14:30:55
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 框架 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 22:43:56
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/LanguAge/LanguAgeController.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/ComicsInfoDownloadController.dart';

class ComicsInfoBoxTitle extends StatefulWidget {
  ComicsInfoBoxTitle({Key key, this.comicsId, this.title})
      : super(key: comicsInfoBoxTitleState);
  final String comicsId;
  final String title;
  @override
  ComicsInfoBoxTitleState createState() => ComicsInfoBoxTitleState();
}

bool downLoadButtonState = false;

class ComicsInfoBoxTitleState extends State<ComicsInfoBoxTitle> {
  @override
  Widget build(BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;
    final double bottomPadding = MediaQuery.of(context).padding.bottom;
    return Container(
      height: ScreenUtil().setHeight(70),
      margin: EdgeInsets.only(top: ScreenUtil().setHeight(topPadding)),
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: GestureDetector(
              child: Container(
                child: Text(
                  languAre().$languAgeOrdinary["basic"]["return"],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 19.0,
                    color: Color(0xFF434343),
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              'BIKA 介绍',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 19.0,
                color: Color(0xFF434343),
                decoration: TextDecoration.none,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Visibility(
              visible: downLoadButtonState,
              child: GestureDetector(
                child: Container(
                  child: Text(
                    '下载',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 19.0,
                      color: Color(0xFF434343),
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => Scaffold(
                        body: ComicsInfoDownloadController(
                          comicsId: widget.comicsId,
                          title: widget.title,
                          chapter:
                              allChapter, //这里的 allChapter 是 ComicsInfoComicsInfoBodyController 这里的 如果之前没有加载出来列表就点击下载会出问题
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        border: new Border(
          bottom: Divider.createBorderSide(
            context,
            color: Color(0xffbfbfbf),
            width: 1,
          ),
        ), // 边色与边宽度
      ),
    );
  }

  bool downLoadButtonSetIsHide(bool state) {
    downLoadButtonState = state;
    setState(() {});
    return true;
  }
}
