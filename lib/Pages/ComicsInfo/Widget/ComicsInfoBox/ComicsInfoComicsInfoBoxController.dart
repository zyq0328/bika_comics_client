/*
 * @Date: 2020-02-03 16:42:35
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 框架控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-14 22:43:10
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/ComicsInfoComicsInfoBoxController.dart
 */
import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxBackground.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart';

class ComicsInfoComicsInfoBoxController extends StatefulWidget {
  ComicsInfoComicsInfoBoxController({Key key, this.comicsId, this.title})
      : super(key: key);
  final String comicsId;
  final String title;
  @override
  _ComicsInfoComicsInfoBoxControllerState createState() =>
      _ComicsInfoComicsInfoBoxControllerState();
}

class _ComicsInfoComicsInfoBoxControllerState
    extends State<ComicsInfoComicsInfoBoxController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ComicsInfoBoxBackground(),
        ComicsInfoBoxTitle(comicsId: widget.comicsId, title: widget.title),
      ],
    );
  }
}
