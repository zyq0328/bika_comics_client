/*
 * @Date: 2020-02-03 16:40:17
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 22:32:50
 * @FilePath: /bica_acg/lib/Pages/ComicsInfo/ComicsInfoController.dart
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/GlobalKey.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBox/ComicsInfoComicsInfoBoxController.dart';
import 'package:pica_acg/tools/extends/Json/ComicsInfo/comics_info.dart';

class ComicsInfoController extends StatefulWidget {
  ComicsInfoController({
    Key key,
    this.title,
    this.author,
    this.categories,
    this.likeNumber,
    this.comicsId,
  }) : super(key: comicsInfoControllerState);
  String title;
  List<Author> author;
  List<String> categories;
  int likeNumber;
  String comicsId;
  @override
  ComicsInfoControllerState createState() => ComicsInfoControllerState();
}

class ComicsInfoControllerState extends State<ComicsInfoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            ComicsInfoComicsInfoBoxController(
              comicsId: widget.comicsId,
              title: widget.title,
            ),
            ComicsInfoComicsInfoBodyController(
              key: comicsInfoComicsInfoBodyControllerState,
              title: widget.title,
              author: widget.author,
              categories: widget.categories,
              likeNumber: widget.likeNumber,
              comicsId: widget.comicsId,
            ),
          ],
        ),
      ),
    );
  }

  uodateView(
    titleNew,
    authorNew,
    categoriesNew,
    likeNumberNew,
    comicsIdNew,
  ) {
    comicsInfoComicsInfoBodyControllerState.currentState.newpage();
    widget.title = titleNew;
    widget.author = authorNew;
    widget.categories = categoriesNew;
    widget.likeNumber = likeNumberNew;
    widget.comicsId = comicsIdNew;
    comicsInfoComicsInfoBodyControllerState.currentState
        .updateDateComicsInfoComicsInfoBodyController(
      title: titleNew, //标题
      author: authorNew, //作者
      categories: categoriesNew, //分类 ['aaa','bbb']
      likeNumber: likeNumberNew, //喜欢数量
      comicsId: comicsIdNew, //书籍id
    );
  }
}
