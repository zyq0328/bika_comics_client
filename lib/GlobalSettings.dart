/*
 * @Date: 2019-11-26 09:59:59
 * @名称: 全局 设置
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 19:58:35
 */

import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

const $deviceWidth = 750.0; //? 设备宽度
const $deviceHeight = 1334.0; //? 设备高度

const $serviceState = 0; //0自己的服务器 1 哔咔服务器
const $global_ServiceDomain_me = 'http://okabe.natapp1.cc/'; //自己的服务器域名
// const $global_ServiceDomain_me = 'http://localhost/'; //自己的服务器域名
const $global_ServiceDomain = 'http://picaapi.picacomic.com/'; //哔咔域名
const $global_ServiceDomain_back = 'http://192.168.1.103/'; //作者备用服务器

//主题颜色

//Home——page
const $global_HomeBannerBannerImagesAndBoxHeight = 380.0; //横幅和图片高度
const $global_HomeBannerBannerImagesAndBoxWidth = 750.0; //横幅和图片宽度
const $global_HomeBannerBannerTextAndBoxHeight = 80.0; //横幅下面文字的背景框
const $global_HomeBannerBannerTextBackgroundColor = 0xFFE27094; //横幅下面文字的背景框的颜色
const $global_HomeBannerBannerImagesAndBoxLogingAnime = Indicator
    .ballSpinFadeLoader; //横幅加载中的动画的名字 具体动画参考 https://pub.dev/packages/loading_indicator
const $global_HomeBannerBannerImagesAndBoxLogingWidth = 300.0; //横幅加载中的动画的大小
const $global_HomeBannerBannerImagesAndBoxLogingHeight = 300.0; //横幅加载中的动画的大小

//Category--page 分类页面
const $global_Category_PopularClassificationBoxHeight = 238.0; //!分类页面头部高度

// My -- page 我的页面
const $global_My_MyBannerWidth = $deviceWidth;
var $hiddenHeadImagesBox = false; //是否隐藏头像框

//ReadingComics 读漫画
const $global_ReadingComicsOpenComicsViewBoxHide = false; //开始读漫画是否隐藏框架
const double $global_ReadingComicsMagnification = 4.0; //开始读漫画图片放大倍数
const $global_ReadingComicsZoomIn = false; //一直放大

//Color
const $global_HomeBannerBannerImagesAndBoxLogingAnimeColor =
    Colors.blue; //横幅加载中的动画的颜色

//images 图片
//没有图片 使用这张图
const $global_ImagesUrlUndefined =
    'https://i0.hdslb.com/bfs/album/9ea4b089e861fde439774fa235738bce07bbd607.jpg';

const $global_DataBaseName = '哔咔漫画'; //数据库名称

//设置
const $global_DownloadHighConcurrency =
    false; //下载漫画 针对单漫画进行高并发下载页面 true 时 会造成下载进度条不准 //!后期修复 //已弃用
const $global_DownloadplayAnalysisMultithreading =
    true; //下载漫画 解析要下载的全部内容的 高并发是否
const $global_DownloadConcurrentNumber = 1; //下载漫画 开始下载的线程数量 同时下载几个图片

var $tempUserToKen =
    "eyJFbmNyeXB0aW9uIjoiU0hBMjU2IiwidmVyaWZpY2F0aW9uIjoiU0hBNTEyIn0=.ODkzZkxwaFBxeWxIY282YlVsdVBzZTI4Mm5CM2xWK3lkdkIzaDRETkJ5dnpsQXBRc1o3bU8vSmN2WjVZQXR5c2t0Z0F3U2hjaDNQdkdHaWtXMHNSajlDRUswSHRSZmEzUUMxU2tybUllb0dPR2NJV3cvZUg5UWFMelpNMWZJYWs2bFhldjhCb3ZKeGF1dVg0dDlIMkFtZUxvQ1FXdXpGeXQ5aGNwWkVjcnVSblVyTXVHZmljN0lXSHBxcE04TnNBTEZtZ3VBMG5kQ3JqVlgzUkxEeFlGOWgxVm5yb3pqOU9uU2RkQmRDOTQvMkJyclVVK2E1RGJXcXJ1bXEzNXNaMWdjY2o1YWFtbmNrbFgrZnMxQWZBVkpybVRSbk4rVVdFT01IUVpmUS9IVjR6RmdHc0l3eUFJV2UvNm5wVDM1MmZsOUViQkMrRUd0RTY1M1VHV0lZZXk5NmJsZjNDMUFSZlA3cU1ldEFQcW5JZjU4L3M2OFJ4dFBzcnU2OHBsQm1FQUtlQXV5WENZWW1SeGk5NUdSeGRtQUtLVVgyMzEyVkFlZVNFSkk2QWtIZ2tHSEJ5NEJ2Zk9NcG9BdTU3NkRzd0ZNSkJ1SDFvektXc2FnRlJuRWl1.ad8a30c76caa374ef18a9b3f207d5ba090f11b433f9685be04227c0863a024a4bb54c46f592a3a1f48121b357836427b801eecb653946f72a44aa2eb5dc98394";

var languAge = 'zh_cn'; //语言
bool translations = false; //是否翻译 如果翻译 languAge的设置无效
