/*
 * @Date: 2020-04-05 14:23:44
 * @名称: 全局key
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-10-12 21:15:37
 * @FilePath: /bica_acg/lib/GlobalKey.dart
 */

import 'package:flutter/material.dart';
import 'package:pica_acg/Pages/Comics/Widget/ComicsComicsBody/ComicsComicsBodyComicsBodyContorller.dart';
import 'package:pica_acg/Pages/ComicsInfo/ComicsInfoController.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4Subsection.dart';
import 'package:pica_acg/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomButton/ComicsInfoDownloadComicsInfoDownloadBottomButton.dart';
import 'package:pica_acg/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomText/ComicsInfoDownloadComicsInfoDownloadBottomText.dart';
import 'package:pica_acg/Pages/Home/Home.dart';
import 'package:pica_acg/Pages/ServiceList/Widget/MaskController.dart';
import 'package:pica_acg/Pages/Settings/Widget/SettingsBody/SettingBodyContorller.dart';
import 'package:pica_acg/Pages/Download/Widget/DownloadDownloadBody/DownloadDownloadBodyDownloadDownloadList/DownloadDownloadBodyDownloadDownloadListController.dart';
import 'package:pica_acg/Pages/Comment/Widget/CommentCommentBody/Widget/CommentCommentCommentModuleController.dart';

/*
 * @Date: 2020-02-03 16:40:17
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart
 */
final comicsInfoComicsInfoBodyControllerState =
    GlobalKey<ComicsInfoComicsInfoBodyControllerState>();
/*
 * @Date: 2020-02-05 12:39:49
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 - 模块4 - 分卷
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/Widget/View2/ComicsInfoComicsInfoBodyModule4/ComicsInfoComicsInfoBodyModule4Subsection.dart
 */
var comicsInfoComicsInfoBodyModule4SubsectionState =
    new GlobalKey<ComicsInfoComicsInfoBodyModule4SubsectionState>();

///更新下载块位置progress 为0-1
/// modifyBlockLocation(
///   $comicsId, //书籍id
///   $chapterId, //分卷id
///   $progress, //进度
/// ) {
///   code
///   }
/// }
///
/*
 * @Date: 2020-02-03 16:40:17
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/ComicsInfoController.dart
 */

var comicsInfoControllerState = new GlobalKey<ComicsInfoControllerState>();

/// 更新视图 这是 漫画详细页面的
///   uodateView(
///     titleNew,//标题
///     authorNew,//作者
///     categoriesNew,//分类 ['aaa','bbb']
///     likeNumberNew,//喜欢
///     comicsIdNew,//书籍id
///   ) {
///     code
///     );
///   }
/*
 * @Date: 2020-02-03 16:40:17
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 内容 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBody/ComicsInfoComicsInfoBodyController.dart
 */
var refreshKey =
    new GlobalKey<ComicsInfoDownloadComicsInfoDownloadBottomTextState>();

/// 更新下载页面里面显示下载数量
/// void updateDownloadValue({
///     int haveChosen,//以选中数量
///     int downloaded,//下载完成数量
///     int downloadedLoading,//下载中数量
///   }) {
///     // code
///   }

/*
 * @Date: 2020-02-07 15:52:05
 * @名称: 分类 - 漫画列表 - 漫画信息 - 下载 - 底部 - 点击下载按钮
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfoDownload/Widget/ComicsInfoDownloadComicsInfoDownloadBottom/Widget/ComicsInfoDownloadComicsInfoDownloadBottomButton/ComicsInfoDownloadComicsInfoDownloadBottomButton.dart
 */
var buttonKey =
    new GlobalKey<ComicsInfoDownloadComicsInfoDownloadBottomButtonState>();

///  更新下载按钮显示数量
///  void updateState(
///    bool state, {//真假
///    bookId,//书籍id
///  }) {
///    code
///  }

/*
 * @Date: 2020-01-31 18:19:27
 * @名称: 分类 - 漫画分类 - 列表 - 分类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/Comics/Widget/ComicsComicsBody/Widget/ConicsComicsBodyComicsBodyClass/ConicsComicsBodyComicsBodyClass.dart
 */

var comicsComicsBodyComicsBodyContorllerState =
    new GlobalKey<ComicsComicsBodyComicsBodyContorllerState>();

/// 更新视图
///
/// void classificationUpdate() {
///    code
/// }

/*
 * @Date: 2020-01-31 14:30:55
 * @名称: 分类 - 漫画分类 - 列表 - 某一漫画的详细信息 - 框架 - 头部
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart
 */
var comicsInfoBoxTitleState = new GlobalKey<ComicsInfoBoxTitleState>();

/// 隐藏/显示 下载按钮
/// downLoadButtonSetIsHide( bool state){//传入按钮状态
///   code
/// }

/*
 * @Date: 2020-01-29 17:59:50
 * @名称: 设置 - 内容 - 控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 */
var settingSettingBodyContorllerState =
    new GlobalKey<SettingSettingBodyContorllerState>();

/// 更新设置数据库
/// updateSetting(字段名,值){
/// #code
/// }

/// 更新视图
/// updateView(){
/// code
/// }

/*
 * @Date: 
 * @名称: bottom 切换
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart
 */
var mainHomeState = new GlobalKey<MainHomeState>();

/// 显示某个页面
/// toPage( Int index){//要专挑的页面
///   code
/// }
/*
 * @Date: 
 * @名称: bottom 切换
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart
 */
var severListMaskState = new GlobalKey<MaskState>();

/// 显示某个页面
/// toPage( Int index){//要专挑的页面
///   code
/// }

/*
 * @Date: 
 * @名称: 下载详情
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @FilePath: /bika_comics_client/lib/Pages/ComicsInfo/Widget/ComicsInfoBox/Widget/ComicsInfoComicsInfoBoxComicsInfoBoxTitle.dart
 */
var downloadDownloadBodyDownloadDownloadListControllerState =
    new GlobalKey<DownloadDownloadBodyDownloadDownloadListControllerState>();

/// 显示某个页面
/// toPage( Int index){//要专挑的页面
///   code
/// }
